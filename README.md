### Thông tin 
#### 1. ZUDM_DBIngestionMW
* Mục đích: sử dụng để  put db đối với những tác vụ có bắn eventbus (cũng như những như tác vụ không bắn eventbus nhưng có nhu cầu custom cao).
* Project bao gồm những thành phần: dbingestionmw-thrift, ZUDM_DBIngestionMWService, dbingestionmw-client (sẽ được mô tả rõ hơn trong từng project).

#### 2. ZUDM_DBIngestionMWP
* Mục đích: sử dụng để  put db đối với những tác vụ không bắn eventbus, đã chạy ổn định (không có nhu cầu custom).
* Project bao gồm những thành phần: dbingestionmw-thrift, ZUDM_DBIngestionMWService, dbingestionmw-client (sẽ được mô tả rõ hơn trong từng project). 

#### 3. dbingestionmw_common
* Mỗi API upload sẽ có 1 KeyType tương ứng. Việc quản lý KeyType giúp xác định thông tin chính xác số lượng KeyType trong từng project (cũng như từng database).
* Chạy `./gradlew build` để compile project thành file jar và tự động copy vào thư mục `/zserver/java/lib/zudm/` với tên `zudmdbingestionmwcommon-{VERSION}.jar`


```
