package com.vng.zing.zudm.common.centralizedkeys.ip;

import com.vng.zing.logger.ZLogger;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.BitSet;
import java.util.List;
import java.util.Objects;
import org.apache.log4j.Logger;

public class IPBitSet {

    private final Class _ThisClass = IPBitSet.class;
    private final Logger _Logger = ZLogger.getLogger(_ThisClass);

    private BitSet negIPs;
    private BitSet posIPs;
    private int negIpId;
    private int posIpId;

    private boolean inNegSet = true;
    private int nextSetBit;

    public BitSet getNegIPs() {
        return negIPs;
    }

    public BitSet getPosIPs() {
        return posIPs;
    }

    public IPBitSet(BitSet negIPs, BitSet posIPs) {
        this.negIPs = negIPs;
        this.posIPs = posIPs;
        negIpId = 0;
        posIpId = 0;
    }

    public IPBitSet() {
        negIpId = 0;
        posIpId = 0;
        this.negIPs = new BitSet(Integer.MAX_VALUE);
        this.posIPs = new BitSet(Integer.MAX_VALUE);
    }

    public int size() {
        return negIPs.cardinality() + posIPs.cardinality();
    }

    public boolean hasNextIP() {

        if (inNegSet) {
            nextSetBit = negIPs.nextSetBit(negIpId);

            if (nextSetBit >= 0) {
                negIpId = nextSetBit + 1;
                return true;
            }
            inNegSet = false;
        }

        nextSetBit = posIPs.nextSetBit(posIpId);
        if (nextSetBit >= 0) {
            posIpId = nextSetBit + 1;
            return true;
        }

        return false;
    }

    public int getNextIP() {
        if (inNegSet) {
            return (int) (nextSetBit | (1L << 31));
        }
        return nextSetBit;
    }

    /**
     * Add an IP to IPBitSet64 IP value must be between 2^31 and 2^31-1
     *
     * @param ip
     */
    public void addIP(int ip) {
        if (ip < 0) {
            ip = (int) (ip & (~(1L << 31)));
            negIPs.set(ip);
        } else {
            posIPs.set(ip);
        }
    }
    
    public void cleanIP() {
        negIPs.clear();
        posIPs.clear();
        
    }

    /**
     * Add list of IP IP value must be between 2^31 and 2^31-1
     *
     * @param listIPs
     */
    public void addListIPs(List<Integer> listIPs) {
        listIPs.stream().forEach((ip) -> {
            addIP(ip);
        });
    }

    /**
     * Add a converted negative IP to IPBitSet This IP must be converted IP
     * value must be between 0 and 2^31-1
     *
     * @param convertedIP
     */
    public void addNegativeIP(int convertedIP) {
        negIPs.set(convertedIP);
    }

    /**
     * Add list converted negative IP to IPBitSet This IP must be converted IP
     * value must be between 0 and 2^31-1
     *
     * @param listNegativeIPs
     */
    public void addListNegativeIP(List<Integer> listNegativeIPs) {
        if (listNegativeIPs.stream().anyMatch(ip -> ip < 0)) {
            throw new IllegalArgumentException("There is a non-converted ip");
        }
        listNegativeIPs.stream().forEach(ip -> addNegativeIP(ip));
    }

    public static int convertNegativeIP(int negativeIP) {
        if (negativeIP >= 0) {
            throw new IllegalArgumentException(negativeIP + ", is it negative number?");
        }

        return (int) (negativeIP & (~(1L << 31)));
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof IPBitSet)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        IPBitSet set = (IPBitSet) obj;

        return this.negIPs.equals(set.negIPs) && this.posIPs.equals(set.posIPs);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.negIPs);
        hash = 47 * hash + Objects.hashCode(this.posIPs);
        return hash;
    }

}
