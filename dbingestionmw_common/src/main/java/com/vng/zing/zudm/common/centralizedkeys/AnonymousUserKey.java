/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class AnonymousUserKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.ZUDM_ZiDB64AnonymousUser KEY_SET = new AllKeyTypeSets.ZUDM_ZiDB64AnonymousUser();
    private final static AllKeyTypeSets.ZUDM_ZiDB64AnonymousUser2 KEY_SET_NEW = new AllKeyTypeSets.ZUDM_ZiDB64AnonymousUser2();
    
    public static long forGenderOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.GENDER_TYPE);
    } 
    
    public static long forGenderV2Of(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.GENDER_TYPE_V2);
    }
    
    public static long forPredictedGenderOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.PRED_GENDER);
    }
    
    
    public static long forShiftedPredictedGenderOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.SHIFTED_PRED_GENDER);
    }

    
    public static long forAgeOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.AGE_TYPE);
    } 
    
    public static long forAgeV2Of(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.AGE_TYPE_V2);
    } 
    
    public static long forProvinceOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.PROVINCE_TYPE);
    } 
    
    public static long forDeviceOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.DEVICE_TYPE);
    } 
    
    public static long forAgeGenderOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.AGE_GENDER_TYPE);
    } 
    
    public static long forAgeRangeOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET_NEW.AGE_RANGE);
    } 
}
