/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class StickerKey {
    private final static AllKeyGenerators.HourlyKeyWithHour KEY_GEN = new AllKeyGenerators.HourlyKeyWithHour(); 
    private final static AllKeyTypeSets.StrList32ScoreSuggestion KEY_SET = new AllKeyTypeSets.StrList32ScoreSuggestion();
    
    public static long forStickerFromChattrendingOf(long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.STICKER_FROM_CHAT_TRENDING, hour);
    }
    
    public static long forStickerCateChattrendingOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.STICKER_CATE_FROM_CHAT_TRENDING, hour);
    }
}
