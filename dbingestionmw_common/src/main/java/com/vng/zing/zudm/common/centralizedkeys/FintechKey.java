/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

import com.vng.zing.logger.ZLogger;
import com.vng.zing.zudm_dmpmiddleware.thrift.TCreditScoreType;
import java.nio.ByteBuffer;
import java.util.Optional;
import javax.xml.bind.DatatypeConverter;
import org.apache.log4j.Logger;

/**
 *
 * @author thient
 */
public class FintechKey {
    private static final Class _ThisClass = FintechKey.class;
    private static final Logger _Logger = ZLogger.getLogger(_ThisClass);
    
    /**
     * Format: CSTYPE + hashedNumber 
     * @param hashedNumber
     * @param csType
     * @return 
     */
    public static Optional<ByteBuffer> forCreditScoreMapping(String hashedNumber, TCreditScoreType csType) {
        //Format: CSTYPE + hashedNumber 
        try {
            String prefix = String.format("%02X", csType.getValue());
            byte[] parseHexBinary = DatatypeConverter.parseHexBinary(prefix.concat(hashedNumber));
            return Optional.ofNullable(ByteBuffer.wrap(parseHexBinary));
        } catch (Exception ex) {
            _Logger.error("Build key credit score: " + ex.getMessage());
            return Optional.empty();
        }
    }

}
