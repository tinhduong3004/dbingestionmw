/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class UserTokenizerKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.HourlyKey();
    private final static AllKeyTypeSets.OfUserTokenizer KEY_SET = new AllKeyTypeSets.OfUserTokenizer();
    
    public static long forUserTokenizer(long zaloId){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.USER_TOKENIZED);
    }
}
