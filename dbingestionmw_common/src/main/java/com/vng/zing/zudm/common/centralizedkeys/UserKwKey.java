/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class UserKwKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.HourlyKey();
    private final static AllKeyTypeSets.ZUDM_ZiDB64UserKWDB KEY_SET = new AllKeyTypeSets.ZUDM_ZiDB64UserKWDB();
    
    public static long forUserPhonebookOf(long zaloId){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.USER_PHONEBOOK);
    }
    
    public static long forUserPhonebookFilterOf(long zaloId){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.USER_PHONEBOOK_FILTER);
    }
    
    public static long forBidirectionalPhonebookOf(int src_id){
        return KEY_GEN.makeKeyFrom( (long) src_id, KEY_SET.BIDIRECTIONAL_PHONEBOOK);
    }
}
