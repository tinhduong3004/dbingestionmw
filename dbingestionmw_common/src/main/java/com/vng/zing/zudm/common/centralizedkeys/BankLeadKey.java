package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class BankLeadKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.ZUDMBankLead_ZiDB KEY_SET = new AllKeyTypeSets.ZUDMBankLead_ZiDB();
    
    public static long forBankleadKeyOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.BANKLEAD);
    }
    
    public static long forDisburseleadKeyOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.DISBURSELEAD);
    }
    
    public static long forDisburseleadShKeyOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.DISBURSELEADSH);
    }
    
    public static long forLoanDemandTagKeyOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.LOAN_DEMAND_TAG);
    }
}
