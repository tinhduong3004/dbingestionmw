/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class RelationshipKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.Relationship KEY_SET = new AllKeyTypeSets.Relationship();
    
    public static long forListColleaguesOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.LIST_COLLEAGUES);
    }
    
    public static long forListColleaguesV2Of(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.LIST_COLLEAGUES_V2);
    }
    
    public static long forListFamilyOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.LIST_FAMILY);
    }
    
    public static long forListFamilyV2Of(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.LIST_FAMILY_V2);
    }
    
    public static long forListRelationshipOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.LIST_RELATIONSHIP);
    }
}
