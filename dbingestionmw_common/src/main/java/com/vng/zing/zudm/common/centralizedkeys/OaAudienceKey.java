package com.vng.zing.zudm.common.centralizedkeys;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author thient
 */
public class OaAudienceKey {

    public static String forZaloUserD01City(int cityId) {
        String CUSTOMAUDIENCE_KEY = "customaudience__zalo_user__d01__city__%d";
        String key = String.format(CUSTOMAUDIENCE_KEY, cityId);
        return key;
    }
}
