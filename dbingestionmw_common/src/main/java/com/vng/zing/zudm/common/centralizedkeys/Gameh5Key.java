package com.vng.zing.zudm.common.centralizedkeys;

/**
 * make key for gameh5
 *
 * @author thient
 */
public class Gameh5Key {
    private final static AllKeyTypeSets.OfGameH5 KEY_SET = new AllKeyTypeSets.OfGameH5();
    /**
     * make String key for ZUDM_Gameh5TienlenPlayingInfo db
     * @deprecated use forPlayingInfo(int uid, int appid) instead
     * @param uid
     * @return String key
     */
    public static String forTienLenPlayingInfo(int uid) {
        //TODO: Add this key type to the common keyset
        
        Long key = KEY_SET.TIENLEN_PLAYING_INFO + uid;
        return key.toString();
    }
    
    /**
     * make String key for ZUDM_Gameh5PlayingInfo db
     * @param uid
     * @param appid
     * @return 
     */
    public static String forPlayingInfo(int uid, int appid) {
        
        Long key = KEY_SET.PLAYING_INFO + KEY_SET.APPID_MULTIPLE*appid + uid;
        return key.toString();
    }

    public static String forWeeklyUserHighWinrate(int appid){
        
        String key = KEY_SET.LIST_WEEKLY_USERHIGHWINRATE + String.valueOf(appid);
        return key;
    }

}
