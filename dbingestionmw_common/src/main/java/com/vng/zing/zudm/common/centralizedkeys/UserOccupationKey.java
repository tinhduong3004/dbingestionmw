/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class UserOccupationKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.HourlyKey();
    private final static AllKeyTypeSets.ZUDM_ZiDB64Occupation KEY_SET = new AllKeyTypeSets.ZUDM_ZiDB64Occupation();
    
    
    // It's seem annoyance when the word 'occupation' was repeated too many times
    public static long forCompanyOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.COMPANY_OCCUPATION);
    }
    
    public static long forUserOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.USER_OCCUPATION);
    }
    
    public static long forUserInterestOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.USER_INTEREST);
    }
    
    public static long forUserModelOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.USER_MODEL_OCCUPATION);
    }
    
    public static long forUserSellerOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.USER_SELLER_OCCUPATION);
    }
    
    public static long forUserStudentOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.USER_STUDENT_OCCUPATION);
    }
    
    public static long forUserBluewhiteOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.USER_BLUEWHITE_OCCUPATION);
    }
    
    public static long forUserUnionOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.USER_UNION_OCCUPATION);
    }
    
    public static long forCompanyTop500Of(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.COMPANY_TOP500);
    }
    
    public static long forUserCompanyOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.USER_COMPANY);
    }
    
    public static long forWorkTypeOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.WORK_TYPE);
    }
}
