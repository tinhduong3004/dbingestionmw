/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class UserIncome {
    
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.ZUDMUserIncome KEY_SET = new AllKeyTypeSets.ZUDMUserIncome();
    
    public static long forOrginalKeyOf(long uid){
        return KEY_GEN.makeKeyFrom(uid, KEY_SET.ORGINAL);
    } 
    
    public static long forIncomeRankOf(long uid){
        return KEY_GEN.makeKeyFrom(uid, KEY_SET.INCOME_RANK);
    } 
    
}
