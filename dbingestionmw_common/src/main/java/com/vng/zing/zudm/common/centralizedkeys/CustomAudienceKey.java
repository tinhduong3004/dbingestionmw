/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */

public class CustomAudienceKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.ZUDM_ZiDB64UsedApps KEY_SET = new AllKeyTypeSets.ZUDM_ZiDB64UsedApps();
    
    public static long forUsedAppsOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.USED_APPS);
    }
    
    public static long forCountedInternalAppsOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.APPS_INTERNAL_COUNT);
    }
    
    public static long forCountedExternalAppsOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.APPS_EXTERNAL_COUNT);
    }
    
    public static long forExternalAppsUninstallOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.APPS_UNINSTALL_EXTERNAL);
    }
    
}
