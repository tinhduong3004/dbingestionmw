package com.vng.zing.zudm.common.centralizedkeys.ip;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.function.LongSupplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.log4j.Logger;

import com.vng.zing.common.ZErrorDef;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.common.ZUtil;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.strlist32bm.thrift.TKeyEntry;
import com.vng.zing.strlist32bm.thrift.TValue;
import com.vng.zing.strlist32bm.thrift.TValueResult;
import com.vng.zing.strlist32bm.thrift.wrapper.StrList32bmClient;
import com.vng.zing.zcommon.thrift.PutPolicy;
import com.vng.zing.zudm.common.centralizedkeys.ip.IPDatabase.Keys;

public class IPDatabaseV2 {
	private final Class _ThisClass = IPDatabaseV2.class;
    private final Logger _Logger = ZLogger.getLogger(_ThisClass);
    public static final IPDatabaseV2 Instance = new IPDatabaseV2();

    public enum Keys {
        NEGATIVE_IP("NegIPsKeyV2"),
        POSITIVE_IP("PosIPsKeyV2");

        private final String value;

        private Keys(String value) {
            this.value = value;
        }
    }

    private static final int SLICE_LIMIT = 200;

    public int put(StrList32bmClient client, IPBitSet allIP) {
        ThreadProfiler tp = Profiler.getThreadProfiler();

        for (Keys key : Keys.values()) {
            tp.push(_ThisClass, "put." + key);
            BitSet IPs = (key == Keys.NEGATIVE_IP) ? allIP.getNegIPs() : allIP.getPosIPs();

            int result = write(key.value, IPs.stream(), client);

            tp.pop(_ThisClass, "put." + key);
            if (ZErrorHelper.isFail(result)) {
                return result;
            }
        }

        return ZErrorDef.SUCCESS;
    }

    public IPBitSetResult getAllIPs(StrList32bmClient client) {
        _Logger.info("Start getAllIPs");
        ThreadProfiler tp = Profiler.getThreadProfiler();
        IPBitSet allIPs = new IPBitSet();
        for (Keys key : Keys.values()) {
            tp.push(_ThisClass, ".get." + key);
            TValueResult result = getIPs(client, key.value);
            if (ZErrorHelper.isFail(result.getError())) {
                return new IPBitSetResult().setError(result.getError());
            }

            if (key == Keys.NEGATIVE_IP) {
                allIPs.addListNegativeIP(result.getValue().entries);
            } else {
                allIPs.addListIPs(result.getValue().entries);
            }
            tp.pop(_ThisClass, ".get." + key);
        }
        return new IPBitSetResult(ZErrorDef.SUCCESS, allIPs);
    }

    private TValueResult getIPs(StrList32bmClient client, String key) {
        ThreadProfiler tp = Profiler.getThreadProfiler();

        int countEntries = client.getCountEntries(key);
        int countSlices = (int) Math.ceil(countEntries * 1.0 / SLICE_LIMIT);

        ArrayList<Integer> listEntries = new ArrayList<>();
        // As phuonglt's suggestion for better performance,
        // Use getSlice at the first time 
        // then use getSliceFrom to get the remaining slice
        int lastEntry = 0;
        for (int i = 0; i < countSlices; i++) {
            tp.push(_ThisClass, ".get." + key + ".slice");
            TValueResult slice;
            if (i == 0) {
                slice = client.getSlice(key, 0, SLICE_LIMIT);
            } else {
                slice = client.getSliceGreaterFrom(key, lastEntry, SLICE_LIMIT, 0);
            }

            if (slice == null) {
                return new TValueResult().setError(ZErrorDef.FAIL);
            } else if (ZErrorHelper.isFail(slice.getError())) {
                return new TValueResult().setError(slice.getError());
            }

            listEntries.addAll(slice.getValue().entries);

            lastEntry = slice.getValue().getEntries().get(slice.getValue().getEntriesSize() - 1);
            tp.pop(_ThisClass, ".get." + key + ".slice");
        }

        return new TValueResult().setError(ZErrorDef.SUCCESS).setValue(new TValue().setEntries(listEntries));
    }

    private int write(String key, IntStream ips, StrList32bmClient ipbmcommon6) {
        ThreadProfiler tp = Profiler.getThreadProfiler();
        Stream<TKeyEntry> entries = ips.mapToObj(ip -> new TKeyEntry(key, ip));
        List<TKeyEntry> listKeyEntry = entries.collect(Collectors.toList());

        List<LongSupplier> actions = Arrays.asList(
                () -> removeAllEntries(ipbmcommon6, key),
                () -> writeStream(ipbmcommon6, listKeyEntry)
        );

        for (LongSupplier action : actions) {
            tp.push(_ThisClass, "put." + key + ".actionCount");
            long res = doAndWaitForIt(ipbmcommon6, action);
            if (ZErrorHelper.isFail(res)) {
                tp.pop(_ThisClass, "put." + key + ".actionCount");
                return (int) res;
            }
            tp.pop(_ThisClass, "put." + key + ".actionCount");
        }

        _Logger.info("NUM_ENTRIES: " + listKeyEntry.size());
        return ZErrorDef.SUCCESS;
    }

    private long doAndWaitForIt(StrList32bmClient client, LongSupplier action) {
        long returnValue = action.getAsLong();
        if (ZErrorHelper.isFail(returnValue)) {
            return returnValue;
        }
        long sequenceNumber;
        do {
            ZUtil.sleep(500);
            sequenceNumber = client.getDequeSeq();
        } while (sequenceNumber < returnValue);
        return returnValue;
    }

    private long writeStream(StrList32bmClient client, List<TKeyEntry> listKeyEntry) {
        return client.mmultiPutEntry(listKeyEntry);
    }

    private long removeAllEntries(StrList32bmClient client, String key) {
        TValue emptyValue = new TValue().setEntries(new ArrayList<>());
        return client.put(key, emptyValue, PutPolicy.ADD_OR_UDP);
    }
}
