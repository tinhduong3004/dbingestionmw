package com.vng.zing.zudm.common.centralizedkeys;

import com.vng.zing.exception.InvalidParamException;

public class Ip2LocationKey {
	private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.zudmcommon5 KEY_SET = new AllKeyTypeSets.zudmcommon5();
    
    public static long forIp2LocationOf(String ip) throws InvalidParamException{
    	int datakey = IPv4TextToI32(ip);
        return KEY_GEN.makeKeyFrom((long) datakey, KEY_SET.IP_LOCATION);
    }
    
    public static long forIp2LocationV2Of(String ip) throws InvalidParamException {
    	int datakey = IPv4TextToI32(ip);
        return KEY_GEN.makeKeyFrom((long) datakey, KEY_SET.IP_LOCATION_V2);
    }
    
    public static int IPv4TextToI32(String sipv4) throws InvalidParamException {
        sipv4 = sipv4.trim();
        final int len = sipv4.length();
        if (len < 7 || 15 < len) {
            throw new InvalidParamException("Invalid ip " + sipv4);
        }
        //
        final int lastIdx = len - 1;
        boolean isValid = true;
        char c;
        for (int i = 0, subStart = 0, subLen; i < len; ++i) {
            c = sipv4.charAt(i);
            if (c != '.' && (c < '0' || '9' < c)) {
                isValid = false;
                break;
            }
            //
            if (c == '.' || i == lastIdx) {
                subLen = i - subStart + (i == lastIdx ? 1 : 0);
                if (subLen == 1) {
                    // ok
                } else if (subLen == 2 || subLen == 3) {
                    if (sipv4.charAt(subStart) == '0') {
                        isValid = false;
                        break;
                    }
                } else {
                    isValid = false;
                    break;
                }
                subStart = i + 1;
            }
        }
        if (!isValid) {
            throw new InvalidParamException("Invalid ip " + sipv4);
        }
        //
        int iip = 0;
        int b = 0;
        for (int i = 0; i < len; ++i) {
            c = sipv4.charAt(i);
            if (c != '.') {
                b = b * 10 + (c - '0');
            }
            if (c == '.' || i == lastIdx) {
                if (b < 0 || b > 255) {
                    throw new InvalidParamException("Invalid ip " + sipv4);
                }
                iip = (iip << 8) | (b & 0xFF);
                b = 0;
            }
        }
        return iip;
    }
    
}
