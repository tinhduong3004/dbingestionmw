/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class FriendRankingKey {
    
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.ZUDM_FriendRanking_ZIDB64 KEY_SET = new AllKeyTypeSets.ZUDM_FriendRanking_ZIDB64();
    
    public static long forInterestFriendOf(int uid){
        return KEY_GEN.makeKeyFrom((long) uid, KEY_SET.INTEREST_FRIEND);
    } 
    
    public static long forFollowerOf(int uid){
        return KEY_GEN.makeKeyFrom((long) uid, KEY_SET.FOLLOWER);
    } 
    
    public static long forFollowerV2Of(int uid){
        return KEY_GEN.makeKeyFrom((long) uid, KEY_SET.FOLLOWER_V2);
    } 
    
    public static long forFollowerV3Of(int uid){
        return KEY_GEN.makeKeyFrom((long) uid, KEY_SET.FOLLOWER_V3);
    } 
    
    public static long forFollowerV4Of(int uid){
        return KEY_GEN.makeKeyFrom((long) uid, KEY_SET.FOLLOWER_V4);
    } 
}
