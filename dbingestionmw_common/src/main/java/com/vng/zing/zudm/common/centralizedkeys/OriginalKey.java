/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class OriginalKey {
    
    public static long getOriginalKeyForGobalIDWithHour(long key){
        long originalKey = Long.parseLong(Long.toString(key).substring(2, 17));
        return originalKey;
    }
    
    public static long getOriginalKeyForGobalID(long key){
        long originalKey = Long.parseLong(Long.toString(key).substring(1, 19));
        return originalKey;
    }
    
    public static int getOriginalKeyForUID(long key){
        int originalKey = Integer.parseInt(Long.toString(key).substring(4, 19));
        return originalKey;
    }
    
    public static int getOriginalKeyForUIDWithHour(long key){
        int originalKey =  Integer.parseInt(Long.toString(key).substring(4, 17));
        return originalKey;
    }
}