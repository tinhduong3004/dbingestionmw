/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 * @date Jul 23, 2019
 * @author quydm
 */
public class AnonymousTraceKey {
private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.ZUDM_ZiDB64AnonymousUserTrace KEY_SET = new AllKeyTypeSets.ZUDM_ZiDB64AnonymousUserTrace();
    
    public static long forLocationOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.LOCATION);
    } 
    
    public static long forAdtimaOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.ADTIMA);
    } 
    
    public static long forZingNewsOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.ZING_NEWS);
    }
    
    public static long forZingMp3Of(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.ZING_MP3);
    }
}
