/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class LocationMining {

    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.ZiDB64LivingLocation KEY_SET = new AllKeyTypeSets.ZiDB64LivingLocation();
    private final static AllKeyTypeSets.HouseholdExtension KEY_SET_HOUSEHOLD = new AllKeyTypeSets.HouseholdExtension();
    private final static AllKeyTypeSets.ZUDMLocationInterest_ZiDB64 KEY_SET_USERSSIDINTEREST = new AllKeyTypeSets.ZUDMLocationInterest_ZiDB64();

    public static long forLivingProvinceOf(int zaloId) {
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.ORGINAL);
    }

    public static long forMostVisitedAdminAreaOf(int zaloId) {
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.ORGINAL);
    }

    public static long forHouseHoldOf(int zaloId) {
        // return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.HOUSEHOLD);
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET_HOUSEHOLD.HOUSEHOLD);
    }

    public static long forUserSSISInterestOf(int zaloId) {
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET_USERSSIDINTEREST.USERSSIDINTEREST);
    }

    public static long forIpLocationf(long dataKey) {
        return KEY_GEN.makeKeyFrom(dataKey, KEY_SET.ORGINAL);
    }
}
