/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 * @date Jul 23, 2019
 * @author quydm
 */
public class AnonymousUnstableKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.UnstableAnonymousUser KEY_SET = new AllKeyTypeSets.UnstableAnonymousUser();
    
    public static long forProvinceOf(long anonymousUserId){
        return KEY_GEN.makeKeyFrom(anonymousUserId, KEY_SET.PROVINCE_TYPE);
    } 
}
