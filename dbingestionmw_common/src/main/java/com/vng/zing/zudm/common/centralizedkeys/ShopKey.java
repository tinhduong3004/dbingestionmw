/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class ShopKey {
    private final static AllKeyGenerators.HourlyKeyWithHour KEY_GEN = new AllKeyGenerators.HourlyKeyWithHour(); 
    private final static AllKeyGenerators.HourlyKeyWith2Key KEY_GEN_2KEY = new AllKeyGenerators.HourlyKeyWith2Key();
    private final static AllKeyTypeSets.StrList32ScoreSuggestion KEY_SET = new AllKeyTypeSets.StrList32ScoreSuggestion();
    
    public static long forProductTFIDFMarketOnlyOf(long id, int hour){
        return KEY_GEN.makeKeyFrom( id , KEY_SET.PRODUCT_TFIDF_MARKET_ONLY, hour);
    }
    
    public static long forProductTFIDFShopOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TFIDF_SHOP_ONLY, hour);
    }
    
    public static long forProductClickTFIDFMarketOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_CLICK_TFIDF_MARKET_ONLY, hour);
    }
    
    public static long forProductClickTFIDFShopOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_CLICK_TFIDF_SHOP_ONLY, hour);
    }
    
    public static long forProductTrendingMarketOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TRENDING_MARKET_ONLY, hour);
    }
    
    public static long forProductTrendingShopOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TRENDING_SHOP_ONLY, hour);
    }
    
    public static long forProductTrendingPenalizedV2MarketOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TRENDING_PENALIZED_V2_MARKET_ONLY, hour);
    }
    
    public static long forProductTrendingDiscountMarketOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TRENDING_DISCOUNT_MARKET_ONLY, hour);
    }
    
    public static long forProductTrendingDiscountShopOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TRENDING_DISCOUNT_SHOP_ONLY, hour);
    }
    
    public static long forProductClickTrendingMarketOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_CLICK_TRENDING_MARKET_ONLY, hour);
    }
    
    public static long forProductClickTrendingShopOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_CLICK_TRENDING_SHOP_ONLY, hour);
    }
    
    public static long forShopTrendingMarketOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.SHOP_TRENDING_MARKET_ONLY, hour);
    }
    
    public static long forShopTrendingShopOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.SHOP_TRENDING_SHOP_ONLY, hour);
    }
    
    public static long forProductTrendingStoreCateMarketOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TRENDING_STORE_CATE_MARKET_ONLY, hour);
    }
    
    public static long forProductTrendingStoreCateShopOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TRENDING_STORE_CATE_SHOP_ONLY, hour);
    }
    
    public static String forProductTrendingInStoreCateMarketOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.PRODUCT_TRENDING_IN_STORE_CATE_MARKET_ONLY, hour);
    }
    
    public static String forProductTrendingInStoreCateShopOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.PRODUCT_TRENDING_IN_STORE_CATE_SHOP_ONLY, hour);
    }
    
    public static long forProductTrendingProductTypeMarketOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TRENDING_PRODUCT_TYPE_MARKET_ONLY, hour);
    }
    
    public static long forProductTrendingProductTypeShopOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.PRODUCT_TRENDING_PRODUCT_TYPE_SHOP_ONLY, hour);
    }
    
    public static String forProductTrendingInShopMarketOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.PRODUCT_TRENDING_IN_SHOP_MARKET_ONLY, hour);
    }
    
    public static String forProductTrendingInShopShopOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.PRODUCT_TRENDING_IN_SHOP_SHOP_ONLY, hour);
    }
    
    public static String forProductTrendingInProductTypeMarketOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.PRODUCT_TRENDING_IN_PRODUCT_TYPE_MARKET_ONLY, hour);
    }
    
    public static String forProductTrendingInProductTypeShopOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.PRODUCT_TRENDING_IN_PRODUCT_TYPE_SHOP_ONLY, hour);
    }
    
    public static long forGlobalProductTrendingMarketOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.GLOBAL_PRODUCT_TRENDING_MARKET_ONLY, hour);
    }
    
    public static long forGlobalProductTrendingShopOnlyOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.GLOBAL_PRODUCT_TRENDING_SHOP_ONLY, hour);
    }
    
    public static long forGlobalProductTrendingPenalizedMarketOnlyV2Of (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.GLOBAL_PRODUCT_TRENDING_PENALIZED_MARKET_ONLY_V2, hour);
    }
    
    public static String forGlobalProductTrendingInStoreCateMarketOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.GLOBAL_PRODUCT_TRENDING_IN_STORE_CATE_MARKET_ONLY, hour);
    }
    
    public static String forGlobalProductTrendingInStoreCateShopOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.GLOBAL_PRODUCT_TRENDING_IN_STORE_CATE_SHOP_ONLY, hour);
    }
    
    public static String forGlobalProductTrendingInProductTypeMarketOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.GLOBAL_PRODUCT_TRENDING_IN_PRODUCT_TYPE_MARKET_ONLY, hour);
    }
    
    public static String forGlobalProductTrendingInProductTypeShopOnlyOf (long id1, long id2, int hour){
        return KEY_GEN_2KEY.makeKeyFrom(id1, id2, KEY_SET.GLOBAL_PRODUCT_TRENDING_IN_PRODUCT_TYPE_SHOP_ONLY, hour);
    }
    
    public static long forOAShopTrustOf (long id, int hour){
        return KEY_GEN.makeKeyFrom(id, KEY_SET.OA_SHOP_TRUST, hour);
    }
}
