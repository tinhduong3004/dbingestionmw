/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 * @date Jul 18, 2019
 * @author quydm
 */
public class AllKeyTypeSets {
      
      
    public static class ZUDM_ZiDB64Occupation {
        public final long COMPANY_OCCUPATION = 1110000000000000000L;
        public final long USER_OCCUPATION = 1120000000000000000L;
        public final long USER_INTEREST = 1130000000000000000L;
        public final long USER_MODEL_OCCUPATION = 1140000000000000000L;
        public final long USER_SELLER_OCCUPATION = 1150000000000000000L;
        public final long USER_STUDENT_OCCUPATION = 1160000000000000000L;
        public final long USER_BLUEWHITE_OCCUPATION = 1170000000000000000L;
        public final long USER_UNION_OCCUPATION = 1180000000000000000L;
        public final long COMPANY_TOP500 = 1290000000000000000L;
        public final long USER_COMPANY = 1200000000000000000L;
        public final long WORK_TYPE = 1210000000000000000L;
    }

    public static class ZUDM_ZiDB64UsedApps {
        public final long USED_APPS = 1000000000000000000L;
        public final long APPS_INTERNAL_COUNT = 2000000000000000000L;
        public final long APPS_EXTERNAL_COUNT = 3000000000000000000L;
        public final long APPS_UNINSTALL_EXTERNAL = 4000000000000000000L;
    }

    public static class OfUserTokenizer {
        public final long USER_TOKENIZED = 1110000000000000000L;
        public final long USER_DUPLICATE = 1120000000000000000L;
        
    }

    public static class ZUDM_ZiDB64UserKWDB {
        public final long USER_PHONEBOOK = 1110000000000000000L;
        public final long USER_PHONEBOOK_FILTER = 1120000000000000000L;
        public final long BIDIRECTIONAL_PHONEBOOK = 1260000000000000000L;
        public final long USER_KEYWORKS = 7110000000000000000L;
        public final long USER_INTEREST = 7120000000000000000L;
        public final long USER_INTEREST_BY_VECTOR_SIMILARITY = 7130000000000000000L;
        public final long USER_INTEREST_BY_ALS = 7170000000000000000L;
        public final long USER_INTEREST_ARTISTS = 7180000000000000000L;
    }

    public static class ZUDMUserCreditScore {
    	public final long ORGINAL = 0;
        public final long CREDIT_SCORE_V2 = 1000000000000000000L;
        public final long CREDIT_SCORE_V3 = 1100000000000000000L;
    }
    
    public static class ZUDMDemographicDB3_ZiDB64 {
    	public final long USER_REAL_NAME = 1110000000000000000L;
        public final long USER_AGE_V2 = 1270000000000000000L;
        public final long USER_GENDER_V2 = 1280000000000000000L;
    	
    }

    public static class ZUDM_RollZiDB64DemographicDB2 {
        public final long USER_GENDER = 1120000000000000000L;
        public final long USER_AGE = 1130000000000000000L;
        public final long MOST_VISITED_PROVINCE = 1150000000000000000L;
        public final long USER_HOMETOWN = 1160000000000000000L;
        public final long FOREIGN_TRIP_COUNT = 1170000000000000000L;
        public final long USER_DEVICE = 1180000000000000000L;
        public final long MARITAL_STATUS = 1190000000000000000L;
        public final long FLIGHT = 1200000000000000000L;
        public final long NUM_FRIEND = 1210000000000000000L;
        public final long USER_DEVICE2 = 1220000000000000000L;
        public final long USER_FULL_NAME = 1230000000000000000L;
        public final long PARENTAL_STATUS = 1240000000000000000L;
        public final long EDUCATION_LEVEL = 1250000000000000000L;
    }

    public static class ZUDM_ZiDB64AnonymousUser {
        public final long GENDER_TYPE = 1000000000000000000L;
        public final long GENDER_TYPE_V2 = 9000000000000000000L;
        public final long PRED_GENDER = 1100000000000000000L;
        public final long SHIFTED_PRED_GENDER = 1200000000000000000L;
        public final long AGE_TYPE = 2000000000000000000L;
        public final long AGE_TYPE_V2 = 0;
        public final long PROVINCE_TYPE = 3000000000000000000L;
        public final long DEVICE_TYPE = 4000000000000000000L;
        public final long AGE_GENDER_TYPE = 8000000000000000000L;
    }
    
    public static class ZUDM_ZiDB64AnonymousUser2 {
        public final long AGE_RANGE = 1000000000000000000L;
    }

    public static class ZUDM_ZiDB64AnonymousUserTrace {
        public final long LOCATION = 3000000000000000000L;
        public final long ADTIMA = 5000000000000000000L;
        public final long ZING_NEWS = 6000000000000000000L;
        public final long ZING_MP3 = 7000000000000000000L;
    }

    public static class UnstableAnonymousUser {
        public final long PROVINCE_TYPE = 3000000000000000000L;
    }

    public static class SuggestedMediaList {
        public final long D00_MEDIA_LISTEN = 1010000000000000000L;
        public final long PAIR_SONG = 1020000000000000000L;
        public final long REF_SONG = 1030000000000000000L;
        public final long WEB_REF = 1040000000000000000L;
        public final long SONG_SONG = 1080000000000000000L;
        public final long VIDEO_VIDEO = 1090000000000000000L;
        public final long PLAYLIST_PLAYLIST = 1100000000000000000L;
        public final long TRENDING = 1110000000000000000L;
        public final long TF_IDF = 1210000000000000000L;
        public final long VIETNAMESE_SPECIFIC = 1220000000000000000L;
        public final long FOREIGN_SPECIFIC = 1230000000000000000L;
        public final long INSTRUMENT_SPECIFIC = 1240000000000000000L;
        public final long USER_INTEREST_ALS = 7150000000000000000L;
        public final long USER_ARTICLE_KEYWORD = 3020000000000000000L;
        public final long ADS_TRENDING = 7140000000000000000L;
        public final long USER_INTEREST_HISTORY = 7160000000000000000L;
    }

    public static class ZUDMUserIncome {
        public final long INCOME_RANK = 1000000000L;
        public final long ORGINAL = 0;
    }

    public static class ZiDB64LivingLocation {
        public final long ORGINAL = 0;
    }
    
    public static class HouseholdExtension {
        public final long HOUSEHOLD = 1000000000000000000L;
    }
    
    public static class ZUDMLocationInterest_ZiDB64 {
        public final long USERSSIDINTEREST = 1000000000000000000L;
    }

    public static class ZUDMDevices_ZiDB64 {
        public final long DEVICE_RAW = 1000000000000000000L;
    }
    
    public static class OfGameH5 {
        public final long TIENLEN_PLAYING_INFO = 1000000000000000000L;
        public final long PLAYING_INFO = 2000000000000000000L;

        public final String LIST_WEEKLY_USERHIGHWINRATE = "LIST_WEEKLY_USERHIGHWINRATE_";

        /**
         * let multiple this with appid
         */
        public final long APPID_MULTIPLE = 1000000000000L;

    }


    public static class StrList32ScoreSuggestion {
        public final long PRODUCT_TFIDF_MARKET_ONLY = 4211000000000000000L;
        public final long PRODUCT_TFIDF_SHOP_ONLY = 4212000000000000000L;
        public final long PRODUCT_CLICK_TFIDF_MARKET_ONLY = 4221000000000000000L;
        public final long PRODUCT_CLICK_TFIDF_SHOP_ONLY	= 4222000000000000000L;
        public final long PRODUCT_TRENDING_MARKET_ONLY = 4710000000000000000L;
        public final long PRODUCT_TRENDING_SHOP_ONLY = 4720000000000000000L;
        public final long PRODUCT_TRENDING_PENALIZED_V2_MARKET_ONLY = 4740000000000000000L;
        public final long PRODUCT_TRENDING_DISCOUNT_MARKET_ONLY	 = 4810000000000000000L;
        public final long PRODUCT_TRENDING_DISCOUNT_SHOP_ONLY = 4820000000000000000L;
        public final long PRODUCT_CLICK_TRENDING_MARKET_ONLY = 4171000000000000000L;
        public final long PRODUCT_CLICK_TRENDING_SHOP_ONLY = 4172000000000000000L;
        public final long SHOP_TRENDING_MARKET_ONLY = 4121000000000000000L;
        public final long SHOP_TRENDING_SHOP_ONLY = 4122000000000000000L;
        public final long PRODUCT_TRENDING_STORE_CATE_MARKET_ONLY = 4131000000000000000L;
        public final long PRODUCT_TRENDING_STORE_CATE_SHOP_ONLY = 4132000000000000000L;
        public final long PRODUCT_TRENDING_IN_STORE_CATE_MARKET_ONLY = 4181000000000000000L;
        public final long PRODUCT_TRENDING_IN_STORE_CATE_SHOP_ONLY = 4182000000000000000L;
        public final long PRODUCT_TRENDING_PRODUCT_TYPE_MARKET_ONLY = 4141000000000000000L;
        public final long PRODUCT_TRENDING_PRODUCT_TYPE_SHOP_ONLY = 4142000000000000000L;
        public final long PRODUCT_TRENDING_IN_SHOP_MARKET_ONLY = 4151000000000000000L;
        public final long PRODUCT_TRENDING_IN_SHOP_SHOP_ONLY = 4152000000000000000L;
        public final long PRODUCT_TRENDING_IN_PRODUCT_TYPE_MARKET_ONLY = 4161000000000000000L;
        public final long PRODUCT_TRENDING_IN_PRODUCT_TYPE_SHOP_ONLY = 4162000000000000000L;
        public final long GLOBAL_PRODUCT_TRENDING_MARKET_ONLY = 4611000000000000000L;
        public final long GLOBAL_PRODUCT_TRENDING_SHOP_ONLY = 4612000000000000000L;
        public final long GLOBAL_PRODUCT_TRENDING_PENALIZED_MARKET_ONLY_V2 = 4614000000000000000L;
        public final long GLOBAL_PRODUCT_TRENDING_IN_STORE_CATE_MARKET_ONLY = 4621000000000000000L;
        public final long GLOBAL_PRODUCT_TRENDING_IN_STORE_CATE_SHOP_ONLY = 4622000000000000000L;
        public final long GLOBAL_PRODUCT_TRENDING_IN_PRODUCT_TYPE_MARKET_ONLY = 4631000000000000000L;
        public final long GLOBAL_PRODUCT_TRENDING_IN_PRODUCT_TYPE_SHOP_ONLY = 4632000000000000000L;
        public final long FPGROWTH_PRODUCT_PAIR_1 = 6110000000000000000L;
        public final long FPGROWTH_PRODUCT_PAIR_2 = 6120000000000000000L;
        public final long FPGROWTH_PRODUCT_PAIR_3 = 6130000000000000000L;
        public final long UID_ITEM_SUGGESTION_1 = 6610000000000000000L;
        public final long UID_ITEM_SUGGESTION_2 = 6620000000000000000L;
        public final long SIMPLE_SUGGESTION_VIEW = 7000000000000000000L;
        public final long HISTORY_VIEW = 7010000000000000000L;
        public final long OA_SHOP_TRUST = 7020000000000000000L;  
        public final long STICKER_FROM_CHAT_TRENDING = 5100000000000000000L;
        public final long STICKER_CATE_FROM_CHAT_TRENDING = 5200000000000000000L;
        public final long CHANNEL_VIEW_CONTENT_OA_TRENDING = 3110000000000000000L;
        public final long CHANNEL_VIEW_CONTENT_CONTENT_TRENDING = 3120000000000000000L;
        public final long ZOA_TRENDING = 2110000000000000000L;
        public final long ZOA_TFIDF = 2210000000000000000L;
  }

  public static class ZUDMBankLead_ZiDB {
      public final long BANKLEAD = 1000000000000000000L;
      public final long LOAN_DEMAND_TAG = 1100000000000000000L;
      public final long DISBURSELEAD = 2000000000000000000L;
      public final long DISBURSELEADSH = 3000000000000000000L;
  }
  
  public static class Relationship {
      public final long LIST_COLLEAGUES = 2000000000000000000L;
      public final long LIST_COLLEAGUES_V2 = 4000000000000000000L;
      public final long LIST_FAMILY = 3000000000000000000L;
      public final long LIST_FAMILY_V2 = 5000000000000000000L;
      public final long LIST_RELATIONSHIP =6000000000000000000L;
  }
  
  public static class ZUDM_FriendRanking_ZIDB64 {
        public final long INTEREST_FRIEND = 1000000000000000000L;
        public final long FOLLOWER = 2000000000000000000L;
        public final long FOLLOWER_V2 = 2100000000000000000L;
        public final long FOLLOWER_V3 = 2200000000000000000L;
        public final long FOLLOWER_V4 = 2300000000000000000L;
    }
  
  public static class zudmcommon5 {
	  public final long IP_LOCATION = 1000000000000000000L;
      public final long IP_LOCATION_V2 = 1100000000000000000L;
  }
}
