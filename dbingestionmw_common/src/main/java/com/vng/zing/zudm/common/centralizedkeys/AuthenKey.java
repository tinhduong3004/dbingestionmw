package com.vng.zing.zudm.common.centralizedkeys;

public class AuthenKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.OfUserTokenizer KEY_SET = new AllKeyTypeSets.OfUserTokenizer();
    
    public static long forDuplicateAccountKeyOf(int zaloId){
        return KEY_GEN.makeKeyFrom((long)zaloId, KEY_SET.USER_DUPLICATE);
    }
}
