/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */

public class DemographicPredictionKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.HourlyKey();
    private final static AllKeyGenerators.i64KeyGen KEY_GEN_DAILY = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.ZUDM_RollZiDB64DemographicDB2 KEY_SET = new AllKeyTypeSets.ZUDM_RollZiDB64DemographicDB2();
    private final static AllKeyTypeSets.ZUDMUserCreditScore KEY_SET_CREDITSCORE = new AllKeyTypeSets.ZUDMUserCreditScore();
    private final static AllKeyTypeSets.ZUDMDemographicDB3_ZiDB64 KEY_SET_DEMOGRAPHIC3 = new AllKeyTypeSets.ZUDMDemographicDB3_ZiDB64();
    
    public static long forUserRealNameOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET_DEMOGRAPHIC3.USER_REAL_NAME);
    }
    
    public static long forUserGenderOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.USER_GENDER);
    }
    
    public static long forUserGenderV2Of(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET_DEMOGRAPHIC3.USER_GENDER_V2);
    }
    
    public static long forUserAgeOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.USER_AGE);
    }
    
    public static long forUserAgeV2Of(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET_DEMOGRAPHIC3.USER_AGE_V2);
    }
    
    
    public static long forMostVisitedProvinceOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.MOST_VISITED_PROVINCE);
    }
    
    public static long forUserHometownOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.USER_HOMETOWN);
    }
    
    public static long forForeignTripCountOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.FOREIGN_TRIP_COUNT);
    }
    
    public static long forUserDeviceOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.USER_DEVICE);
    }
    
    public static long forMaritalStatusOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.MARITAL_STATUS);
    }
    
    public static long forFlightOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.FLIGHT);
    }
    
    public static long forNumFriendOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.NUM_FRIEND);
    }
    
    public static long forUserDevice2Of(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.USER_DEVICE2);
    }
    public static long forUserFullNameOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.USER_FULL_NAME);
    }
    
    public static long forCreditScoreOf(int zaloId){
        return KEY_GEN_DAILY.makeKeyFrom((long) zaloId, KEY_SET_CREDITSCORE.ORGINAL);
    }
    
    public static long forCreditScoreV2Of(int zaloId){
        return KEY_GEN_DAILY.makeKeyFrom((long) zaloId, KEY_SET_CREDITSCORE.CREDIT_SCORE_V2);
    }
    
    public static long forCreditScoreV3Of(int zaloId){
        return KEY_GEN_DAILY.makeKeyFrom((long) zaloId, KEY_SET_CREDITSCORE.CREDIT_SCORE_V3);
    }
    
    public static long forParentalStatusOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.PARENTAL_STATUS);
    }
    
    public static long forEducationLevelOf(int zaloId){
        return KEY_GEN.makeKeyFrom( (long) zaloId, KEY_SET.EDUCATION_LEVEL);
    }
    
}
