package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */

public class ZingMp3Key {
    private final static AllKeyGenerators.HourlyKeyWithHour KEY_GEN = new AllKeyGenerators.HourlyKeyWithHour();
    private final static AllKeyTypeSets.SuggestedMediaList KEY_SET = new AllKeyTypeSets.SuggestedMediaList();
    
    public static long forDOOMediaListenOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.D00_MEDIA_LISTEN, hour);
    }    
    
    public static long forPairSongOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.PAIR_SONG, hour);
    }   
    
    public static long forRefSongOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.REF_SONG, hour);
    }   
    
    public static long forWebRefOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.WEB_REF, hour);
    }   
    
    public static long forSongOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.SONG_SONG, hour);
    }   
    
    public static long forVideoOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.VIDEO_VIDEO, hour);
    }   
    
    public static long forPlaylistOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.PLAYLIST_PLAYLIST, hour);
    }   
    
    public static long forTrendingOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.TRENDING, hour);
    }   
    
    public static long forTF_IDFOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.TF_IDF, hour);
    }   
    
    public static long forVietnameseSpecificOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.VIETNAMESE_SPECIFIC, hour);
    }   
    
    public static long forForeignSpecificOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.FOREIGN_SPECIFIC, hour);
    }   
    
    public static long forInstrumentSpecificOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.INSTRUMENT_SPECIFIC, hour);
    }  
    
    public static long forUserInterestAlsOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom(zaloId, KEY_SET.USER_INTEREST_ALS, hour);
    }
}
