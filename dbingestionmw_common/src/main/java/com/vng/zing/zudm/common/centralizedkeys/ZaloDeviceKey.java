/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class ZaloDeviceKey {
    private final static AllKeyGenerators.i64KeyGen KEY_GEN = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.ZUDMDevices_ZiDB64 KEY_SET = new AllKeyTypeSets.ZUDMDevices_ZiDB64();
    
    public static long forDeviceRawOf(long uid){
        return KEY_GEN.makeKeyFrom(uid, KEY_SET.DEVICE_RAW);
    }
    
}
