/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */
public class TimeKey {

    public static String DateTimeKey(String DBKey ,String time) {
            return "datetime_" + DBKey + "_" + time.substring(8, 10);
        }
    
    public static String LastestTimeKey(String DBKey) {
            return "lastest_" + DBKey;
        }
}
