/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zing.zudm.common.centralizedkeys;

/**
 * @date Jul 18, 2019
 * @author quydm
 */
class AllKeyGenerators {
    interface i64KeyGen{
        public long makeKeyFrom(long id,long keyType); 
    }
      
    public static class HourlyKey implements i64KeyGen{
        @Override
        public long makeKeyFrom(long id, long keyType) {
            return id * 100L + keyType;
        }
    }
    public static class DailyKey implements i64KeyGen{
        @Override
        public long makeKeyFrom(long id, long keyType) {
            return id + keyType;
        }
    }
    public static class HourlyKeyWithHour{
        public long makeKeyFrom(long id, long keyType, int hour) {
            return id * 100L + keyType + hour;
        }
    }
    public static class HourlyKeyWith2Key{
        public String makeKeyFrom(long id1,long id2, long keyType, int hour) {
            long key1 = id1 * 100L + keyType + hour;
            String dataKey = String.valueOf(key1) + "_" + id2;
            return dataKey;
        }
    }
    public static class DateTimeKey{
        public String makeKeyFrom(String DBKey ,String time) {
            return "datetime_" + DBKey + "_" + time.substring(8, 10);
        }
    }
    public static class LastestTimeKey{
        public String makeKeyFrom(String DBKey) {
            return "lastest_" + DBKey;
        }
    }
}
