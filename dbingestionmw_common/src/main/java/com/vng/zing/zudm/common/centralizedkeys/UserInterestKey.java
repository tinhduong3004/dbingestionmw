package com.vng.zing.zudm.common.centralizedkeys;

/**
 *
 * @author tinhdt
 */

public class UserInterestKey {
    private final static AllKeyGenerators.HourlyKeyWithHour KEY_GEN = new AllKeyGenerators.HourlyKeyWithHour();
    private final static AllKeyGenerators.DailyKey KEY_GEN_DAILY = new AllKeyGenerators.DailyKey();
    private final static AllKeyTypeSets.SuggestedMediaList KEY_SET = new AllKeyTypeSets.SuggestedMediaList();
    private final static AllKeyTypeSets.ZUDM_ZiDB64UserKWDB KEY_SET_USERKW = new AllKeyTypeSets.ZUDM_ZiDB64UserKWDB();
    
    public static long forUserKeyworksOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET_USERKW.USER_KEYWORKS, hour);
    }    
    
    public static long forUserInterestOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET_USERKW.USER_INTEREST, hour);
    } 
    
    public static long forUserInterestByVectorSimilarityOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET_USERKW.USER_INTEREST_BY_VECTOR_SIMILARITY, hour);
    } 
    
    public static long forAdsTrendingOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.ADS_TRENDING, hour);
    } 
    
    public static long forUserInterestHistoryOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET.USER_INTEREST_HISTORY, hour);
    } 
    
    public static long forUserInterestByAlsOf(int zaloId, int hour){
        return KEY_GEN.makeKeyFrom((long) zaloId, KEY_SET_USERKW.USER_INTEREST_BY_ALS, hour);
    } 
    
    public static long forUserInterestArtistsOf(int zaloId){
        return KEY_GEN_DAILY.makeKeyFrom((long) zaloId, KEY_SET_USERKW.USER_INTEREST_ARTISTS);
    } 
}
