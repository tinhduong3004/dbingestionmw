/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zing.zudm.common.centralizedkeys.ip;

/**
 *
 * @author nhatminh2947
 */
public class IPBitSetResult {

    private int error;
    private IPBitSet ipBitSet64;

    public IPBitSetResult(int error, IPBitSet ipBitSet64) {
        this.error = error;
        this.ipBitSet64 = ipBitSet64;
    }

    public IPBitSetResult() {
        this.ipBitSet64 = null;
    }

    public int getError() {
        return error;
    }

    public IPBitSet getIpBitSet64() {
        return ipBitSet64;
    }

    public IPBitSetResult setError(int error) {
        this.error = error;
        return this;
    }

    public IPBitSetResult setIpBitSet64(IPBitSet ipBitSet64) {
        this.ipBitSet64 = ipBitSet64;
        return this;
    }
}
