plugins {
    `java-library`
}
version = "1.0.0.4"

repositories {
    jcenter()
}

dependencies {
    implementation(files(
    "/zserver/java/lib/jzcommon-thrift9-1.2.0.0.jar",
    "/zserver/java/lib/3rdparties/libthrift-0.9.0.jar",
    "/zserver/java/lib/3rdparties/slf4j-api-1.7.2.jar",
    "/zserver/java/lib/3rdparties/slf4j-log4j12-1.7.2.jar",
    "/zserver/java/lib/zudm/jzudmmiddleware-thrift9-1.1.2.5.jar",
    "/zserver/java/lib/zudm/jzuserdatamining-thrift9-1.0.8.4.jar"
    ))
}
tasks {
    val thriftgen by creating {
//        dependsOn(clean)
        doFirst {
            // Execute thrift gen
            val sourceDir = "thrift/"
            val targetDir = "."
            val thrift = "thrift-0.9.0"
            File("$sourceDir")
                    .walk()
                    .filter { file -> file.extension == "thrift" }
                    .forEach { file ->
                        exec {
                            commandLine("$thrift")
                            args("--gen", "java", "-o", "$targetDir", "$file")
                        }
                    }
        }
        doLast {
            val sourceDir = File("gen-java/")
            val targetDir = File("src/main/java")
            if (targetDir.exists()) {
                delete(targetDir)
            }
            copy {
                from(sourceDir)
                into(targetDir)
            }
            delete(sourceDir)
        }
    }
    val copyToZServer by creating {
        dependsOn(build)
        doLast {
            val targetDir = File("/zserver/java/lib/zudm/")
            val sourceDir = File("build/libs/")
            copy {
                from(sourceDir)
                into(targetDir)
            }
        }
        finalizedBy(clean)
    }
    build {
        dependsOn(thriftgen)
        finalizedBy(copyToZServer)
    }
}

