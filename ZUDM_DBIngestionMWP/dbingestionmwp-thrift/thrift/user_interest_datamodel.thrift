namespace java com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift

enum Hour{
	T00 = 0,
	T01 = 1,
	T02 = 2,
	T03 = 3,
	T04 = 4,
	T05 = 5,
	T06 = 6,
	T07 = 7,
	T08 = 8,
	T09 = 9,
	T10 = 10,
	T11 = 11,
	T12 = 12,
	T13 = 13,
	T14 = 14,
	T15 = 15,
	T16 = 16,
	T17 = 17,
	T18 = 18,
	T19 = 19,
	T20 = 20,
	T21 = 21,
	T22 = 22,
	T23 = 23
}

struct TSuggestion{
	1: required i32 productId,
	2: required i32 score
}

struct TSuggestionList{
	1: required string id,
	2: required list<TSuggestion> suggestList,
	3: required i32 hour
}

struct TSuggestionList2{
	1: required list<TSuggestion> suggestList,
	2: required i32 hour
}
