include "dbingestionmw_shared.thrift"
include "articleinterest_datamodel.thrift"
include "customaudience_datamodel.thrift"
include "locationmining_datamodel.thrift"
include "user_interest_datamodel.thrift"
include "zingmp3_suggestion_datamodel.thrift"

namespace java com.vng.zalo.zte.rnd.dbingestionmw.thrift

service MiddlewareServiceBase {
	///test if connection to service is alive
	i32 ping();
}

service DBIngestionMWP extends MiddlewareServiceBase {

	// Common
	dbingestionmw_shared.TActionResult addClientCaller(1:required string adminKey,2:required string callerName);
	dbingestionmw_shared.TActionResult removeClientCaller(1:required string adminKey,2:required string callerName);
	bool validCaller(1:required string adminKey,2:required string callerName);

	// ArticleInterest
	dbingestionmw_shared.TActionResult uploadUserKeywords(1:required string callerName,2:required i32 dataKey, 3:required i32 hour, 4:required articleinterest_datamodel.TUserKeywords inputData);
	dbingestionmw_shared.TActionResult uploadUserInterestByVectorSimilarity(1:required string callerName, 2:required i32 dataKey, 3:required i32 hour, 4:required articleinterest_datamodel.TUserKeywords inputData);
	dbingestionmw_shared.TActionResult uploadUserInterestByAls(1:required string callerName,2:required i32 dataKey, 3:required i32 hour, 4:required articleinterest_datamodel.TUserKeywords inputData);
	
	// CustomAudience
	dbingestionmw_shared.TActionResult ingestTrackedApps(1:required string callerName,2:required i32 dataKey, 3:required customaudience_datamodel.TListTrackedApp inputData);
	dbingestionmw_shared.TActionResult ingestInternalApps(1:required string callerName,2:required i32 uid, 3:required customaudience_datamodel.TListInternalApps inputData);
	dbingestionmw_shared.TActionResult ingestExternalApps(1:required string callerName,2:required i32 uid, 3:required customaudience_datamodel.TListExternalApps inputData);
	dbingestionmw_shared.TActionResult ingestExternalAppsUninstall(1:required string callerName,2:required i32 uid, 3:required customaudience_datamodel.TListExternalAppsUninstall inputData);
	

	// UserInterest
	dbingestionmw_shared.TActionResult ingestZOATrending(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestZOATfidf(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestChannelViewContentOATrending(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestChannelViewContentContentTrending(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTfidfMarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTfidfShopOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductClickTfidfMarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductClickTfidfShopOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingMarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingShopOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingPenalizedV2MarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingDiscountMarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingDiscountShopOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductClickTrendingMarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductClickTrendingShopOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestShopTrendingMarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestShopTrendingShopOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingStoreCateMarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingStoreCateShopOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingInStoreCateMarketOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingInStoreCateShopOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingProductTypeMarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingProductTypeShopOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingInShopMarketOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingInShopShopOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingInProductTypeMarketOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestProductTrendingInProductTypeShopOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestGlobalProductTrendingMarketOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestGlobalProductTrendingShopOnly(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestGlobalProductTrendingPenalizedMarketOnlyV2(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestGlobalProductTrendingInStoreCateMarketOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestGlobalProductTrendingInStoreCateShopOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestGlobalProductTrendingInProductTypeMarketOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestGlobalProductTrendingInProductTypeShopOnly(1:required string callerName, 2:required i64 dataKey1, 3:required i64 dataKey2, 4:required user_interest_datamodel.TSuggestionList2 inputData, 5:required string time);
	dbingestionmw_shared.TActionResult ingestStickerFromChatTrending(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestStickerCateFromChatTrending(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);
	dbingestionmw_shared.TActionResult ingestOAShopTrust(1:required string callerName, 2:required i64 dataKey, 3:required user_interest_datamodel.TSuggestionList2 inputData, 4:required string time);

	//Location Mining 
	dbingestionmw_shared.TActionResult ingestDensityCellCheckinInfo(1:required string callerName,2:required string dataKey, 3:required locationmining_datamodel.ZUDM_DensityCellCheckinInfo inputData);
	dbingestionmw_shared.TActionResult ingestDensityCellHomeInfo(1:required string callerName,2:required string dataKey, 3:required locationmining_datamodel.ZUDM_DensityCellHomeInfo inputData);
	dbingestionmw_shared.TActionResult ingestTravelPath(1:required string callerName,2:required string dataKey, 3:required locationmining_datamodel.ZUDM_TravelPath_City inputData);
	dbingestionmw_shared.TActionResult ingestPOIPath(1:required string callerName,2:required string dataKey, 3:required locationmining_datamodel.ZUDM_TravelPath_POI inputData);
	dbingestionmw_shared.TActionResult ingestSSIDInfo(1:required string callerName,2:required string dataKey, 3:required locationmining_datamodel.ZUDM_WifiInfo inputData);
	dbingestionmw_shared.TActionResult ingestHousehold(1:required string callerName,2:required i32 userId, 3:required locationmining_datamodel.ZUDM_Household inputData);
	dbingestionmw_shared.TActionResult ingestLivingProvince(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_LivingLoc inputData);
	dbingestionmw_shared.TActionResult ingestForeignTripCount(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_ForeignTripCount inputData);
	dbingestionmw_shared.TActionResult IngestUserSSIDInterest(1:required string callerName,2:required i32 uid, 3:required locationmining_datamodel.ZUDM_UserInterestLocation inputData);
	
	//Zingmp3Suggestion
	dbingestionmw_shared.TActionResult uploadD00MediaListen(1:required string callerName, 2:required i32 dataKey, 3:required string time,  4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadPairSong(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadRefSong(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadWebRef(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadVideo(1:required string callerName, 2:required i32 dataKey, 3:required string timer, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadSong(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadPlaylist(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadTrending(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadTfIdf(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadVietnameseSpecific(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadForeignSpecific(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadInstrumentSpecific(1:required string callerName, 2:required i32 dataKey, 3:required string time, 4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadAdsTrending(1:required string callerName, 2:required i32 dataKey, 3:required string time,  4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadUserInterestAls(1:required string callerName, 2:required i32 dataKey, 3:required string time,  4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);
	dbingestionmw_shared.TActionResult uploadUserInterestHistory(1:required string callerName, 2:required i32 dataKey, 3:required string time,  4:required zingmp3_suggestion_datamodel.TSuggestionSongList inputData);

	//Cache
	i32 getHourCache(1:required string api);

}
