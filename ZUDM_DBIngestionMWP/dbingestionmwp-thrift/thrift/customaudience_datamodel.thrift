namespace java com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift


enum DBAction {
	CLEAR = 0,
	INGEST = 1,
	SWAP = 2
}

struct TAudiencePartition{
	1: required list<i32> audiences,
	2: required DBAction command 
}

struct TTrackedAppInfo {
	1: required string appID,  
	2: required string platform,   
	3: required string lastAccess,  
}


struct TListTrackedApp {
	1: required list<TTrackedAppInfo> apps
}

struct TInternalAppsInfo {
	1: required string app,  
	2: required string platform,   
	3: required i32 count  
}


struct TListInternalApps {
	1: required list<TInternalAppsInfo> apps,
	2: required string date
}

struct TExternalAppsInfo {
	1: required i32 packageID,  
	2: required string platform,   
	3: required i32 count
}


struct TListExternalApps {
	1: required list<TExternalAppsInfo> apps,
	2: required string date
}

struct TExternalAppsUninstallInfo {
	1: required i32 packageID,  
	2: required string platform
}


struct TListExternalAppsUninstall {
	1: required list<TExternalAppsUninstallInfo> apps,
	2: required string date
}
