#!/bin/bash
set -xe
if [[ -d dbingestionmwp-thrift ]]; then
    (   
        cd dbingestionmwp-thrift
        ./gradlew build
    )
else
    notfound dbingestionmwp-thrift 
fi

if [[ -d dbingestionmwp-client ]]; then
    (
        cd dbingestionmwp-client
        ./gradlew build
    )
else
    notfound dbingestionmwp-client
fi

if [[ -d ZUDM_DBIngestionMWService_Production ]]; then
    (
        cd ZUDM_DBIngestionMWService_Production
        ./deploy.sh
    )
else
    notfound ZUDM_DBIngestionMWService_Production
fi

