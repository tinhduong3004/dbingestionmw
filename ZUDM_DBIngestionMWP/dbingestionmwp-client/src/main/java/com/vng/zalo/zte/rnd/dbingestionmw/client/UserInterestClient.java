package com.vng.zalo.zte.rnd.dbingestionmw.client;


import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserKeywords;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMWP;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;

public class UserInterestClient extends AbstractDBIngestionMWPClient {

    public UserInterestClient(String host,int port){
        super(host,port);
    }

    public UserInterestClient(){
        super();
    }

    private final String CALLERNAME = "Article";
    
    public TActionResult uploadUserKeywords(int zaloId, int hour, TUserKeywords inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadUserKeywords(CALLERNAME, zaloId, hour,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadUserInterestByVectorSimilarity(int zaloId, int hour, TUserKeywords inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadUserInterestByVectorSimilarity(CALLERNAME, zaloId, hour,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadUserInterestByAls(int zaloId, int hour, TUserKeywords inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadUserInterestByAls(CALLERNAME, zaloId, hour,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
