/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMWP;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift.TSuggestionList2;

/**
 *
 * @author tinhdt
 */
public class ShopTrendingClient extends AbstractDBIngestionMWPClient{
    private final String CALLERNAME = "ShopTrending";
    
    public ShopTrendingClient(String host,int port){
        super(host,port);
    }

    public ShopTrendingClient(){
        super();
    }
    
    public TActionResult ingestZOATrending(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestZOATrending(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestZOATfidf(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestZOATfidf(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestChannelViewContentOATrending(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestChannelViewContentOATrending(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestChannelViewContentContentTrending(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestChannelViewContentContentTrending(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTfidfMarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTfidfMarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTfidfShopOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTfidfShopOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductClickTfidfMarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductClickTfidfMarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductClickTfidfShopOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductClickTfidfShopOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingMarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingMarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingShopOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingShopOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingPenalizedV2MarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingPenalizedV2MarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingDiscountMarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingDiscountMarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingDiscountShopOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingDiscountShopOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductClickTrendingMarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductClickTrendingMarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductClickTrendingShopOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductClickTrendingShopOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestShopTrendingMarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestShopTrendingMarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestShopTrendingShopOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestShopTrendingShopOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingStoreCateMarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingStoreCateMarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingStoreCateShopOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingStoreCateShopOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingInStoreCateMarketOnly(long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingInStoreCateMarketOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingInStoreCateShopOnly(long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingInStoreCateShopOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingProductTypeMarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingProductTypeMarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingProductTypeShopOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingProductTypeShopOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingInShopMarketOnly(long dataKey1,long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingInShopMarketOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingInShopShopOnly(long dataKey1,long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingInShopShopOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingInProductTypeMarketOnly(long dataKey1,long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingInProductTypeMarketOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestProductTrendingInProductTypeShopOnly(long dataKey1,long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestProductTrendingInProductTypeShopOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestGlobalProductTrendingMarketOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestGlobalProductTrendingMarketOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestGlobalProductTrendingShopOnly(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestGlobalProductTrendingShopOnly(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestGlobalProductTrendingPenalizedMarketOnlyV2(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestGlobalProductTrendingPenalizedMarketOnlyV2(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestGlobalProductTrendingInStoreCateMarketOnly(long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestGlobalProductTrendingInStoreCateMarketOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestGlobalProductTrendingInStoreCateShopOnly(long dataKey1,long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestGlobalProductTrendingInStoreCateShopOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestGlobalProductTrendingInProductTypeMarketOnly(long dataKey1,long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestGlobalProductTrendingInProductTypeMarketOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestGlobalProductTrendingInProductTypeShopOnly(long dataKey1,long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestGlobalProductTrendingInProductTypeShopOnly(CALLERNAME, dataKey1, dataKey2, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestStickerFromChatTrending(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestStickerFromChatTrending(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestStickerCateFromChatTrending(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestStickerCateFromChatTrending(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestOAShopTrust(long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestOAShopTrust(CALLERNAME, dataKey, inputData, time);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
