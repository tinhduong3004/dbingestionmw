package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListInternalApps;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListExternalApps;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListExternalAppsUninstall;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListTrackedApp;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMWP;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;

/**
 *
 * @author tinhdt
 */

public class CustomAudienceClient extends AbstractDBIngestionMWPClient {
	public CustomAudienceClient(String host, int port) {
        super(host, port);
    }

    public CustomAudienceClient() {
        super();
    }
    
    private final String CALLERNAME = "CustomAudience";

    public TActionResult uploadCustomAudience(int uid, TListTrackedApp inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestTrackedApps(CALLERNAME, uid, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadCountedInternalApps(int uid, TListInternalApps inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestInternalApps(CALLERNAME, uid, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadExternalApps(int uid, TListExternalApps inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestExternalApps(CALLERNAME, uid, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadExternalAppsUninstall(int uid, TListExternalAppsUninstall inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestExternalAppsUninstall(CALLERNAME, uid, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
