/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.common;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;
import org.apache.thrift.TServiceClient;
import org.apache.thrift.TServiceClientFactory;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;

/**
 *
 * @author quydm
 */
public class ThriftClientPool<T extends TServiceClient> {

    protected final static Logger logger = Logger.getLogger(ThriftClientPool.class);
    protected GenericObjectPool<T> _internalPool;
    public String _host = "0.0.0.0";
    public int _port = 8080;
    public int _timeout = 10000;

    public int _maxTotal = 8;
    public int _maxIdle = 8;
    public int _minIdle = 0;

    public ThriftClientPool(String host, int port, int timeout, int maxTotal, int maxIdle, int minIdle) {
        this._host = host;
        this._port = port;
        this._timeout = timeout < 1 ? this._timeout : timeout;

        this._maxTotal = maxTotal < 1 ? this._maxTotal : maxTotal;
        this._maxIdle = maxIdle < 1 ? this._maxIdle : maxIdle;
        this._minIdle = minIdle < 1 ? this._minIdle : minIdle;
    }

    public void init(TServiceClientFactory clientFactory) {
        init(clientFactory, null);
    }

    public void init(TServiceClientFactory clientFactory, TProtocolFactory protocolFactory) {
        if (this._internalPool == null) {
            synchronized (ThriftClientPool.class) {
                if (this._internalPool == null) {
                    GenericObjectPool.Config config = new GenericObjectPool.Config();
                    config.maxIdle = _maxIdle;
                    config.minIdle = _minIdle;
                    config.maxActive = _maxTotal;
//                    config.setMinIdle(_minIdle);
//                    config.setMaxTotal(_maxTotal);
                    this._internalPool = new GenericObjectPool<>(new ThriftClientFactory(clientFactory, protocolFactory), config);
                }
            }
        }
    }

    public T borrowClient() throws Exception {
        return _internalPool.borrowObject();
    }

    public void invalidateClient(T client) {
        try {
            _internalPool.invalidateObject(client);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void invalidateClient(T client, Exception reason) {
        invalidateClient(client);
    }

    public void returnClient(T client) {
        try {
            _internalPool.returnObject(client);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    public void close() {
        try {
            _internalPool.close();
        } catch (Exception ex) {
            logger.error("Cannot close pool", ex);
        }
    }

    class ThriftClientFactory extends BasePoolableObjectFactory<T> {

        private TServiceClientFactory<T> _clientFactory;
        private TProtocolFactory _protocolFactory;

        public ThriftClientFactory(TServiceClientFactory<T> clientFactory) {
            this._clientFactory = clientFactory;
        }

        public ThriftClientFactory(TServiceClientFactory<T> clientFactory, TProtocolFactory protocolFactory) {
            this._clientFactory = clientFactory;
            this._protocolFactory = protocolFactory;
        }

        @Override
        public T makeObject() throws Exception {
            TSocket tSocket = new TSocket(_host, _port, _timeout);
            TFramedTransport transport = new TFramedTransport(tSocket);
            assert (transport != null);
            TProtocol protocol;
            if (_protocolFactory != null) {
                protocol = _protocolFactory.getProtocol(transport);
            } else {
                protocol = new TBinaryProtocol(transport);
            }
            transport.open();
            return _clientFactory.getClient(protocol);
        }
    }
}
