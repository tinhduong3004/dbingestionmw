/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.client;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMWP;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zalo.zte.rnd.dbingestionmw.zingmp3_suggestion.thrift.TSuggestionSongList;

/**
 *
 * @author tinhdt
 */
public class ZingMp3Client extends AbstractDBIngestionMWPClient{

    private final String CALLERNAME = "MP3";

    public ZingMp3Client(String host,int port){
        super(host,port);
    }

    public ZingMp3Client(){
        super();
    }

    public TActionResult uploadD00MediaListen(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadD00MediaListen(CALLERNAME, zaloId, time, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadPairSong(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadPairSong(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadRefSong(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadRefSong(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadWebRef(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadWebRef(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadVideo(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadVideo(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadSong(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadSong(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadPlaylist(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadPlaylist(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadTrending(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadTrending(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadTfIdf(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadTfIdf(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadVietnameseSpecific(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadVietnameseSpecific(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadForeignSpecific(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadForeignSpecific(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadInstrumentSpecific(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadInstrumentSpecific(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadAdsTrending(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadAdsTrending(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadUserInterestHistory(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadUserInterestHistory(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult uploadUserInterestAls(int zaloId, String time, TSuggestionSongList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadUserInterestAls(CALLERNAME, zaloId, time,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
