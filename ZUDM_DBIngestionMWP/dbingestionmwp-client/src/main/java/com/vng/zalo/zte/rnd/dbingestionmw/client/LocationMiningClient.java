// Created By PTP
package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_LivingLoc;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMWP;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_Household;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_TravelPath_City;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_TravelPath_POI;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_WifiInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_DensityCellCheckinInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_DensityCellHomeInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_ForeignTripCount;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_UserInterestLocation;


/**
 *
 * @author tinhdt
 */
public class LocationMiningClient extends AbstractDBIngestionMWPClient{
    public LocationMiningClient(String host,int port){
        super(host,port);
    }

    public LocationMiningClient(){
        super();
    }

    private final String CALLERNAME = "Location@Mining";
    
    public TActionResult ingestDensityCellCheckinInfo(String dataKey, ZUDM_DensityCellCheckinInfo inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestDensityCellCheckinInfo(CALLERNAME, dataKey, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestDensityCellHomeInfo(String dataKey, ZUDM_DensityCellHomeInfo inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestDensityCellHomeInfo(CALLERNAME, dataKey, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestLivingProvince(int zaloId, ZUDM_LivingLoc inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestLivingProvince(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult ingestTravelPath(String datakey, ZUDM_TravelPath_City inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestTravelPath(CALLERNAME, datakey, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
     public TActionResult ingestForeignTripCount(int zaloId, ZUDM_ForeignTripCount inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestForeignTripCount(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
 
    
    public TActionResult ingestPOIPath(String dataKey, ZUDM_TravelPath_POI inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestPOIPath(CALLERNAME, dataKey, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
 
    
    public TActionResult ingestSSIDInfo(String dataKey, ZUDM_WifiInfo inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestSSIDInfo(CALLERNAME, dataKey, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    

    
    public TActionResult ingestHousehold(int userId, ZUDM_Household inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestHousehold(CALLERNAME, userId,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult IngestUserSSIDInterest(int userId, ZUDM_UserInterestLocation inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.IngestUserSSIDInterest(CALLERNAME, userId,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}