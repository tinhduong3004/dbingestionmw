package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMWP;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;

public class AdminDBMWClient extends AbstractDBIngestionMWPClient {
    public AdminDBMWClient(String host,int port){
        super(host,port);
    }

    public AdminDBMWClient(){
        super();
    }

    public TActionResult addClientCaller(String adminKey,String callerName) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.addClientCaller(adminKey, callerName);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    public TActionResult removeClientCaller(String adminKey,String callerName) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.removeClientCaller(adminKey, callerName);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    public boolean validCaller(String adminKey,String callerName) throws Exception {
        boolean result = false;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMWP.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.validCaller(adminKey, callerName);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

}
