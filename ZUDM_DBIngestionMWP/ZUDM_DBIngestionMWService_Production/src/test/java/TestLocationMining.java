
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserKeywords;
import com.vng.zalo.zte.rnd.dbingestionmw.client.LocationMiningClient;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb64.thrift.TValue;
import com.vng.zing.zidb64.thrift.TValueResult;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.ZUDM_Util;
import com.vng.zing.zudm.common.centralizedkeys.DemographicPredictionKey;
import com.vng.zing.zudm.common.centralizedkeys.LocationMining;
import com.vng.zing.zuserdatamining.thrift.ZUDM_MostVisitedArea;
import org.apache.log4j.Logger;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_LivingLoc;
import com.vng.zing.zuserdatamining.thrift.ZUDM_UserCreditScore;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_ForeignTripCount;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_UserInterestLocation;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_UserInterestLocationCategory;
import java.util.List;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cpu11232
 */
public class TestLocationMining {
    public static final LocationMiningClient Location = new LocationMiningClient();
    private static final Class MAIN_CLASS = LocationMiningClient.class;
    private static final Logger LOGGER = ZLogger.getLogger(MAIN_CLASS);
    public static final ZiDB64Client DemographicDB = new ZiDB64Client("ZUDM_ZiDB64DemographicDB");
    public static final ZiDB64Client MostVisitedAADB = new ZiDB64Client("ZiDB64LivingLocation");
    public static final ZiDB64Client _DBCmon3Cli = new ZiDB64Client("zudmcommon3");
    public static final ZiDB64Client LOCATIONINTEREST = new ZiDB64Client("ZUDMLocationInterest_ZiDB64");

//    private static final ZiDBClient _ZiDBTravelPathCity = new ZiDBClient("ZUDM_ZiDBTravelPathCity");
//    public static final ZiDBClient _DBClientTravelPathCity = new ZiDBClient("ZUDM_ZiDBTravelPathCity");
    

    public static void main(String[] args) throws Exception {
//        ZUDM_MostVisitedArea data = new ZUDM_MostVisitedArea();
//        ZUDM_MostVisitedArea sample = new ZUDM_MostVisitedArea();
//        data.setAdministrativeLocID(100);
//        data.setUpdatedDate(20191028);
        int id = 11;
        System.out.println(Long.valueOf(id));
//        Location.ingestMostVisitedAdminArea(id, data);
//        System.out.println(Location.ingestMostVisitedAdminArea(id, data));
//        TValueResult valres = ZUDM_Util.getWithProfiler(MAIN_CLASS, "uploadMostVisitedAdminArea", MostVisitedAADB, 1111l);
//        ZUDM_MostVisitedArea usr = ZUDM_Util.getTbase(LOGGER, new ZUDM_MostVisitedArea(), valres);
//        System.out.println(usr);

//        ZUDM_LivingLoc data = new ZUDM_LivingLoc();
//        data.setId(9);
//        data.setNCheckIn(1224);
//        Location.ingestLivingProvince(111, data);
//        System.out.println(Location.ingestLivingProvince(111, data));
//        System.out.println(LocationMining.forLivingProvinceOf(156062122l));
////        ZUDM_LivingLoc result = (ZUDM_LivingLoc)WithCompression.getFrom(_DBCmon3Cli, LocationMining.forLivingProvinceOf(156062122), data);
//        TValueResult tValueResult = ZUDM_Util.getWithProfiler(MAIN_CLASS, "livigProv.DBCmon3Cli.get", _DBCmon3Cli, 156062122l);
//        ZUDM_LivingLoc oldLivingLoc = ZUDM_Util.getTbase(LOGGER, new ZUDM_LivingLoc(), tValueResult);
//        System.out.println(oldLivingLoc.toString());


        ZUDM_UserInterestLocationCategory sub = new ZUDM_UserInterestLocationCategory();
//        sub.setValue(1);
        sub.setConfidence(1);
        ZUDM_UserInterestLocation data = new ZUDM_UserInterestLocation();
        data.addToInterest_category(sub);
        data.setUpdateDate(2019);
        Location.IngestUserSSIDInterest(id, data);
        System.out.println(Location.IngestUserSSIDInterest(id, data));
        long genergateKey = LocationMining.forUserSSISInterestOf(id);
        System.out.println(genergateKey);
        ZUDM_UserInterestLocation result = (ZUDM_UserInterestLocation)WithCompression.getFrom(LOCATIONINTEREST, LocationMining.forUserSSISInterestOf(id), data);
        System.out.println(result);

//            ZUDM_ForeignTripCount data = new ZUDM_ForeignTripCount();
//            data.setForeignTripCount((short)12);
//            data.setUpdatedDate((short)20);
//            System.out.println(Location.ingestForeignTripCount(id, data));;
//            ZUDM_ForeignTripCount result = (ZUDM_ForeignTripCount)WithCompression.getFrom(DemographicDB, DemographicPredictionKey.forForeignTripCountOf(id), data);
//            System.out.println(result);
            
    }
}


 