/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.vng.zalo.zte.rnd.dbingestionmw.client.CacheMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.client.ShopTrendingClient;
import com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift.TSuggestion;
import com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift.TSuggestionList2;
import com.vng.zing.strlist32score.thrift.wrapper.StrList32ScoreClient;
import com.vng.zing.zudm.common.centralizedkeys.ShopKey;
import com.vng.zing.zudm.common.centralizedkeys.TimeKey;
import com.vng.zing.zudm.common.centralizedkeys.ZOAKey;
/**
 *
 * @author tinhdt
 */
public class TestShopTrending {
    private static final ShopTrendingClient CLIENT = new ShopTrendingClient();
    private static final StrList32ScoreClient DBCLIENT = new StrList32ScoreClient("StrList32ScoreSuggestion");
    private static final CacheMWClient CACHE = new CacheMWClient();
    
    private static TSuggestion genergateTSuggestion (int id, int score) {
        TSuggestion data = new TSuggestion();
        data.setProductId(id);
        data.setScore(score);
        return data;
    }
    
    private static void ingestGlobalProductTrendingShopOnly (long id, TSuggestionList2 inputData, String time) throws Exception {
//        CLIENT.ingestZOATrending(dataKey, inputData, time);
//        System.out.println(CLIENT.ingestGlobalProductTrendingMarketOnly(id, inputData, time));
        long genergateKey = ShopKey.forGlobalProductTrendingMarketOnlyOf(id, 19);
        System.out.println(genergateKey);
        System.out.println(DBCLIENT.get(String.valueOf(genergateKey)).toString());
        String DateTimeKey = TimeKey.DateTimeKey("4141000000000000000", time);
        String LastestTimeKey = TimeKey.LastestTimeKey("4141000000000000000");
        int lastestTime = DBCLIENT.get(LastestTimeKey).value.entries.get(0).id;
        int dateTime = DBCLIENT.get(DateTimeKey).value.entries.get(0).id;
        System.out.println("lastestKey:  " +LastestTimeKey+"   lastestTime:  " +lastestTime);
        System.out.println("dateKey:  " +DateTimeKey+"   dateTime:  " +dateTime);
    }
    
    private static void ingestProductTfidfMarketOnly (long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        CLIENT.ingestProductTfidfMarketOnly(dataKey, inputData, time);
        System.out.println(CLIENT.ingestProductTfidfMarketOnly(dataKey, inputData, time));
        long genergateKey = ShopKey.forProductTFIDFMarketOnlyOf(dataKey, 07);
        System.out.println(genergateKey);
        System.out.println(DBCLIENT.get(String.valueOf(genergateKey)).toString());
        String DateTimeKey = TimeKey.DateTimeKey("4211000000000000000", time);
        String LastestTimeKey = TimeKey.LastestTimeKey("4211000000000000000");
        int lastestTime = DBCLIENT.get(LastestTimeKey).value.entries.get(0).id;
        int dateTime = DBCLIENT.get(DateTimeKey).value.entries.get(0).id;
        System.out.println("lastestKey:  " +LastestTimeKey+"   lastestTime:  " +lastestTime);
        System.out.println("dateKey:  " +DateTimeKey+"   dateTime:  " +dateTime);
    }
    
    private static void ingestProductTrendingInShopMarketOnly (long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
        CLIENT.ingestProductTrendingInShopMarketOnly(dataKey1, dataKey2, inputData, time);
        System.out.println(CLIENT.ingestProductTrendingInShopMarketOnly(dataKey1, dataKey2, inputData, time));
        String genergateKey = String.valueOf(ShopKey.forProductTrendingInShopMarketOnlyOf(dataKey1, dataKey2, 06));
        System.out.println(genergateKey);
        System.out.println(DBCLIENT.get(genergateKey).toString());
        String DateTimeKey = TimeKey.DateTimeKey("4151000000000000000", time);
        String LastestTimeKey = TimeKey.LastestTimeKey("4151000000000000000");
        int lastestTime = DBCLIENT.get(DateTimeKey).value.entries.get(0).id;
        int dateTime = DBCLIENT.get(LastestTimeKey).value.entries.get(0).id;
        System.out.println("lastestKey:  " +LastestTimeKey+"   lastestTime:  " +lastestTime);
        System.out.println("dateKey:  " +DateTimeKey+"   dateTime:  " +dateTime);
    }
    
    private static void ingestProductTrendingPenalizedV2MarketOnly (long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        CLIENT.ingestProductTrendingPenalizedV2MarketOnly(dataKey, inputData, time);
        System.out.println(CLIENT.ingestProductTrendingPenalizedV2MarketOnly(dataKey, inputData, time));
        long genergateKey = ShopKey.forProductTrendingPenalizedV2MarketOnlyOf(dataKey, 8);
        System.out.println(genergateKey);
        System.out.println(DBCLIENT.get(String.valueOf(genergateKey)).toString());
        String DateTimeKey = TimeKey.DateTimeKey("4740000000000000000", time);
        String LastestTimeKey = TimeKey.LastestTimeKey("4740000000000000000");
        int lastestTime = DBCLIENT.get(LastestTimeKey).value.entries.get(0).id;
        int dateTime = DBCLIENT.get(DateTimeKey).value.entries.get(0).id;
        System.out.println("lastestKey:  " +LastestTimeKey+"   lastestTime:  " +lastestTime);
        System.out.println("dateKey:  " +DateTimeKey+"   dateTime:  " +dateTime);
    }
    
    private static void ingestOAShopTrust (long dataKey, TSuggestionList2 inputData, String time) throws Exception {
        CLIENT.ingestOAShopTrust(dataKey, inputData, time);
        System.out.println(CLIENT.ingestOAShopTrust(dataKey, inputData, time));
        long genergateKey = ShopKey.forOAShopTrustOf(dataKey, 1);
        System.out.println(genergateKey);
        System.out.println(DBCLIENT.get(String.valueOf(genergateKey)).toString());
        String DateTimeKey = TimeKey.DateTimeKey("7020000000000000000", time);
        String LastestTimeKey = TimeKey.LastestTimeKey("7020000000000000000");
        int lastestTime = DBCLIENT.get(LastestTimeKey).value.entries.get(0).id;
        int dateTime = DBCLIENT.get(DateTimeKey).value.entries.get(0).id;
        System.out.println("lastestKey:  " +LastestTimeKey+"   lastestTime:  " +lastestTime);
        System.out.println("dateKey:  " +DateTimeKey+"   dateTime:  " +dateTime);
    }
    
    private static void ingestGlobalProductTrendingInProductTypeMarketOnly (long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws Exception {
//        CLIENT.ingestGlobalProductTrendingInProductTypeMarketOnly(dataKey1, dataKey2, inputData, time);
        System.out.println(CLIENT.ingestProductTrendingInShopMarketOnly(dataKey1, dataKey2, inputData, time));
        String genergateKey = String.valueOf(ShopKey.forProductTrendingInShopMarketOnlyOf(dataKey1, dataKey2, 06));
        System.out.println(genergateKey);
        System.out.println(DBCLIENT.get(genergateKey));
        String DateTimeKey = TimeKey.DateTimeKey("4631000000000000000", time);
        String LastestTimeKey = TimeKey.LastestTimeKey("4631000000000000000");
        int lastestTime = DBCLIENT.get(DateTimeKey).value.entries.get(0).id;
        int dateTime = DBCLIENT.get(LastestTimeKey).value.entries.get(0).id;
        System.out.println("lastestKey:  " +LastestTimeKey+"   lastestTime:  " +lastestTime);
        System.out.println("dateKey:  " +DateTimeKey+"   dateTime:  " +dateTime);
    }
    
    public static void main(String[] args) throws Exception {
        TSuggestionList2 data = new TSuggestionList2();
        long datakey = 00;
        long datakey2 = 22;
        String time = "2019112918";
        int id = 10;
        int score = 11;
        data.addToSuggestList(genergateTSuggestion(id, score));
//        ingestGlobalProductTrendingInProductTypeMarketOnly(datakey, datakey2,data, time);
        ingestGlobalProductTrendingShopOnly(datakey, data, time);
//        ingestProductTrendingInShopMarketOnly(datakey1, datakey2, data, time);
        System.out.println(CACHE.GetTimeCache("ingestProductTrendingProductTypeMarketOnly"));
    }
}
