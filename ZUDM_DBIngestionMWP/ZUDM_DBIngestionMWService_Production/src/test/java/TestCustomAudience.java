

import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListTrackedApp;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TTrackedAppInfo;
//import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zalo.zte.rnd.dbingestionmw.client.CustomAudienceClient;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TExternalAppsUninstallInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TInternalAppsInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListExternalApps;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListExternalAppsUninstall;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListInternalApps;
import static com.vng.zalo.zte.rnd.dbingestionmw.model.BaseModel.alertBotClient;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.alert_bot.thrift.ZUDM_AlertResult;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.CustomAudienceKey;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


public class TestCustomAudience {
    private final static CustomAudienceClient DBCLIENT = new CustomAudienceClient("127.0.0.1",18782);
    private final static ZiDB64Client DB = new ZiDB64Client("ZUDM_ZiDB64UsedApps");
	private static TTrackedAppInfo generateTListAudience(String appID, String platform, String lastAccess) {
		TTrackedAppInfo pAucience = new TTrackedAppInfo();
		pAucience.setAppID(appID);
		pAucience.setPlatform(platform);
		pAucience.setLastAccess(lastAccess);
		return pAucience;
	}

        private static TInternalAppsInfo generateTListCountedApps(String appID, String platform, int count) {
		TInternalAppsInfo CountedApps = new TInternalAppsInfo();
		CountedApps.setApp(appID);
		CountedApps.setPlatform(platform);
		CountedApps.setCount(count);
		return CountedApps;
	};
        
	public static void main(String[] args) throws Exception {
			
//			TListTrackedApp plistAudience = new TListTrackedApp();
//                        int key = 111;
//                        String appID = "baomoi";
//                        String platform = "app";
//                        String lastAccess = "20191230";
//                        plistAudience.addToApps(generateTListAudience(appID, platform, lastAccess));
//			TActionResult ar = DBCLIENT.uploadCustomAudience(key, plistAudience);
//			System.out.println(ar.toString());
//			System.out.println(key+" ----------- "+plistAudience);
//                        TListTrackedApp ggender = (TListTrackedApp)WithCompression.getFrom(DB, CustomAudienceKey.forUsedAppsOf(key), plistAudience);
//                        System.out.println(ggender.toString());
                        System.out.println("start");
                        TListInternalApps plistAudience = new TListInternalApps();
                        int key = 100505810;
                        String appID = "baomoi";
                        String platform = "app";
                        int count = 20;
                        plistAudience.addToApps(generateTListCountedApps(appID, platform, count));
			TActionResult ar = DBCLIENT.uploadCountedInternalApps(key, plistAudience);
			System.out.println(ar.toString());
			System.out.println(key+" ----------- "+plistAudience);
//                        TListExternalApps ex = (TListExternalApps)WithCompression.getFrom(DB, CustomAudienceKey.forCountedExternalAppsOf(key), new TListExternalApps());
// 
//                        TListExternalAppsUninstall un = (TListExternalAppsUninstall)WithCompression.getFrom(DB, CustomAudienceKey.forExternalAppsUninstallOf(key), new TListExternalAppsUninstall());
                        TListInternalApps ggender = (TListInternalApps)WithCompression.getFrom(DB, CustomAudienceKey.forCountedInternalAppsOf(key), new TListInternalApps());
//                        TListTrackedApp ggender = (TListTrackedApp)WithCompression.getFrom(DB, CustomAudienceKey.forUsedAppsOf(key), plistAudience);                        
                        System.out.println(ggender.toString());
                        
	}
}
