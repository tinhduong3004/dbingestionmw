/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.vng.zalo.zte.rnd.dbingestionmw.zingmp3_suggestion.thrift.TSuggestionSong;
import com.vng.zalo.zte.rnd.dbingestionmw.zingmp3_suggestion.thrift.TSuggestionSongList;
import com.vng.zalo.zte.rnd.dbingestionmw.client.ZingMp3Client;
import com.vng.zalo.zte.rnd.dbingestionmw.client.CacheMWClient;
import com.vng.zing.strlist32score.thrift.wrapper.StrList32ScoreClient;
import com.vng.zing.zudm.common.centralizedkeys.TimeKey;
import com.vng.zing.zudm.common.centralizedkeys.ZingMp3Key;
/**
 *
 * @author tinhdt
 */
public class TestMp3 {
    private static final CacheMWClient CACHE = new CacheMWClient();
    private static final ZingMp3Client CLIENT = new ZingMp3Client();
    private static final StrList32ScoreClient ZINGMP3 = new StrList32ScoreClient("SuggestedMediaList");
    
    private static TSuggestionSong genergateTSuggestionSong (int id, int score) {
        TSuggestionSong data = new TSuggestionSong();
        data.setId(id);
        data.setCount(score);
        return  data;
    }
    
    private static void testUploadD00MediaListen (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadD00MediaListen(dataKey, time, inputData);
        System.out.println(CLIENT.uploadD00MediaListen(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forDOOMediaListenOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(CACHE.GetTimeCache("uploadD00MediaListen"));
        System.out.println(result);
    }
    
    private static void testUploadPairSong (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadPairSong(dataKey, time, inputData);
        System.out.println(CLIENT.uploadPairSong(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forPairSongOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(CACHE.GetTimeCache("uploadPairSong"));
        System.out.println(result);
    }
    
    private static void testUploadRefSong (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadRefSong(dataKey, time, inputData);
        System.out.println(CLIENT.uploadRefSong(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forRefSongOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(CACHE.GetTimeCache("uploadRefSong"));
        System.out.println(result);
    }
    
    private static void testUploadWebRef (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadWebRef(dataKey, time, inputData);
        System.out.println(CLIENT.uploadWebRef(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forWebRefOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(CACHE.GetTimeCache("uploadWebRef"));
        System.out.println(result);
    }
    
    private static void testUploadvideo (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadVideo(dataKey, time, inputData);
        System.out.println(CLIENT.uploadVideo(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forVideoOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(CACHE.GetTimeCache("uploadVideo"));
        System.out.println(result);
    }
    
    private static void testUploadSong (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
//        CLIENT.uploadSong(dataKey, time, inputData);
        System.out.println(CLIENT.uploadSong(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forSongOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadSong"));
        System.out.println(result);
        String DateTimeKey = TimeKey.DateTimeKey("1080000000000000000", time);
        String LastestTimeKey = TimeKey.LastestTimeKey("1080000000000000000");
        int lastestTime = ZINGMP3.get(LastestTimeKey).value.entries.get(0).id;
        int dateTime = ZINGMP3.get(DateTimeKey).value.entries.get(0).id;
        System.out.println("lastestKey:  " +LastestTimeKey+"   lastestTime:  " +lastestTime);
        System.out.println("dateKey:  " +DateTimeKey+"   dateTime:  " +dateTime);
    }
    
    private static void testUploadPlaylist (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadPlaylist(dataKey, time, inputData);
        System.out.println(CLIENT.uploadPlaylist(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forPlaylistOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadPlaylist"));
        System.out.println(result);
    }
    
    private static void testUploadTrending (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadTrending(dataKey, time, inputData);
        System.out.println(CLIENT.uploadTrending(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forTrendingOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadTrending"));
        System.out.println(result);
    }
    
   
    private static void testUploadVietnameseSpecific (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadVietnameseSpecific(dataKey, time, inputData);
        System.out.println(CLIENT.uploadVietnameseSpecific(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forVietnameseSpecificOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadVietnameseSpecific"));
        System.out.println(result);
    }
    
    private static void testUploadTfIdf (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
//        CLIENT.uploadTfIdf(dataKey, time, inputData);
//        System.out.println(CLIENT.uploadTfIdf(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forTF_IDFOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadTfIdf"));
        System.out.println(result);
        String DateTimeKey = TimeKey.DateTimeKey("1210000000000000000", time);
        String LastestTimeKey = TimeKey.LastestTimeKey("1210000000000000000");
        int lastestTime = ZINGMP3.get(LastestTimeKey).value.entries.get(0).id;
        int dateTime = ZINGMP3.get(DateTimeKey).value.entries.get(0).id;
        System.out.println("lastestKey:  " +LastestTimeKey+"   lastestTime:  " +lastestTime);
        System.out.println("dateKey:  " +DateTimeKey+"   dateTime:  " +dateTime);
    }
    
    
    private static void testUploadForeignSpecific (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadForeignSpecific(dataKey, time, inputData);
        System.out.println(CLIENT.uploadForeignSpecific(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forForeignSpecificOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadForeignSpecific"));
        System.out.println(result);
    }
    
    private static void testUploadInstrumentSpecific (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadInstrumentSpecific(dataKey, time, inputData);
        System.out.println(CLIENT.uploadInstrumentSpecific(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forInstrumentSpecificOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadInstrumentSpecific"));
        System.out.println(result);
    }
    
    private static void testUploadAdsTrending (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
        CLIENT.uploadAdsTrending(dataKey, time, inputData);
        System.out.println(CLIENT.uploadAdsTrending(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forInstrumentSpecificOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadAdsTrending"));
        System.out.println(result);
    }
    
    private static void testuploadUserInterestAls (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
//        CLIENT.uploadUserInterestAls(dataKey, time, inputData);
        System.out.println(CLIENT.uploadUserInterestAls(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forUserInterestAlsOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadUserInterestAls"));
        System.out.println(result);
        String DateTimeKey = TimeKey.DateTimeKey("7150000000000000000", time);
        String LastestTimeKey = TimeKey.LastestTimeKey("7150000000000000000");
        int lastestTime = ZINGMP3.get(LastestTimeKey).value.entries.get(0).id;
        int dateTime = ZINGMP3.get(DateTimeKey).value.entries.get(0).id;
        System.out.println("lastestKey:  " +LastestTimeKey+"   lastestTime:  " +lastestTime);
        System.out.println("dateKey:  " +DateTimeKey+"   dateTime:  " +dateTime);
    }
    
    private static void testUploadUserInterestHistory (int dataKey, String time, TSuggestionSongList inputData) throws Exception {
//        CLIENT.uploadUserInterestHistory(dataKey, time, inputData);
        System.out.println(CLIENT.uploadUserInterestHistory(dataKey, time, inputData));
        int hour = Integer.parseInt(time.substring(8, 10));
        String genergateKey = String.valueOf(ZingMp3Key.forInstrumentSpecificOf(dataKey, hour));
        String result = ZINGMP3.get(genergateKey).toString();
        System.out.println(genergateKey);
        System.out.println(CACHE.GetTimeCache("uploadUserInterestHistory"));
        System.out.println(result);
    }
    
    public static void main(String[] args) throws Exception {
        TSuggestionSongList data = new TSuggestionSongList();
        Long datakey = 100l;
        int hour = 2019082703;
        int id = 100;
        int score = 24;
        data.addToSuggestList(genergateTSuggestionSong(id, score));
//        testUploadD00MediaListen(100l, 2019091115, data);
//        testUploadAdsTrending(datakey, 2019082703, data);
//        testUploadForeignSpecific(datakey, hour, data);
//        testUploadInstrumentSpecific(datakey, hour, data);
//        testUploadPairSong(datakey, 2019082707, data);
//        testUploadPlaylist(datakey, hour, data);
//        testUploadRefSong(datakey, 2019082705, data);
        testUploadSong(111, "2020010801", data);
//        testUploadTrending(datakey, 2019082614, data);
//        testUploadUserInterestHistory(datakey, hour, data);
//        testUploadVietnameseSpecific(datakey, 2019082704, data);
//        testUploadWebRef(datakey, 2019082614, data);
//        testUploadvideo(222l, 2019082614, data);
//        testUploadPairSong(111, "2019110205", data);
//        testUploadTfIdf(datakey, "2019091908", data);
//        String genergateKey = String.valueOf(ZingMp3Key.forSongOf(0155l, 3));
//        String result = ZINGMP3.get(genergateKey).toString();
//        System.out.println(CACHE.GetTimeCache("uploadTfIdf"));
//        System.out.println(result);
//        System.out.println(genergateKey);
//        System.out.println(OriginalKey.getOriginalKey(1000345678912311803l));
    }
}
