/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.handlers;

import com.vng.zalo.zte.rnd.dbingestionmw.admin.AdminModel;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserKeywords;
import com.vng.zalo.zte.rnd.dbingestionmw.cache.LastUpdateHourCache;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListExternalApps;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListExternalAppsUninstall;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListInternalApps;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListTrackedApp;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_DensityCellCheckinInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_DensityCellHomeInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.model.CacheHourModel;
import com.vng.zalo.zte.rnd.dbingestionmw.model.CustomAudienceModel;
import com.vng.zalo.zte.rnd.dbingestionmw.model.LocationMiningModel;
import com.vng.zalo.zte.rnd.dbingestionmw.model.ShopTrendingModel;
import com.vng.zalo.zte.rnd.dbingestionmw.model.UserInterestModel;
import com.vng.zalo.zte.rnd.dbingestionmw.model.ZingMp3Model;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMWP;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift.TSuggestionList2;
import com.vng.zalo.zte.rnd.dbingestionmw.zingmp3_suggestion.thrift.TSuggestionSongList;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_ForeignTripCount;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_Household;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_LivingLoc;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_TravelPath_City;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_TravelPath_POI;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_UserInterestLocation;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_WifiInfo;
import org.apache.thrift.TException;

/**
 * @date Dec 18, 2018
 * @author quydm
 */
public class DBMiddlewareHandler implements DBIngestionMWP.Iface {

    public static final LastUpdateHourCache cache = new LastUpdateHourCache("LastUpdateHourCache");

    @Override
    public int ping() throws TException {
        ThreadProfiler tp = Profiler.createThreadProfiler("DBIngestionMW.ping", false);
        tp.push(DBMiddlewareHandler.class, "ping");
        tp.pop(DBMiddlewareHandler.class, "ping");
        return 0; // succeeded
    }

    //----------------------------------------------------admin api---------------------------------------------------
    @Override
    public TActionResult addClientCaller(String adminKey, String callerName) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.addClientCaller", false);
        try {
            return AdminModel.INSTANCE.addClientCaller(adminKey, callerName);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult removeClientCaller(String adminKey, String callerName) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.removeClientCaller", false);
        try {
            return AdminModel.INSTANCE.removeClientCaller(adminKey, callerName);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public boolean validCaller(String adminKey, String callerName) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.validCaller", false);
        try {
            return AdminModel.INSTANCE.validCaller(adminKey, callerName);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }


//    -----------------------------------------------------------Zing Mp3 api------------------------------------------------------------    
    @Override
    public TActionResult uploadD00MediaListen(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadD00MediaListen", false);
        try {
            return ZingMp3Model.INSTANCE.uploadD00MediaListen(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadPairSong(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadPairSong", false);
        try {
            return ZingMp3Model.INSTANCE.uploadPairSong(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadRefSong(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadRefSong", false);
        try {
            return ZingMp3Model.INSTANCE.uploadRefSong(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadWebRef(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadWebRef", false);
        try {
            return ZingMp3Model.INSTANCE.uploadWebRef(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadVideo(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadVideo", false);
        try {
            return ZingMp3Model.INSTANCE.uploadVideo(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadSong(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadSong", false);
        try {
            return ZingMp3Model.INSTANCE.uploadSong(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadPlaylist(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadPlaylist", false);
        try {
            return ZingMp3Model.INSTANCE.uploadPlaylist(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadTrending(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadTrending", false);
        try {
            return ZingMp3Model.INSTANCE.uploadTrending(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadTfIdf(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadTfIdf", false);
        try {
            return ZingMp3Model.INSTANCE.uploadTfIdf(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadVietnameseSpecific(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadVietnameseSpecific", false);
        try {
            return ZingMp3Model.INSTANCE.uploadVietnameseSpecific(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadForeignSpecific(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadForeignSpecific", false);
        try {
            return ZingMp3Model.INSTANCE.uploadForeignSpecific(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadInstrumentSpecific(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadInstrumentSpecific", false);
        try {
            return ZingMp3Model.INSTANCE.uploadInstrumentSpecific(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadAdsTrending(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadAdsTrending", false);
        try {
            return ZingMp3Model.INSTANCE.uploadAdsTrending(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadUserInterestHistory(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserInterestHistory", false);
        try {
            return ZingMp3Model.INSTANCE.uploadUserInterestHistory(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadUserInterestAls(String callerName, int dataKey, String time, TSuggestionSongList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserInterestAls", false);
        try {
            return ZingMp3Model.INSTANCE.uploadUserInterestAls(callerName, dataKey, time, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

//--------------------------------------------CustomAudience api-----------------------------------   

    @Override
    public TActionResult ingestTrackedApps(String callerName, int dataKey, TListTrackedApp inputdata) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestTrackedApps", false);
        try {
            return CustomAudienceModel.INSTANCE.ingestTrackedApps(callerName, dataKey, inputdata);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestInternalApps(String callerName, int uid, TListInternalApps inputdata) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestInternalApps", false);
        try {
            return CustomAudienceModel.INSTANCE.ingestInternalApps(callerName, uid, inputdata);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestExternalApps(String callerName, int uid, TListExternalApps inputdata) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestExternalApps", false);
        try {
            return CustomAudienceModel.INSTANCE.ingestExternalApps(callerName, uid, inputdata);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestExternalAppsUninstall(String callerName, int uid, TListExternalAppsUninstall inputdata) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestExternalAppsUninstall", false);
        try {
            return CustomAudienceModel.INSTANCE.ingestExternalAppsUninstall(callerName, uid, inputdata);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }


    //--------------------------------------------------------cache api-----------------------------------------------------
    @Override
    public int getHourCache(String apiName) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.getHourCache", false);
        try {
            return CacheHourModel.INSTANCE.getHourCache(apiName);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    //--------------------------------------------------------Shop Trending API-----------------------------------------------------
    @Override
    public TActionResult ingestZOATrending(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestZOATrending(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestZOATfidf(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestZOATfidf(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestChannelViewContentOATrending(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestChannelViewContentOATrending(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestChannelViewContentContentTrending(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestChannelViewContentContentTrending(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTfidfMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTfidfMarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTfidfShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTfidfShopOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductClickTfidfMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductClickTfidfMarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductClickTfidfShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductClickTfidfShopOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingMarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingShopOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingPenalizedV2MarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingPenalizedV2MarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingDiscountMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingDiscountMarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingDiscountShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingDiscountShopOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductClickTrendingMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductClickTrendingMarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductClickTrendingShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductClickTrendingShopOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestShopTrendingMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestShopTrendingMarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestShopTrendingShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestShopTrendingShopOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingStoreCateMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingStoreCateMarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingStoreCateShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingStoreCateShopOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingInStoreCateMarketOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingInStoreCateMarketOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingInStoreCateShopOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingInStoreCateShopOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingProductTypeMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingProductTypeMarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingProductTypeShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingProductTypeShopOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingInShopMarketOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingInShopMarketOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingInShopShopOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingInShopShopOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingInProductTypeMarketOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingInProductTypeMarketOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestProductTrendingInProductTypeShopOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestProductTrendingInProductTypeShopOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestGlobalProductTrendingMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestGlobalProductTrendingMarketOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestGlobalProductTrendingShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestGlobalProductTrendingShopOnly(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestGlobalProductTrendingPenalizedMarketOnlyV2(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestGlobalProductTrendingPenalizedMarketOnlyV2(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestGlobalProductTrendingInStoreCateMarketOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestGlobalProductTrendingInStoreCateMarketOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestGlobalProductTrendingInStoreCateShopOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestGlobalProductTrendingInStoreCateShopOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestGlobalProductTrendingInProductTypeMarketOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestGlobalProductTrendingInProductTypeMarketOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestGlobalProductTrendingInProductTypeShopOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestGlobalProductTrendingInProductTypeShopOnly(callerName, dataKey1, dataKey2, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestStickerFromChatTrending(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestStickerFromChatTrending(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestStickerCateFromChatTrending(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestStickerCateFromChatTrending(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestOAShopTrust(String callerName, long dataKey, TSuggestionList2 inputData, String time) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShopTrending", false);
        try {
            return ShopTrendingModel.INSTANCE.ingestOAShopTrust(callerName, dataKey, inputData, time);
        } finally {
            Profiler.closeThreadProfiler();
        }

    }

    //------------------------------------------API LocationMining---------------------------------------------

    @Override
    public TActionResult ingestDensityCellCheckinInfo(String callerName, String dataKey, ZUDM_DensityCellCheckinInfo inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestDensityCellCheckinInfo", false);
        try {
            return LocationMiningModel.INSTANCE.ingestDensityCellCheckinInfo(callerName, dataKey, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestDensityCellHomeInfo(String callerName, String dataKey, ZUDM_DensityCellHomeInfo inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestDensityCellHomeInfo", false);
        try {
            return LocationMiningModel.INSTANCE.ingestDensityCellHomeInfo(callerName, dataKey, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestLivingProvince(String callerName, int zaloId, ZUDM_LivingLoc inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestLivingProvince", false);
        try {
            return LocationMiningModel.INSTANCE.uploadLivingProvince(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestTravelPath(String callerName, String dataKey, ZUDM_TravelPath_City inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestTravelPath", false);
        try {
            return LocationMiningModel.INSTANCE.uploadTravelPath(callerName, dataKey, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    

    @Override
    public TActionResult ingestForeignTripCount(String callerName, int dataKey, ZUDM_ForeignTripCount inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestForeignTripCount", false);
        try {
            return LocationMiningModel.INSTANCE.uploadForeignTripCount(callerName, dataKey, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestPOIPath(String callerName, String dataKey, ZUDM_TravelPath_POI inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestPOIPath", false);
        try {
            return LocationMiningModel.INSTANCE.uploadPOIpath(callerName, dataKey, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestSSIDInfo(String callerName, String dataKey, ZUDM_WifiInfo inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestSSIDInfo", false);
        try {
            return LocationMiningModel.INSTANCE.uploadSSIDInfo(callerName, dataKey, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestHousehold(String callerName, int zaloId, ZUDM_Household inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestHousehold", false);
        try {
            return LocationMiningModel.INSTANCE.uploadHousehold(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult IngestUserSSIDInterest(String callerName, int zaloId, ZUDM_UserInterestLocation inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.IngestUserSSIDInterest", false);
        try {
            return LocationMiningModel.INSTANCE.IngestUserSSIDInterest(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    //-------------------------------------UserInterest api---------------------------------

    @Override
    public TActionResult uploadUserKeywords(String callerName, int dataKey, int hour, TUserKeywords tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserKeywords", false);
        try {
            return UserInterestModel.INSTANCE.uploadUserKeywords(callerName, dataKey, hour, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    
    @Override
    public TActionResult uploadUserInterestByVectorSimilarity(String callerName, int dataKey, int hour, TUserKeywords tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserInterestByVectorSimilarity", false);
        try {
            return UserInterestModel.INSTANCE.uploadUserInterestByVectorSimilarity(callerName, dataKey, hour, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadUserInterestByAls(String callerName, int dataKey, int hour, TUserKeywords tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserInterestByAls", false);
        try {
            return UserInterestModel.INSTANCE.uploadUserInterestByAls(callerName, dataKey, hour, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
 
}