package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift.TSuggestionList2;
import com.vng.zing.logger.ZLogger;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zing.zudm.common.centralizedkeys.AllKeyTypeSets;
import com.vng.zing.zudm.common.centralizedkeys.ChannelKey;
import com.vng.zing.zudm.common.centralizedkeys.ShopKey;
import com.vng.zing.zudm.common.centralizedkeys.StickerKey;
import com.vng.zing.zudm.common.centralizedkeys.ZOAKey;
import org.apache.log4j.Logger;

/**
 *
 * @author tinhdt
 */
public class ShopTrendingModel extends BaseModel{
    private static final Class THIS_CLASS = ShopTrendingModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final ShopTrendingModel INSTANCE = new ShopTrendingModel();
    private final static AllKeyTypeSets.StrList32ScoreSuggestion KEY = new AllKeyTypeSets.StrList32ScoreSuggestion();

    public TActionResult ingestZOATrending(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestZOATrending", dataKey, time,
                (x,y) -> ZOAKey.forZOATrendingOf(x,y), 
                inputData, callerName, String.valueOf(KEY.ZOA_TRENDING), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestZOATfidf(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestZOATfidf", dataKey, time,
                (x,y) -> ZOAKey.forZOATFIDFOf(x,y), 
                inputData, callerName,  String.valueOf(KEY.ZOA_TFIDF),
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestChannelViewContentOATrending(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestChannelViewContentOATrending", dataKey, time,
                (x,y) -> ChannelKey.forChannelViewContentOATrendingOf(x,y), 
                inputData, callerName, String.valueOf(KEY.CHANNEL_VIEW_CONTENT_OA_TRENDING), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestChannelViewContentContentTrending(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestChannelViewContentContentTrending", dataKey, time, 
                (x,y) -> ChannelKey.forChannelViewContentContentTrendingOf(x,y), 
                inputData, callerName, String.valueOf(KEY.CHANNEL_VIEW_CONTENT_CONTENT_TRENDING), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTfidfMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTfidfMarketOnly", dataKey, time,
                (x,y) -> ShopKey.forProductTFIDFMarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TFIDF_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTfidfShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTfidfShopOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTFIDFShopOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TFIDF_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductClickTfidfMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductClickTfidfMarketOnly", dataKey, time,
                (x,y) -> ShopKey.forProductClickTFIDFMarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_CLICK_TFIDF_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductClickTfidfShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductClickTfidfShopOnly", dataKey, time,
                (x,y) -> ShopKey.forProductClickTFIDFShopOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_CLICK_TFIDF_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingMarketOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTrendingMarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingShopOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTrendingShopOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingPenalizedV2MarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingPenalizedV2MarketOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTrendingPenalizedV2MarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_PENALIZED_V2_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingDiscountMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingDiscountMarketOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTrendingDiscountMarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_DISCOUNT_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingDiscountShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingDiscountShopOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTrendingDiscountShopOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_DISCOUNT_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductClickTrendingMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductClickTrendingMarketOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductClickTrendingMarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_CLICK_TRENDING_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductClickTrendingShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductClickTrendingShopOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductClickTrendingShopOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_CLICK_TRENDING_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestShopTrendingMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestShopTrendingMarketOnly", dataKey, time, 
                (x,y) -> ShopKey.forShopTrendingMarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.SHOP_TRENDING_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestShopTrendingShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestShopTrendingShopOnly", dataKey, time, 
                (x,y) -> ShopKey.forShopTrendingShopOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.SHOP_TRENDING_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingStoreCateMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTfidfMarketOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTrendingStoreCateMarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_STORE_CATE_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingStoreCateShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingStoreCateShopOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTrendingStoreCateShopOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_STORE_CATE_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingInStoreCateMarketOnly(String callerName, long dataKey1, long dataKey2,  TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingInStoreCateMarketOnly", dataKey1, dataKey2, time, 
                (x,y,z) -> ShopKey.forProductTrendingInStoreCateMarketOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_IN_STORE_CATE_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingInStoreCateShopOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingInStoreCateShopOnly", dataKey1, dataKey2, time, 
                (x,y,z) -> ShopKey.forProductTrendingInStoreCateShopOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_IN_STORE_CATE_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingProductTypeMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingProductTypeMarketOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTrendingProductTypeMarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_PRODUCT_TYPE_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingProductTypeShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingProductTypeShopOnly", dataKey, time, 
                (x,y) -> ShopKey.forProductTrendingProductTypeShopOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_PRODUCT_TYPE_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingInShopMarketOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingInShopMarketOnly", dataKey1, dataKey2, time,
                (x,y,z) -> ShopKey.forProductTrendingInShopMarketOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_IN_SHOP_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingInShopShopOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingInShopShopOnly", dataKey1, dataKey2, time, 
                (x,y,z) -> ShopKey.forProductTrendingInShopShopOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_IN_SHOP_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingInProductTypeMarketOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingInProductTypeMarketOnly", dataKey1, dataKey2, time, 
                (x,y,z) -> ShopKey.forProductTrendingInProductTypeMarketOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_IN_PRODUCT_TYPE_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestProductTrendingInProductTypeShopOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestProductTrendingInProductTypeShopOnly", dataKey1, dataKey2, time, 
                (x,y,z) -> ShopKey.forProductTrendingInProductTypeShopOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.PRODUCT_TRENDING_IN_PRODUCT_TYPE_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestGlobalProductTrendingMarketOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestGlobalProductTrendingMarketOnly", dataKey, time, 
                (x,y) -> ShopKey.forGlobalProductTrendingMarketOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.GLOBAL_PRODUCT_TRENDING_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestGlobalProductTrendingShopOnly(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestGlobalProductTrendingShopOnly", dataKey, time, 
                (x,y) -> ShopKey.forGlobalProductTrendingShopOnlyOf(x,y), 
                inputData, callerName, String.valueOf(KEY.GLOBAL_PRODUCT_TRENDING_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestGlobalProductTrendingPenalizedMarketOnlyV2(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestGlobalProductTrendingPenalizedMarketOnlyV2", dataKey, time, 
                (x,y) -> ShopKey.forGlobalProductTrendingPenalizedMarketOnlyV2Of(x,y), 
                inputData, callerName, String.valueOf(KEY.GLOBAL_PRODUCT_TRENDING_PENALIZED_MARKET_ONLY_V2), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestGlobalProductTrendingInStoreCateMarketOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestGlobalProductTrendingInStoreCateMarketOnly", dataKey1, dataKey2, time, 
                (x,y,z) -> ShopKey.forGlobalProductTrendingInStoreCateMarketOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.GLOBAL_PRODUCT_TRENDING_IN_STORE_CATE_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestGlobalProductTrendingInStoreCateShopOnly(String callerName, long dataKey1, long dataKey2 , TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestGlobalProductTrendingInStoreCateShopOnly", dataKey1, dataKey2, time, 
                (x,y,z) -> ShopKey.forGlobalProductTrendingInStoreCateShopOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.GLOBAL_PRODUCT_TRENDING_IN_STORE_CATE_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestGlobalProductTrendingInProductTypeMarketOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestGlobalProductTrendingInProductTypeMarketOnly", dataKey1, dataKey2, time, 
                (x,y,z) -> ShopKey.forGlobalProductTrendingInProductTypeMarketOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.GLOBAL_PRODUCT_TRENDING_IN_PRODUCT_TYPE_MARKET_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestGlobalProductTrendingInProductTypeShopOnly(String callerName, long dataKey1, long dataKey2, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestGlobalProductTrendingInProductTypeShopOnly", dataKey1, dataKey2, time, 
                (x,y,z) -> ShopKey.forGlobalProductTrendingInProductTypeShopOnlyOf(x,y,z), 
                inputData, callerName, String.valueOf(KEY.GLOBAL_PRODUCT_TRENDING_IN_PRODUCT_TYPE_SHOP_ONLY), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestStickerFromChatTrending(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestStickerFromChatTrending", dataKey, time, 
                (x,y) -> StickerKey.forStickerFromChattrendingOf(x,y), 
                inputData, callerName,  String.valueOf(KEY.STICKER_FROM_CHAT_TRENDING), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestStickerCateFromChatTrending(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestStickerCateFromChatTrending", dataKey, time, 
                (x,y) -> StickerKey.forStickerCateChattrendingOf(x,y), 
                inputData, callerName, String.valueOf(KEY.STICKER_CATE_FROM_CHAT_TRENDING), 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestOAShopTrust(String callerName, long dataKey, TSuggestionList2 inputData, String time){
        return ingest(DBClients.SHOP_TRENDING, "ingestOAShopTrust", dataKey, time, 
                (x,y) -> ShopKey.forOAShopTrustOf(x,y), 
                inputData, callerName, String.valueOf(KEY.OA_SHOP_TRUST), 
                THIS_CLASS, LOGGER);
    }
}
