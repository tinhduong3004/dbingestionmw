/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.admin.AdminModel;
import static com.vng.zalo.zte.rnd.dbingestionmw.handlers.DBMiddlewareHandler.*;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift.TSuggestion;
import com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift.TSuggestionList2;
import com.vng.zalo.zte.rnd.dbingestionmw.zingmp3_suggestion.thrift.Hour;
import com.vng.zalo.zte.rnd.dbingestionmw.zingmp3_suggestion.thrift.TSuggestionSong;
import com.vng.zalo.zte.rnd.dbingestionmw.zingmp3_suggestion.thrift.TSuggestionSongList;
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.common.ZErrorDef;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.common.ZUtil;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.strlist32score.thrift.TValue;
import com.vng.zing.strlist32score.thrift.TValueEntry;
import com.vng.zing.strlist32score.thrift.wrapper.StrList32ScoreClient;
import com.vng.zing.zcommon.thrift.PutPolicy;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.ZUDM_Util;
import com.vng.zing.zudm.common.centralizedkeys.TimeKey;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.apache.thrift.TBase;
import com.vng.zing.alert_bot.thrift.ZUDM_AlertResult;
import com.vng.zing.alert_bot.thrift.wrapper.ZUDM_AlertBotThriftClient;

/**
 * @date Jul 15, 2019
 * @author quydm
 */
public class BaseModel {
    public static ZUDM_AlertBotThriftClient alertBotClient = new ZUDM_AlertBotThriftClient("alertbotclient");
     
    /**
     * A handler function to run all the step of ingestion process
     *  1. Validate the source
     *  2. Put data to db
     *  3. Push event bus (if step 2 is succeed)
     * @param <T>
     * @param client
     * @param apiName
     * @param src
     * @param globalId
     * @param keyMaker
     * @param inputData
     * @param clazz
     * @param logger
     * @return Error enumerate
     */
    
    //For ZiDB64Client (CustomAudienceModel, LocationMiningModel)
    public <T extends TBase> TActionResult ingest(
            ZiDB64Client client,String apiName,String src, // config info
            int globalId,Function<Integer,Long> keyMaker, T inputData, // key-value
            Class clazz,Logger logger
            ){ // eventbus
        
        if ( AdminModel.INSTANCE.validCaller(src) ){
            long error = WithCompression.putTo(client, 
                    keyMaker.apply(globalId), inputData, logger, clazz); // return error code
            if ( ZErrorHelper.isSuccess(error)) { // ingestion is successful
                countSuccessFor(clazz,apiName);
                logSuccessData(logger, apiName, keyMaker.apply(globalId), inputData);
                return TActionResult.SUCCESS;
            }else{
                sendAlertBotMsgForError(src, apiName, keyMaker.apply(globalId).toString());
                countErrorFor(clazz,apiName);
                return TActionResult.ERROR;
            }
        }else{ 
            sendAlertBotMsgForWrongAuth(src, apiName, keyMaker.apply(globalId).toString());
            countWrongAuthenFor(clazz,apiName);
            logWrongAuthenSrc(logger,apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }
    
    //ingest to ZiDBClient (LocationMiningModel)
    public <T extends TBase> TActionResult ingest(
            ZiDBClient client,String apiName,String src, // config info
            String globalId, T inputData, // key-value
            Class clazz,Logger logger){ // eventbus
        if (checkValidCaller(src)) {
            try {
                ByteBuffer key = ByteBufferUtil.fromString(globalId);
                com.vng.zing.zidb.thrift.TValue tvalue = ZUDM_Util.createZiDBTValueFromTBase(inputData);
                long error = client.put(key, tvalue, PutPolicy.ADD_OR_UDP);
                
                if (ZErrorHelper.isSuccess(error)) {
                    countSuccessFor(clazz, apiName);
                    return TActionResult.SUCCESS;
                } else {
                    sendAlertBotMsgForError(src, apiName, globalId);
                    countErrorFor(clazz, apiName);
                    return TActionResult.ERROR;
                }
            } catch (Exception ex) {
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            sendAlertBotMsgForWrongAuth(src, apiName, globalId);
            countWrongAuthenFor(clazz,apiName);
            logWrongAuthenSrc(logger,apiName, src);
            return TActionResult.WRONGAUTH;
        }  
    }
    
    //For StrList32ScoreClient with BiFunction (ShopTrendingModel)
    public <T extends TBase> TActionResult ingest(
            StrList32ScoreClient client,String apiName, // config info
            long globalId, String time, BiFunction<Long, Integer, Long> keyMaker, TSuggestionList2 inputData, String src,String DBKey, // key-value
            Class clazz,Logger logger) { // eventbus 
        
        int hour = Integer.parseInt(time.substring(8, 10));
        String DateTimeKey = TimeKey.DateTimeKey(DBKey, time);
        String LastestTimeKey = TimeKey.LastestTimeKey(DBKey);
        
        if ( AdminModel.INSTANCE.validCaller(src) ){
            if (Integer.parseInt(time) != cache.get(apiName)){
                
                cache.put(apiName,Integer.parseInt(time));
                TValueEntry dateEntry = new TValueEntry();
                dateEntry.setId(Integer.parseInt(time));
                TValue dateValue = new TValue();
                dateValue.setEntries(new ArrayList<>());
                dateValue.entries.add(dateEntry);

                long putSttDateTime = client.put(DateTimeKey, dateValue, PutPolicy.ADD_OR_UDP);//checkpoint for the lastest 24 hour with each API
                long putSttLatest = 0;
                try {
                    int recentDateTime = client.get(LastestTimeKey).value.entries.get(0).id;
                    if (Integer.parseInt(time) >= recentDateTime) {
                        putSttLatest = client.put(LastestTimeKey, dateValue, PutPolicy.ADD_OR_UDP);//checkpoint for the lastest hour with each API
                    }
                } catch (NumberFormatException e) { 
                    putSttLatest = client.put(LastestTimeKey, dateValue, PutPolicy.ADD_OR_UDP);
                }
                if (ZErrorHelper.isFail(putSttDateTime) || ZErrorHelper.isFail(putSttLatest)) {
                    sendAlertBotMsgForError(src, apiName, keyMaker.apply(globalId, hour).toString());
                    countErrorFor(clazz,apiName);
                    return TActionResult.ERROR;
                }
            }
            TValue value = buildValue(inputData);
            long error = client.put(String.valueOf(keyMaker.apply(globalId, hour)), value, PutPolicy.ADD_OR_UDP);
            String errString = ZErrorHelper.errorToString(error);
            long waitingStt = waitForDBTaskTurn(client, client.getEnqueSeq(), apiName, clazz);
                if (ZErrorHelper.isFail(error) || ZErrorHelper.isFail(waitingStt)) {
                    sendAlertBotMsgForError(src, apiName, keyMaker.apply(globalId, hour).toString());
                    System.out.println("put FAILED!!!!!!!!!!!!!!\nPut Error String: " + errString);
                    countErrorFor(clazz,apiName);
                    return TActionResult.ERROR;
                }
            countSuccessFor(clazz,apiName);
            return TActionResult.SUCCESS;
        }else{ 
            sendAlertBotMsgForWrongAuth(src, apiName, keyMaker.apply(globalId, hour).toString());
            countWrongAuthenFor(clazz,apiName);
            logWrongAuthenSrc(logger,apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }

    
    //For StrList32ScoreClient with TriFunction (ShopTrendingModel)
    public <T extends TBase> TActionResult ingest(
            StrList32ScoreClient client,String apiName, // config info
            long id1,long id2,  String time, TriFunction<Long, Long, Integer, String> keyMaker, TSuggestionList2 inputData, String src,String DBKey, // key-value
            Class clazz,Logger logger) { // eventbus 
        
        int hour = Integer.parseInt(time.substring(8, 10));
        String DateTimeKey = TimeKey.DateTimeKey(DBKey, time);
        String LastestTimeKey = TimeKey.LastestTimeKey(DBKey);
        
        if ( AdminModel.INSTANCE.validCaller(src) ){
            if (Integer.parseInt(time) != cache.get(apiName)){
                
                cache.put(apiName,Integer.parseInt(time));
                TValueEntry dateEntry = new TValueEntry();
                dateEntry.setId(Integer.parseInt(time));
                TValue dateValue = new TValue();
                dateValue.setEntries(new ArrayList<>());
                dateValue.entries.add(dateEntry);

                long putSttDateTime = client.put(DateTimeKey, dateValue, PutPolicy.ADD_OR_UDP); //checkpoint for the lastest 24 hour with each API
                long putSttLatest = 0;
                try {
                    int recentDateTime = client.get(LastestTimeKey).value.entries.get(0).id;
                    if (Integer.parseInt(time) >= recentDateTime) {
                        putSttLatest = client.put(LastestTimeKey, dateValue, PutPolicy.ADD_OR_UDP);//checkpoint for the lastest hour with each API
                    }
                } catch (Exception e) { 
                    putSttLatest = client.put(LastestTimeKey, dateValue, PutPolicy.ADD_OR_UDP);
                }
                if (ZErrorHelper.isFail(putSttDateTime) || ZErrorHelper.isFail(putSttLatest)) {
                    sendAlertBotMsgForError(src, apiName, keyMaker.apply(id1, id2, hour));
                    countErrorFor(clazz,apiName);
                    return TActionResult.ERROR;
                }
            }
            TValue value = buildValue(inputData);
            long error = client.put(keyMaker.apply(id1, id2, hour), value, PutPolicy.ADD_OR_UDP);
            long waitingStt = waitForDBTaskTurn(client, client.getEnqueSeq(), apiName, clazz);
                if (ZErrorHelper.isFail(error) || ZErrorHelper.isFail(waitingStt)) {
                    sendAlertBotMsgForError(src, apiName, keyMaker.apply(id1, id2, hour));
                    countErrorFor(clazz,apiName);
                    return TActionResult.ERROR;
                }
            countSuccessFor(clazz, apiName);
            return TActionResult.SUCCESS;
        }else{ 
            sendAlertBotMsgForWrongAuth(src, apiName, keyMaker.apply(id1, id2, hour));
            countWrongAuthenFor(clazz,apiName);
            logWrongAuthenSrc(logger,apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }
    
    private long waitForDBTaskTurn(StrList32ScoreClient client, long lastSequence, String apiName, Class clazz ) {
        int count = 0;
        ThreadProfiler profiler = Profiler.getThreadProfiler();
        while (client.getDequeSeq() < lastSequence) {
            ZUtil.sleep(100);
            profiler.push(clazz, "waitFor " + apiName + " turn");
            profiler.pop(clazz, "waitFor " + apiName + " turn");
            if (++count >= 1200) {
                break;
            }
        }
        if (client.getDequeSeq() < lastSequence) {
            return ZErrorDef.FAIL;
        } else {
            return ZErrorDef.SUCCESS;
        }
    }
    
    private TValue buildValue(TSuggestionList2 tList) {
        TValue tValue = new TValue();

        tValue.setEntries(tList.suggestList
                .stream()
                .filter((TSuggestion t) -> {
                    return t.getProductId() > 0;
                })
                .limit(200)
                .map((TSuggestion t) -> {
                    TValueEntry entry = new TValueEntry();
                    entry.setId(t.getProductId());
                    entry.setScore(t.getScore());
                    return entry;
                })
                .collect(Collectors.toList()));
        tValue.setUpdatedDttm(tList.getHour());
        return tValue;
    }
    
    //For ZiDB64Client with profiler with log (LocationModel)
    public <T extends TBase> TActionResult ingestWithProfilerWithLog(
            ZiDB64Client client,String apiName,String src, // config info
            int globalId,Function<Integer,Long> keyMaker, T inputData, // key-value
            Class clazz,Logger logger
            ){ // eventbus
        
        if ( AdminModel.INSTANCE.validCaller(src) ){
            long error = ZUDM_Util.putWithProfilerWithLog(logger, clazz, apiName, client, keyMaker.apply(globalId), inputData, PutPolicy.ADD_OR_UDP); // return error code
            if ( ZErrorHelper.isSuccess(error)) { // ingestion is successful
                countSuccessFor(clazz,apiName);
                logSuccessData(logger, apiName, keyMaker.apply(globalId), inputData);
                return TActionResult.SUCCESS;
            }else{
                sendAlertBotMsgForError(src, apiName, keyMaker.apply(globalId).toString());
                countErrorFor(clazz,apiName);
                return TActionResult.ERROR;
            }
        }else{ 
            sendAlertBotMsgForWrongAuth(src, apiName, keyMaker.apply(globalId).toString());
            countWrongAuthenFor(clazz,apiName);
            logWrongAuthenSrc(logger,apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }
    
    // ingest to StrList32ScoreClient with hourlyKey (ZingMP3Model)
    public <T extends TBase> TActionResult ingestWithHour(
            StrList32ScoreClient client,String apiName,String src,String DBKey, // config info
            int globalId, String time, BiFunction<Integer, Integer, Long> keyMaker, TSuggestionSongList inputData, // key-value
            Class clazz,Logger logger){ // eventbus
        int hour = Integer.parseInt(time.substring(8, 10));
        String DateTimeKey = TimeKey.DateTimeKey(DBKey, time);
        String LastestTimeKey = TimeKey.LastestTimeKey(DBKey);
        
        if ( AdminModel.INSTANCE.validCaller(src) == true) { 
            if (Integer.parseInt(time) != cache.get(apiName)){
                
                cache.put(apiName,Integer.parseInt(time));
                TValueEntry dateEntry = new TValueEntry();
                dateEntry.setId(Integer.parseInt(time));
                TValue dateValue = new TValue();
                dateValue.setEntries(new ArrayList<>());
                dateValue.entries.add(dateEntry);

                long putSttDateTime = client.put(DateTimeKey, dateValue, PutPolicy.ADD_OR_UDP); //checkpoint for the lastest 24 hour with each API
                long putSttLatest = 0;
                try {
                    int recentDateTime = client.get(LastestTimeKey).value.entries.get(0).id;
                    if (Integer.parseInt(time) >= recentDateTime) {
                        putSttLatest = client.put(LastestTimeKey, dateValue, PutPolicy.ADD_OR_UDP);//checkpoint for the lastest hour with each API
                    }
                } catch (Exception e) { 
                    putSttLatest = client.put(LastestTimeKey, dateValue, PutPolicy.ADD_OR_UDP);
                }
                if (ZErrorHelper.isFail(putSttDateTime) || ZErrorHelper.isFail(putSttLatest)) {
                    return TActionResult.ERROR;
                }
            }
            TValue tvalue = makeTValue(inputData);
            long error = client.put(keyMaker.apply(globalId, hour).toString(), tvalue, PutPolicy.ADD_OR_UDP);
            if ( ZErrorHelper.isSuccess(error)) { // ingestion is successful
                countSuccessFor(clazz,apiName);
                logSuccessData(logger, apiName, keyMaker.apply(globalId, hour), inputData);
                return TActionResult.SUCCESS;
            }else{
                sendAlertBotMsgForError(src, apiName, keyMaker.apply(globalId, hour).toString());
                countErrorFor(clazz,apiName);
                return TActionResult.ERROR;
            }
        }else{ 
            sendAlertBotMsgForWrongAuth(src, apiName, keyMaker.apply(globalId, hour).toString());
            countWrongAuthenFor(clazz,apiName);
            logWrongAuthenSrc(logger,apiName, src);
            return TActionResult.WRONGAUTH;
        }
        
    }
    
    // ingest to ZiDB64Client with hourlyKey (UserInterestModel)
    public <T extends TBase> TActionResult ingestWithHour(
            ZiDB64Client client,String apiName,String src, // config info
            int globalId, int hour, BiFunction<Integer, Integer, Long> keyMaker, T inputData,// key-value
            Class clazz,Logger logger){ // eventbus
        if ( AdminModel.INSTANCE.validCaller(src) == true && hour == Hour.findByValue(hour).getValue()){
            long error = WithCompression.putTo(client, 
                    keyMaker.apply(globalId, hour), inputData, logger, clazz); // return error code

            if ( ZErrorHelper.isSuccess(error)) { // ingestion is successful
                countSuccessFor(clazz,apiName);
                logSuccessData(logger, apiName, keyMaker.apply(globalId,hour), inputData);
                return TActionResult.SUCCESS;
            }else{
                sendAlertBotMsgForError(src, apiName, keyMaker.apply(globalId, hour).toString());
                countErrorFor(clazz,apiName);
                return TActionResult.ERROR;
            }

        }else{ 
            sendAlertBotMsgForWrongAuth(src, apiName, keyMaker.apply(globalId, hour).toString());
            countWrongAuthenFor(clazz,apiName);
            logWrongAuthenSrc(logger,apiName, src);
            return TActionResult.WRONGAUTH;
        }

    }
    
    public static boolean isNormalGlobalId(long globalId){
        return globalId != 9000000000000000000L; // this is at the dump global used for QoS service
    }
    
    public static void countWrongAuthenFor(Class clazz, String apiName){
        ThreadProfiler tp = com.vng.zing.stats.Profiler.getThreadProfiler();
        tp.push(clazz, apiName+".WRONGAUTH");
        tp.pop(clazz, apiName+".WRONGAUTH");
    }
    
    public static void countSuccessFor(Class clazz,String apiName){
        ThreadProfiler tp = com.vng.zing.stats.Profiler.getThreadProfiler();
        tp.push(clazz, apiName+".SUCCESS");
        tp.pop(clazz, apiName+".SUCCESS");
    }
    
    public static void countErrorFor(Class clazz,String apiName){
        ThreadProfiler tp = com.vng.zing.stats.Profiler.getThreadProfiler();
        tp.push(clazz, apiName+".ERROR");
        tp.pop(clazz, apiName+".ERROR");
    }
    
    public static void countEventBusFor(Class clazz,String apiName){
        ThreadProfiler tp = com.vng.zing.stats.Profiler.getThreadProfiler();
        tp.push(clazz, apiName+".EVENTBUS");
        tp.pop(clazz, apiName+".EVENTBUS");
    }
    
    public static void logWrongAuthenSrc(Logger logger,String apiName,String src){
        logger.error("Wrong Authen "+apiName+" with token "+src);
    }
    
    public static <T extends TBase> void logSuccessData(Logger logger,String apiName,long dataKey, T data){
        logger.info(apiName+"\tkey: "+dataKey+"\tvalue: "+data.toString());
    }
    
    @FunctionalInterface //Declare TriFunction
    interface TriFunction<One, Two, Three, Four> {
        public Four apply(One one, Two two, Three three);
    }
    
    public TValueEntry toTValueEntry(TSuggestionSong x) {
        TValueEntry valueEntry = new TValueEntry();
        valueEntry.setId(x.getId());
        valueEntry.setScore(x.getCount());
        return valueEntry;
    }

    public TValue makeTValue(TSuggestionSongList listSuggestion) {
        TValue tvalue = new TValue();
        tvalue.setEntries(listSuggestion.suggestList
                .stream()
                .filter(x -> x.getId() > 0)
                .limit(100)
                .map(x -> toTValueEntry(x))
                .collect(Collectors.toList()));
        tvalue.setUpdatedDttm(System.currentTimeMillis());
        return tvalue;
    }
    
    boolean checkValidCaller(String caller) {
        return AdminModel.INSTANCE.validCaller(caller);
    }
    
    public ZUDM_AlertResult sendAlertBotMsgForError(String projectName, String apiName, String dataKey) {
        ZUDM_AlertResult result = alertBotClient.sendAllMemberInProject(projectName, "ZUDM_DBIngestionMW \napiName: " +apiName + " , dataKey: " +dataKey + " \nreason: ERROR");
        return result;
    }
    
    public ZUDM_AlertResult sendAlertBotMsgForWrongAuth(String projectName, String apiName, String dataKey) {
        ZUDM_AlertResult result = alertBotClient.sendAllMemberInProject(projectName, "ZUDM_DBIngestionMW \napiName: " +apiName + " , dataKey: " +dataKey + " \nreason: WRONGAUTH");
        return result;
    }
    
}