package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TKeywordInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserKeywords;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zing.zudm.common.centralizedkeys.UserInterestKey;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
/**
 *
 * @author tinhdt
 */
public class UserInterestModel extends BaseModel{
     private static final Class THIS_CLASS = UserInterestModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final UserInterestModel INSTANCE = new UserInterestModel();
    
    
    public List<TKeywordInfo> limit50(List<TKeywordInfo> keywordList){
        return keywordList.stream().limit(50).collect(Collectors.toList());
    }
    
    public TActionResult uploadUserKeywords(String authenToken, int zaloId,int hour, TUserKeywords inputData){
        inputData.setKeywords(limit50(inputData.getKeywords())); // add limit 50 'cause overheating db
        return ingestWithHour(DBClients.UserKWDB, "uploadUserKeywords", authenToken, zaloId, hour, 
                (x,y)->UserInterestKey.forUserKeyworksOf(x,y), inputData, 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult uploadUserInterestByVectorSimilarity(String authenToken, int dataKey,int hour, TUserKeywords inputData){
        inputData.setKeywords(limit50(inputData.getKeywords())); // add limit 50 'cause overheating db
        return ingestWithHour(DBClients.UserKWDB, "uploadUserInterestByVectorSimilarity", authenToken, dataKey, hour, 
                (x,y)->UserInterestKey.forUserInterestByVectorSimilarityOf(x,y), inputData, THIS_CLASS, LOGGER);
    }
    
    public TActionResult uploadUserInterestByAls(String authenToken, int dataKey,int hour, TUserKeywords inputData){
        inputData.setKeywords(limit50(inputData.getKeywords())); // add limit 50 'cause overheating db
        return ingestWithHour(DBClients.UserKWDB, "uploadUserInterestByAls", authenToken, dataKey, hour, 
                (x,y)->UserInterestKey.forUserInterestByAlsOf(x,y), inputData, THIS_CLASS, LOGGER);
    }
}