/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.admin;

import com.vng.zalo.zte.rnd.dbingestionmw.client.AdminDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.configer.ZConfig;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.storage.PropertiesTable;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;


/**
 * @date Dec 18, 2018
 * @author quydm
 */
public class AdminModel {
    private final Class THIS_CLASS = AdminModel.class;
    private final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    private final String _ADMIN_KEY = ZConfig.Instance.getString(THIS_CLASS, "main", "adminKey", "677209a70fdee83e1b05fd40bd14a409");
    private final PropertiesTable CALLER_TABLE = new PropertiesTable("DBIngestionMWService");
    
    public AdminModel(){
        CALLER_TABLE.load();
    }
    
    public static final AdminModel INSTANCE = new AdminModel();
    
    
    private enum Authentication {
        NOT_EXIST(0), ALLOW(1);
        private final int value;
        private Authentication(int value) {
            this.value = value;
        }
    }
    
    public TActionResult addClientCaller(String adminKey, String callerName) {
        TActionResult result = validateAdminKey(adminKey);
        ThreadProfiler tp = Profiler.getThreadProfiler();
        switch (result) {
            case SUCCESS:
                tp.push(THIS_CLASS, "syn_addSrc.Admin");
                CALLER_TABLE.setInt(callerName, Authentication.ALLOW.value, true);
                tp.pop(THIS_CLASS, "syn_addSrc.Admin");
                if ( validCaller(callerName) ){
                    return TActionResult.SUCCESS;
                }else{
                    return TActionResult.ERROR;
                }
            case WRONGAUTH:
                tp.push(THIS_CLASS, "syn_addSrc.WrAuth");
                tp.pop(THIS_CLASS, "syn_addSrc.WrAuth");
                break;
        }
        return result;
    }
    
    private TActionResult validateAdminKey(String adminKey) {
        String keyBuilt = generateToken(adminKey);
        if (!keyBuilt.equals(_ADMIN_KEY)) {
            LOGGER.error("Wrong admin key \"" + adminKey + "\"");
            return TActionResult.WRONGAUTH;
        }else{
            return TActionResult.SUCCESS;
        }
    }
    
    public TActionResult removeClientCaller(String adminKey, String callerName) {
        int   authenValue = CALLER_TABLE.getInt(callerName, Authentication.NOT_EXIST.value);
        if ( authenValue == Authentication.ALLOW.value ){
            CALLER_TABLE.setInt(callerName, Authentication.NOT_EXIST.value, true);
            return TActionResult.SUCCESS;
        }else{
            return TActionResult.NOT_EXIST;
        }
    }
    
    private static String generateToken(String pass) {
        return DigestUtils.md5Hex(pass + "_RGWhTaIb0u");
    }
    public boolean validCaller(String adminKey,String callerName) {
        TActionResult result = validateAdminKey(adminKey);
        switch (result) {
            case SUCCESS:
                return validCaller(callerName);
            case WRONGAUTH:
                return false;
        }
        return false;
    }
    public boolean validCaller(String callerName) {
        int errorCode = CALLER_TABLE.getInt(callerName, Authentication.NOT_EXIST.value);
//        LOGGER.info("Validate caller: "+callerName+"\t"+errorCode);
        return errorCode == Authentication.ALLOW.value;
    }
    public static void main(String[] args) throws Exception {
        String adminKey = "zalo.rnd@2019";
        String callerName = "Location@Mining";
        AdminDBMWClient client = new AdminDBMWClient("10.50.9.16",18782); 
//        AdminDBMWClient client = new AdminDBMWClient("127.0.0.1",18782); 
        client.addClientCaller(adminKey,callerName);
        TActionResult ac = client.addClientCaller(adminKey,callerName );
        System.out.println(ac.toString());
//        System.out.println(client.validCaller(adminKey, callerName));
        
    }
}
