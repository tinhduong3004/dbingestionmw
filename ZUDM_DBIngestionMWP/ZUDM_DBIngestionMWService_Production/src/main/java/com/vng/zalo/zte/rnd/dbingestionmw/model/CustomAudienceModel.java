/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListExternalApps;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListExternalAppsUninstall;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListInternalApps;
import com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift.TListTrackedApp;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zudm.common.centralizedkeys.CustomAudienceKey;
import org.apache.log4j.Logger;

/**
 *forCountedAppsOf
 * @author tinhdt
 */

public class CustomAudienceModel extends BaseModel{

    private static final Class THIS_CLASS = CustomAudienceModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final CustomAudienceModel INSTANCE = new CustomAudienceModel();
    
    public TActionResult ingestTrackedApps(String src, int zaloId, TListTrackedApp inputData){
        return ingest(DBClients.USEDAPP_DB, "ingestUsedApps", 
                src, zaloId,x-> CustomAudienceKey.forUsedAppsOf(x), inputData, 
                THIS_CLASS,LOGGER);
    } 
    
    public TActionResult ingestInternalApps(String src, int zaloId, TListInternalApps inputData){
        return ingest(DBClients.USEDAPP_DB, "ingestInternalApps", 
                src, zaloId,x-> CustomAudienceKey.forCountedInternalAppsOf(x), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestExternalApps(String src, int zaloId, TListExternalApps inputData){
        return ingest(DBClients.USEDAPP_DB, "ingestExternalApps", 
                src, zaloId,x-> CustomAudienceKey.forCountedExternalAppsOf(x), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestExternalAppsUninstall (String src, int zaloId, TListExternalAppsUninstall inputData){
        return ingest(DBClients.USEDAPP_DB, "ingestExternalAppsUninstall", 
                src, zaloId,x-> CustomAudienceKey.forExternalAppsUninstallOf(x), inputData, 
                THIS_CLASS,LOGGER);
    }
}