/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import static com.vng.zalo.zte.rnd.dbingestionmw.handlers.DBMiddlewareHandler.*;
import com.vng.zing.stats.Profiler;

/**
 *
 * @author tinhdt
 */
public class CacheHourModel {
    public static final CacheHourModel INSTANCE = new CacheHourModel();
    
    public int getHourCache(String apiName){
        try {
            return cache.get(apiName);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
}
