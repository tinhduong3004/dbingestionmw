// Created By PTP
package com.vng.zalo.zte.rnd.dbingestionmw.common;

import com.vng.zing.strlist32score.thrift.wrapper.StrList32ScoreClient;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;

/**
 *
 * @author phucpt2
 */
//TODO remove it after the centralized was done
public class DBClients {
    ////////////////////////////////////////////
    /////////////// client /////////////////////
    ////////////////////////////////////////////

    public static final ZiDB64Client _DBCmon3Cli = new ZiDB64Client("zudmcommon3");
    public static final ZiDB64Client DemographicDB = new ZiDB64Client("ZUDM_ZiDB64DemographicDB");
    public static final ZiDBClient _DBClientTravelPathCity = new ZiDBClient("ZUDM_ZiDBTravelPathCity");
    public static final ZiDB64Client UserKWDB = new ZiDB64Client("ZUDM_ZiDB64UserKWDB");
    public static final ZiDBClient _ZiDBTravelPathPOI = new ZiDBClient("ZiDBTravelPathPOI");
    public static final ZiDBClient _ZiDBWifiInfo = new ZiDBClient("ZiDBWifiInfo");
    public static final ZiDB64Client ANONYMOUSUSER_DB = new ZiDB64Client("ZUDM_ZiDB64AnonymousUser");
    public static final ZiDB64Client HOUSE_HOLD = new ZiDB64Client("HouseholdExtension");
    public static final ZiDB64Client USEDAPP_DB = new ZiDB64Client("ZUDM_ZiDB64UsedApps");
    public static final ZiDBClient GEO_CELL_DENSITY = new ZiDBClient("ZUDMGeoCellDensity");
    public static final StrList32ScoreClient SHOP_TRENDING = new StrList32ScoreClient("StrList32ScoreSuggestion");
    public static final StrList32ScoreClient ZINGMP3 = new StrList32ScoreClient("SuggestedMediaList");
    public static final ZiDB64Client LOCATIONINTEREST = new ZiDB64Client("ZUDMLocationInterest_ZiDB64");
   
}
