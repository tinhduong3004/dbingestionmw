/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zalo.zte.rnd.dbingestionmw.zingmp3_suggestion.thrift.TSuggestionSongList;
import com.vng.zing.logger.ZLogger;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zing.zudm.common.centralizedkeys.AllKeyTypeSets;
import com.vng.zing.zudm.common.centralizedkeys.UserInterestKey;
import com.vng.zing.zudm.common.centralizedkeys.ZingMp3Key;
import org.apache.log4j.Logger;
/**
 *
 * @author tinhdt
 */
public class ZingMp3Model extends BaseModel{
    private static final Class THIS_CLASS = ZingMp3Model.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final ZingMp3Model INSTANCE = new ZingMp3Model();
    private final static AllKeyTypeSets.SuggestedMediaList KEY = new AllKeyTypeSets.SuggestedMediaList();    
    
    public TActionResult uploadD00MediaListen(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadD00MediaListen", src, String.valueOf(KEY.D00_MEDIA_LISTEN), dataKey, time,
                (x,y)->ZingMp3Key.forDOOMediaListenOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadPairSong(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadPairSong", src, String.valueOf(KEY.PAIR_SONG), dataKey, time,
                (x,y)->ZingMp3Key.forPairSongOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadRefSong(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadRefSong", src, String.valueOf(KEY.REF_SONG), dataKey, time,
                (x,y)->ZingMp3Key.forRefSongOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadWebRef(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadWebRef", src, String.valueOf(KEY.WEB_REF), dataKey, time,
                (x,y)->ZingMp3Key.forWebRefOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadVideo(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadVideo", src, String.valueOf(KEY.VIDEO_VIDEO), dataKey, time,
                (x,y)->ZingMp3Key.forVideoOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadSong(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadSong", src, String.valueOf(KEY.SONG_SONG), dataKey, time,
                (x,y)->ZingMp3Key.forSongOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadPlaylist(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadPlaylist", src, String.valueOf(KEY.PLAYLIST_PLAYLIST), dataKey, time,
                (x,y)->ZingMp3Key.forPlaylistOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadTrending(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadTrending", src, String.valueOf(KEY.TRENDING), dataKey, time,
                (x,y)->ZingMp3Key.forTrendingOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadTfIdf(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadTfIdf", src, String.valueOf(KEY.TF_IDF), dataKey, time,
                (x,y)->ZingMp3Key.forTF_IDFOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadVietnameseSpecific(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadVietnameseSpecific", src, String.valueOf(KEY.VIETNAMESE_SPECIFIC), dataKey, time,
                (x,y)->ZingMp3Key.forVietnameseSpecificOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadForeignSpecific(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadForeignSpecific", src, String.valueOf(KEY.FOREIGN_SPECIFIC), dataKey, time,
                (x,y)->ZingMp3Key.forForeignSpecificOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadInstrumentSpecific(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadInstrumentSpecific", src, String.valueOf(KEY.INSTRUMENT_SPECIFIC), dataKey, time,
                (x,y)->ZingMp3Key.forInstrumentSpecificOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadAdsTrending(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadAdsTrending", src, String.valueOf(KEY.ADS_TRENDING), dataKey, time,
                (x,y)->UserInterestKey.forAdsTrendingOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadUserInterestHistory(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadUserInterestHistory", src, String.valueOf(KEY.USER_INTEREST_HISTORY), 
                dataKey, time,(x,y)->UserInterestKey.forUserInterestHistoryOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadUserInterestAls(String src, int dataKey, String time, TSuggestionSongList inputData){
        return ingestWithHour(DBClients.ZINGMP3, "uploadUserInterestAls", src, String.valueOf(KEY.USER_INTEREST_ALS), dataKey, time,
                (x,y)->ZingMp3Key.forUserInterestAlsOf(x,y),inputData,THIS_CLASS,LOGGER);
    }
}
