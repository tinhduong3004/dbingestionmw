// Created By PTP
package com.vng.zalo.zte.rnd.dbingestionmw.model;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_DensityCellCheckinInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_DensityCellHomeInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.exception.InvalidParamException;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zudm.common.centralizedkeys.DemographicPredictionKey;
import com.vng.zing.zudm.common.centralizedkeys.Ip2LocationKey;
import com.vng.zing.zudm.common.centralizedkeys.LocationMining;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_ForeignTripCount;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_Household;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_LivingLoc;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_TravelPath_City;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_TravelPath_POI;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_UserInterestLocation;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_WifiInfo;
import org.apache.log4j.Logger;

/**
 *
 * @author tinhdt
 */
public class LocationMiningModel extends BaseModel {

    private static final Class THIS_CLASS = LocationMiningModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final LocationMiningModel INSTANCE = new LocationMiningModel();
    
    
    public TActionResult ingestDensityCellCheckinInfo(String callerName, String dataKey, ZUDM_DensityCellCheckinInfo inputData) {
        return ingest(DBClients.GEO_CELL_DENSITY, "ingestDensityCellCheckinInfo", callerName, dataKey,inputData, THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestDensityCellHomeInfo(String callerName, String dataKey, ZUDM_DensityCellHomeInfo inputData) {
        return ingest(DBClients.GEO_CELL_DENSITY, "ingestDensityCellHomeInfo", callerName,dataKey, inputData, THIS_CLASS, LOGGER);
    }
    
    public TActionResult uploadHousehold(String src, int userId, ZUDM_Household inputData) {
        return ingest(DBClients.HOUSE_HOLD, "uploadHousehold", 
                src, userId,x-> LocationMining.forHouseHoldOf(x), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadLivingProvince(String callerName, int zaloId, ZUDM_LivingLoc inputData) {
        return ingestWithProfilerWithLog(DBClients._DBCmon3Cli, "ingestLivingProvince", callerName, zaloId, 
                x -> LocationMining.forLivingProvinceOf(x), inputData, THIS_CLASS, LOGGER);
    }
    

    public TActionResult uploadForeignTripCount(String callerName, int datakey, ZUDM_ForeignTripCount inputData) {
        return ingest(DBClients.DemographicDB,"ingestForeignTripCount", callerName, datakey,
                x -> DemographicPredictionKey.forForeignTripCountOf(x) , inputData, THIS_CLASS, LOGGER);
    }

    public TActionResult uploadTravelPath(String callerName, String zaloId, ZUDM_TravelPath_City inputData) {
        return ingest(DBClients._DBClientTravelPathCity, "ingestTravelPath", callerName, zaloId, inputData, THIS_CLASS, LOGGER);
    }

  
    public TActionResult uploadPOIpath(String callerName, String dataKey, ZUDM_TravelPath_POI inputData) {
        return ingest(DBClients._ZiDBTravelPathPOI, "ingestPOIpath", callerName, dataKey,inputData, THIS_CLASS, LOGGER);
    }
    
    public TActionResult uploadSSIDInfo(String callerName, String dataKey, ZUDM_WifiInfo inputData) {
        return ingest(DBClients._ZiDBWifiInfo, "ingestSSIDInfo", callerName, dataKey,inputData, THIS_CLASS, LOGGER);
    }
    
    public TActionResult IngestUserSSIDInterest(String src, int userId, ZUDM_UserInterestLocation inputData) {
        return ingest(DBClients.LOCATIONINTEREST, "IngestUserSSIDInterest", 
                src, userId,x-> LocationMining.forUserSSISInterestOf(x), inputData, 
                THIS_CLASS,LOGGER);
    }
    
}