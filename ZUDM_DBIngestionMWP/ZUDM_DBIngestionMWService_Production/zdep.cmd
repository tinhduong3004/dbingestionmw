#!/bin/sh

HOST=$1
SUBDIR=$2
CP_DIST_LIB=$3

PRJ=ZUDM_DBIngestionMWService_Production
IMG=dist/ZUDM_DBIngestionMWService_Production.jar

if [ ! -e $IMG ]
then
echo Execution file \"$IMG\" is not exist!
exit;
fi

if [ "x$SUBDIR" = "x" ];
then
ZDIR=/zserver/java-projects/$PRJ
else
ZDIR=/zserver/java-projects/$SUBDIR/$PRJ
fi

MKDIR_CMD="mkdir -p $ZDIR/"
if [ "x$HOST" = "x" ] || [ "x$HOST" = "xlo" ];
then
RSYNC_DEST="$ZDIR/"
else
RSYNC_DEST="$HOST:$ZDIR/"
MKDIR_CMD="ssh $HOST $MKDIR_CMD"
fi

echo Deploying to \"$RSYNC_DEST\" ...

if [ "x$CP_DIST_LIB" = "x0" ] || [ "x$CP_DIST_LIB" = "xno" ] || [ "x$CP_DIST_LIB" = "xfalse" ];
then
EXCLUDE="--exclude=dist/lib --exclude=dist/README.TXT"
fi

SRC_EXCLUDE="--exclude=views --exclude=servers --exclude=model --exclude=handlers --exclude=common --exclude=app --exclude=zaloidmw --exclude=zprofilesearch"

RSYNC_CMD="rsync -aurvC conf cmd dist src runservice $RSYNC_DEST $EXCLUDE $SRC_EXCLUDE"
$MKDIR_CMD
$RSYNC_CMD

echo
echo Done! Deployed in \"$RSYNC_DEST\".

