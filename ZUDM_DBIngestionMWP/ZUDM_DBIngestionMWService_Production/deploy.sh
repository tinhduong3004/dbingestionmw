#/usr/bin/sh
set -xe
./build.cmd
./zdep.cmd zdeploy@10.30.80.18 zudm 1
ssh zdeploy@10.50.9.16 /home/zdeploy/rsync_ZUDM_DBIngestionMWService_Production.sh
ssh zdeploy@10.50.9.16 /home/zdeploy/restart_ZUDM_DBIngestionMWService_Production.sh
