plugins {
    `java-library`
}

version = "1.0.2.1"

repositories {
    mavenCentral()
}

dependencies {
    // This dependency is found on compile classpath of this component and consumers.
    implementation("com.google.guava:guava:26.0-jre")
    
    implementation(files(
        "/zserver/java/lib/zthrift-0.9.1.6.jar",
        "/zserver/java/lib/zudm/jzudmdbingestionmw-thrift9-1.0.2.1.jar",
        "/zserver/java/lib/zudm/jzudmmiddleware-thrift9-1.1.0.8.jar",
        "/zserver/java/lib/zudm/zudmgismiddlewareclientwrapper-1.0.0.4.jar",
        "/zserver/java/lib/3rdparties/commons-pool-1.6.jar",
        "/zserver/java/lib/3rdparties/log4j-1.2.17.jar",
        "/zserver/java/lib/3rdparties/commons-logging-1.1.1.jar",
        "/zserver/java/lib/3rdparties/slf4j-api-1.7.2.jar",
        "/zserver/java/lib/3rdparties/slf4j-log4j12-1.7.2.jar",
        "/zserver/java/lib/zudm/jzuserdatamining-thrift9-1.0.8.4.jar"
    ))

    // Use JUnit test framework
    testImplementation("junit:junit:4.12")
}

tasks{
    val copyToZServer by creating {
        dependsOn(build)

    }
    build {
        finalizedBy(copyToZServer)
    }
    test {
        useJUnitPlatform()
        jvmArgs("-ea",
                "-Dzappname=zudmdbingestionmw-client",
                "-Dzappprof=production",
                "-Dzconfdir=conf",
                "-Dzconffiles=config.ini",
                "-Djzcommonx.version=LATEST",
                "-Dzicachex.version=LATEST")
    }
}

