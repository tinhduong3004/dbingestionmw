// Created By PTP
package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TMultigetDoubleResult;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TMultigetIntResult;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TMultigetLongResult;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TUserCoverageResult;
import java.util.List;

/**
 *
 * @author phucpt2
 */
public class UserCoverageMWClient extends AbstractDBIngestionMWClient{
    public UserCoverageMWClient(String host,int port){
        super(host, port, 300000);
    }

    public UserCoverageMWClient(){
        super();
    }
    
    public TUserCoverageResult checkCoverage(List<Long> uid_list, short dbtype) throws Exception {
        TUserCoverageResult result = new TUserCoverageResult();
        result.setError(-1);
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.checkCoverage("USERCOVERAGE", uid_list, dbtype);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                System.out.println(ex);
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TMultigetIntResult multigetIntFromDB(List<Long> uid_list, String dbtype) throws Exception {
        TMultigetIntResult result = new TMultigetIntResult();
        result.setError(-1);
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.multigetIntFromDB("USERCOVERAGE", uid_list, dbtype);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                System.out.println(ex);
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TMultigetDoubleResult multigetDoubleFromDB(List<Long> uid_list, String dbtype) throws Exception {
        TMultigetDoubleResult result = new TMultigetDoubleResult();
        result.setError(-1);
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.multigetDoubleFromDB("USERCOVERAGE", uid_list, dbtype);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                System.out.println(ex);
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
 
    public TMultigetLongResult multigetLongFromDB(List<Long> uid_list, String dbtype) throws Exception {
        TMultigetLongResult result = new TMultigetLongResult();
        result.setError(-1);
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.multigetLongFromDB("USERCOVERAGE", uid_list, dbtype);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                System.out.println(ex);
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
}
