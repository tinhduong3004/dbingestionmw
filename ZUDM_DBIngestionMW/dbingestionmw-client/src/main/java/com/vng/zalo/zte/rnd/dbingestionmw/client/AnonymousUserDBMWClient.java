package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TListDevice;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TListHistory;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TListLocation;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictAgeGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedAge;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedAgeRange;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedGenderV2;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedProvince;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;

public class AnonymousUserDBMWClient extends AbstractDBIngestionMWClient{

    public AnonymousUserDBMWClient(String host,int port){
        super(host,port);
    }

    public AnonymousUserDBMWClient(){
        super();
    }

    private final String CALLERNAME = "Anonymous@Mining";

    public TActionResult ingestAUPredictedGender(long dataKey, TPredictedGender inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUPredictedGender(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    public TActionResult ingestAUPredictedAge(long dataKey, TPredictedAge inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUPredictedAge(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    public TActionResult ingestAUPredictedProvince(long dataKey, TPredictedProvince inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUPredictedProvince(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    public TActionResult ingestUAUProvince(long dataKey, TPredictedProvince inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestUAUProvince(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult ingestAUListDevice(long dataKey, TListDevice inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUListDevice(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    public TActionResult ingestAUAdtimaHistory(long dataKey, TListHistory inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUAdtimaHistory(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult ingestAUZingMp3History(long dataKey, TListHistory inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUZingMp3History(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public TActionResult ingestAUZingNewsHistory(long dataKey, TListHistory inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUZingNewsHistory(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    public TActionResult ingestAUListLocation(long dataKey, TListLocation inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUListLocation(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    

    public TActionResult ingestAUPredictedGenderV2(long dataKey, TPredictedGender inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUPredictedGenderV2(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestPredictedGender(long dataKey, TPredictedGenderV2 inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestPredictedGender(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestShiftedPredictedGender(long dataKey, TPredictedGenderV2 inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestShiftedPredictedGender(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestAUPredictedAgeV2(long dataKey, TPredictedAgeRange inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUPredictedAge2(CALLERNAME,dataKey,inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult ingestAUPredictedAgeRange(long dataKey, TPredictedAgeRange inputData, boolean bln) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestAUPredictedAgeRange(CALLERNAME,dataKey,inputData, bln);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
