package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.common.ThriftClientPool;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;

/**
 *
 * @author tinhdt
 */
public abstract class AbstractDBIngestionMWClient {

    protected ThriftClientPool<DBIngestionMW.Client> CLIENT_POOL;
    final int nRetry = 4;

    public AbstractDBIngestionMWClient(String host, int port, int timeout, int maxTotal, int maxIdle, int minIdle) {
        CLIENT_POOL = new ThriftClientPool<>(host, port, timeout, maxTotal, maxIdle, minIdle);
        CLIENT_POOL.init(new DBIngestionMW.Client.Factory());
    }
    public AbstractDBIngestionMWClient(String host, int port) {
        CLIENT_POOL = new ThriftClientPool<>(host, port, 5000, 8, 8, 8);
        CLIENT_POOL.init(new DBIngestionMW.Client.Factory());
    }
    public AbstractDBIngestionMWClient(String host, int port, int timeout) {
        CLIENT_POOL = new ThriftClientPool<>(host, port, timeout, 8, 8, 8);
        CLIENT_POOL.init(new DBIngestionMW.Client.Factory());
    }
    public AbstractDBIngestionMWClient(){
        this("10.30.65.168",18782);
    }

    public int ping() throws Exception {
        int pingResponse = 1;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client cli = CLIENT_POOL.borrowClient();
            try {
                pingResponse = cli.ping();
                CLIENT_POOL.returnClient(cli);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(cli, ex);
            }
        }
        return pingResponse;
    }
}