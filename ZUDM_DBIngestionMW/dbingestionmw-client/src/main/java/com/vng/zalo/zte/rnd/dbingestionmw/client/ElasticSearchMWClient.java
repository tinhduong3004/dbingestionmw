/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.KeywordInfoList;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.UserLocationCheckinCount;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TStringSearchResult;

import java.util.List;

/**
 *
 * @author phucpt2
 */
public class ElasticSearchMWClient extends AbstractDBIngestionMWClient{
    public ElasticSearchMWClient(String host, int port, int timeout){
        super(host, port, timeout);
    }

    public ElasticSearchMWClient(){
        super();
    }

    private final String CALLERNAME = "Location@Mining";
    
    public TStringSearchResult ESLocationGetListUser(String string, List<Long> list) throws Exception {
        TStringSearchResult result = new TStringSearchResult();
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ESLocationGetListUser(string, list);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    
    public TStringSearchResult ESLocationQueryNearBy(String string, double lat, double lon, double distance, double score_threshold) throws Exception {
        TStringSearchResult result = new TStringSearchResult();
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ESLocationQueryNearBy(string, lat, lon, distance, score_threshold);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
     
     
    public TStringSearchResult ESLocationInsertListUser(String string, List<UserLocationCheckinCount> list) throws Exception {
        TStringSearchResult result = new TStringSearchResult();
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ESLocationInsertListUser(string, list);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    
    public TStringSearchResult ESIngestUserKeyword(String str, KeywordInfoList list_kw, boolean notify) throws Exception{
        TStringSearchResult result = new TStringSearchResult();
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ESIngestUserKeyword(str, list_kw, notify);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    
    public TStringSearchResult ESIngestAnonymousUserKeyword(String str, KeywordInfoList list_kw, boolean notify) throws Exception{
        TStringSearchResult result = new TStringSearchResult();
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ESIngestAnonymousUserKeyword(str, list_kw, notify);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
