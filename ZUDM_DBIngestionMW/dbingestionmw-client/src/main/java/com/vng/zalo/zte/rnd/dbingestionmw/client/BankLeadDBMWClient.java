/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandScore;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScore;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScoreV2;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandTag;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;

/**
 *
 * @author cpu11232
 */
public class BankLeadDBMWClient extends AbstractDBIngestionMWClient {
	public BankLeadDBMWClient(String host, int port) {
        super(host, port);
    }

    public BankLeadDBMWClient() {
        super();
    }
    
    private final String CALLERNAME = "BankLead";

    public TActionResult uploadLoanDemandScore(int zaloId, ZUDM_LoanDemandScore inputData, boolean notify ) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadLoanDemandScore(CALLERNAME, zaloId, inputData, notify);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadDisburseLeadScore(int zaloId, ZUDM_DisburseLeadScore inputData, boolean notify ) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadDisburseLeadScore(CALLERNAME, zaloId, inputData, notify);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadDisburseLeadShScore(int zaloId, ZUDM_DisburseLeadScoreV2 inputData, boolean notify ) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadDisburseLeadShScore(CALLERNAME, zaloId, inputData, notify);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadLoanDemandTag(int zaloId, ZUDM_LoanDemandTag inputData, boolean notify ) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestLoanDemandTag(CALLERNAME, zaloId, inputData, notify);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
