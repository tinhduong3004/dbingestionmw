package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.gameh5.thrift.ZUDM_Gameh5PlayingInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;

import java.util.List;

/**
 * client for GameH5 suggestion
 * @author thient
 */
public class GameH5DBMWClient extends AbstractDBIngestionMWClient{
    public GameH5DBMWClient(String host,int port){
        super(host,port);
    }

    public GameH5DBMWClient(){
        super();
    }

    /**
     *
     * @param uid
     * @param playerInfo
     * @return
     * @throws Exception
     */
    public int uploadGameh5PlayingInfo(int uid, int appid , ZUDM_Gameh5PlayingInfo playerInfo) throws Exception {
        int result = -1;
        String callerName = "GameH5";
        assert (playerInfo.getUid() == uid);
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                TActionResult resultObj = client.uploadGameh5PlayingInfo(callerName, uid, appid , playerInfo);
                result = resultObj.getValue();
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    public int uploadGameh5PlayingInfo(int uid, int appid ,int nMatch, int nWin) {
        int result = -1;
        ZUDM_Gameh5PlayingInfo playingInfo = new ZUDM_Gameh5PlayingInfo();
        playingInfo.setUid(uid);
        playingInfo.setAppid(appid);
        playingInfo.setN_match(nMatch);
        playingInfo.setN_win(nWin);
        try {
            result = this.uploadGameh5PlayingInfo(uid, appid, playingInfo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * put list of uid and join with old data in db
     * it does not clean old data
     * call removeUserHighWinrate to remove old data
     * @param uidList
     * @param appid
     * @return
     * @throws Exception
     */
    public int putUserHighWinrate(List<Integer> uidList, int appid) throws Exception {
        int result = -1;
        String callerName = "GameH5";
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                TActionResult resultObj = client.putUserHighWinrate(uidList, appid);
                result = resultObj.getValue();
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

    /**
     * remove old data in db
     * @param appid
     * @return
     * @throws Exception
     */
    public int removeUserHighWinrate(int appid) throws Exception {
        int result = 0;
        String callerName = "GameH5";
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                TActionResult resultObj = client.removeUserHighWinrate(appid);
                result = resultObj.getValue();
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }

}