/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;

/**
 *
 * @author tinhdt
 */
public class CacheMWClient extends AbstractDBIngestionMWClient{
    public CacheMWClient(String host,int port){
        super(host,port);
    }

    public CacheMWClient(){
        super();
    } 
    
    public int GetTimeCache(String apiName) throws Exception {
        int result = 0;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.getHourCache(apiName);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
