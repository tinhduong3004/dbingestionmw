/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_ListFriendRanking;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;


/**
 *
 * @author tinhdt
 */
public class FriendRankingMWClient extends AbstractDBIngestionMWClient{
    public FriendRankingMWClient(String host,int port){
        super(host,port);
    }

    public FriendRankingMWClient(){
        super();
    }

    private final String CALLERNAME = "Profile@Mining";

    public TActionResult uploadInterestFriend(int zaloId, ZUDM_ListFriendRanking inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestInterestFriend(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadFollower(int zaloId, ZUDM_ListFriendRanking inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestFollower(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadFollowerV2(int zaloId, ZUDM_ListFriendRanking inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestFollowerV2(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadFollowerV3(int zaloId, ZUDM_ListFriendRanking inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestFollowerV3(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadFollowerV4(int zaloId, ZUDM_ListFriendRanking inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestFollowerV4(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
}