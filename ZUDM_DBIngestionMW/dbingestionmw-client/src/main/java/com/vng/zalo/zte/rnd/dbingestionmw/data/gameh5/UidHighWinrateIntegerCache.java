package com.vng.zalo.zte.rnd.dbingestionmw.data.gameh5;

import com.vng.zalo.zte.rnd.dbingestionmw.client.GameH5DBMWClient;

import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class UidHighWinrateIntegerCache {
    private ArrayList<Integer> uidList;
    public int cap;
    private GameH5DBMWClient client;
    private int appid;

    public UidHighWinrateIntegerCache(int appid ,int cap, GameH5DBMWClient client) {
        this.resetList();
        this.cap = cap;
        this.client = client;
        this.appid = appid;
    }

    public void resetList(){
        this.uidList = new ArrayList<>();
    }

    public ArrayList<Integer> getUidList(){
        return (ArrayList<Integer>) this.uidList.clone();
    }

    /**
     * put all uid int list to DB Client
     * @return string to write into log
     */
    public String putUidList() {
        List<Integer> cloneUidList = this.getUidList();
        this.resetList();
        int result = -1;
        try {
            result = client.putUserHighWinrate(cloneUidList, appid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (result < 0){
            // put failed, restore list
            this.uidList.addAll(cloneUidList);
        }
        StringBuilder resultStrBuilder = new StringBuilder();
        if (result < 0){
            resultStrBuilder.append("PUT_FAILED");
            resultStrBuilder.append("_");
            resultStrBuilder.append(String.valueOf(result));
            resultStrBuilder.append("_");

        } else if (result == 0){
            resultStrBuilder.append("PUT_SUCCESS");
            resultStrBuilder.append("_");
            resultStrBuilder.append(String.valueOf(result));
            resultStrBuilder.append("_");
        }
        return resultStrBuilder.toString();
    }

    /**
     * put one entry to queue
     * put all queue to Client if full
     * @param uid
     * @return
     */
    public String putEntry(int uid){
        StringBuilder resultStrBuilder = new StringBuilder();
        uidList.add(uid);
        resultStrBuilder.append("QUEUE_UID_");
        resultStrBuilder.append(String.valueOf(uid));
        if (this.uidList.size() > this.cap){
            resultStrBuilder.append("_");
            String putResult = putUidList();
            resultStrBuilder.append(putResult);
        }

        return resultStrBuilder.toString();
    }

    /**
     * put one entry to queue
     * put all queue to Client if full
     * @param uid String, will Integer.parseInt to get integer
     * @return
     */
    public String putEntry(String uid){
        StringBuilder resultStrBuilder = new StringBuilder();
        try {
            resultStrBuilder.append(Integer.parseInt(uid));
        } catch (Exception e){
            resultStrBuilder.append("PUT_FAILED");
            e.printStackTrace();
        }
        return resultStrBuilder.toString();
    }




}
