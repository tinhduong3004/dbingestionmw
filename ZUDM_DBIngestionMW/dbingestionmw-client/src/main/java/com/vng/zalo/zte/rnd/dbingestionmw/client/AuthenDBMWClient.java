/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.fraudmining.thrift.TListDuplicateAccount;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;

/**
 *
 * @author tinhdt
 */
public class AuthenDBMWClient extends AbstractDBIngestionMWClient {
	public AuthenDBMWClient(String host, int port) {
        super(host, port);
    }

    public AuthenDBMWClient() {
        super();
    }
    
    private final String CALLERNAME = "Profile@Mining";

    public TActionResult uploadListDuplicateAccount(int zaloId, TListDuplicateAccount inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestListDuplicateAccount(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
