package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyList;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyListV2;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyTop500;
import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_UserOccupationList;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;


/**
 * @author baopng
 */

public class UserOccupationDBMWClient extends AbstractDBIngestionMWClient {

    public UserOccupationDBMWClient(String host, int port) {
        super(host, port);
    }

    public UserOccupationDBMWClient() {
        super();
    }

    private final String CALLERNAME = "Profile@Mining";

    public TActionResult uploadUserStudentOccupation(int zaloId, ZUDM_UserOccupationList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadUserStudentOccupation(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadUserUnionOccupation(int zaloId, ZUDM_UserOccupationList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadUserUnionOccupation(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadUserBlueWhiteOccupation(int zaloId, ZUDM_UserOccupationList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadUserBlueWhiteOccupation(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadModelOccupation(int zaloId, ZUDM_UserOccupationList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadModelOccupation(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadUserCompany(int zaloId, ZUDM_UserCompanyList inputData, boolean notify) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestUserCompany(CALLERNAME, zaloId, inputData, notify);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadUserCompanyTop500(int zaloId, ZUDM_UserCompanyTop500 inputData, boolean notify) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestUserCompanyTop500(CALLERNAME, zaloId, inputData, notify);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadUserCompanyV2(int zaloId, ZUDM_UserCompanyListV2 inputData, boolean notify) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestUserCompanyV2(CALLERNAME, zaloId, inputData, notify);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
    public TActionResult uploadWorkType(int zaloId, ZUDM_UserOccupationList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.uploadWorkType(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
}
