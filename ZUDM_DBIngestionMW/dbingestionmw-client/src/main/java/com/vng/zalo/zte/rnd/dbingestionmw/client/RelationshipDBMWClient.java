/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.client;

import com.vng.zalo.zte.rnd.dbingestionmw.relationship.thrift.ZUDM_RelationshipList;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;


/**
 *
 * @author tinhdt
 */
public class RelationshipDBMWClient extends AbstractDBIngestionMWClient{
    public RelationshipDBMWClient(String host,int port){
        super(host,port);
    }

    public RelationshipDBMWClient(){
        super();
    }

    private final String CALLERNAME = "Profile@Mining";

    public TActionResult uploadRelationship(int zaloId, ZUDM_RelationshipList inputData) throws Exception {
        TActionResult result = TActionResult.ERROR;
        for (int retry = 0; retry < nRetry; ++retry) {
            DBIngestionMW.Client client = CLIENT_POOL.borrowClient();
            try {
                result = client.ingestRelationship(CALLERNAME, zaloId, inputData);
                CLIENT_POOL.returnClient(client);
                break;
            } catch (Exception ex) {
                CLIENT_POOL.invalidateClient(client, ex);
            }
        }
        return result;
    }
    
}
