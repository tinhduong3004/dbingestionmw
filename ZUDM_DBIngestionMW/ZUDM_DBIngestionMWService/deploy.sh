#/usr/bin/sh
set -xe
./build.cmd
./zdep.cmd zdeploy@10.30.80.18 zudm 1
ssh zdeploy@10.30.65.168 /home/zdeploy/rsync_ZUDM_DBIngestionMWService.sh
ssh zdeploy@10.30.65.168 /home/zdeploy/restart_ZUDM_DBIngestionMWService.sh
