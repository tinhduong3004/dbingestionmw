#!/bin/bash

SERVICE="ZUDM_DBIngestionMWService"
PORT=18782
HOST=127.0.0.1
HOME_DIR="/zserver/java-projects/zudm/ZUDM_DBIngestionMWService"

CMD_START="./runservice start production"
CMD_STOP="./runservice stop production"
PID_FILE="/zserver/tmp/$SERVICE/$SERVICE.pid"




func_start() {
	echo "Starting service $SERVICE ..........."
	$CMD_START
	sleep 1
	echo "Checking .."
	PID=`cat $PID_FILE`
	## check process
	
	echo;echo "Started.";echo

}
func_stop() {
	if ! [ -f $PID_FILE ]; then
		echo "PID file of $SERVICE not found"
        ##Check process
		PID=`ps -ef|grep -v grep|grep -w $SERVICE|awk '{print $2}'`
		if ! [ -z $PID ]; then
			echo "Process $SERVICE is running !!!!. Kill -9 $SERVICE"
			kill -9 $PID
		fi
		return 1
	fi
	PID=`cat $PID_FILE`
	if  [ -z $PID ]; then
		echo "PID of $SERVICE not found"
		PID=`ps -ef|grep -v grep|grep -w $SERVICE|awk '{print $2}'`
		if ! [ -z $PID ]; then
			echo "Process $SERVICE is running !!!!. Kill -9 $SERVICE"
			kill -9 $PID
		fi
		rm -f $PID_FILE
		return 2
	fi
	PID=`cat $PID_FILE`
	echo "Stopping service $SERVICE ..........."
	$CMD_STOP
	## check listen port
	while (true); do
        	/usr/local/nagios/libexec/check_tcp -H $HOST -p $PORT
		if [ $? -eq 2 ];then
			break
		fi
		echo  "Waiting process $SERVICE stop ..."
		sleep 1 
	done
	## check process


	echo;echo "Stopped.";echo
}

##Main
if [ "$PORT" == "_port" ]; then
	echo "pls update running port.."
	exit 1
fi

##restart
func_stop
func_start

