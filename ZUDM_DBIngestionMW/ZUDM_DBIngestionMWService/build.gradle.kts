plugins {
    java
    application
}

repositories {
    jcenter()
}

dependencies {
    // local jar dependencies
    implementation(
        files(
            "/zserver/java/lib/zudm/3rdparties/auto-value-1.1.jar",
			"/zserver/java/lib/3rdparties/commons-codec-1.7.jar",
			"/zserver/java/lib/3rdparties/commons-collections-3.2.1.jar",
			"/zserver/java/lib/3rdparties/commons-configuration-1.9.jar",
			"/zserver/java/lib/3rdparties/commons-lang-2.5.jar",
			"/zserver/java/lib/3rdparties/commons-lang3-3.1.jar",
			"/zserver/java/lib/3rdparties/commons-logging-1.1.1.jar",
			"/zserver/java/lib/3rdparties/commons-pool-1.6.jar",
			"/zserver/java/lib/3rdparties/hamcrest-core-1.3.jar",
			"/zserver/java/lib/3rdparties/hapax-2.3.5-r91mod.jar",
			"/zserver/java/lib/3rdparties/high-scale-lib-1.1.2.jar",
			"/zserver/java/lib/3rdparties/htmlcompressor-1.5.3.jar",
			"/zserver/java/lib/3rdparties/javassist-3.22.0.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-annotations-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-client-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-continuation-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-deploy-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-http-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-io-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-jmx-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-jndi-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-patch-9.3.9.v20160517-1.0.0.1.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-plus-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-rewrite-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-security-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-server-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-servlet-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-servlets-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-util-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-webapp-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/jetty-9.3.9/jetty-xml-9.3.9.v20160517.jar",
			"/zserver/java/lib/3rdparties/joda-time-2.3.jar",
			"/zserver/java/lib/3rdparties/jofc2-1.0-0.jar",
			"/zserver/java/lib/3rdparties/json-simple-1.1.1.jar",
			"/zserver/java/lib/jstrlist32bm-thrift9-1.0.0.1.jar",
			"/zserver/java/lib/jstrlist32score-thrift9-1.0.0.0.jar",
			"/zserver/java/lib/3rdparties/junit-4.12.jar",
			"/zserver/java/lib/zp/zaoauth/jzalooauthmw-thrift9-1.0.3.5.jar",
			"/zserver/java/lib/jzcommon-corelib-1.2.1.9.jar",
			"/zserver/java/lib/jzebservice-thrift9-1.0.0.5.jar",
			"/zserver/java/lib/jzidb-thrift9-1.0.0.1.jar",
			"/zserver/java/lib/jzidb64-thrift9-1.0.0.1.jar",
			"/zserver/java/lib/zp/zaoauth/jzodt-thrift9-1.0.3.3.jar",
			"/zserver/java/lib/zudm/jzudmdbingestionmw-thrift9-1.0.2.1.jar",
			"/zserver/java/lib/zudm/jzudmmiddleware-thrift9-1.1.2.7.jar",
			"/zserver/java/lib/zudm/zudmdmpmiddleware-thrift9-1.0.3.6.jar",
			"/zserver/java/lib/zudm/jzuserdatamining-thrift9-1.0.8.4.jar",
			"/zserver/java/lib/3rdparties/log4j-1.2.17.jar",
			"/zserver/java/lib/3rdparties/servlet-api-3.1.jar",
			"/zserver/java/lib/3rdparties/slf4j-api-1.7.2.jar",
			"/zserver/java/lib/3rdparties/slf4j-log4j12-1.7.2.jar",
			"/zserver/java/lib/zp/zcentralizeid-thrift9-1.0.2.6.jar",
			"/zserver/java/lib/zinstrumentation-1.0.0.0.jar",
			"/zserver/java/lib/zthrift-0.9.3.2.jar",
            "/zserver/java/lib/zudm/zudmdbingestionmwcommon-1.0.0.1.jar",
			"/zserver/java/lib/zudm/zudmdbingestionmwclient-1.0.2.1.jar",
			"/zserver/java/lib/zudm/zudmgismiddlewareclientwrapper-1.0.0.4.jar",
			"/zserver/java/lib/zudm/zudmcommon-1.0.5.9.jar"
        )
    )
    implementation("commons-codec:commons-codec:1.14")
    // Use JUnit Jupiter API for testing.
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    // Use JUnit Jupiter Engine for testing.
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.6.2")
}

application {
    mainClassName = "com.vng.zalo.zte.rnd.dbingestionmw.app.MainApp"
    applicationDefaultJvmArgs = listOf("-ea",
            "-Dzappname=ZUDM_DBIngestionMWService",
            "-Dzappprof=production",
            "-Dzconfdir=conf",
            "-Dzconffiles=config.ini",
            "-Djzcommonx.version=LATEST",
            "-Dzicachex.version=LATEST")
}

tasks{
    test{
        useJUnitPlatform()
        jvmArgs("-ea",
                "-Dzappname=ZUDM_DBIngestionMWService",
                "-Dzappprof=production",
                "-Dzconfdir=conf",
                "-Dzconffiles=config.ini",
                "-Djzcommonx.version=LATEST",
                "-Dzicachex.version=LATEST")
//        exclude("**/*ClientTest.class")
    }



    val distLibDir = File("dist/lib")
    val distDir = File("dist")

    val removeExistDist by creating {
        dependsOn(build)
        doLast{
            if ( distDir.exists() ){
                delete(distDir)
            }
            mkdir(distLibDir)
        }
    }
    // copy libs to dist/lib folder 
    val copyLibs by creating {
        dependsOn(removeExistDist)
        doLast {
            copy {
                from(configurations.compileClasspath)
                into(distLibDir)
            }
        }
    }

    // copy built jar to dist folder 
    val copyBuiltJar by creating {
        dependsOn(copyLibs)
        doLast {
            copy {
                from("$buildDir/libs")
                into(distDir)
            }
        }
    }
    build {
        finalizedBy(copyBuiltJar)
    }
    sourceSets {                                
	    main {                                  
	        resources.srcDir("build/resources")
	    }
	}
    // Jar file config
    jar {
        // Fill manifest for Zte service run
        manifest {
            val classPath = distLibDir.walk()
                                    .filter { file -> file.extension == ".jar" }
                                    .map { file -> file.path}
                                    .joinToString(" ")

            attributes["Main-Class"] = application.mainClassName
            attributes["Class-Path"] = classPath
        }
    }
}

sourceSets {
    create("intTest") {
        compileClasspath += sourceSets.main.get().output
        runtimeClasspath += sourceSets.main.get().output
    }
}

val intTestImplementation by configurations.getting {
    extendsFrom(configurations.testImplementation.get())
}

configurations["intTestRuntimeOnly"].extendsFrom(configurations.runtimeOnly.get())

dependencies {
    intTestImplementation("junit:junit:4.12")
}

val integrationTest = task<Test>("integrationTest") {
    description = "Runs integration tests."
    group = "verification"

    testClassesDirs = sourceSets["intTest"].output.classesDirs
    classpath = sourceSets["intTest"].runtimeClasspath
    shouldRunAfter("test")
}

tasks.check { dependsOn(integrationTest) }
