package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.client.BankLeadDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.fraudmining.thrift.TListDuplicateAccount;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.AuthenKey;


public class TestAuthen {

    private final static BankLeadDBMWClient DBCLIENT = new BankLeadDBMWClient();
    public static final ZiDB64Client Tokenizer = new ZiDB64Client("ZUDM_ZiDB64UserTokenizedKWDB");

    public static void main(String[] args) throws Exception {
        int dataKey = 127778754;
        long GenKey = AuthenKey.forDuplicateAccountKeyOf(dataKey);
        TListDuplicateAccount rel = (TListDuplicateAccount) WithCompression.getFrom(Tokenizer, GenKey,new TListDuplicateAccount());
        System.out.println(rel);

    }
}
