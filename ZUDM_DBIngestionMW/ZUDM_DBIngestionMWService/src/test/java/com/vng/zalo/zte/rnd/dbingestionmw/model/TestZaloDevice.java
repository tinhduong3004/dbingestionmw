package com.vng.zalo.zte.rnd.dbingestionmw.model;
import com.vng.zalo.zte.rnd.dbingestionmw.client.DemographicDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.device.thrift.ZUDM_UserDeviceRaw;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.ZaloDeviceKey;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cpu11232
 */
public class TestZaloDevice {
    private static final DemographicDBMWClient CLIENT = new DemographicDBMWClient();
   private static final ZiDB64Client DEVICE_DB = new ZiDB64Client("ZUDMDevices_ZiDB64");

    private static void testUploadZaloDevice(int dataKey, ZUDM_UserDeviceRaw inputData) throws Exception {
//        CLIENT.uploadRelationship(dataKey, inputData);
//        System.out.println(CLIENT.uploadRelationship(dataKey, inputData));
        Long genergateKey = ZaloDeviceKey.forDeviceRawOf(dataKey);
        ZUDM_UserDeviceRaw relationship = (ZUDM_UserDeviceRaw) WithCompression.getFrom(DEVICE_DB, genergateKey, new ZUDM_UserDeviceRaw());
        System.out.println(relationship);
        
    }

    public static void main(String[] args) throws Exception {

//test Relationship
        int uid = 292601834;
        ZUDM_UserDeviceRaw device = new ZUDM_UserDeviceRaw();
        testUploadZaloDevice(uid, device);
    }
}
