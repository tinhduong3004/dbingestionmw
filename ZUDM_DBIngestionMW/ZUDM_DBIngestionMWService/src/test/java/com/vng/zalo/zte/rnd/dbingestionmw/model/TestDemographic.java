package com.vng.zalo.zte.rnd.dbingestionmw.model;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.vng.zalo.zte.rnd.dbingestionmw.client.DemographicDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.client.KeyRemoveClient;
import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.*;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.DemographicPredictionKey;
import com.vng.zing.zudm.common.centralizedkeys.UserIncome;
import com.vng.zing.zudm.common.centralizedkeys.UserKwKey;
import com.vng.zing.zudm.util.encrypt.CryptoUtil;
import org.apache.log4j.Logger;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author tinhdt
 */
public class TestDemographic {

    private static final DemographicDBMWClient CLIENT = new DemographicDBMWClient("127.0.0.1", 18782);
    private static final KeyRemoveClient RemoveClient = new KeyRemoveClient();
    public static final ZiDB64Client DEMO_GRAPHIC = new ZiDB64Client("ZUDM_ZiDB64DemographicDB");
    public static final ZiDB64Client DEMO_GRAPHIC3= new ZiDB64Client("ZUDMDemographicDB3_ZiDB64");
    public static final ZiDB64Client UserKWDB = new ZiDB64Client("ZUDM_ZiDB64UserKWDB");
    public static final ZiDB64Client UserIncomeDB = new ZiDB64Client("ZUDMUserIncome");
    public static final ZiDB64Client CreditScoreDB = new ZiDB64Client("ZUDMUserCreditScore");
    public static final ZiDB64Client DemographicDB2 = new ZiDB64Client("ZUDM_RollZiDB64DemographicDB2");
    private static final Class THIS_CLASS = TestDemographic.class;
    private static final ZiDBClient MAPPING_CLIENT = new ZiDBClient("ZUDMCSMapping");
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);

    private static void testUploadGender(int dataKey, ZUDM_UserPredictedGender inputData) throws Exception {
//        CLIENT.uploadGender(dataKey, inputData);
        System.out.println(CLIENT.uploadGender(dataKey, inputData, false));
        Long genergateKey = DemographicPredictionKey.forUserGenderOf(dataKey);
        ZUDM_UserPredictedGender ggender = (ZUDM_UserPredictedGender)WithCompression.getFrom(DemographicDB2, genergateKey,new ZUDM_UserPredictedGender());
        System.out.println(ggender);
    }
    
    private static void testUploadGenderV2(int dataKey, ZUDM_UserPredictedGender inputData) throws Exception {
//        CLIENT.uploadGenderV2(dataKey, inputData);
//        System.out.println(CLIENT.uploadGenderV2(dataKey, inputData));
        Long genergateKey = DemographicPredictionKey.forUserGenderV2Of(dataKey);
        ZUDM_UserPredictedGender ggender = (ZUDM_UserPredictedGender)WithCompression.getFrom(DemographicDB2, genergateKey,new ZUDM_UserPredictedGender());
        System.out.println(ggender);
    }
    
    private static void testUploadAge(int dataKey, ZUDM_UserAge inputData) throws Exception {
//        System.out.println(CLIENT.uploadAge(dataKey, inputData, false));
        Long genergateKey = DemographicPredictionKey.forUserAgeOf(dataKey);
        ZUDM_UserAge gAge = (ZUDM_UserAge)WithCompression.getFrom(DemographicDB2, genergateKey,new ZUDM_UserAge());
        System.out.println(gAge);
    }
    
    private static void testUploadAgeV2(int dataKey, ZUDM_UserAge inputData) throws Exception {
//        CLIENT.uploadAgeV2(dataKey, inputData);
//        System.out.println(CLIENT.uploadAgeV2(dataKey, inputData));
        Long genergateKey = DemographicPredictionKey.forUserAgeV2Of(dataKey);
        System.out.println(genergateKey);
        ZUDM_UserAge gAge = (ZUDM_UserAge)WithCompression.getFrom(DEMO_GRAPHIC, genergateKey,new ZUDM_UserAge());
        System.out.println(gAge);
    }
    
    private static void testUploadMaritalStatus(int dataKey, ZUDM_UserMaritalStatus inputData) throws Exception {
//        System.out.println(CLIENT.uploadMaritalStatus(dataKey, inputData));
        
        Long genergateKey = DemographicPredictionKey.forMaritalStatusOf(dataKey);
        System.out.println(genergateKey);
        ZUDM_UserMaritalStatus maritalStatus = (ZUDM_UserMaritalStatus)WithCompression.getFrom(DemographicDB2, genergateKey,new ZUDM_UserMaritalStatus());
        System.out.println(maritalStatus);
    }
        
        
    private static void testUploadParentalStatus(int dataKey, ZUDM_UserParentalStatus inputData) throws Exception {
//        CLIENT.uploadParentalStatus(dataKey, inputData, true);
//        System.out.println(CLIENT.uploadParentalStatus(dataKey, inputData, true));
        Long genergateKey = DemographicPredictionKey.forParentalStatusOf(dataKey);
        ZUDM_UserParentalStatus parentalStatus = (ZUDM_UserParentalStatus)WithCompression.getFrom(DemographicDB2, genergateKey,new ZUDM_UserParentalStatus());
        System.out.println(parentalStatus);
//        List<ZUDM_AgeRangeOfChild> listAgeRangeOfChild= parentalStatus.getAgeRangeOfChilds().stream().forEach(age -> {
//            
//        });
    }
    
    private static void testUploadBidirectionalPhonebook(int src_id, ZUDM_BidirectionalPhonebookList inputData) throws Exception {
        System.out.println(CLIENT.UploadBidirectionalPhonebook(src_id, inputData));
        Long genergateKey = UserKwKey.forBidirectionalPhonebookOf(src_id);
        ZUDM_BidirectionalPhonebookList parentalStatus = (ZUDM_BidirectionalPhonebookList)WithCompression.getFrom(DEMO_GRAPHIC, genergateKey,new ZUDM_BidirectionalPhonebookList());
        System.out.println(parentalStatus);
//        List<ZUDM_AgeRangeOfChild> listAgeRangeOfChild= parentalStatus.getAgeRangeOfChilds().stream().forEach(age -> {
//            
//        });
    }
    
    private static void testEducationLevel(int zaloId, ZUDM_UserEducationLevel inputData) throws Exception {
//        CLIENT.uploadGender(dataKey, inputData);
//        System.out.println(CLIENT.uploadEducationLevel(zaloId, inputData, false));
        Long genergateKey = DemographicPredictionKey.forEducationLevelOf(zaloId);
        ZUDM_UserEducationLevel ggender = (ZUDM_UserEducationLevel)WithCompression.getFrom(DEMO_GRAPHIC, genergateKey,new ZUDM_UserEducationLevel());
        System.out.println(ggender.toString());
    }

    private static void testuploadPhonebook(int dataKey, ZUDM_UserPhonebookName inputData) throws Exception {
//        CLIENT.uploadPhonebook(dataKey, inputData);
//        System.out.println(CLIENT.uploadPhonebook(dataKey, inputData));
        Long genergateKey = UserKwKey.forUserPhonebookOf(dataKey);
        System.out.println(genergateKey);
        ZUDM_UserPhonebookName ggender = (ZUDM_UserPhonebookName)WithCompression.getFrom(UserKWDB, genergateKey,new ZUDM_UserPhonebookName());
        System.out.println(ggender);
    }

    private static void testuploadUserIncome(int dataKey, ZUDM_UserIncome inputData) throws Exception {
//        CLIENT.uploadIncome(dataKey, inputData);
//        System.out.println(CLIENT.uploadIncome(dataKey, inputData));
        Long genergateKey = UserIncome.forOrginalKeyOf(dataKey);
        System.out.println(genergateKey);
        String result = UserIncomeDB.get(genergateKey).toString();
        System.out.println(result);
    }

    private static ZUDM_AgeRangeOfChild generateTListAgeRangeOfChild(String ageRange, short count) {
		ZUDM_AgeRangeOfChild ageRangeOfChild = new ZUDM_AgeRangeOfChild();
                    ageRangeOfChild.setAgeRangeOfChild(ageRange);
                ageRangeOfChild.setCount(count);
		return ageRangeOfChild;
	}
    
    private static ZUDM_BidirectionalPhonebook generateTListBidirectionalPhonebook(int des_id, String src_pb_name, String des_pb_name) {
		ZUDM_BidirectionalPhonebook bidirectionalPhonebook = new ZUDM_BidirectionalPhonebook();
		bidirectionalPhonebook.setDes_id(des_id);
                bidirectionalPhonebook.setSrc_pb_name(src_pb_name);
                bidirectionalPhonebook.setDes_pb_name(des_pb_name);
		return bidirectionalPhonebook;
	}
    
    private static void testUploadCreditScore(int dataKey, ZUDM_UserCreditScore inputData) throws Exception {
//        CLIENT.uploadCreditScoreV2(dataKey, inputData, false);
//        System.out.println(CLIENT.uploadCreditScoreV2(dataKey, inputData, true));
//        CreditScoreDB.mmultiRemove(listKey);
        Long genergateKey = DemographicPredictionKey.forCreditScoreV2Of(dataKey);
        System.out.println(genergateKey);
        ZUDM_UserCreditScore gCreditScore = (ZUDM_UserCreditScore)WithCompression.getFrom(CreditScoreDB, genergateKey,new ZUDM_UserCreditScore());
        System.out.println(gCreditScore);
    }
    
    private static void testRemoveCreditScore(int dataKey) throws Exception {
//        RemoveClient.removeCreditScoreKey(dataKey);
        System.out.println(RemoveClient.removeCreditScoreV3Key(dataKey,false));
        Long genergateKey = DemographicPredictionKey.forCreditScoreV2Of(dataKey);
        System.out.println(genergateKey);
        ZUDM_UserCreditScore gCreditScore = (ZUDM_UserCreditScore)WithCompression.getFrom(CreditScoreDB, genergateKey,new ZUDM_UserCreditScore());
        System.out.println(gCreditScore);
    }
    
    public static void main(String[] args) throws Exception {
//        ZUDM_UserIncome data = new ZUDM_UserIncome();
        int datakey = 100505810;
        
//        Long genergateKey = DemographicPredictionKey.forUserAgeOf(datakey);
//        ZUDM_UserAge gAge = (ZUDM_UserAge)WithCompression.getFrom(DemographicDB2, genergateKey,new ZUDM_UserAge());
//        System.out.println(gAge);
        
        
//        int hour = 2019082703;
//        byte id = (byte) 100;
//        double score = 11;
//        int date = 20190927;
//        data.setValue("123");
//        data.setConfidence(score);
//        data.setExpiryDate((short) date);
//        testuploadUserIncome(datakey, data);

        ZUDM_UserPhonebookName dataPB = new ZUDM_UserPhonebookName();
        List<String> Names = Arrays.asList("222");
        dataPB.setName(Names);
        
        ZUDM_UserPredictedGender dataGender = new ZUDM_UserPredictedGender();
        dataGender.setGender_id((byte ) 1);
        dataGender.setScore(0.5);
//        testuploadPhonebook(datakey, dataPB);
//        testUploadGenderV2(datakey, dataGender);

//        testUploadGender(datakey, dataGender);




//test upload UserHometown
//        ZUDM_UserHometown test = new ZUDM_UserHometown(); 
//        
//            TCountryCity f = new TCountryCity();
//            f.setCityId((short)11);
//            f.setCountry(TCountry.VIET_NAM);
//            test.setProvince(f);
//            test.setConfident(10);
//            CLIENT.uploadHometown(111, test);
//            Long genergateKey = DemographicPredictionKey.forUserHometownOf(114463932);
//            ZUDM_UserHometown ggender = (ZUDM_UserHometown)WithCompression.getFrom(DemographicDB2, genergateKey,new ZUDM_UserHometown());
//            System.out.println(ggender);
            

// test upload ParentalStatus
//            ZUDM_UserParentalStatus data = new ZUDM_UserParentalStatus();
//            data.setParentalStatus((short) 0);
//            data.setUpdatedDate((short) 2020);
//            data.addToAgeRangeOfChilds(generateTListAgeRangeOfChild("[0, 6)",(short) 1));
//            testUploadParentalStatus(datakey, data);

         
//            data.setAgeRangeOfChild((short)1);

//test upload EducationLevel
//            ZUDM_UserEducationLevel data = new ZUDM_UserEducationLevel();
//            data.setLevel((short)1);
//            data.setUpdatedDate((short) 2019);
//            testEducationLevel(datakey, data);

//test upload bBidirectionalPhonebook
//            ZUDM_BidirectionalPhonebookList data = new ZUDM_BidirectionalPhonebookList();
//            data.addToBidirectionalPhonebook(generateTListBidirectionalPhonebook(11, "a", "b"));
//            data.addToBidirectionalPhonebook(generateTListBidirectionalPhonebook(12, "c", "e"));
//            data.setUpdatedDate((short)2019);
//            testUploadBidirectionalPhonebook(datakey, data);

//    int dataKey = 145530547;
//    Long genergateKey = UserKwKey.forUserPhonebookOf(dataKey);
//    ZUDM_UserPhonebookName ggender = (ZUDM_UserPhonebookName)WithCompression.getFrom(UserKWDB, genergateKey,new ZUDM_UserPhonebookName());
//    System.out.println(ggender);

//        Long genergateKey = DemographicPredictionKey.forUserAgeOf(13451141);
//        ZUDM_UserAge gAge = (ZUDM_UserAge)WithCompression.getFrom(DemographicDB2, genergateKey,new ZUDM_UserAge());
//        System.out.println(gAge);
        
//        ArrayList<Long> listKey = new ArrayList<Long>();
//        listKey.add(DemographicPredictionKey.forUserAgeOf(274518560));
//        listKey.add(DemographicPredictionKey.forUserAgeOf(274518561));
//        HashMap<Long, ZUDM_UserAge> gAge = (HashMap<Long, ZUDM_UserAge>) WithCompression.multiGetFrom(DEMO_GRAPHIC, listKey, new ZUDM_UserAge());        
//        System.out.println(gAge);


//test realName
    int dataKey = 210016704;
    Long genergateKey = DemographicPredictionKey.forUserFullNameOf(dataKey);
    ZUDM_UserRealNameList realName = (ZUDM_UserRealNameList)WithCompression.getFrom(DemographicDB2, genergateKey,new ZUDM_UserRealNameList());
    System.out.println(realName);


//test ageV2
//    int dataKey = 278200605;
    ZUDM_UserAge age = new ZUDM_UserAge();
    age.setAge(24.0);
//    testUploadAge(dataKey, age);

//test CreditScore
//    int dataKey = 100056608;
    ZUDM_UserCreditScore cs = new ZUDM_UserCreditScore();
//    cs.setValue(0.07026241485998687);
//    cs.setExpiryDate((short)2451);
//    testUploadCreditScore(dataKey, cs);
    
//test income
//        int dataKey = 100001340;
//    ZUDM_UserCreditScore cs = new ZUDM_UserCreditScore();
//    cs.setValue(0.017093336228946862);
//    testUploadCreditScore(dataKey, cs);


//test RemoveCreditScore
//    int dataKey = 3321346;
//    testRemoveCreditScore(dataKey);


    
//    String incomeData = getLas(String.valueOf(183790731));
//    System.out.println(incomeData);


//test maritalStatus
//    int dataKey = 100505810;
//    ZUDM_UserMaritalStatus data = new ZUDM_UserMaritalStatus();
//    testUploadMaritalStatus(dataKey, data);
                                        
    }
    
    public static String getLas(String uid) {

        if (uid.equals("uid")) {
            return "las";
        }

        if (uid.equals("0")) {
            return "";
        }

        try {


            long key = Long.valueOf(uid) ;
            ZUDM_UserIncome visited = (ZUDM_UserIncome)WithCompression.getFrom(UserIncomeDB, (long) key,new ZUDM_UserIncome());
            
            
            String passphrase = String.valueOf(key * key).substring(0, 10);
            
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(passphrase.getBytes());
            String encrkey = new String(messageDigest.digest());
            
            return CryptoUtil.decryptF(visited.getValue(), encrkey) + "";

        } catch (Exception e) {
            e.printStackTrace();
        }

        return "";
    }
        
}
