package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zing.centralozeid.common.GlobalIdResult;
import com.vng.zing.centralozeid.common.ZCentralizeCommon;
import com.vng.zing.jni.ClassLoaderUtil;
import com.vng.zing.zcentralizeid.thrift.ZPlatform;

/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

/**
 * @date Oct 9, 2019
 * @author quydm
 */
public class GidCheck {
    static {
        ClassLoaderUtil.loadCommonLibs();
    }
    public static void main(String[] args) {
        
//        Convert uid to gid
//        long gid =  360287970393531513l;
//        GlobalIdResult res = ZCentralizeCommon.decodeGlobalID(gid);
//        System.out.println(res.getId()+"\t"+res.getCreatedTime());
        
//        Convert gid to uid
        long uid = 100505810;
        GlobalIdResult res = ZCentralizeCommon.decodeGlobalID(uid);
        Long LongGid = ZCentralizeCommon.encodeIntoGlobalId(uid, ZPlatform.ZP_Zalo);
        System.out.println(LongGid);
    }
}
