package com.vng.zalo.zte.rnd.dbingestionmw.model;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScore;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScoreV2;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandScore;
import com.vng.zalo.zte.rnd.dbingestionmw.client.BankLeadDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandTag;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.BankLeadKey;
/**
 *
 * @author tinhdt
 */
public class TestBankLead {
    private final static BankLeadDBMWClient DBCLIENT = new BankLeadDBMWClient();
    private static final ZiDB64Client BANKLEAD = new ZiDB64Client("ZUDMBankLead_ZiDB");
    
    public static void main(String[] args) throws Exception {
//        ZUDM_LoanDemandScore data = new ZUDM_LoanDemandScore();
//        ZUDM_LoanDemandScore re = new ZUDM_LoanDemandScore();
//        ZUDM_DisburseLeadScoreV2 data = new ZUDM_DisburseLeadScoreV2();
//        data.setScore(0.11517000687268149);
//        data.setTag(8);
//        data.setUpdatedDate((short) 132128886);
        int dataKey = 127024666;
//        TActionResult ar = DBCLIENT.uploadDisburseLeadScore(dataKey, data, true);
//        System.out.println(ar.toString());
        long GenKey = BankLeadKey.forDisburseleadShKeyOf(dataKey);
//        System.out.println(GenKey);
////        DBCLIENT.uploadDisburseLeadShScore(dataKey, data, false);
        ZUDM_DisburseLeadScoreV2 rel = (ZUDM_DisburseLeadScoreV2) WithCompression.getFrom(BANKLEAD, GenKey,new ZUDM_DisburseLeadScoreV2());
        System.out.println(rel);


//        ZUDM_LoanDemandTag data = new ZUDM_LoanDemandTag();
//        data.setTag(21);
//        DBCLIENT.uploadLoanDemandTag(dataKey,data,false);
//        System.out.println(DBCLIENT.uploadLoanDemandTag(dataKey,data,false));
//        ZUDM_LoanDemandTag rel = (ZUDM_LoanDemandTag) WithCompression.getFrom(BANKLEAD, GenKey,new ZUDM_LoanDemandTag());
//        System.out.println(rel);
    }
}
