package com.vng.zalo.zte.rnd.dbingestionmw.model;
import com.vng.zalo.zte.rnd.dbingestionmw.client.LocationDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.*;
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.DemographicPredictionKey;
import com.vng.zing.zuserdatamining.thrift.ZUDM_LivingLoc;
import org.apache.log4j.Logger;

import java.nio.ByteBuffer;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cpu11232
 */
public class TestLocationMining {
    public static final LocationDBMWClient Location = new LocationDBMWClient("127.0.0.1", 18782);
    private static final Class MAIN_CLASS = testLocationDB.class;
    private static final Logger LOGGER = ZLogger.getLogger(MAIN_CLASS);
    public static final ZiDB64Client MostVisitedAADB = new ZiDB64Client("ZiDB64LivingLocation");
    public static final ZiDB64Client _DBCmon3Cli = new ZiDB64Client("zudmcommon3");
//    private static final ZiDBClient _ZiDBTravelPathCity = new ZiDBClient("ZUDM_ZiDBTravelPathCity");
//    public static final ZiDBClient _DBClientTravelPathCity = new ZiDBClient("ZUDM_ZiDBTravelPathCity");
    

    public static void main(String[] args) throws Exception {
//        ZUDM_MostVisitedArea data = new ZUDM_MostVisitedArea();
//        ZUDM_MostVisitedArea sample = new ZUDM_MostVisitedArea();
//        data.setAdministrativeLocID(100);
//        data.setUpdatedDate(20191002);
//        int id = 1111;
//        System.out.println(Long.valueOf(id));
//        Location.ingestMostVisitedAdminArea(id, data);
//        System.out.println(Location.ingestMostVisitedAdminArea(id, data));
//        TValueResult d = ZUDM_Util.getWithProfiler(MAIN_CLASS, "ingestMostVisitedAdminArea", MostVisitedAADB, 100001113l);
//        ZUDM_MostVisitedArea usr = ZUDM_Util.getTbase(LOGGER, new ZUDM_MostVisitedArea(), d);
//        System.out.println(usr);

        ZUDM_LivingLoc data = new ZUDM_LivingLoc();
        data.setId(9);
        data.setNCheckIn(1224);
//        Location.ingestLivingProvince(139939754, data);
//        System.out.println(Location.ingestLivingProvince(156062122, data));
//        System.out.println(LocationMining.forLivingProvinceOf(156062122));
//        ZUDM_LivingLoc result = (ZUDM_LivingLoc)WithCompression.getFrom(_DBCmon3Cli, LocationMining.forLivingProvinceOf(181795235), data);
//        TValueResult tValueResult = ZUDM_Util.getWithProfiler(MAIN_CLASS, "livigProv.DBCmon3Cli.get", _DBCmon3Cli, 156062122l);
//        ZUDM_LivingLoc oldLivingLoc = ZUDM_Util.getTbase(LOGGER, new ZUDM_LivingLoc(), tValueResult);
//        System.out.println(oldLivingLoc.id);
   
        

            ByteBuffer key = ByteBufferUtil.fromString("139939754");
//            com.vng.zing.zidb.thrift.TValueResult valres = ZUDM_Util.getWithProfiler(MAIN_CLASS, "updateDB.ZiDBHomeWorkLocation.get", _ZiDBHomeWorkLocation, key);
//            ZUDM_ListLocationHomeWork old_result = ZUDM_Util.getTbase(LOGGER, new ZUDM_ListLocationHomeWork(), valres);
//            System.out.println(old_result);
            ZUDM_LocationHomeWork home = new ZUDM_LocationHomeWork();
            ZUDM_LocationHomeWork work = new ZUDM_LocationHomeWork();

//            for (ZUDM_LocationHomeWork loc : old_result.getListLocationHomeWork()) {
//                if (loc.type == ZUDM_LocationType.Home) {
//                    home = loc;
//                } else if (loc.type == ZUDM_LocationType.Work) {
//                    work = loc;
//                }
//            }

            home.setLat(0);
            home.setLon(0);
            home.setScore(0.9970484840888645);
            home.setAdministrativeAreaId(40080000);
            home.setType(ZUDM_LocationType.Home);

            work.setLat(43.65638184130675);
            work.setLon(-79.38061932833915);
            work.setScore(1.6446067811865472);
            work.type = ZUDM_LocationType.Work;

            ZUDM_ListLocationHomeWork toput = new ZUDM_ListLocationHomeWork();
            
//            toput.addToListLocationHomeWork(work);
            toput.addToListLocationHomeWork(home);
//            toput.addToListLocationHomeWork(work);
            

//            System.out.println(Location.ingestPartialHomeWorkLocation(100010719 , toput));
            
            ByteBuffer keys = ByteBufferUtil.fromString("134645080");
//            com.vng.zing.zidb.thrift.TValueResult valress = ZUDM_Util.getWithProfiler(MAIN_CLASS, "updateDB.ZiDBHomeWorkLocation.get", _ZiDBHomeWorkLocation, keys);
//            ZUDM_ListLocationHomeWork old_results = ZUDM_Util.getTbase(LOGGER, new ZUDM_ListLocationHomeWork(), valress);
//            System.out.println(old_results);
//            
            
            
        int id = 241420204; 
        ZUDM_UserInterestLocationCategory sub = new ZUDM_UserInterestLocationCategory();
        sub.setValue(12);
        sub.setConfidence(1);
        ZUDM_UserInterestLocation dataT = new ZUDM_UserInterestLocation();
        dataT.addToInterest_category(sub);
        dataT.setUpdateDate(2019);
//        Location.IngestUserSSIDInterest(id, dataT, false);
//        System.out.println(Location.ingestUserSSIDInterest(id, dataT, false));
//        long genergateKey = LocationMining.forUserSSISInterestOf(id);
//        System.out.println(genergateKey);
//        ZUDM_UserInterestLocation result = (ZUDM_UserInterestLocation)WithCompression.getFrom(DBClients.LOCATIONINTEREST, genergateKey,new ZUDM_UserInterestLocation());
//        System.out.println(result);



        ZUDM_ForeignTripCount Data = new ZUDM_ForeignTripCount();
        Data.setForeignTripCount((short)1);
//        Location.ingestForeignTripCount(111, Data);
//        System.out.println(Location.ingestForeignTripCount(1212, Data));
        long genergateKey = DemographicPredictionKey.forForeignTripCountOf(116476989);
        ZUDM_ForeignTripCount result = (ZUDM_ForeignTripCount)WithCompression.getFrom(DBClients.DemographicDB2, genergateKey,new ZUDM_ForeignTripCount());
        
        System.out.println(result);
        
//        ZUDM_MostVisitedProvinces visited = (ZUDM_MostVisitedProvinces) WithCompression.getFrom(DBClients.DemographicDB2, DemographicPredictionKey.forMostVisitedProvinceOf(100000682), new ZUDM_MostVisitedProvinces());
//        System.out.println(visited);
    
//        int ip = ZUDM_Util.IPv4TextToI32("14.167.114.148");
//        long IPKey = ZUDM_Util.buildListIpLocationKey(ip);
//        TValueResult val = ZUDM_Util.getWithProfiler(MAIN_CLASS, "IPLoc.get", DBClients._DBCmon5Cli, IPKey);
//        ZUDM_MapIpLocation mapLocation = ZUDM_Util.getTbase(LOGGER, new ZUDM_MapIpLocation(), val);
//        System.out.println(mapLocation);
    }
}

// try {
//            LocationDBMWClient client = new LocationDBMWClient();
//            ZUDM_LivingLoc livingLoc = new ZUDM_LivingLoc(1, (int) 123);
//            TActionResult a = client.ingestLivingProvince(156062122, livingLoc);
//            
//            System.out.println(a);
//            
//            System.out.println(ZErrorHelper.errorToString(a.getValue()));
//            
//            System.exit(0);
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }
//
//    }
//}
 
