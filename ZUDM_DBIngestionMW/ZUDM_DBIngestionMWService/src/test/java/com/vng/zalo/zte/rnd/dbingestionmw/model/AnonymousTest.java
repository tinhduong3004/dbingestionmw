package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TAge;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictAgeGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedAgeRange;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedGenderV2;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedProvince;
import com.vng.zalo.zte.rnd.dbingestionmw.client.AnonymousUserDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.centralozeid.common.ZCentralizeCommon;
import com.vng.zing.zcentralizeid.thrift.ZPlatform;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.AnonymousUserKey;

/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

/**
 * @date Feb 13, 2019 
 * @author quydm
 */
public class AnonymousTest {
    private static final String THRIFT_IP = "10.30.65.168";
//    private static final String THRIFT_IP = "127.0.0.1";
    private static final int THRIFT_PORT = 18782;
    static long GENDER_TYPE = 1000000000000000000L;
    static long ANONYMOUS_DEVICE = 4000000000000000000L;

    static long AGE_TYPE = 1100000000000000000L;
    static long PROVINCE_TYPE = 1200000000000000000L;
    static long DEVICE_TYPE = 1300000000000000000L;
    static long HISTORY_TYPE = 1400000000000000000L;
    
    static AnonymousUserDBMWClient mwClient = new AnonymousUserDBMWClient();
    static ZiDB64Client dbClient = new ZiDB64Client("ZUDM_ZiDB64AnonymousUser");
    static ZiDB64Client dbClient2 = new ZiDB64Client("ZUDM_ZiDB64AnonymousUser2");
    static ZiDB64Client dbClientTrace = new ZiDB64Client("ZUDM_ZiDB64AnonymousUserTrace");
    static ZiDB64Client dbClientUnstable= new ZiDB64Client("UnstableAnonymousUser");
    public static void main(String[] args) throws Exception {
//        TValueResult holder = dbClient.get(4216172783233432445L);
        
//        TListTrackedApp app = new TListTrackedApp(Arrays.asList(new TTrackedAppInfo("1", "2", "3")));
        TPredictAgeGender pAgeGender = new TPredictAgeGender();
        pAgeGender.setAge(2);
	pAgeGender.setGender(1);
//        TActionResult ac = mwClient.ingestBMPredictedAgeGender(72057594703725490l, pAgeGender);
//        System.out.println(ac.toString());
//        System.out.println(AnonymousUserKey.forAgeGenderOf(72057595434276625l));
//        System.out.println(dbClient.get(AnonymousUserKey.forAgeGenderOf(72057595434276625l)));
//        TPredictAgeGender pDevice = (TPredictAgeGender)WithCompression.getFrom(dbClient, AnonymousUserKey.forAgeGenderOf(72057595434276625l), new TPredictAgeGender());
//        System.out.println(pDevice.toString());
        
//        TListDevice rel = (TListDevice)WithCompression.getFrom(dbClient, AnonymousUserKey.forDeviceOf(216172783651517206l), new TListDevice());
//        System.out.println(rel);
          Long LongGid = ZCentralizeCommon.encodeIntoGlobalId((long) 132622122, ZPlatform.ZP_Web);
//          System.out.println(LongGid);
//        TActionResult ac = cClient.uploadCustomAudience(99999, app);
        TPredictedGender gender = new TPredictedGender();
        gender.setValue(TGender.MALE);
        gender.setExpiryDate("20200117");
        gender.setError(0);
        
//        long key = 72057594418106760l;
//        TPredictedGenderV2 ggender = (TPredictedGenderV2)WithCompression.getFrom(dbClient,AnonymousUserKey.forShiftedPredictedGenderOf(key), new TPredictedGenderV2());
//        System.out.println(ggender);

//        long key = 72057608232818370l;
//        TPredictedProvince ggender = (TPredictedProvince)WithCompression.getFrom(dbClient,AnonymousUserKey.forProvinceOf(key), new TPredictedProvince());
//        System.out.println(ggender);
//        TActionResult ar = mwClient.ingestAUPredictedGenderV2(key, gender);
//        System.out.println(ar.toString());
        
//        TPredictedGender ggender = (TPredictedGender)WithCompression.getFrom(dbClient,AnonymousUserKey.forGenderV2Of(key), new TPredictedGender());
//        System.out.println(ggender);
        
//        253478185
        
//        System.out.println(ac.toString());
        
//        long globalid = 216172783324095617L;
//        genderPut(globalid);
//        agePut(globalid);
//        provincePut(globalid);
//        historyPut(globalid);
//        anonymousGet(globalid);
//        System.out.println(holder);
//        provinceUnstableGet();

    TPredictedAgeRange data = new TPredictedAgeRange();
//    data.setValue(TAge.FROM_25_TO_34);
    testUploadAgeRange(72057608253409237l, data);
    
    
    }
//    static void anonymousGet(long globalid) throws Exception{
//        
////        TListDevice devices = new TListDevice();
////        TDeviceInfo device = new TDeviceInfo();
////        device.setBrand("A");
////        device.setName("A");
////        device.setOs("A");
////        device.setOsVersion("A");
////        device.setLatestLogin("A");
////        devices.setDevices(Arrays.asList(device));
////        devices.setUpdatedDate("2019");
//        
//        long key = globalid + ANONYMOUS_DEVICE;
////        TActionResult ar = mwClient.uploadDeviceLoginAnonymous(key, devices);
////        System.out.println(ar.toString());
//        
//        
//        System.out.println(key);
//        
//        System.out.println(pDevice.toString());
//    }
//    static void genderPut(long globalid) throws Exception{
//        TPredictedGender gender = new TPredictedGender();
//        gender.setValue(TGender.MALE);
//        gender.setExpiryDate("20200117");
//        gender.setError(0);
//        
//        long key = 72057594037927941l;
//        TActionResult ar = mwClient.ingestAUPredictedGenderV2(key, gender);
//        System.out.println(ar.toString());
//        
//        TPredictedGender ggender = (TPredictedGender)WithCompression.getFrom(dbClient,AnonymousUserKey.forGenderV2Of(key), new TPredictedGender());
//        System.out.println(ggender);
//    }
//    static void agePut(long globalid) throws Exception{
//        TPredictedAge age = new TPredictedAge();
//        age.setValue(26);
//        age.setExpiryDate("20190214");
//        age.setError(0);
//        
//        long key = globalid + GENDER_TYPE;
//        TActionResult ar = mwClient.ingestAUPredictedAge(key, age);
//        System.out.println(ar.toString());
//        
//        TPredictedAge gage = (TPredictedAge)WithCompression.getFrom(dbClient, key, age);
//        System.out.println(gage.toString());
//    }
//    static void provincePut(long globalid) throws Exception{
//        TPredictedProvince province = new TPredictedProvince();
//        province.setId(4);
//        province.setExpiryDate("20190214");
//        province.setError(0);
//        
//        long key = globalid + GENDER_TYPE;
//        TActionResult ar = mwClient.ingestUAUProvince(key, province);
//        System.out.println(ar.toString());
//        
//        TPredictedProvince gpro = (TPredictedProvince)WithCompression.getFrom(dbClientUnstable, key, province);
//        System.out.println(gpro.toString());
//    }
//    
//    static void provinceUnstableGet() throws Exception{
//        long key = 3072057607509824967L;
//        TPredictedProvince gpro = (TPredictedProvince)WithCompression.getFrom(dbClientUnstable, key, new TPredictedProvince());
//        System.out.println(gpro.toString());
//    }
//    
//    static void historyPut(long globalid) throws Exception{
//        TListHistory history = new TListHistory();
//        ArrayList sampleHistory = new ArrayList();
//        sampleHistory.add("12341234123_29_01_2019_test");
//        sampleHistory.add("12341234123_30_01_2019_test");
//        sampleHistory.add("12341234123_01_01_2019_test");
//        sampleHistory.add("12341234123_02_01_2019_test");
//        history.setValue(sampleHistory);
//        history.setExpiryDate("20190214");
//        history.setError(0);
//        
//        long key = globalid + HISTORY_TYPE;
//        TActionResult ar = mwClient.ingestAUListHistory(key, history);
//        System.out.println("123123"+ar.toString());
//        
//        TListHistory gpro = (TListHistory)WithCompression.getFrom(dbClientTrace, key, history);
//        System.out.println(gpro.toString());
//    }
    
    
//    test AUPredictedAgeRange
    private static void testUploadAgeRange(long dataKey, TPredictedAgeRange inputData) throws Exception {
//        mwClient.ingestAUPredictedAgeRange(dataKey, inputData, false);
//        System.out.println(mwClient.ingestAUPredictedAgeRange(dataKey, inputData,false));
        Long genergateKey = AnonymousUserKey.forAgeRangeOf(dataKey);
        TPredictedAgeRange age = (TPredictedAgeRange)WithCompression.getFrom(dbClient2, genergateKey,new TPredictedAgeRange());
        System.out.println(age);
    }
}
