/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_IP2Location;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.strlist32bm.thrift.wrapper.StrList32bmClient;
import com.vng.zing.zidb64.thrift.TValueResult;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.TimeInDayUtil;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.DemographicPredictionKey;
import com.vng.zing.zudm.common.centralizedkeys.Ip2LocationKey;
import com.vng.zing.zudm.common.ZUDM_Util;
import com.vng.zing.zudm.common.centralizedkeys.ip.IPBitSet;
import com.vng.zing.zudm.common.centralizedkeys.ip.IPBitSetResult;
import com.vng.zing.zudm.common.centralizedkeys.ip.IPDatabase;
import com.vng.zing.zudm.common.centralizedkeys.ip.IPDatabaseV2;
import com.vng.zing.zuserdatamining.thrift.ZUDM_MapIpLocation;
import com.vng.zing.zuserdatamining.thrift.ZUDM_UserCreditScore;
import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 *
 * @author tinhdt
 */
public class Ip2LocationTest {

    private static final Class _ThisClass = Ip2LocationTest.class;
    private static final Logger _Logger = ZLogger.getLogger(_ThisClass);
    private static final ZiDB64Client DBCmon5Cli = new ZiDB64Client("zudmcommon5");
    private static IPBitSet ipBitSet = new IPBitSet();

    private static void testIP2LocationV2(String ip) throws Exception {
        long key = Ip2LocationKey.forIp2LocationV2Of(ip);
        System.out.println(key);
        TValueResult valres = ZUDM_Util.getWithProfiler(_ThisClass, "IPLoc.get", DBCmon5Cli, key);
        ZUDM_IP2Location Location = ZUDM_Util.getTbase(_Logger, new ZUDM_IP2Location(), valres);
//        ZUDM_MapIpLocation Location = (ZUDM_MapIpLocation)WithCompression.get(DBCmon5Cli, key,new ZUDM_MapIpLocation());
        System.out.println(Location);
    }


    public static void main(String[] args) throws Exception {
        String ip = "1.52.10.92";
//        testIP2LocationV2(ip);

        StrList32bmClient client = new StrList32bmClient("ipbmcommon6read");
        IPBitSetResult ipBitSetResult = IPDatabaseV2.Instance.getAllIPs(client);
        ipBitSet = ipBitSetResult.getIpBitSet64();
//        System.out.println(ipBitSet.getNegIPs());
        System.out.println(ipBitSet.size());
//        ipBitSet.size();
        System.out.println("Done");
        
//        com.vng.zing.strlist32bm.thrift.TValueResult values = client.get("NegIPsKey");
//        ArrayList<Integer> listEntries = new ArrayList<>();
//        listEntries.addAll(values.getValue().entries);
//        System.out.println(listEntries);
//        System.out.println(client.getCountEntries("NegIPsKey"));
//        System.out.println(client.getCountEntries("PosIPsKey"));
    }



}
