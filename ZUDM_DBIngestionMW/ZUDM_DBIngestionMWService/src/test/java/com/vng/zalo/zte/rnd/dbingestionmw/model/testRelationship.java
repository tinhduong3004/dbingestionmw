package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.client.RelationshipDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.relationship.thrift.ZUDM_RelationshipInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.relationship.thrift.ZUDM_RelationshipList;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.RelationshipKey;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author tinhdt
 */
public class testRelationship {

    private static final RelationshipDBMWClient CLIENT = new RelationshipDBMWClient("127.0.0.1",18782);
    public static final ZiDB64Client relationShipDB = new ZiDB64Client("relationship");
   public static int count  = 0; 

    private static void testUploadRelationship(int dataKey, ZUDM_RelationshipList inputData) throws Exception {
//        CLIENT.uploadRelationship(dataKey, inputData);
//        System.out.println(CLIENT.uploadRelationship(dataKey, inputData));
        Long genergateKey = RelationshipKey.forListRelationshipOf(dataKey);
        ZUDM_RelationshipList relationship = (ZUDM_RelationshipList) WithCompression.getFrom(relationShipDB, genergateKey, new ZUDM_RelationshipList());
        System.out.println(relationship);
        System.out.println(genergateKey);
        relationship.relationships.forEach(f -> {
            count = count + 1;
        });
        System.out.println(count);
        System.out.println(relationship.getUpdatedDate());
        
    }

    public static void main(String[] args) throws Exception {

//test Relationship
        int uid = 70610499;
        ZUDM_RelationshipList relationshipList = new ZUDM_RelationshipList();
        ZUDM_RelationshipInfo relationship = new ZUDM_RelationshipInfo();
        relationship.setDes_id(103457961);
        relationship.setRelationship_code(1000);
        relationshipList.addToRelationships(relationship);
        testUploadRelationship(uid, relationshipList);
    }
}
