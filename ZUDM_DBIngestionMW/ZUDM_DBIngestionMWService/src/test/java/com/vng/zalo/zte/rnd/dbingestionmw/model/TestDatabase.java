package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedProvince;
import static com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients.ANONYMOUSUSER_DB;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.AnonymousUserKey;

/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

/**
 * @date Dec 20, 2018
 * @author quydm
 */
public class TestDatabase {
    private static final ZiDB64Client DB_USERKEYWORDS = new ZiDB64Client("ZUDM_ZiDB64UserKeyword");
    private static final ZiDB64Client AnonymousUser = new ZiDB64Client("ZUDM_ZiDB64AnonymousUserTrace");
    public static void main(String[] args) {
//        long key = 3100000002534010406L;
//        TUserKeywords userKw = (TUserKeywords)WithCompression.getFrom(DB_USERKEYWORDS, key, new TUserKeywords());
        System.out.println(AnonymousUser.ping());
        long uid = 72057607735162611L;
        TPredictedProvince living = (TPredictedProvince)WithCompression.getFrom(ANONYMOUSUSER_DB,  AnonymousUserKey.forProvinceOf(Long.valueOf(uid)),  new TPredictedProvince());
        System.out.println(living.toString());
    }
}
