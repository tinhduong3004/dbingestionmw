package com.vng.zalo.zte.rnd.dbingestionmw.model;
import com.vng.zalo.zte.rnd.dbingestionmw.client.ElasticSearchMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.KeywordInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.KeywordInfoList;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TStringSearchResult;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ngocbk
 */
public class TestESUploadUserKW {

    private static ElasticSearchMWClient ESCLIENT = new ElasticSearchMWClient();

    public static void main(String[] args) {
        KeywordInfo kw_info = new KeywordInfo();
        kw_info.setArticle_count(1);
        kw_info.setKw_count(1);
        kw_info.setValue("fortest");

        List<KeywordInfo> tlist = new ArrayList();
        tlist.add(kw_info);

        KeywordInfoList kw_list = new KeywordInfoList();
        kw_list.setList_kw(tlist);
        kw_list.setUid(88);
        kw_list.setUpdateOn("2020070623");
        try {
            TStringSearchResult res = ESCLIENT.ESIngestUserKeyword("userkw", kw_list, false
            );
            System.out.println(res.getResult());
        }catch(Exception e){
            System.out.println(e);
        }
    }
}
