package com.vng.zalo.zte.rnd.dbingestionmw.model;// Created By PTP
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb.thrift.TValueResult;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.ZUDM_Util;
import com.vng.zing.zuserdatamining.thrift.ZUDM_WifiInfo;
import java.nio.ByteBuffer;
import org.apache.log4j.Logger;
//import com.vng.zing.zuserdatamining.thrift.ZUDM_MostVisitedProvinces;
//import com.vng.zing.zuserdatamining.thrift.ZUDM_UserVisitedProvince;
/**
 *
 * @author phucpt2
 */
public class testGetDB {
    
    private static final Class THIS_CLASS = testGetDB.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    
    public static final ZiDB64Client DemographicDB = new ZiDB64Client("ZUDM_ZiDB64DemographicDB");
    
    public static final ZiDBClient _ZiDBWifiInfo = new ZiDBClient("ZiDBWifiInfo");
    public static void main(String[] args) {
        ByteBuffer key = ByteBufferUtil.fromString("6a2076ec10a69b5f8da2a2bfd8ede8b5");
        TValueResult a = _ZiDBWifiInfo.get(key);
        
        ZUDM_WifiInfo usrpath = ZUDM_Util.getTbase(LOGGER, new ZUDM_WifiInfo(), a);
        
        System.out.println(usrpath);
        
//        TCountryCity city2 = new TCountryCity();
//            city.setCountry(TCountry.VIET_NAM);
//            city.setCityId((short)45);
//        ZUDM_UserVisitedProvince f = new ZUDM_UserVisitedProvince();
//            f.setCount((short)234);
//            f.setProvince(city2);
//            test.addToProvinces(f);
//        long error = WithCompression.putTo(DemographicDB, userId, inputData, LOGGER, THIS_CLASS);
        
        System.exit(0);
    }
}
