package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.client.UserOccupationDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_CompanyInfoV2;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyListV2;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.UserOccupationKey;
import com.vng.zing.zuserdatamining.thrift.ZUDM_OccupationInfo;
import com.vng.zing.zuserdatamining.thrift.ZUDM_UserOccupationList;
import org.apache.log4j.Logger;

/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

/**
 * @date Apr 16, 2019
 * @author quydm
 */
public class Occupation {
    private static UserOccupationDBMWClient OccupationClient = new UserOccupationDBMWClient();
     private static long LongTestKey = 9000000000000000000L;
    private static String StringTestKey = "9000000000000000000";
    public static final ZiDB64Client _OccupationDB = new ZiDB64Client("ZUDM_ZiDB64Occupation");
    private static final Logger LOGGER = ZLogger.getLogger(Occupation.class);
    
    private static void testUploadUseCompany (int dataKey, ZUDM_UserCompanyListV2 inputData) throws Exception {
//        OccupationClient.uploadUserCompanyV2(dataKey, inputData, false);
//        System.out.println(OccupationClient.uploadUserCompanyV2(dataKey, inputData, false));
        Long genergateKey = UserOccupationKey.forUserCompanyOf(dataKey);
        ZUDM_UserCompanyListV2 userCompany = (ZUDM_UserCompanyListV2)WithCompression.getFrom(_OccupationDB, genergateKey,new ZUDM_UserCompanyListV2());
        System.out.println(userCompany);
    }
    
    
    private static ZUDM_OccupationInfo generateTListAudience(int error, int appID, double score) {
		ZUDM_OccupationInfo pAucience = new ZUDM_OccupationInfo();
		pAucience.setOccupationId(appID);
		pAucience.setScore(score);
		pAucience.setError(error);
		return pAucience;
	}
    public static void main(String[] args) throws Exception {
//        OccupationClient.uploadUserOccupation(StringTestKey, new ZUDM_UserOccupationList());
    ZUDM_UserOccupationList plistAudience = new ZUDM_UserOccupationList();
    plistAudience.addToOccupations(generateTListAudience(0,11 , 3.5698));
//    TActionResult ar = OccupationClient.uploadUserStudentOccupation(11, plistAudience);
//    System.out.println(ar.toString());
    long Key = UserOccupationKey.forUserUnionOf(128545085);
//        System.out.println(Key);
//    TValueResult result = _OccupationDB.get(Key);
    ZUDM_UserOccupationList ggender = (ZUDM_UserOccupationList)WithCompression.getFrom(_OccupationDB, Key, plistAudience);
    System.out.println(ggender);

    int uid = 115541471;
    ZUDM_CompanyInfoV2 info = new ZUDM_CompanyInfoV2();
    info.setOrganization("101eyewear.com");
    info.setScore(1.0);
    ZUDM_UserCompanyListV2 list = new ZUDM_UserCompanyListV2();
    list.addToCompanys(info);
//    testUploadUseCompany(uid, list);
    }
    
    
}
