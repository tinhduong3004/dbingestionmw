package com.vng.zalo.zte.rnd.dbingestionmw.model;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TArtistInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TKeywordInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserInterestArtists;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserKeywords;
import com.vng.zalo.zte.rnd.dbingestionmw.client.ArticleInterestDBMWClient;
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb.thrift.TValueResult;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.UserInterestKey;
import java.nio.ByteBuffer;
import org.apache.log4j.Logger;
import com.vng.zing.zudm.common.ZUDM_Util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tinhdt
 */
public class TestUserInterest {
//    private static final CacheMWClient CACHE = new CacheMWClient("127.0.0.1",18782);
    private static final Class MAIN_CLASS = TestUserInterest.class;
    private static final Logger LOGGER = ZLogger.getLogger(MAIN_CLASS);
    private static final ArticleInterestDBMWClient CLIENT = new ArticleInterestDBMWClient();
    private static final ZiDB64Client ZINGMP3 = new ZiDB64Client("ZUDM_ZiDB64UserKWDB");
    public static final ZiDB64Client USERKW = new ZiDB64Client("ZUDM_ZiDB64UserKWDB");
    public static final ZiDBClient ANONYMOUSUSERINTEREST = new ZiDBClient("ZUDMAnonymousUserInterest_ZiDB");

    
    private static TKeywordInfo genergateTKeywordInfo (String value, int score, int type) {
        TKeywordInfo data = new TKeywordInfo();
        data.setValue(value);
        data.setScore(score);
        data.setType(type);
        return data;
    }
    
//    private static TUserKeywords genergateTUserKeywords (int id, String score) {
//        TUserKeywords data = new TUserKeywords();
//        data.setUserId(id);
//        data.setUpdatedOn(score);
//        data.setKeywords((List<TKeywordInfo>) genergateTKeywordInfo("success", 111, 222));
//        return  data;
//    }
    
    private static void testUploadUserKeywords (int dataKey, int hour, TUserKeywords inputData) throws Exception {

//        CLIENT.uploadUserInterest(dataKey,hour, inputData);
//        System.out.println(CLIENT.uploadUserInterest(dataKey,hour, inputData));
        String genergateKey = String.valueOf(UserInterestKey.forUserInterestOf(dataKey, 23));
        String result = ZINGMP3.get(Long.parseLong(genergateKey)).toString();
        TUserKeywords ggender = (TUserKeywords)WithCompression.getFrom(ZINGMP3, UserInterestKey.forUserInterestOf(dataKey, hour), new TUserKeywords() );
        System.out.println(ggender);
        System.out.println(genergateKey);
//        System.out.println(CACHE.GetTimeCache("uploadUserKeywords"));
//        System.out.println(result);
    }
    
    private static void testUploadUserArtists (int dataKey, TUserInterestArtists inputData) throws Exception {

//        CLIENT.uploadUserInterestArtists(dataKey, inputData, false);
//        System.out.println(CLIENT.uploadUserInterestArtists(dataKey, inputData, false));
        TUserInterestArtists ggender = (TUserInterestArtists)WithCompression.getFrom(USERKW, UserInterestKey.forUserInterestArtistsOf(dataKey), new TUserInterestArtists() );
        System.out.println(ggender);
    }
    
    private static void testUploadAnonymousUserInterest (long dataKey, TUserKeywords inputData) throws Exception {

//        CLIENT.uploadAnonymousUserInterest(dataKey, inputData);
//        System.out.println(CLIENT.uploadAnonymousUserInterest(dataKey, inputData));
//        TUserKeywords ggender = (TUserKeywords)WithCompression.getFrom(ANONYMOUSUSERINTEREST, UserInterestKey.forAnonymousUserInterestOf(dataKey), new TUserKeywords() );
//        System.out.println(ggender);
        ByteBuffer key = ByteBufferUtil.fromString(String.valueOf(dataKey));
        TValueResult rel = ANONYMOUSUSERINTEREST.get(key);
        TUserKeywords usr = ZUDM_Util.getTbase(LOGGER, new TUserKeywords(), rel);
        System.out.println(usr);
//        System.out.println(CACHE.GetTimeCache("uploadUserKeywords"));
//        System.out.println(result);
    }
    
    public static void main(String[] args) throws Exception {
        TUserKeywords data = new TUserKeywords();
        int datakey = 100012801;
        int hour = 23;
        int id = 0;
        String score = "2";
        data.setUserId(id);
        data.setUpdatedOn(score);
        
//        data.setKeywords((List<TKeywordInfo>) genergateTKeywordInfo("success", 111, 222));
        data.addToKeywords(genergateTKeywordInfo("2010", 6, 4));
//        testUploadUserKeywords(datakey, hour, data);
//        testUploadAnonymousUserInterest(datakey, data);
//        String result = ZINGMP3.get(7120000000000010023l).toString();
//        System.out.println(result);

        TUserKeywords da = new TUserKeywords();
        TKeywordInfo In = new TKeywordInfo();
        In.setScore(10);
        In.setValue("40300");
        In.setType(1);
        TKeywordInfo In2 = new TKeywordInfo();
        In2.setScore(10);
        In2.setValue("40100");
        In2.setType(1);
        TKeywordInfo In3 = new TKeywordInfo();
        In3.setScore(10);
        In3.setValue("40200");
        In3.setType(1);
        da.addToKeywords(In);
        da.addToKeywords(In2);
        da.addToKeywords(In3);
        da.setUpdatedOn("2020062223");
        
//        CLIENT.uploadUserInterest(datakey, 23, da, false);

//        CLIENT.updateUserInterest(datakey);

        TUserKeywords userInterest = (TUserKeywords)WithCompression.getFrom(USERKW, UserInterestKey.forUserInterestOf(datakey, 23), new TUserKeywords() );
//        System.out.println(UserInterestKey.forUserInterestOf(datakey, 23));
//        System.out.println(userInterest);
//
//        CLIENT.updateAnonymousUserInterest(72057594417858969l);

        TArtistInfo info = new TArtistInfo();
        info.setArtist_id(44522);
        info.setScore(146.17230591398525);
        TUserInterestArtists data2 = new TUserInterestArtists();
        data2.addToList_artists(info);
        testUploadUserArtists(100, data2);


    }
}
