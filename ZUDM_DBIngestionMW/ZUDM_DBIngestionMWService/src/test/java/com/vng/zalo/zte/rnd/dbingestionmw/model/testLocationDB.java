package com.vng.zalo.zte.rnd.dbingestionmw.model;// Created By PTP

//import com.vng.zalo.zte.rnd.dbingestionmw.client.ProfileDemographicDBMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.client.LocationDBMWClient;
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.ZUDM_Util;
import com.vng.zing.zudm.common.centralizedkeys.DemographicPredictionKey;
import com.vng.zing.zudm_middleware.thrift.TCountry;
import com.vng.zing.zudm_middleware.thrift.TCountryCity;
import com.vng.zing.zuserdatamining.thrift.ZUDM_LivingLoc;
import com.vng.zing.zuserdatamining.thrift.ZUDM_TravelPath_City;
import com.vng.zing.zuserdatamining.thrift.ZUDM_TravelPath_CityInfo;
import java.nio.ByteBuffer;
import org.apache.log4j.Logger;
 
/**
 *
 * @author phucpt2
 */
public class testLocationDB {

//    private static final String THRIFT_IP = "127.0.0.1";
//    private static final int THRIFT_PORT = 18782;
    public static final LocationDBMWClient DEMO_GRAPHIC = new LocationDBMWClient();
    private static final Class MAIN_CLASS = testLocationDB.class;
    private static final Logger LOGGER = ZLogger.getLogger(MAIN_CLASS);
    public static final ZiDB64Client DATA = new ZiDB64Client("ZiDB64LivingLocation");
    public static final ZiDB64Client DBCmon3Cli = new ZiDB64Client("zudmcommon3");
    private static final ZiDBClient _ZiDBTravelPathCity = new ZiDBClient("ZUDM_ZiDBTravelPathCity");
    public static final ZiDBClient _DBClientTravelPathCity = new ZiDBClient("ZUDM_ZiDBTravelPathCity");

//    private static ZUDM_TravelPath_CityInfo generateTListAudience(String appID, String platform, String lastAccess) {
//		ZUDM_TravelPath_CityInfo pAucience = new ZUDM_TravelPath_CityInfo();
//		pAucience.s(appID);
//		pAucience.setPlatform(platform);
//		return pAucience;
//	}
//    
    public static void main(String[] args) throws Exception {
        ZUDM_LivingLoc Location = new ZUDM_LivingLoc();
//        ZUDM_TravelPath_City Location = new ZUDM_TravelPath_City();
        ZUDM_LivingLoc living = new ZUDM_LivingLoc();
        int id = 222;
//         ByteBuffer key = ByteBufferUtil.fromString(String.valueOf(139939754l));
//        com.vng.zing.zidb.thrift.TValueResult valres = ZUDM_Util.getWithProfiler(MAIN_CLASS, "UserTralvelPath.get", _ZiDBTravelPathCity,key );
//        
//        ZUDM_TravelPath_City usrpath = ZUDM_Util.getTbase(LOGGER, new ZUDM_TravelPath_City(), valres);
//        System.out.println(usrpath);
//        Location.setId(111);
//        Location.setUpdatedDate(222);
        System.out.println(DemographicPredictionKey.forForeignTripCountOf(id));
//        living.setId((int) id);
//        living.setNCheckIn(11);
//        DEMO_GRAPHIC.ingestLivingProvince(id, living);
//        System.out.println(DEMO_GRAPHIC.ingestLivingProvince(id, living));
//        long GEN = LocationMining.forLivingProvinceOf(id);
//        TValueResult result1 = DBCmon3Cli.get(GEN);
//        System.out.println(result1);
        
//        DEMO_GRAPHIC.ingestMostVisitedAdminArea(id, Location);
//        System.out.println(DEMO_GRAPHIC.ingestMostVisitedAdminArea(id, Location));
//        long genergateKey = LocationMining.forMostVisitedAdminAreaOf(id);
//        System.out.println(genergateKey);
//        TValueResult result = DATA.get(genergateKey);
//        System.out.println(result);
        
        
//        try {
//
//            TCountryCity city = new TCountryCity();
//            city.setCountry(TCountry.CAMBODIA);
//            city.setCityId((short) 3);
//
//            TCountryCity city2 = new TCountryCity();
//            city2.setCountry(TCountry.VIET_NAM);
//            city2.setCityId((short) 35);
//            client.uploadDevice(String.valueOf(key), udevcie);
//            System.out.println(key);

//            ZUDM_FlightList test = new ZUDM_FlightList(); 
//             
//            test.setCount(1);
//            
//            ZUDM_Flight f1 = new ZUDM_Flight();
//            f1.setFrom_location(city);
//            f1.setTo_location(city);
//            
//            test.addToFlight_list(f1);
//            
//            client.uploadFlightCount("139939754", test); 
//
//            UserCoverageMWClient client = new UserCoverageMWClient("10.30.65.168", 18782); 
//            List<Long> data = new ArrayList<>();
//        
//            data.add(139939754l * 100 + MOST_VISITED_CITY);
//            data.add(156062122l);
//            data.add(142154923l);
//            data.add(265369918l);
//            
//            TUserCoverageResult a = client.checkCoverage(data, (short)1);
//            System.out.println(a.isSetResult());
//            System.out.println(a);
            
// --- 
//            ProfileDemographicDBMWClient client = new ProfileDemographicDBMWClient("127.0.0.1", 18782); 
//
//            ZUDM_UserIncome ic = new ZUDM_UserIncome();
//            ic.setConfidence(654);
//            ic.setValue("aidsahio");
//            
//            client.uploadIncome("139939754", ic, false);
//            
//            String uid = "139939754";
//
//            long key = Long.valueOf(uid) ;
//            ZUDM_UserIncome visited = (ZUDM_UserIncome)WithCompression.getFrom(UserIncomeDB, (long) key,new ZUDM_UserIncome());
//            
//            System.out.println(visited);
//            
//            String passphrase = String.valueOf(key * key).substring(0, 10);
//            
//            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
//            messageDigest.update(passphrase.getBytes());
//            String encrkey = new String(messageDigest.digest());
//            
//            System.out.println(CryptoUtil.decryptF(visited.getValue(), encrkey));
            
//            
//            ZUDM_MostVisitedArea a = new ZUDM_MostVisitedArea();
//        
//            a.setAdministrativeLocID(3311);
//            a.setUpdatedDate(1543);
//            
//            client.uploadMostVisitedAdminArea("32154", a);
            
            


//            ZUDM_ForeignTripCount test = new ZUDM_ForeignTripCount(); 
//            test.setForeignTripCount((short)123);
//            client.uploadForeignTripCount("139939754", test); 
//            LocationMiningDBMWClient client = new LocationMiningDBMWClient(THRIFT_IP, 18782); 
//            ZUDM_MostVisitedProvinces test = new ZUDM_MostVisitedProvinces(); 
//            ZUDM_UserVisitedProvince f = new ZUDM_UserVisitedProvince();
//            f.setCount((short)123);
//            f.setProvince(city2);
//            test.addToProvinces(f);
//            System.out.println(client.uploadMostVisitedCity("139939754", test)); 
            ByteBuffer key = ByteBufferUtil.fromString( String.valueOf(139939754));
//
             TCountryCity city = new TCountryCity();
            city.setCountry(TCountry.CAMBODIA);
            city.setCityId((short) 3);
            com.vng.zing.zidb.thrift.TValueResult valres = ZUDM_Util.getWithProfiler(MAIN_CLASS, "UserTralvelPath.get", _DBClientTravelPathCity, key);
            ZUDM_TravelPath_City usrpath = ZUDM_Util.getTbase(LOGGER, new ZUDM_TravelPath_City(), valres);

            ZUDM_TravelPath_CityInfo adding = new ZUDM_TravelPath_CityInfo();
            adding.setCity(city);
//            ZUDM_TravelPath_City urs = new ZUDM_TravelPath_City();
            usrpath.putToListCities(1546623818, adding);
//            LocationMiningDBMWClient client = new LocationMiningDBMWClient("127.0.0.1", 18782); 
//            TActionResult rs = DEMO_GRAPHIC.ingestTravelPath("139939754", usrpath); 
//            System.err.println(rs.toString());
            com.vng.zing.zidb.thrift.TValueResult val = ZUDM_Util.getWithProfiler(MAIN_CLASS, "UserTralvelPath.get", _ZiDBTravelPathCity,key );
//        
        ZUDM_TravelPath_City usr = ZUDM_Util.getTbase(LOGGER, new ZUDM_TravelPath_City(), val);
        System.out.println(usr);
            
//            System.out.println(usrpath);
//            LocationMiningDBMWClient client = new LocationMiningDBMWClient("127.0.0.1", 18782);
//
//            ByteBuffer key = ByteBufferUtil.fromString("139939754");
//            TValueResult valres = ZUDM_Util.getWithProfiler(MAIN_CLASS, "updateDB.ZiDBHomeWorkLocation.get", _ZiDBHomeWorkLocation, key);
//            ZUDM_ListLocationHomeWork old_result = ZUDM_Util.getTbase(LOGGER, new ZUDM_ListLocationHomeWork(), valres);
//
//            ZUDM_LocationHomeWork home = null;
//            ZUDM_LocationHomeWork work = null;
//
//            for (ZUDM_LocationHomeWork loc : old_result.getListLocationHomeWork()) {
//                if (loc.type == ZUDM_LocationType.Home) {
//                    home = loc;
//                } else if (loc.type == ZUDM_LocationType.Work) {
//                    work = loc;
//                }
//            }
//
//            home.lat = 11.355473;
//            home.lon = 109.025419;
//
//            work.lat = 10.355473;
//            work.lon = 12.025419;
//
//            ZUDM_ListLocationHomeWork toput = new ZUDM_ListLocationHomeWork();
//            toput.addToListLocationHomeWork(home);
////            toput.addToListLocationHomeWork(work);
//
//            client.uploadHomeworkPartial("139939754", toput);
//
//        } catch (Exception ex) {
//            System.out.println(ex);
//        }
        System.exit(0);
    }
}
