/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_FriendRanking;
import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_ListFriendRanking;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.FriendRankingKey;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author cpu11232
 */
public class FriendRankingModelTest {
    public static final ZiDB64Client FriendRankingDB = new ZiDB64Client("ZUDM_FriendRanking_ZIDB64");
    
    public FriendRankingModelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of uploadInterestFriend method, of class FriendRankingModel.
     */
    @Test
    public void testUploadInterestFriend() {
        System.out.println("uploadInterestFriend");
        String src = "Profile@Mining";
        int zaloId = 100000029;
        ZUDM_ListFriendRanking inputData = new ZUDM_ListFriendRanking();
        ZUDM_FriendRanking FriendRanking=new ZUDM_FriendRanking();
        FriendRanking.setRank((short)1);
        FriendRanking.setScore(0.09707229222391991);
        FriendRanking.setUid(101618248);
        
        inputData.addToListFriendRanking(FriendRanking);
        FriendRankingModel instance = new FriendRankingModel();
        TActionResult result = instance.uploadInterestFriend(src, zaloId, inputData);
        System.out.println(result);
        long GenKey = FriendRankingKey.forInterestFriendOf(zaloId);
        ZUDM_ListFriendRanking rel = (ZUDM_ListFriendRanking) WithCompression.getFrom(FriendRankingDB, GenKey,new ZUDM_ListFriendRanking());
        System.out.println(rel);
        
        
    }

    /**
     * Test of uploadFollower method, of class FriendRankingModel.
     */
    @Test
    public void testUploadFollower() {
        System.out.println("uploadFollower");
        String src = "";
        int zaloId = 0;
        ZUDM_ListFriendRanking inputData = null;
        FriendRankingModel instance = new FriendRankingModel();
        TActionResult expResult = null;
        TActionResult result = instance.uploadFollower(src, zaloId, inputData);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
    
}
