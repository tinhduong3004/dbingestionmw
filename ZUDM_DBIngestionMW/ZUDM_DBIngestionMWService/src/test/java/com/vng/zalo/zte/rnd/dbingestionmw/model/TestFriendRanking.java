package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.client.FriendRankingMWClient;
import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_FriendRanking;
import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_ListFriendRanking;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.FriendRankingKey;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;


public class TestFriendRanking {

		private static final FriendRankingMWClient CLIENT = new FriendRankingMWClient("127.0.0.1", 18782);
		public static final ZiDB64Client FriendRankingDB = new ZiDB64Client("ZUDM_FriendRanking_ZIDB64");
		
	    private static void testUploadInterestFriend(int zaloId, ZUDM_ListFriendRanking inputData) throws Exception {
//	        System.out.println(CLIENT.uploadFollower(zaloId, inputData));
	        Long genergateKey = FriendRankingKey.forFollowerOf(zaloId);
	        System.out.println(genergateKey);
	        ZUDM_ListFriendRanking InterestFriend = (ZUDM_ListFriendRanking)WithCompression.getFrom(FriendRankingDB, genergateKey,new ZUDM_ListFriendRanking());
	        System.out.println(InterestFriend);
			System.out.println(InterestFriend.listFriendRanking.size());
	    }

	private static byte[] compressWithGzip(byte[] data) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		try (GZIPOutputStream zos = new GZIPOutputStream(baos)) {
			zos.write(data);
		}

		return baos.toByteArray();
	}

	    public static void main(String[] args) throws Exception {
	    	int zaloId = 169673189;
	    	ZUDM_ListFriendRanking inputData = new ZUDM_ListFriendRanking();
	        ZUDM_FriendRanking FriendRanking=new ZUDM_FriendRanking();
	        FriendRanking.setRank((short)1);
	        FriendRanking.setScore(0.8476318113801404);
	        FriendRanking.setUid(100000629);
	        FriendRanking.setAge((short)1985);
	        FriendRanking.setCount((short) 161);
	        for (int j = 0; j< 10 ; j++) {
				inputData.addToListFriendRanking(FriendRanking);
			}
			testUploadInterestFriend(zaloId, inputData);
//			List<ZUDM_FriendRanking> fr = inputData.getListFriendRanking();
//			int index = fr.size();
//			ZUDM_ListFriendRanking listFriendRankingV1 = new ZUDM_ListFriendRanking();
//			ZUDM_ListFriendRanking listFriendRankingV2 = new ZUDM_ListFriendRanking();
//			ZUDM_ListFriendRanking listFriendRankingV3 = new ZUDM_ListFriendRanking();
//			ZUDM_ListFriendRanking listFriendRankingV4 = new ZUDM_ListFriendRanking();
//			if (index < 2) {
//				listFriendRankingV1.setListFriendRanking(fr.subList(0, index));
//				try {
//					CLIENT.uploadFollower(zaloId, listFriendRankingV1);
//				} catch (Exception e) {
//					System.out.println(e);
//				}
//			} else if (index >= 2 && index < 5) {
//				listFriendRankingV1.setListFriendRanking(fr.subList(0, 249));
//				listFriendRankingV2.setListFriendRanking(fr.subList(250, index));
//				try {
//					CLIENT.uploadFollower(zaloId, listFriendRankingV1);
//					CLIENT.uploadFollowerV2(zaloId, listFriendRankingV2);
//				} catch (Exception e) {
//					System.out.println(e);
//				}
//			} else if (index >= 5 && index < 7) {
//				listFriendRankingV1.setListFriendRanking(fr.subList(0, 249));
//				listFriendRankingV2.setListFriendRanking(fr.subList(250, 499));
//				listFriendRankingV3.setListFriendRanking(fr.subList(500, index));
//				try {
//					CLIENT.uploadFollower(zaloId, listFriendRankingV1);
//					CLIENT.uploadFollowerV2(zaloId, listFriendRankingV2);
//					CLIENT.uploadFollowerV3(zaloId, listFriendRankingV3);
//				} catch (Exception e) {
//					System.out.println(e);
//				}
//			} else {
//				listFriendRankingV1.setListFriendRanking(fr.subList(0, 2));
//				listFriendRankingV2.setListFriendRanking(fr.subList(2, 5));
//				listFriendRankingV3.setListFriendRanking(fr.subList(5, 7));
//				listFriendRankingV4.setListFriendRanking(fr.subList(7, index));
//				try {
//					CLIENT.uploadFollower(zaloId, listFriendRankingV1);
//					CLIENT.uploadFollowerV2(zaloId, listFriendRankingV2);
//					CLIENT.uploadFollowerV3(zaloId, listFriendRankingV3);
//					CLIENT.uploadFollowerV4(zaloId, listFriendRankingV4);
//				} catch (Exception e) {
//					System.out.println(e);
//				}
//			}


//			System.out.println(inputData);
//			byte[] serializedData = ByteBufferUtil.fromTBase(inputData).array();
//			byte[] compressedData = compressWithGzip(serializedData);
//			System.out.println("size:"  + compressedData.length);
//
//			final byte[] utf8Bytes = inputData.toString().getBytes("UTF-8");
//			System.out.println(utf8Bytes.length);

	    }
}
