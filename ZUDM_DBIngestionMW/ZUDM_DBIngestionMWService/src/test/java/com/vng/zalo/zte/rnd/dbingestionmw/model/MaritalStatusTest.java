package com.vng.zalo.zte.rnd.dbingestionmw.model;
import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_UserMaritalStatus;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import org.apache.log4j.Logger;

/**
 *
 * @author minhthanh
 */
public class MaritalStatusTest {
    private static final Class THIS_CLASS = testGetDB.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final ZiDB64Client DemographicDB = new ZiDB64Client("ZUDM_ZiDB64DemographicDB");
    public static final ZiDBClient _ZiDBWifiInfo = new ZiDBClient("ZiDBWifiInfo");
    
    public static void main(String[] args) {
        long key_nf = 1190000014376714000L;
        System.out.println(WithCompression.getFrom(DemographicDB, (long) key_nf, new ZUDM_UserMaritalStatus()));
        System.exit(0);
    }
}
