package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_UserInterestLocation;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.centralizedkeys.LocationMining;
import com.vng.zing.zudm.common.centralizedkeys.OriginalKey;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tinhdt
 */
public class TestGetOrginalKey {
    public static final ZiDB64Client DemographicDB = new ZiDB64Client("ZUDM_ZiDB64DemographicDB");
    public static void main(String[] args) {
        int id = 100505810;
        long genergateKey = LocationMining.forUserSSISInterestOf(id);
        System.out.println(genergateKey);
        ZUDM_UserInterestLocation result = (ZUDM_UserInterestLocation)WithCompression.getFrom(DBClients.LOCATIONINTEREST, genergateKey,new ZUDM_UserInterestLocation());
        System.out.println(result);
        long zaloId = OriginalKey.getOriginalKeyForUID(genergateKey);
        System.out.println(zaloId);
    }
}
