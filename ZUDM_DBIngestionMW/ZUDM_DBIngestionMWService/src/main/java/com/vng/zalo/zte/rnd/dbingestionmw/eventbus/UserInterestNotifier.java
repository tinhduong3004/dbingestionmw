/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserInterestArtists;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserKeywords;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zalo.zte.rnd.dbingestionmw.common.KeywordMapper;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.KeywordInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.KeywordInfoList;
import static com.vng.zalo.zte.rnd.dbingestionmw.eventbus.DMPNotifier.serialize;
import com.vng.zing.common.ZErrorDef;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TAction;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TDMPUpdatedField;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPArtistInfo;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPInterest;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserInterest;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserInterestArtists;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserInterestTags;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;

/**
 * @date Oct 1, 2019
 * @author quydm
 */
public class UserInterestNotifier extends DMPNotifier{
    
    public static final UserInterestNotifier INSTANCE = new UserInterestNotifier();
    
    private static boolean hasGameInterest(int zaloId){
        return DBClients.CustomAudienceDB.hasEntry("customaudience__zalo_user_game__d30", zaloId) == 0;
    }
    private static boolean hasShoppingInterest(int zaloId){
        return DBClients.CustomAudienceDB.hasEntry("customaudience__zalo_user_shop__d30", zaloId) == 0;
    }
    
    private KeywordMapper interestMapper = KeywordMapper.getInstance();

    public static void notifyUserInterest(int zaloId,TUserKeywords userInterest){
        TDMPUserInterest interests = new TDMPUserInterest(-9);
        if (userInterest.keywords.size() > 0) {
        List<TDMPInterest> interestLst = userInterest.keywords.stream().map(kw -> kw.value)
                    .filter(StringUtils::isNumeric)
                    .filter(interest -> !interest.equals("100"))
                    .map(interest -> TDMPInterest.findByValue(Integer.parseInt(interest)))
                    .filter(interest -> interest != null)
                    .collect(Collectors.toList());
        
        if ( hasGameInterest(zaloId) ){
            interestLst.add(TDMPInterest.GAME);
        }
        if ( hasShoppingInterest(zaloId)){
            interestLst.add(TDMPInterest.SHOPPING);
        }
        interests.setError(0);
        interests.setInterests(interestLst);
        ByteBuffer serializedData = serialize(interests);
        notify(zaloId, TDMPUpdatedField.UserInterest, serializedData,TAction.Change, "IDENTIFIED");
        } 
    }
    
    public static void notifyAnonymousUserInterest(long gId,TUserKeywords userInterest){
        TDMPUserInterest interests = new TDMPUserInterest(-9);
        List<TDMPInterest> interestLst = userInterest.keywords.stream().map(kw -> kw.value)
                    .filter(StringUtils::isNumeric)
                    .filter(interest -> !interest.equals("100"))
                    .map(interest -> TDMPInterest.findByValue(Integer.parseInt(interest)))
                    .filter(interest -> interest != null)
                    .collect(Collectors.toList());

       
        interests.setError(0);
        interests.setInterests(interestLst);
        ByteBuffer serializedData = serialize(interests);
        notify(gId, TDMPUpdatedField.UserInterest, serializedData,TAction.Change, "ANONYMOUS");
    }
    
    public  long notifyUserInterestTags( KeywordInfoList keywordList){
        int zaloId =(int) keywordList.getUid();
        TDMPUserInterestTags interests = new TDMPUserInterestTags(-9);
        List<KeywordInfo> kwList = keywordList.getList_kw();
            Map<Integer, Double> resultMap = makeResultMap(kwList);
            interests.setTagScores(resultMap);
            if (resultMap.size() > 0) {
                interests.setError(ZErrorDef.SUCCESS);
                ByteBuffer serializedData = serialize(interests);
                return notify((int)zaloId, TDMPUpdatedField.UserInterestTags, serializedData,TAction.Change, "IDENTIFIED");
            } else {
                interests.setError(-9);
                return -1;
            }
    }
    
    public long notifyAUInterestTags(KeywordInfoList keywordList){
        int zaloId =(int) keywordList.getUid();
        TDMPUserInterestTags interests = new TDMPUserInterestTags(-9);
        List<KeywordInfo> kwList = keywordList.getList_kw();
            Map<Integer, Double> resultMap = makeResultMap(kwList);
            interests.setTagScores(resultMap);
            if (resultMap.size() > 0) {
                interests.setError(ZErrorDef.SUCCESS);
            } else {
                interests.setError(-9);
            }
        ByteBuffer serializedData = serialize(interests);
        System.out.println(resultMap); 
       return notify(zaloId, TDMPUpdatedField.UserInterestTags, serializedData,TAction.Change, "ANONYMOUS");
    }
    
    /**
     *
     * @param kwList
     * @return
     */
    public Map<Integer, Double> makeResultMap(List<KeywordInfo> kwList) {
        Map<Integer, Double> resultMap = new HashMap<>();
        for (KeywordInfo kw : kwList) {
            String word = kw.getValue();
            String idStr = interestMapper.getIdFromKeyword(word);
            Integer id = Integer.parseInt(idStr);
            if (id > 0 && kw.getScore() != 0) {
                Double score = kw.getScore();
                resultMap.put(id, score);
            }
        }
        return resultMap;
    }
    
    public static long notifyUserInterestArtists(int uId,TUserInterestArtists userInterestArtists){
        TDMPUserInterestArtists interests = new TDMPUserInterestArtists(-9);
        List<TDMPArtistInfo> interestLst = userInterestArtists.getList_artists().stream().map(Artists -> {
            TDMPArtistInfo artistInfo = new TDMPArtistInfo();
            artistInfo.setArtist_id(Artists.getArtist_id());
            artistInfo.setScore(Artists.getScore());
            return artistInfo;
        }).collect(Collectors.toList());

        interests.setError(0);
        interests.setExpiryDate(ExpiryDate.setExpriDate((int) userInterestArtists.getUpdatedDate(), ExpiryDate._3MONTHS));
        interests.setList_artists(interestLst);
        ByteBuffer serializedData = serialize(interests);
        return notify(uId, TDMPUpdatedField.UserInterestArtists, serializedData,TAction.Change, "IDENTIFIED");
    }
}
