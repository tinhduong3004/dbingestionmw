/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.app;

import com.vng.zalo.zte.rnd.dbingestionmw.servers.TServers;

/**
 * @date Dec 18, 2018
 * @author quydm
 */
public class MainApp {

    public static void main(String[] args) {
        TServers tServers = new TServers();
        if (!tServers.setupAndStart()) {
                System.err.println("Could not start thrift servers! Exit now.");
                System.exit(1);
        }
    }
}
