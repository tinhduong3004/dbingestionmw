/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch;

import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.KeywordInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.KeywordInfoList;
import static com.vng.zalo.zte.rnd.dbingestionmw.model.BaseModel.countEventBusFor;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ngocbk
 */
public class DBHandleUserInterestOperations extends DBHandleBase {

    public static DBHandleUserInterestOperations INSTANCE = new DBHandleUserInterestOperations("ES_UserInterest");

    public DBHandleUserInterestOperations(String config_name) {
        super(config_name);
    }

    void setupDB() {
        final String mapping_json = "{\"mappings\":{\"properties\":{\"keywords\":{\"properties\":{\"value\":{\"type\":\"text\"},\"kw_count\":{\"type\":\"long\"},\"article_count\":{\"type\":\"long\"}}}}}}"; // v2
        String url = SERVER_URL + index_name;
                System.out.println(submit("PUT", url, mapping_json));
    }

    private String createJSONString(long uid, List<KeywordInfo> list_kw, String updatetime) {
        try {
            JSONArray kw_array = new JSONArray();
            for (KeywordInfo kw_info : list_kw) {
                JSONObject kw_node = new JSONObject();

                kw_node.put("value", kw_info.getValue());
                kw_node.put("kw_count", kw_info.getKw_count());
                kw_node.put("article_count", kw_info.getArticle_count());
                kw_node.put("score", kw_info.getScore());

                kw_array.put(kw_node);
            }

            JSONObject wrapper = new JSONObject();
            wrapper.put("uid", uid);
            wrapper.put("keywords", kw_array);
            wrapper.put("updatetime", updatetime);

            return wrapper.toString();
        } catch (Exception e) {
            Logger.getLogger(ElasticSearchDBManager.class.getName()).log(Level.SEVERE, null, e);
            return "";
        }
    }

    public String ingestUserKw(String apiName, KeywordInfoList list_kw,
            Supplier<Long> notifier, boolean notify,
            Class clazz) {

        ThreadProfiler tp = Profiler.getThreadProfiler();
        StringBuilder bulk_request_body = new StringBuilder();

        try {
            String url = SERVER_URL + index_name + "/_bulk";
            tp.push(DBHandleUserInterestOperations.class, "ElasticSearch.createJson");
            bulk_request_body.append(createIndexJsonObject(list_kw.getUid() + ""));
            bulk_request_body.append(createJSONString(list_kw.getUid(), list_kw.getList_kw(), list_kw.getUpdateOn()));
            bulk_request_body.append("\n");

            tp.pop(DBHandleUserInterestOperations.class, "ElasticSearch.createJson");
            tp.push(DBHandleUserInterestOperations.class, "ElasticSearch.index");
            String res = submit("POST", url, bulk_request_body.toString());
            tp.pop(DBHandleUserInterestOperations.class, "ElasticSearch.index");
            if (notify == true) {
                notifier.get();
                countEventBusFor(clazz, apiName);
            }
            Profiler.closeThreadProfiler();
            return res;
        } catch (Exception e) {
            System.out.println(e);
            Logger.getLogger(ElasticSearchDBManager.class.getName()).log(Level.SEVERE, null, e);
        }

        Profiler.closeThreadProfiler();
        return "";
    }

    public static void main(String[] args) {
//        INSTANCE.setupDB();
        KeywordInfoList hehe = new KeywordInfoList();

        hehe.setUid(99);
        hehe.addToList_kw(new KeywordInfo("pikachu1"));
        hehe.addToList_kw(new KeywordInfo("pikachu2"));
        hehe.addToList_kw(new KeywordInfo("pikachu3"));

//        String res = INSTANCE.ingestUserKw(hehe);
//        System.out.println(res);
        System.exit(0);

    }

}
