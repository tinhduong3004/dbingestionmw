package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScore;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScoreV2;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandScore;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandTag;
import static com.vng.zalo.zte.rnd.dbingestionmw.eventbus.DMPNotifier.serialize;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TAction;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TDMPUpdatedField;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPLoanDemandTag;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserDisburseLeadScore;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserLoanDemandScore;
import java.nio.ByteBuffer;

/**
 *
 * @author tinhdt
 */
public class BankLeadNotifier extends DMPNotifier{
    public static long notifyLoanDemandScore(int zaloId,ZUDM_LoanDemandScore pLoanDemandScore){
        TDMPUserLoanDemandScore ldScore = new TDMPUserLoanDemandScore();
        ldScore.setValue(pLoanDemandScore.getScore());
        ldScore.setExpiryDate(ExpiryDate.setExpriDate((int) pLoanDemandScore.getUpdatedDate(), ExpiryDate._3MONTHS));
        ldScore.setError(0);
        ByteBuffer serializedData = serialize(ldScore);
        return notify(zaloId, TDMPUpdatedField.UserLoanDemandScore, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyDisburseLeadScore(int zaloId,ZUDM_DisburseLeadScore pDisburseLead){
        TDMPUserDisburseLeadScore ldScore = new TDMPUserDisburseLeadScore();
        ldScore.setValue(pDisburseLead.getScore());
        ldScore.setExpiryDate(ExpiryDate.setExpriDate((int) pDisburseLead.getUpdatedDate(), ExpiryDate._3MONTHS));
        ldScore.setError(0);
        ByteBuffer serializedData = serialize(ldScore);
        return notify(zaloId, TDMPUpdatedField.UserDisburseLeadScore, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyDisburseLeadShScore(int zaloId,ZUDM_DisburseLeadScoreV2 pDisburseLeadSh){
        TDMPUserDisburseLeadScore ldScore = new TDMPUserDisburseLeadScore();
        ldScore.setValue(pDisburseLeadSh.getTag());
        ldScore.setExpiryDate(ExpiryDate.setExpriDate((int) pDisburseLeadSh.getUpdatedDate(), ExpiryDate._3MONTHS));
        ldScore.setError(0);
        ByteBuffer serializedData = serialize(ldScore);
        return notify(zaloId, TDMPUpdatedField.UserDisburseLeadShScore, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyLoanDemandTag(int zaloId,ZUDM_LoanDemandTag pLoanDemandTag){
        TDMPLoanDemandTag ldTag = new TDMPLoanDemandTag();
        ldTag.setValue(pLoanDemandTag.getTag());
        ldTag.setError(0);
        ByteBuffer serializedData = serialize(ldTag);
        return notify(zaloId, TDMPUpdatedField.UserLoanDemandTag, serializedData,TAction.Change, "IDENTIFIED");
    }
}
