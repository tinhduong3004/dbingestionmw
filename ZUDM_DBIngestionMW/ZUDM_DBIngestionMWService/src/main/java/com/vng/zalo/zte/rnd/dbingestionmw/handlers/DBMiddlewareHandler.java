/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.handlers;

import com.vng.zalo.zte.rnd.dbingestionmw.admin.AdminModel;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.*;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserInterestArtists;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserKeywords;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScore;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScoreV2;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandScore;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandTag;
import com.vng.zalo.zte.rnd.dbingestionmw.cache.LastUpdateHourCache;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyList;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyListV2;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyTop500;
import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.*;
import com.vng.zalo.zte.rnd.dbingestionmw.device.thrift.ZUDM_UserDeviceRaw;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.KeywordInfoList;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.UserLocationCheckinCount;
import com.vng.zalo.zte.rnd.dbingestionmw.fraudmining.thrift.TListDuplicateAccount;
import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_ListFriendRanking;
import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_UserNumFriend;
import com.vng.zalo.zte.rnd.dbingestionmw.gameh5.thrift.ZUDM_Gameh5PlayingInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.gameh5.thrift.ZUDM_Gameh5TienlenPlayingInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.*;
import com.vng.zalo.zte.rnd.dbingestionmw.model.*;
import com.vng.zalo.zte.rnd.dbingestionmw.relationship.thrift.ZUDM_RelationshipList;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.*;
import com.vng.zing.exception.InvalidParamException;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import org.apache.thrift.TException;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @date Dec 18, 2018
 * @author quydm
 */
public class DBMiddlewareHandler implements DBIngestionMW.Iface {

    public static final LastUpdateHourCache cache = new LastUpdateHourCache("LastUpdateHourCache");

    @Override
    public int ping() throws TException {
        ThreadProfiler tp = Profiler.createThreadProfiler("DBIngestionMW.ping", false);
        tp.push(DBMiddlewareHandler.class, "ping");
        tp.pop(DBMiddlewareHandler.class, "ping");
        return 0; // succeeded
    }

    //----------------------------------------------------admin api---------------------------------------------------
    @Override
    public TActionResult addClientCaller(String adminKey, String callerName) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.addClientCaller", false);
        try {
            return AdminModel.INSTANCE.addClientCaller(adminKey, callerName);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult removeClientCaller(String adminKey, String callerName) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.removeClientCaller", false);
        try {
            return AdminModel.INSTANCE.removeClientCaller(adminKey, callerName);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public boolean validCaller(String adminKey, String callerName) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.validCaller", false);
        try {
            return AdminModel.INSTANCE.validCaller(adminKey, callerName);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    //-------------------------------------------------------------location mining api --------------------------------------------


    @Override
    public TActionResult uploadHousehold(String src, int userId, ZUDM_Household inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadHousehold", false);
        try {
            return LocationMiningModel.INSTANCE.ingestHousehold(src, userId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }


    @Override
    public TActionResult ingestFlightCount(String callerName, int zaloId, ZUDM_FlightList inputData, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestFlightCount", false);
        try {
            return LocationMiningModel.INSTANCE.ingestFlightCount(callerName, zaloId, inputData, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestForeignTripCount(String callerName, int zaloId, ZUDM_ForeignTripCount inputData, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestForeignTripCount", false);
        try {
            return LocationMiningModel.INSTANCE.ingestForeignTripCount(callerName, zaloId, inputData, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestMostVisitedCity(String callerName, int dataKey, ZUDM_MostVisitedProvinces inputData, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestMostVisitedCity", false);
        try {
            return LocationMiningModel.INSTANCE.ingestMostVisitedCity(callerName, dataKey, inputData, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }


    @Override
    public TActionResult ingestFullHomeWorkLocation(String callerName, int zaloId, ZUDM_ListLocationHomeWork inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestFullHomeWorkLocation", false);
        try {
            return LocationMiningModel.INSTANCE.ingestFullHomeWorkLocation(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestPartialHomeWorkLocation(String callerName, int zaloId, ZUDM_ListLocationHomeWork inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestPartialHomeWorkLocation", false);
        try {
            return LocationMiningModel.INSTANCE.ingestPartialHomeWorkLocation(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestHomeLocation(String callerName, int zaloId, ZUDM_ListLocationHomeWork inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestHomeLocation", false);
        try {
            return LocationMiningModel.INSTANCE.ingestHomeLocation(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestWorkLocation(String callerName, int zaloId, ZUDM_ListLocationHomeWork inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestWorkLocation", false);
        try {
            return LocationMiningModel.INSTANCE.ingestWorkLocation(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestMostVisitedAdminArea(String callerName, int dataKey, ZUDM_MostVisitedArea inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestMostVisitedAminArea", false);
        try {
            return LocationMiningModel.INSTANCE.ingestMostVisitedAdminArea(callerName, dataKey, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestUserSSIDInterest(String callerName, int zaloId, ZUDM_UserInterestLocation inputData, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.IngestUserSSIDInterest", false);
        try {
            return LocationMiningModel.INSTANCE.ingestUserSSIDInterest(callerName, zaloId, inputData, bln);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestIP2Location(String callerName, String dataKey, ZUDM_IP2Location inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestIP2Location", false);
        try {
            return LocationMiningModel.INSTANCE.ingestIP2Location(callerName, dataKey, inputData);
        } catch (InvalidParamException ex) {
            Logger.getLogger(DBMiddlewareHandler.class.getName()).log(Level.SEVERE, null, ex);
            return TActionResult.ERROR;
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestListIP2Location(String callerName, String ip, boolean put) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestListIP2Location", false);
        try {
            return LocationMiningModel.INSTANCE.ingestListIP2Location(callerName, ip, put);
        } catch (InvalidParamException ex) {
            Logger.getLogger(DBMiddlewareHandler.class.getName()).log(Level.SEVERE, null, ex);
            return TActionResult.ERROR;
        } finally {
            Profiler.closeThreadProfiler();
        }
    }


    //--------------------------------------------user occupation api-----------------------------------


    @Override
    public TActionResult uploadUserStudentOccupation(String callerName, int zaloId, ZUDM_UserOccupationList inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserStudentOccupation", false);
        try {
            return UserOccupationModel.INSTANCE.uploadUserStudentOccupation(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadUserUnionOccupation(String callerName, int zaloId, ZUDM_UserOccupationList inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserUnionOccupation", false);
        try {
            return UserOccupationModel.INSTANCE.uploadUserUnionOccupation(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadUserBlueWhiteOccupation(String callerName, int zaloId, ZUDM_UserOccupationList inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserBlueWhiteOccupation", false);
        try {
            return UserOccupationModel.INSTANCE.uploadUserBlueWhiteOccupation(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadModelOccupation(String callerName, int zaloId, ZUDM_UserOccupationList inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadModelOccupation", false);
        try {
            return UserOccupationModel.INSTANCE.uploadModelOccupation(callerName, zaloId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestUserCompany(String callerName, int zaloId, ZUDM_UserCompanyList tlist, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestUserCompany", false);
        try {
            return UserOccupationModel.INSTANCE.uploadUserCompany(callerName, zaloId, tlist, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestUserCompanyV2(String callerName, int zaloId, ZUDM_UserCompanyListV2 tlist, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestUserCompanyV2", false);
        try {
            return UserOccupationModel.INSTANCE.uploadUserCompanyV2(callerName, zaloId, tlist, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestUserCompanyTop500(String callerName, int zaloId, ZUDM_UserCompanyTop500 tlist, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestUserCompanyTop500", false);
        try {
            return UserOccupationModel.INSTANCE.uploadUserCompanyTop500(callerName, zaloId, tlist, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadWorkType(String callerName, int zaloId, ZUDM_UserOccupationList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestWorkType", false);
        try {
            return UserOccupationModel.INSTANCE.uploadWorkType(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

//---------------------------------------------------Demographic api----------------------------------------    
    @Override
    public TActionResult ingestPhonebook(String callerName, int zaloId, ZUDM_UserPhonebookName tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadPhonebook", false);
        try {
            return DemographicModel.INSTANCE.uploadPhonebook(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestGender(String callerName, int zaloId, ZUDM_UserPredictedGender tlist, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadGender", false);
        try {
            return DemographicModel.INSTANCE.uploadGender(callerName, zaloId, tlist, notify);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestGender2(String callerName, int zaloId, ZUDM_UserPredictedGender tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestGender2", false);
        try {
            return DemographicModel.INSTANCE.uploadGenderV2(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestUserNumFriend(String callerName, int zaloId, ZUDM_UserNumFriend tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserNumFriend", false);
        try {
            return DemographicModel.INSTANCE.uploadUserNumFriend(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestAge(String callerName, int zaloId, com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_UserAge tlist, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadAge", false);
        try {
            return DemographicModel.INSTANCE.uploadAge(callerName, zaloId, tlist, notify);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestAgeV2(String callerName, int zaloId, ZUDM_UserAge tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAgeV2", false);
        try {
            return DemographicModel.INSTANCE.uploadAgeV2(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestRealname(String callerName, int zaloId, ZUDM_UserRealNameList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadRealname", false);
        try {
            return DemographicModel.INSTANCE.uploadRealName(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestFullName(String callerName, int zaloId, ZUDM_UserRealNameList tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadFullName", false);
        try {
            return DemographicModel.INSTANCE.uploadFullName(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestUserIncome(String callerName, int zaloId, ZUDM_UserIncome tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserIncome", false);
        try {
            return DemographicModel.INSTANCE.uploadUserIncome(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestIncomeRank(String callerName, int zaloId, ZUDM_UserIncome income) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadIncomeRank", false);
        try {
            return DemographicModel.INSTANCE.uploadIncomeRank(callerName, zaloId, income);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestCreditscore(String callerName, int zaloId, ZUDM_UserCreditScore tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadCreditscore", false);
        try {
            return DemographicModel.INSTANCE.uploadCreditScore(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestCreditscoreV2(String callerName, int zaloId, ZUDM_UserCreditScore tlist, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadCreditscoreV2", false);
        try {
            return DemographicModel.INSTANCE.uploadCreditScoreV2(callerName, zaloId, tlist, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestCreditscoreV3(String callerName, int zaloId, ZUDM_UserCreditScore tlist, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadCreditscoreV3", false);
        try {
            return DemographicModel.INSTANCE.uploadCreditScoreV3(callerName, zaloId, tlist, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestNewCreditscoreV2(String callerName, int zaloId, ZUDM_UserCreditScore tlist, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestNewCreditscoreV2", false);
        try {
            return DemographicModel.INSTANCE.uploadNewCreditScoreV2(callerName, zaloId, tlist, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestNewCreditscoreV3(String callerName, int zaloId, ZUDM_UserCreditScore tlist, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestNewCreditscoreV3", false);
        try {
            return DemographicModel.INSTANCE.uploadNewCreditScoreV3(callerName, zaloId, tlist, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestMaritalStatus(String callerName, int zaloId, ZUDM_UserMaritalStatus tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadMaritalStatus", false);
        try {
            return DemographicModel.INSTANCE.uploadMaritalStatus(callerName, zaloId, tlist);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestHometown(String callerName, int zaloId, ZUDM_UserHometown inputData, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadHometown", false);
        try {
            return DemographicModel.INSTANCE.uploadHometown(callerName, zaloId, inputData, notify);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestParentalStatus(String callerName, int zaloId, ZUDM_UserParentalStatus tlist, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestParentalStatus", false);
        try {
            return DemographicModel.INSTANCE.uploadParentalStatus(callerName, zaloId, tlist, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestEducationLevel(String callerName, int zaloId, ZUDM_UserEducationLevel inputData, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadEducationLevel", false);
        try {
            return DemographicModel.INSTANCE.uploadEducationLevel(callerName, zaloId, inputData, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestBidirectionalPhonebook(String callerName, int src_id, ZUDM_BidirectionalPhonebookList inputData) throws TException {
         Profiler.createThreadProfiler("DBIngestionMW.ingestBidirectionalPhonebook", false);
        try {
            return DemographicModel.INSTANCE.ingestBidirectionalPhonebook(callerName, src_id, inputData);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    // TODO: GET must not be here
    // ----------------------------------------------User Coverage api------------------------------------------
    @Override
    public TUserCoverageResult checkCoverage(String callerName, List<Long> zaloId_list, short dbtype) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.CheckCoverage", false);
        try {
            return CoverageTracker.INSTANCE.checkCoverage(callerName, zaloId_list, dbtype);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TMultigetIntResult multigetIntFromDB(String callerName, List<Long> zaloId_list, String dbtype) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.CheckCoverage", false);
        try {
            return CoverageTracker.INSTANCE.multigetIntFromDB(callerName, zaloId_list, dbtype);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TMultigetDoubleResult multigetDoubleFromDB(String callerName, List<Long> zaloId_list, String dbtype) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.CheckCoverage", false);
        try {
            return CoverageTracker.INSTANCE.multigetDoubleFromDB(callerName, zaloId_list, dbtype);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TMultigetLongResult multigetLongFromDB(String callerName, List<Long> zaloId_list, String dbtype) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.CheckCoverage", false);
        try {
            return CoverageTracker.INSTANCE.multigetLongFromDB(callerName, zaloId_list, dbtype);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    // ------------------------------------------Anonymous User api------------------------------------------------------------
    @Override
    public TActionResult ingestAUPredictedGenderV2(String callerName, long globalId, TPredictedGender inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUPredictedGenderV2", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUPredictedGenderV2(callerName, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestAUListLocation(String src, long globalId, TListLocation inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUListLocation", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUListLocation(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestAUPredictedProvince(String src, long globalId, TPredictedProvince inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUPredictedProvince", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUPredictedProvince(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestUAUProvince(String src, long globalId, TPredictedProvince inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestUnstableAUProvince", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestUAUProvince(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestAUListDevice(String src, long globalId, TListDevice inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUListDevice", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUListDevice(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestAUPredictedGender(String src, long globalId, TPredictedGender inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUPredictedGender", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUPredictedGender(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override

    public TActionResult ingestAUPredictedAge(String src, long globalId, TPredictedAge inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUPredictedAge", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUPredictedAge(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    @Override
    public TActionResult ingestAUPredictedAge2(String src, long globalId, TPredictedAgeRange inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUPredictedAgeV2", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUPredictedAge2(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    

    @Override
    public TActionResult ingestAUAdtimaHistory(String src, long globalId, TListHistory inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUAdtimaHistory", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUAdtimaHistory(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestAUZingNewsHistory(String src, long globalId, TListHistory inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUZingNewsHistory", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUZingNewsHistory(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestAUZingMp3History(String src, long globalId, TListHistory inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUZingMp3History", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUZingMp3History(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestPredictedGender(String src, long globalId, TPredictedGenderV2 inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestPredictedGender", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestPredictedGender(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestShiftedPredictedGender(String src, long globalId, TPredictedGenderV2 inputData) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestShiftedPredictedGender", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestShiftedPredictedGender(src, globalId, inputData);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestAUPredictedAgeRange(String src, long globalId, TPredictedAgeRange inputData, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestAUPredictedAgeRange", false);
        try {
            return AnonymousUserModel.INSTANCE.ingestAUPredictedAgeRange(src, globalId, inputData, bln);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    //------------------------------------------------------------User Interest api--------------------------------------------------
    @Override
    public TActionResult uploadUserKeywords(String callerName, int dataKey, int hour, TUserKeywords tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserKeywords", false);
        try {
            return UserInterestModel.INSTANCE.uploadUserKeywords(callerName, dataKey, hour, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadUserInterest(String callerName, int dataKey, int hour, TUserKeywords tlist, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserInterest", false);
        try {
            return UserInterestModel.INSTANCE.uploadUserInterest(callerName, dataKey, hour, tlist, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult updateUserInterest(String callerName, int uid) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.updateUserInterest", false);
        try {
            return UserInterestModel.INSTANCE.UpdateUserInterest(callerName, uid);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadUserInterestByVectorSimilarity(String callerName, int dataKey, int hour, TUserKeywords tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserInterestByVectorSimilarity", false);
        try {
            return UserInterestModel.INSTANCE.uploadUserInterestByVectorSimilarity(callerName, dataKey, hour, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadUserInterestByAls(String callerName, int dataKey, int hour, TUserKeywords tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserInterestByAls", false);
        try {
            return UserInterestModel.INSTANCE.uploadUserInterestByAls(callerName, dataKey, hour, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadAnonymousUserInterest(String callerName, long dataKey, TUserKeywords tlist) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadAnonymousUserInterest", false);
        try {
            return UserInterestModel.INSTANCE.uploadAnonymousUserInterest(callerName, dataKey, tlist);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult updateAnonymousUserInterest(String callerName, long gid) throws TException {
            Profiler.createThreadProfiler("DBIngestionMW.updateAnonymousUserInterest", false);
        try {
            return UserInterestModel.INSTANCE.UpdateAnonymousUserInterest(callerName, gid);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    //--------------------------------------------------------cache api-----------------------------------------------------
    @Override
    public int getHourCache(String apiName) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.getHourCache", false);
        try {
            return CacheHourModel.INSTANCE.getHourCache(apiName);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    //-------------------------------------------------GameH5 api---------------------------------------
    @Override
    /**
     * @deprecated use uploadGameh5PlayingInfo with appid instead
     *
     */
    public TActionResult uploadGameh5TienlenPlayingInfo(String callerName, int zaloId, ZUDM_Gameh5TienlenPlayingInfo playerInfo) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadGameh5TienlenPlayingInfo", false);
        try {
            //TODO: do something with callerName
            return GameH5Model.getInstance().putTienlenPlayingInfo(zaloId, playerInfo);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadGameh5PlayingInfo(String callerName, int zaloId, int appid, ZUDM_Gameh5PlayingInfo playingInfo) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadGameh5PlayingInfo", false);
        try {
            //TODO: do something with callerName
            return GameH5Model.getInstance().putPlayingInfo(zaloId, appid, playingInfo);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    //------------------------------------------ BankLead api ----------------------------------------
    @Override
    public TActionResult uploadLoanDemandScore(String callerName, int zaloId, ZUDM_LoanDemandScore inputData, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadLoanDemandScore", false);
        try {
            return BankLeadModel.INSTANCE.uploadBankLead(callerName, zaloId, inputData, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult putUserHighWinrate(List<Integer> list, int appid) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.putUserHighWinrate", false);
        try {
            return GameH5Model.getInstance().putUserHighWinrate(list, appid);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult removeUserHighWinrate(int appid) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.removeUserHighWinrate", false);
        try {
            return GameH5Model.getInstance().removeUserHighWinrate(appid);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadDisburseLeadScore(String callerName, int zaloId, ZUDM_DisburseLeadScore inputData, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadDisburseLeadScore", false);
        try {
            return BankLeadModel.INSTANCE.uploadDisburseLead(callerName, zaloId, inputData, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
        @Override
    public TActionResult uploadDisburseLeadShScore(String callerName, int zaloId, ZUDM_DisburseLeadScoreV2 inputData, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadDisburseLeadShScore", false);
        try {
            return BankLeadModel.INSTANCE.uploadDisburseLeadSh(callerName, zaloId, inputData, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    
    @Override
    public TActionResult uploadDeviceRaw(String callerName, int userId, ZUDM_UserDeviceRaw data) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadDeviceRaw", false);
        try {
            return ZaloDeviceModel.INSTANCE.uploadDeviceRaw(callerName, userId, data);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult ingestLoanDemandTag(String callerName, int zaloId, ZUDM_LoanDemandTag inputData, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestLoanDemandTag", false);
        try {
            return BankLeadModel.INSTANCE.uploadLoanDemandTag(callerName, zaloId, inputData, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    //Elastic search
    // ----------------------------------------------Elastic search------------------------------------------
    
    @Override
    public TStringSearchResult ESLocationGetListUser(String string, List<Long> list) throws TException {
        Profiler.createThreadProfiler("ElasticSearchLocation.GetListUser", false);
        try {
            return ElasticSearchLocationModel.INSTANCE.getListUser(string, list);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TStringSearchResult ESLocationQueryNearBy(String string, double lat, double lon, double distance, double score_threshold) throws TException {
        Profiler.createThreadProfiler("ElasticSearchLocation.QueryNearBy", false);
        try {
            return ElasticSearchLocationModel.INSTANCE.queryNearBy(string, lat, lon, distance, score_threshold);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TStringSearchResult ESLocationInsertListUser(String string, List<UserLocationCheckinCount> list) throws TException {
        Profiler.createThreadProfiler("ElasticSearchLocation.InsertListUser", false);
        try {
            return ElasticSearchLocationModel.INSTANCE.addListUser(string, list);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TStringSearchResult ESIngestUserKeyword(String string, KeywordInfoList kil, boolean bln) throws TException {
        Profiler.createThreadProfiler("ElasticSearchUserInterest.IngestUserKW", false);
        try {
            return ElasticSearchUserInterestModel.INSTANCE.ingestUserKw(string, kil, bln);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TStringSearchResult ESIngestAnonymousUserKeyword(String string, KeywordInfoList kil, boolean bln) throws TException {
        Profiler.createThreadProfiler("ElasticSearchUserInterest.IngestAnonymousUserKW", false);
        try {
            return ElasticSearchUserInterestModel.INSTANCE.ingestAnonymousUserKw(string, kil, bln);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult uploadUserInterestArtists(String callerName, int userId, TUserInterestArtists data, boolean bln) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.uploadUserInterestArtists", false);
        try {
            return UserInterestModel.INSTANCE.uploadUserInterestArtists(callerName, userId, data, bln);

        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult removeDemographicKey(String src, long dataKey) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.removeDemographicKey", false);
        try {
            return KeyRemovingModel.INSTANCE.removeDemographicKey(src, dataKey);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult removeCreditScoreV2Key(String src, int zaloId, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.removeCreditScoreV2Key", false);
        try {
            return KeyRemovingModel.INSTANCE.removeCreditScoreV2Key(src, zaloId, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
    
    @Override
    public TActionResult removeCreditScoreV3Key(String src, int zaloId, boolean notify) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.removeCreditScoreV3Key", false);
        try {
            return KeyRemovingModel.INSTANCE.removeCreditScoreV3Key(src, zaloId, notify);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    //------------------------------------------ Relationship api ----------------------------------------

    @Override
    public TActionResult ingestRelationship(String src, int zaloId, ZUDM_RelationshipList data) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestRelationship", false);
        try {
            return RelationshipModel.INSTANCE.uploadRelationship(src, zaloId, data);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    // ------------------------------------------ Friend-ranking api ----------------------------------------
    @Override
    public TActionResult ingestInterestFriend(String src, int zaloId, ZUDM_ListFriendRanking data) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestInterestFriend", false);
        try {
            return FriendRankingModel.INSTANCE.uploadInterestFriend(src, zaloId, data);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestFollower(String src, int zaloId, ZUDM_ListFriendRanking data) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestFollower", false);
        try {
            return FriendRankingModel.INSTANCE.uploadFollower(src, zaloId, data);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestFollowerV2(String src, int zaloId, ZUDM_ListFriendRanking data) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestFollowerV2", false);
        try {
            return FriendRankingModel.INSTANCE.uploadFollowerV2(src, zaloId, data);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestFollowerV3(String src, int zaloId, ZUDM_ListFriendRanking data) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestFollowerV3", false);
        try {
            return FriendRankingModel.INSTANCE.uploadFollowerV3(src, zaloId, data);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    @Override
    public TActionResult ingestFollowerV4(String src, int zaloId, ZUDM_ListFriendRanking data) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestFollowerV4", false);
        try {
            return FriendRankingModel.INSTANCE.uploadFollowerV4(src, zaloId, data);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }

    //----------------------------------------------- Authen --------------------------------------------------
    @Override
    public TActionResult ingestListDuplicateAccount(String src, int zaloId, TListDuplicateAccount data) throws TException {
        Profiler.createThreadProfiler("DBIngestionMW.ingestListDuplicateAccount", false);
        try {
            return Authen.INSTANCE.uploadListDuplicateAccount(src, zaloId, data);
        } finally {
            Profiler.closeThreadProfiler();
        }
    }
}
