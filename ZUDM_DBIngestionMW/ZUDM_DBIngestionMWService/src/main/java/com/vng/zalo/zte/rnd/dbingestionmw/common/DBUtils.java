/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.common;

import com.vng.zalo.zte.rnd.dbingestionmw.model.GameH5Model;
import com.vng.zing.common.ZErrorDef;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.common.ZUtil;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.strlist32bm.thrift.TValue;
import com.vng.zing.strlist32bm.thrift.wrapper.StrList32bmClient;
import com.vng.zing.zcommon.thrift.PutPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.LongSupplier;
import org.apache.log4j.Logger;

/**
 *
 * @author thient
 */
public class DBUtils {

    private static final Class THIS_CLASS = GameH5Model.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);

    /**
     * use this to remove key instead of client.remove
     * @param client
     * @param key
     * @return 
     */
    public static long removeAllEntries(StrList32bmClient client, String key) {
        TValue emptyValue = new TValue().setEntries(new ArrayList<>());
        List<LongSupplier> actions = Arrays.asList(
                () -> client.put(key, emptyValue, PutPolicy.ADD_OR_UDP),
                () -> waitForDBTaskDone(client, key, 0, "RemoveAllEntries")
        );
        return execute(actions, key, "RemoveAllEntries");
    }

    public static long waitForDBTaskDone(StrList32bmClient client, String key, int expectedEntries, String taskName) {
        return waitForDBTaskDone(client, key, expectedEntries, 6000, 500, taskName);
    }

    public static long waitForDBTaskDone(StrList32bmClient client, String key, int expectedEntries,
            int timeInMiliseconds, int sleeptime, String taskName) {
        ThreadProfiler profiler = Profiler.getThreadProfiler();
        int iterators = timeInMiliseconds / sleeptime;
        for (int i = 0; i < iterators; ++i) {
            if (client.getCountEntries(key) != expectedEntries) {
                profiler.push(THIS_CLASS, "waitFor" + taskName);
                profiler.pop(THIS_CLASS, "waitFor" + taskName);
                ZUtil.sleep(sleeptime);
            } else {
                break;
            }
        }
        if (client.getCountEntries(key) != expectedEntries) {
            String message = String.format("%s task cannot be done after 1 minute of waiting. "
                    + "Real value = %d, expected value = %d",
                    taskName, client.getCountEntries(key), expectedEntries);
            LOGGER.error(message);
            return ZErrorDef.FAIL;
        }
        return ZErrorDef.SUCCESS;
    }

    public static long execute(List<LongSupplier> actions, String key, String taskName) {
        for (LongSupplier action : actions) {
            long status = action.getAsLong(); // execute the command
            if (ZErrorHelper.isFail(status) && !taskName.equals("mainTask")) { // Notify when sub task is fail
                return status;
            }
        }
        LOGGER.info(taskName + " for key " + key + " has DONE with successful signal");
        return ZErrorDef.SUCCESS;
    }
}
