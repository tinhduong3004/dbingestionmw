package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.*;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zudm.common.TimeInDayUtil;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TAction;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TDMPUpdatedField;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPFlightFrequency;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserAddress;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserFlightCount;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserForeignTripCount;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserHomePredict;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserInterestLocation;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserInterestLocationItem;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserLivingProvincePredict;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserMostVisitedProvinces;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserVisitedProvince;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserWorkPredict;
import com.vng.zing.zudm_gismiddleware.thrift.wrapper.ZUDM_GISMiddlewareClient;
import com.vng.zing.zudm_gismiddleware.thrift.wrapper.ZUDM_GISMiddlewareClientV2;
import com.vng.zing.zudm_middleware.thrift.TAdministrativeAreaResult;
import com.vng.zing.zudm_dmpmiddleware.thrift.TCountry;
import com.vng.zing.zudm_middleware.thrift.TProvince;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;

/**
 *
 * @author tinhdt
 */
public class LocationMiningNotifier extends DMPNotifier{
    
    private static final String THRIFT_IP    = "10.30.65.168";
    private static final int THRIFT_PORT     = 18680;
    private static final String THRIFT_SRC   = "ZServer";
    private static final ZUDM_GISMiddlewareClientV2 GISclientV2 = new ZUDM_GISMiddlewareClientV2(THRIFT_IP, THRIFT_PORT);
    private static final ZUDM_GISMiddlewareClient GisClient = new ZUDM_GISMiddlewareClient("gismiddleware");
    private static final Logger LOGGER = ZLogger.getLogger(LocationMiningNotifier.class);

    
    public static long notifyUserLivingProvince(int zaloId, ZUDM_MostVisitedArea pProvince){
        
        try {
            TDMPUserLivingProvincePredict livingProvice = new TDMPUserLivingProvincePredict(-1);
            TDMPUserAddress province = new TDMPUserAddress();
            TAdministrativeAreaResult location_data;
            location_data = GISclientV2.getVietnamAdministrativeAreaFromId(pProvince.getAdministrativeLocID(), THRIFT_SRC);
            province.setError(0);
            province.setWard(location_data.l3_name);
            province.setDistrict(location_data.l2_name);
            province.setCity(location_data.l1_name);
            province.setCountry(TCountry.VIET_NAM);
            province.setLocation_id(pProvince.getAdministrativeLocID());
            livingProvice.setLiving(province);
            livingProvice.setScore(1.0);
            livingProvice.setExpiryDate(ExpiryDate.setExpriDate((int) TimeInDayUtil.Instance.getCurDate(), ExpiryDate._3MONTHS));
            livingProvice.setError(0);
            
            ByteBuffer serializedData = serialize(livingProvice);
            return notify(zaloId, TDMPUpdatedField.UserLivingProvincePredict, serializedData, TAction.Change, "IDENTIFIED" );
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            return -1;
        }
    }
    public static long notifyForeignTripCount(int zaloId, ZUDM_ForeignTripCount zudmforeigntripcount){
        TDMPUserForeignTripCount foreignTripCount = new TDMPUserForeignTripCount(-1);
        foreignTripCount.setCount((short) zudmforeigntripcount.getForeignTripCount());
        foreignTripCount.expiryDate = ExpiryDate.setExpriDate((int) zudmforeigntripcount.getUpdatedDate(), ExpiryDate.WEEK);
        foreignTripCount.setError(0);
        ByteBuffer serializedData = serialize(foreignTripCount);
        return notify(zaloId, TDMPUpdatedField.UserForeignTripCount, serializedData, TAction.Change, "IDENTIFIED" );
    }
    
    public static long notifyMostVisitedCity(int zaloId, ZUDM_MostVisitedProvinces provinces){
        TDMPUserMostVisitedProvinces mostprovince = new TDMPUserMostVisitedProvinces(-1);
        List<TDMPUserVisitedProvince> listvistited = provinces.getProvinces().stream().map(loc->{
            TDMPUserVisitedProvince record = new TDMPUserVisitedProvince();
            TDMPUserAddress provinceName = new TDMPUserAddress();
            provinceName.setError(0);
            provinceName.setCity(TProvince.findByValue(loc.province.cityId).name());
            provinceName.setCountry(TCountry.VIET_NAM);
            provinceName.setLocation_id(TCountry.VIET_NAM.getValue() * 1000000 + loc.province.cityId * 10000);

            record.setProvince(provinceName);
            record.setCount((short) loc.getCount());
            return record;
        }).collect(Collectors.toList());
        mostprovince.setProvinces(listvistited);
        mostprovince.expiryDate = ExpiryDate.setExpriDate((int) provinces.getUpdatedDate(), ExpiryDate.MONTH);
        mostprovince.setError(0);
        ByteBuffer serializedData = serialize(mostprovince);
        return notify(zaloId, TDMPUpdatedField.UserMostVisitedProvinces, serializedData, TAction.Change, "IDENTIFIED" );
    }
    
    public static long notifyFightCount(int zaloId, ZUDM_FlightList list_flight){
        TDMPUserFlightCount flightCountResult = new TDMPUserFlightCount();
        if (list_flight.getFlight_list() == null || list_flight.getFlight_list().isEmpty()) {
            flightCountResult.setFreq(TDMPFlightFrequency.None);
        } else if (list_flight.getFlight_list().size() <= 4) {
            flightCountResult.setFreq(TDMPFlightFrequency.Low);
        } else {
            flightCountResult.setFreq(TDMPFlightFrequency.High);
        }

        flightCountResult.setExpiryDate(ExpiryDate.setExpriDate((int) 
                TimeInDayUtil.Instance.getCurDate(), ExpiryDate._3MONTHS));
        flightCountResult.setError(0);
        ByteBuffer serializedData = serialize(flightCountResult);
        return notify(zaloId, TDMPUpdatedField.UserFlightCount, serializedData, TAction.Change, "IDENTIFIED" );
    }
    
    public static long notifyHomeLocation(int zaloId, ZUDM_ListLocationHomeWork zudmListLocationHomeWork){
        TDMPUserHomePredict zaloHomePredict = new TDMPUserHomePredict(-1);
        if (zudmListLocationHomeWork != null && zudmListLocationHomeWork.getListLocationHomeWork() != null) {
                    List<ZUDM_LocationHomeWork> listLocationHomeWork = zudmListLocationHomeWork.getListLocationHomeWork();

                    listLocationHomeWork.forEach((ZUDM_LocationHomeWork loc) -> {
                        if (loc.type == ZUDM_LocationType.Home) {
                            TDMPUserAddress address = new TDMPUserAddress();
                            Double lat = loc.getLat();
                            Double lon = loc.getLon();
                            int location_id = loc.getAdministrativeAreaId();
                            if (location_id > 0) {
                                address.setLocation_id(location_id);
                                address.setCountry(TCountry.findByValue(location_id/1000000));
                            } else {
                                TAdministrativeAreaResult administrativeArea = GisClient.getVietnamAdministrativeArea(lon, lat);

                                if (administrativeArea.error == 0) {
                                    address.setLocation_id(administrativeArea.getAdministrativeAreaId());
                                    address.setDistrict(administrativeArea.getL2_name());
                                    address.setWard(administrativeArea.getL3_name());
                                    address.setCity(administrativeArea.getL1_name());
                                    address.setCountry(TCountry.VIET_NAM);

                                }
                            }

                            if (address != null) {
                                zaloHomePredict.setHome(address);
                                zaloHomePredict.setScore(1.0);
                                zaloHomePredict.setExpiryDate(ExpiryDate.setExpriDate((int) TimeInDayUtil.Instance.getCurDate(), ExpiryDate._3MONTHS));
                                zaloHomePredict.setError(0);

                            } else {
                                zaloHomePredict.setError(-9);
                            }
                        }
                    });
                }else{
                    zaloHomePredict.setError(-9);
                }
        ByteBuffer serializedData = serialize(zaloHomePredict);
        return notify(zaloId, TDMPUpdatedField.UserHomePredict, serializedData, TAction.Change, "IDENTIFIED" );
    }
    
    public static long notifyWorkLocation(int zaloId, ZUDM_ListLocationHomeWork zudmListLocationHomeWork){
        TDMPUserWorkPredict zaloWorkPredict = new TDMPUserWorkPredict(-1);
        if (zudmListLocationHomeWork != null && zudmListLocationHomeWork.getListLocationHomeWork() != null) {
                    List<ZUDM_LocationHomeWork> listLocationHomeWork = zudmListLocationHomeWork.getListLocationHomeWork();

                    listLocationHomeWork.forEach((ZUDM_LocationHomeWork loc) -> {
                        if (loc.type == ZUDM_LocationType.Work) {
                            TDMPUserAddress address = new TDMPUserAddress();

                            Double lat = loc.getLat();
                            Double lon = loc.getLon();
                            int location_id = loc.getAdministrativeAreaId();

                            if (location_id > 0) {
                                address.setLocation_id(location_id);
                                address.setCountry(TCountry.findByValue(location_id/1000000));
                            } else {
                                TAdministrativeAreaResult administrativeArea = GisClient.getVietnamAdministrativeArea(lon, lat);

                                if (administrativeArea.error == 0) {
                                    address.setLocation_id(administrativeArea.getAdministrativeAreaId());
                                    address.setDistrict(administrativeArea.getL2_name());
                                    address.setWard(administrativeArea.getL3_name());
                                    address.setCity(administrativeArea.getL1_name());
                                    address.setCountry(TCountry.VIET_NAM);

                                }
                            }

                            if (address != null) {
                                zaloWorkPredict.setWork(address);
                                zaloWorkPredict.setScore(1.0);
                                zaloWorkPredict.setExpiryDate(ExpiryDate.setExpriDate((int) TimeInDayUtil.Instance.getCurDate(), ExpiryDate._3MONTHS));

                                zaloWorkPredict.setError(0);
                            } else {
                                zaloWorkPredict.setError(-9);

                            }
                        }
                    });
                }else{
                    zaloWorkPredict.setError(-9);
                }
        ByteBuffer serializedData = serialize(zaloWorkPredict);
        return notify(zaloId, TDMPUpdatedField.UserWorkPredict, serializedData, TAction.Change, "IDENTIFIED" );
    }
    
    public static long notifyUserSSIDInterest(int zaloId, ZUDM_UserInterestLocation listUserInterestLocation){
        TDMPUserInterestLocation pUserInterestLocation = new TDMPUserInterestLocation();
        List<TDMPUserInterestLocationItem> listvistited = listUserInterestLocation.getInterest_category().stream().map(UserInterestLocation -> {
            TDMPUserInterestLocationItem record = new TDMPUserInterestLocationItem();
            record.setValue(UserInterestLocation.getValue());
            record.setConfidence(UserInterestLocation.getConfidence());
            return record;
        }).collect(Collectors.toList());
        pUserInterestLocation.setInterestLocationList(listvistited);
        pUserInterestLocation.setExpiryDate(ExpiryDate.setExpriDate((int) TimeInDayUtil.Instance.getCurDate(), ExpiryDate._3MONTHS));
        pUserInterestLocation.setError(0);
        ByteBuffer serializedData = serialize(pUserInterestLocation);
        return notify(zaloId, TDMPUpdatedField.UserInterestLocation, serializedData, TAction.Change, "IDENTIFIED" );
    }
}
