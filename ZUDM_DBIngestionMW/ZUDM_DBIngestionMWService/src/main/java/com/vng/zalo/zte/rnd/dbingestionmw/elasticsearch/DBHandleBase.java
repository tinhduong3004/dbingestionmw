/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch;

import com.vng.zing.configer.ZConfig;
import com.vng.zing.logger.ZLogger;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Base64;

/**
 *
 * @author phucpt2
 */
public class DBHandleBase {
    
    private static final Class _ThisClass = DBHandleBase.class;
    private static final org.apache.log4j.Logger _Logger = ZLogger.getLogger(_ThisClass);
    
    protected String SERVER_URL = "EMPTY";
    protected String index_name = "EMPTY";
    protected String Credentials = "EMPTY";
    
    public DBHandleBase(String config_name){
        SERVER_URL = ZConfig.Instance.getString(_ThisClass, config_name, "host", "localhost");
        index_name = ZConfig.Instance.getString(_ThisClass, config_name, "index", "none");
        Credentials = ZConfig.Instance.getString(_ThisClass, config_name, "authen", "");
    }
    
    public String getListUser(List<Long> li){
        
        try {
            JSONObject node = new JSONObject();
            JSONArray ids = new JSONArray();
            
            for (long uid : li){
                ids.put(String.valueOf(uid));
            }
            
            node.put("ids", ids);
            
            String url = SERVER_URL + index_name + "/_mget";
            return submit("POST", url, node.toString());
            
        } catch (Exception e){
            Logger.getLogger(ElasticSearchDBManager.class.getName()).log(Level.SEVERE, null, e);
        }
    
        return "";
    }
    
    protected String submit(String type, String url, String json_string){

        String result = "";
        
        try {
            
            HttpURLConnection conn = (HttpURLConnection) (new URL(url)).openConnection();
            conn.setRequestProperty("Content-Type", "application/json");
            
            String userCredentials = Credentials;
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));

            conn.setRequestProperty ("Authorization", basicAuth);
            
            conn.setRequestMethod(type);
            
            conn.setDoOutput(true);
            conn.setDoInput(true);
            
            try(OutputStream os = conn.getOutputStream()) {
                byte[] input = json_string.getBytes("utf-8");
                os.write(input, 0, input.length);           
            }
            
            try (BufferedReader reader = new BufferedReader ( new InputStreamReader(conn.getInputStream()))) {
                for (String line; (line = reader.readLine()) != null;) {
                    result = result + line + "\n";
                }
            }
            
        } catch (Exception ex) {
            System.out.println(ex);
        }
        
        return result;
    }
    
    protected JSONObject createLocationJsonNode(double lat, double lon){
        try {
            JSONObject location = new JSONObject();
            location.put("lat", lat);
            location.put("lon", lon);
            
            return location;
        } catch (JSONException ex) {
            Logger.getLogger(ElasticSearchDBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;// how do we end up at this point though ....
    }
    
    protected JSONObject createRangeGeNode(double score_threshold){
        try {
            JSONObject require_node = new JSONObject();
            require_node.put("gte", score_threshold);
            
            JSONObject range_node = new JSONObject();
            range_node.put("checkin.score", require_node);
            
            return range_node;
        } catch (Exception ex) {
            Logger.getLogger(ElasticSearchDBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;// how do we end up at this point though ....
    }
    
    protected String createIndexJsonObject(String uid){
        return "{ \"index\" : {\"_id\" : \"" + uid + "\" } }\n";
    }
    
//    static List<FullUserInfo> parseResult(String json){
//        List<FullUserInfo> res = new ArrayList<>();
//        try {
//            JSONArray result = (new JSONObject(json)).getJSONObject("hits").getJSONArray("hits");
//            
//            for (int i = 0; i < result.length(); ++ i){
//                JSONObject node = result.getJSONObject(i);
//                
//                FullUserInfo info = new FullUserInfo();
//                info.setUid(node.getInt("_id"));
//                
//                JSONArray list_location = node.getJSONObject("_source").getJSONArray("checkin");
//                for (int j = 0; j < list_location.length(); ++j){
//                    JSONObject location_node = list_location.getJSONObject(j);
//                    Location loc = new Location(location_node.getString("location"), location_node.getDouble("score"));
//                    info.addLocation(loc);
//                }
//                
//                res.add(info);
//            }
//            
//            
//        } catch (Exception ex) {
//            Logger.getLogger(DBManager.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return res;
//    }
    
}
