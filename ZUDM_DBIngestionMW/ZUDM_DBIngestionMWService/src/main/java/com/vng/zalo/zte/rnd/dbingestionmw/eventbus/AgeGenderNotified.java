package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_UserAge;
import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_UserPredictedGender;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TAction;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TDMPUpdatedField;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserAgePredict;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserGenderPredict;
import java.nio.ByteBuffer;

/**
 *
 * @author tinhdt
 */
public class AgeGenderNotified extends DMPProfileUpdate {
    
    public static long notifyIdentifiedAge(int zaloId, ZUDM_UserAge pAge){
        TDMPUserAgePredict age = new TDMPUserAgePredict();
        age.setAge((double) pAge.getAge());
        age.setExpiryDate(ExpiryDate.setExpriDate((int) pAge.getUpdatedDate(), ExpiryDate._3MONTHS));
        age.setScore(1.0);
        age.setError(0);
        ByteBuffer serializedData = serialize(age);
        return notify(zaloId, TDMPUpdatedField.UserAgePredict, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyIdentifiedGender(int zaloId, ZUDM_UserPredictedGender pGender){
        TDMPUserGenderPredict gender = new TDMPUserGenderPredict();
        gender.setGender(pGender.gender_id);
        gender.setExpiryDate(ExpiryDate.setExpriDate((int) pGender.getUpdatedDate(), ExpiryDate._3MONTHS));
        gender.setScore(1.0);
        gender.setError(0);
        ByteBuffer serializedData = serialize(gender);
        return notify(zaloId, TDMPUpdatedField.UserGenderPredict, serializedData,TAction.Change, "IDENTIFIED");
    }
}
