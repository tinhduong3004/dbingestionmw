// Created By PTP
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.admin.AdminModel;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedAge;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedGender;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserKeywords;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_ListLocationHomeWork;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_LocationType;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TMultigetDoubleResult;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TMultigetIntResult;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TMultigetLongResult;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TUserCoverageResult;
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.zidb.thrift.TMapDataResult;
import com.vng.zing.zidb.thrift.TValue;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.TMapErrorResult;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.ZUDM_Util;
import org.apache.log4j.Logger;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients.*;

/**
 *
 * @author phucpt2
 */
public class CoverageTracker extends BaseModel {

    private static final Class THIS_CLASS = CoverageTracker.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final CoverageTracker INSTANCE = new CoverageTracker();

    boolean checkValidCaller(String caller) {
        return AdminModel.INSTANCE.validCaller(caller);
//        return true;
    }

    public TUserCoverageResult checkCoverageMultigetRequiredByteBuffer(List<ByteBuffer> uid_list, short dbtype, TUserCoverageResult result) {
        if (dbtype == 1001) { //home
            ZiDBClient client = _ZiDBHomeWorkLocation;
            
            TMapDataResult res = client.mmultiGet(uid_list);
            
            if (res.isSetErrorMap()) {
                for (Map.Entry<ByteBuffer, Integer> error_entry : res.errorMap.entrySet()){
                    result.addToResult(Long.valueOf(ByteBufferUtil.toString(error_entry.getKey())));
                }
            }
            
            for (Map.Entry<ByteBuffer, TValue> hit_entry : res.dataMap.entrySet()){
                try {
                    ZUDM_ListLocationHomeWork zudmListLocationHomeWork
                            = ZUDM_Util.getTbase(LOGGER, new ZUDM_ListLocationHomeWork(), hit_entry.getValue());

                    if ((zudmListLocationHomeWork.getListLocationHomeWorkSize() < 2) 
                            && (zudmListLocationHomeWork.getListLocationHomeWork().get(0)).getType() != ZUDM_LocationType.Home) {
                        result.addToResult(Long.valueOf(ByteBufferUtil.toString(hit_entry.getKey())));
                    }
                    
                } catch (Exception ex) {
                    result.addToResult(Long.valueOf(ByteBufferUtil.toString(hit_entry.getKey())));
                }
            }
            return result;
        }
        
        else if (dbtype == 1002) { //home
            ZiDBClient client = _ZiDBHomeWorkLocation;
            
            TMapDataResult res = client.mmultiGet(uid_list);
            
            if (res.isSetErrorMap()) {
                for (Map.Entry<ByteBuffer, Integer> error_entry : res.errorMap.entrySet()){
                    result.addToResult(Long.valueOf(ByteBufferUtil.toString(error_entry.getKey())));
                }
            }
            
            for (Map.Entry<ByteBuffer, TValue> hit_entry : res.dataMap.entrySet()){
                try {
                    ZUDM_ListLocationHomeWork zudmListLocationHomeWork
                            = ZUDM_Util.getTbase(LOGGER, new ZUDM_ListLocationHomeWork(), hit_entry.getValue());

                    if ((zudmListLocationHomeWork.getListLocationHomeWorkSize() < 2) 
                            && (zudmListLocationHomeWork.getListLocationHomeWork().get(0)).getType() != ZUDM_LocationType.Work) {
                        result.addToResult(Long.valueOf(ByteBufferUtil.toString(hit_entry.getKey())));
                    }
                    
                } catch (Exception ex) {
                    result.addToResult(Long.valueOf(ByteBufferUtil.toString(hit_entry.getKey())));
                }
            }
            
            return result;
        }
        
        else if (dbtype == 1003) { //anonymous interest
            ZiDBClient client = ANONYMOUSUSERINTEREST;
            
            TMapDataResult res = client.mmultiGet(uid_list);
            
            if (res.isSetErrorMap()) {
                for (Map.Entry<ByteBuffer, Integer> error_entry : res.errorMap.entrySet()){
                    result.addToResult(Long.valueOf(ByteBufferUtil.toString(error_entry.getKey())));
                }
            }
            
            return result;
        }
        
        
        return null;
    }
    
    
    public TUserCoverageResult checkCoverage(String callerName, List<Long> uid_list, short dbtype) {
        /*
        dtype:
        < 0 : zidb 
        > 0 : zidb64
        ---
        1: las
         */

        ThreadProfiler tp = Profiler.getThreadProfiler();
        TUserCoverageResult result = new TUserCoverageResult();

        try {
            if (dbtype > 1000) {
                ZiDBClient client = null;
                
                List<ByteBuffer> key_list = new ArrayList<>();
                
                for (long uid : uid_list){
                    key_list.add(ByteBufferUtil.fromString("" + uid));
                }
                
                switch (dbtype) {
                    case 1001: // Home
                    case 1002: // Work
                    case 1003: // Anonymous Interest 
                        return checkCoverageMultigetRequiredByteBuffer(key_list, dbtype, result);
                    case 1004:
                        break;
                    default:
                        result.setError(-2);
                        return result;
                }

                
                
                tp.push(THIS_CLASS, "COVERAGE.SUCCESS");
                com.vng.zing.zidb.thrift.TMapErrorResult temp_result = client.mmultiExist(key_list);
                
                tp.pop(THIS_CLASS, "COVERAGE.SUCCESS");

                result.setError(0);
                if (temp_result.getError() == 0) {
                    return result;
                }

                
                for (ByteBuffer i : temp_result.getErrorMap().keySet()) {
                    long value = Long.valueOf(ByteBufferUtil.toString(i));
                    result.addToResult(value);
                }

                return result;
            }
            else if (dbtype > 0) {
                ZiDB64Client client = null;
                
                switch (dbtype) {
                    case 1: // UserIncomeDB
                        client = UserIncomeDB;
                        break;
                    case 2: // UserInterest
                        client = UserKWDB;
                        break;
                    case 3: // Anonymous 
                        client = ANONYMOUSUSER_DB;
                        break;
                    case 4: // HOUSE_HOLD 
                        client = HOUSE_HOLD;
                        break;
                    case 5: // Zalo Device 
                        client = DBClients.DeviceDB;
                        break;
                    case 6: // Zalo Demographic (transfer DemographicDB to DemographicDB2)
                        client = DBClients.DemographicDB2;
                        break;
                    case 7: // Occupation 
                        client = DBClients.OccupationDB;
                        break;
                    case 8: // Living DMP 
                        client = DBClients.MostVisitedAADB;
                        break;
                    case 9: // SSID Interest 
                        client = DBClients.LOCATIONINTEREST;
                        break;
                    case 10: //  ANONYMOUSUSER_DB2 
                        client = DBClients.ANONYMOUSUSER_DB2;
                        break;
                    case 11: //  DEMO_GRAPHIC3
                        client = DBClients.DemographicDB3;
                        break;
                    default:
                        result.setError(-2);
                        return result;
                }

                tp.push(THIS_CLASS, "COVERAGE.SUCCESS");
                TMapErrorResult temp_result = client.mmultiExist(uid_list);
                
                tp.pop(THIS_CLASS, "COVERAGE.SUCCESS");

                result.setError(0);
                if (temp_result.getError() == 0) {
                    return result;
                }

                for (long i : temp_result.getErrorMap().keySet()) {
                    result.addToResult(i);
                }

                return result;

            }

            return null;
        } catch (Exception e) {
            System.out.println(e);
            result.setError(-1);
            return result;
        }
    }

    public TMultigetDoubleResult multigetDoubleFromDB(String callerName, List<Long> uid_list, String dbtype) {
        ThreadProfiler tp = Profiler.getThreadProfiler();
        TMultigetDoubleResult result = new TMultigetDoubleResult();

        try {
            tp.push(THIS_CLASS, "COVERAGE.MMGET");
            switch (dbtype) {

                case "1": // Anonymous Age
                {
                    Map<Long, TPredictedAge> res = WithCompression.multiGetFrom(ANONYMOUSUSER_DB, uid_list, new TPredictedAge());
                    result.setError(0);

                    for (Map.Entry<Long, TPredictedAge> entry : res.entrySet()) {

                        TPredictedAge age = entry.getValue();
                        result.putToMap_result(entry.getKey(), age.getValue());
                    }

                    break;
                }
                default:
                    result.setError(-2);
                    return result;
            }
            tp.pop(THIS_CLASS, "COVERAGE.MMGET");

            result.setError(0);
            return result;

        } catch (Exception e) {
            System.out.println(e);
            result.setError(-1);
            return result;
        }
    }

    public TMultigetIntResult multigetIntFromDB(String callerName, List<Long> uid_list, String dbtype) {

        ThreadProfiler tp = Profiler.getThreadProfiler();
        TMultigetIntResult result = new TMultigetIntResult();

        try {
            tp.push(THIS_CLASS, "COVERAGE.MMGET");
            switch (dbtype) {

                case "1": // Anonymous Gender
                {
                    Map<Long, TPredictedGender> res = WithCompression.multiGetFrom(ANONYMOUSUSER_DB, uid_list, new TPredictedGender());
                    result.setError(0);

                    for (Map.Entry<Long, TPredictedGender> entry : res.entrySet()) {

                        TPredictedGender gender = entry.getValue();
                        result.putToMap_result(entry.getKey(), gender.getValue().getValue());
                    }

                    break;
                }
                default:
                    result.setError(-2);
                    return result;
            }
            tp.pop(THIS_CLASS, "COVERAGE.MMGET");

            result.setError(0);
            return result;

        } catch (Exception e) {
            System.out.println(e);
            result.setError(-1);
            return result;
        }
    }

    public TMultigetLongResult multigetLongFromDB(String callerName, List<Long> uid_list, String dbtype) {

        ThreadProfiler tp = Profiler.getThreadProfiler();
        TMultigetLongResult result = new TMultigetLongResult();

        try {
            if (dbtype.equals("interest")) {

                Map<Long, TUserKeywords> datamap = WithCompression.multiGetFrom(UserKWDB, uid_list, new TUserKeywords());

                for (Map.Entry<Long, TUserKeywords> entry : datamap.entrySet()) {

                    TUserKeywords interest = entry.getValue();
                    result.putToMap_result(entry.getKey(), Long.valueOf(interest.updatedOn));
                }

            } else {
                return null;
            }

        } catch (Exception e) {
            System.out.println(e);
            result.setError(-1);
            return result;
        }
        return result;
    }

    static long createKey(long uid) {
        return uid * 100L + 23 + 7120000000000000000l;
    }

    public static void main(String[] args) { // for test only
        List<Long> li = new ArrayList<>();

//        li.add(139939754l);
//        li.add(156062122l);
//        li.add(199838431l);
//        li.add(142154923l);
//        li.add(268248042l);
//        li.add(142154923321l);
//        
//        List<Long> key_list = new ArrayList<>();
        
//        List<ByteBuffer> key_list = new ArrayList<>();
//                
//        for (long uid : li){
//                    key_list.add(ByteBufferUtil.fromString("" + uid));
//        }
//        
//        TMapDataResult a =_ZiDBHomeWorkLocation.mmultiGet(key_list);
//        System.out.println(a);
                
//        li.add(139939754l);
//        li.add(139939754l);
        
        li.add(72057594419380754l);
        li.add(72057594425220738l);
        li.add(72057594434116880l);
        li.add(72057594450233360l);
        li.add(216172783608619511l);
//        li.add(createKey(100140051));
//        li.add(createKey(100140058));
//        li.add(createKey(100140067));
//        li.add(createKey(600140133));
//        li.add(createKey(139939754));

//        System.out.println(li);

        System.out.println("------------");
        System.out.println(INSTANCE.checkCoverage("aa", li, (short) 1003));
        System.out.println("------------");

//        ZUDM_UserDeviceRaw zudmDevice = (ZUDM_UserDeviceRaw) WithCompression.getFrom(
//                DBClients.DEVICE_DB, ZaloDeviceKey.forDeviceRawOf(139939754), new ZUDM_UserDeviceRaw());
//
//        System.out.println(zudmDevice);
//        
//        System.out.println(OriginalKey.getOriginalKey(ZaloDeviceKey.forDeviceRawOf(139939754)));
        
//        TUserKeywords data = (TUserKeywords) WithCompression.getFrom(UserKWDB, (long) createKey(139939754), new TUserKeywords());
//        TUserKeywords data = (TUserKeywords) WithCompression.getFrom(UserKWDB, (long) createKey(139939754), new TUserKeywords());
//        System.out.println(data);
//
//        System.out.println("--------");
//
//        System.out.println(WithCompression.multiGetFrom(UserKWDB, li, new TUserKeywords()));

//        System.out.println(b);

//        System.out.println(INSTANCE.multigetLongFromDB("aa", li, "interest"));

        System.exit(0);
    }
//    
}
