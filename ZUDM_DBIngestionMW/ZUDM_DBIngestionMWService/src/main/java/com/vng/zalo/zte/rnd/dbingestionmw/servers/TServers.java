/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.servers;

import com.vng.zalo.zte.rnd.dbingestionmw.handlers.DBMiddlewareHandler;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.DBIngestionMW;
import com.vng.zing.thriftserver.ThriftServers;

/**
 * @date Dec 18, 2018
 * @author quydm
 */
public class TServers {

    public boolean setupAndStart() {
        ThriftServers servers = new ThriftServers("zudm_dbingestionmw");
        DBIngestionMW.Processor processor = new DBIngestionMW.Processor(new DBMiddlewareHandler());
        servers.setup(processor);
        return servers.start();
    }
}
