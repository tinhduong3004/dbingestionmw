/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch.ElasticSearchDBManager;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.UserLocationCheckinCount;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TStringSearchResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zcommon.thrift.ECode;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author phucpt2
 */
public class ElasticSearchLocationModel extends BaseModel{
    private static final Class THIS_CLASS = ElasticSearchLocationModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final ElasticSearchLocationModel INSTANCE = new ElasticSearchLocationModel();
    
    boolean checkValidCaller(String caller) {
//        return AdminModel.INSTANCE.validCaller(caller);
        return true;
    }
    
    public TStringSearchResult getListUser(String src, List<Long> uidlist){
        TStringSearchResult res = new TStringSearchResult();
        
        if (!checkValidCaller(src)){
            res.setError(ECode.WRONG_AUTH.getValue());
            return res;
        }
        
        String result = ElasticSearchDBManager.location_elastic_search.getListUser(uidlist);
        res.setResult(result);
        
        return res;
        
    }
    
    
    public TStringSearchResult queryNearBy(String src, double lat, double lon, double distance, double score_threshold){
        TStringSearchResult res = new TStringSearchResult();
        
        if (!checkValidCaller(src)){
            res.setError(ECode.WRONG_AUTH.getValue());
            return res;
        }
        
        String result = ElasticSearchDBManager.location_elastic_search.getUserListNear(lat, lon, distance, score_threshold);
        res.setResult(result);
        
        return res;
        
    }
    
    
    public TStringSearchResult addListUser(String src, List<UserLocationCheckinCount> list_user){
        TStringSearchResult res = new TStringSearchResult();
        
        if (!checkValidCaller(src)){
            res.setError(ECode.WRONG_AUTH.getValue());
            return res;
        }
        
        String result = ElasticSearchDBManager.location_elastic_search.addListUser(list_user);
        res.setResult(result);
        
        return res;
        
    }
}
