/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_ListFriendRanking;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import org.apache.log4j.Logger;
import static com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients.FriendRankingDB;
import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_FriendRanking;
import com.vng.zing.zudm.common.centralizedkeys.FriendRankingKey;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tinhdt
 */
public class FriendRankingModel extends DMPModel{
    private static final Class THIS_CLASS = FriendRankingModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final FriendRankingModel INSTANCE = new FriendRankingModel();
    
    public List<ZUDM_FriendRanking> limit500(List<ZUDM_FriendRanking> keywordList) {
        return keywordList.stream().limit(500).collect(Collectors.toList());
    }
    
    public TActionResult uploadInterestFriend(String src, int zaloId, ZUDM_ListFriendRanking inputData){
//        inputData.setListFriendRanking(limit500(inputData.getListFriendRanking())); // add limit 1000 'cause overheating db
        return ingest(FriendRankingDB, "ingestInterestFriend",
               src, FriendRankingKey.forInterestFriendOf(zaloId),inputData,
               THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadFollower(String src, int zaloId, ZUDM_ListFriendRanking inputData){
        return ingest(FriendRankingDB, "ingestFollower",
               src, FriendRankingKey.forFollowerOf(zaloId),inputData,
               THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadFollowerV2(String src, int zaloId, ZUDM_ListFriendRanking inputData){
        return ingest(FriendRankingDB, "ingestFollowerV2",
               src, FriendRankingKey.forFollowerV2Of(zaloId),inputData,
               THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadFollowerV3(String src, int zaloId, ZUDM_ListFriendRanking inputData){
        return ingest(FriendRankingDB, "ingestFollowerV3",
               src, FriendRankingKey.forFollowerV3Of(zaloId),inputData,
               THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadFollowerV4(String src, int zaloId, ZUDM_ListFriendRanking inputData){
        return ingest(FriendRankingDB, "ingestFollowerV4",
               src, FriendRankingKey.forFollowerV4Of(zaloId),inputData,
               THIS_CLASS,LOGGER);
    }
}
