/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch;

import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.LocationScore;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.UserLocationCheckinCount;
import com.vng.zalo.zte.rnd.dbingestionmw.handlers.DBMiddlewareHandler;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author phucpt2
 */
public class DBHandleLocationQuery extends DBHandleBase {

    public DBHandleLocationQuery(String config_name) {
        super(config_name);
    }
    private String queryNear(double lat, double lon, double distance, double score_threshold){
        
        try {
            JSONObject geo_distance = new JSONObject();
            geo_distance.put("distance", distance + "km");
            geo_distance.put("checkin.location", createLocationJsonNode(lat, lon));
            
            JSONObject filter_node = new JSONObject();
            filter_node.put("geo_distance", geo_distance);
            
            JSONObject must_node = new JSONObject();
            must_node.put("range", createRangeGeNode(score_threshold));
            
            JSONObject bool_node = new JSONObject();
            bool_node.put("must", must_node);
            bool_node.put("filter", filter_node);
            
            JSONObject query_node = new JSONObject();
            query_node.put("bool", bool_node);
            
            JSONObject wrapper = new JSONObject();
            wrapper.put("query", query_node);
            
            String url = SERVER_URL + index_name + "/_search";
            
            return submit("GET", url, wrapper.toString());
        } catch (JSONException ex) {
            Logger.getLogger(ElasticSearchDBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
    public String getUserListNear(double lat, double lon, double distance, double score){
        String res_query = queryNear(lat, lon, distance, score);
        return res_query;
    }
    
    private String getInsertQuery(long uid, List<LocationScore> list_loc){
         try {
            JSONArray checkin_arr = new JSONArray();
            for (LocationScore loc : list_loc) {
                JSONObject node = new JSONObject();
                node.put("location", loc.getLat() + ", " + loc.getLon());
                node.put("score", loc.getScore());
                node.put("last_date", loc.getLast_date());
                checkin_arr.put(node);
            }
            
            JSONObject wrapper = new JSONObject();
            wrapper.put("uid", uid);
            wrapper.put("checkin", checkin_arr);
            
            
            return wrapper.toString();
        } catch (Exception ex) {
            Logger.getLogger(ElasticSearchDBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
    public String addUser(long uid, List<LocationScore> list_loc){
        try {
            String url = SERVER_URL + index_name + "/_doc/" + uid;
            
            return submit("PUT", url, getInsertQuery(uid, list_loc));
        } catch (Exception ex) {
            Logger.getLogger(ElasticSearchDBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
    
    public String addListUser(List<UserLocationCheckinCount> list_user){
        
        ThreadProfiler tp = Profiler.getThreadProfiler();
        String request_body = "";
        
        List<String> list_insert_query = new LinkedList<>();
        
        try {
            
            
            String url = SERVER_URL + index_name + "/_bulk";
            
            tp.push(DBHandleLocationQuery.class, "ElasticSearch.createJson");
            for (UserLocationCheckinCount info : list_user){
                list_insert_query.add(createIndexJsonObject(info.getUid() + ""));
                list_insert_query.add(getInsertQuery(info.getUid(), info.getList_location()) + "\n");
                
            }
            
            request_body = String.join("", list_insert_query);
            
            tp.pop(DBHandleLocationQuery.class, "ElasticSearch.createJson");
            tp.push(DBHandleLocationQuery.class, "ElasticSearch.index");
            String res = submit("POST", url, request_body);
            tp.pop(DBHandleLocationQuery.class, "ElasticSearch.index");
        
            Profiler.closeThreadProfiler();
            
            return res;

//            return "";
        } catch (Exception ex) {
            System.out.println(ex);
            Logger.getLogger(ElasticSearchDBManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Profiler.closeThreadProfiler();
        
        return "";
    }
 
    
}
