/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.eventbus.ZaloDeviceNotifier;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.centralizedkeys.ZaloDeviceKey;
import com.vng.zalo.zte.rnd.dbingestionmw.device.thrift.ZUDM_UserDeviceRaw;
import org.apache.log4j.Logger;

/**
 * @date Aug 14, 2019
 * @author quydm
 */
public class ZaloDeviceModel extends DMPModel{
    private static final Class THIS_CLASS = ZaloDeviceModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final ZaloDeviceModel INSTANCE = new ZaloDeviceModel();
    private static final ZiDB64Client DEVICE_DB = new ZiDB64Client("ZUDMDevices_ZiDB64");

    public TActionResult uploadDeviceRaw(String src, int zaloId, ZUDM_UserDeviceRaw inputData) {
        return ingestAndNotify(DEVICE_DB, "uploadDevice", src, 
                zaloId, (x) -> ZaloDeviceKey.forDeviceRawOf(x), inputData,
                ()->ZaloDeviceNotifier.notifyZaloDevice(zaloId, inputData),
                THIS_CLASS,LOGGER);
    }
}
