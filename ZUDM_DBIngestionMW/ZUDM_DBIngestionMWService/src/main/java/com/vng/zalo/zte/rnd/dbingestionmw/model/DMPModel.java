/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import org.apache.log4j.Logger;
import org.apache.thrift.TBase;

import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @date Oct 17, 2019
 * @author quydm
 */
public class DMPModel extends BaseModel{
    
    //Ingest ZiDB64Client with eventbus (zaloId)
    public <T extends TBase> TActionResult ingestAndNotify(
            ZiDB64Client client,String apiName,String src, 
            int zaloId,Function<Integer,Long> keyMaker, T inputData, 
            Supplier<Long> notifier,
            Class clazz,Logger logger
            ){ 
        
        TActionResult ingestionResult = ingest(client, apiName, src, 
                                            keyMaker.apply(zaloId), inputData, 
                                            clazz, logger);
        
        if ( ingestionResult == TActionResult.SUCCESS && isNormalGlobalId(zaloId) ){ // Ingestion successfully and not-QoS globalId
            notifier.get();
            countEventBusFor(clazz,apiName);
        }
        countSuccessFor(clazz, apiName);
        return TActionResult.SUCCESS;
    }
}
