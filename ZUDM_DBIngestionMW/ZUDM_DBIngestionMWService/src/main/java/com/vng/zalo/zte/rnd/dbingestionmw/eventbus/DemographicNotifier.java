package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.*;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_UserHometown;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TAction;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TDMPUpdatedField;
import com.vng.zing.zudm_dmpmiddleware.thrift.*;
import com.vng.zing.zudm_middleware.thrift.TProvince;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tinhdt
 */
public class DemographicNotifier extends DMPNotifier{
    
    public static long notifyIdentifiedAge(int zaloId, com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_UserAge pAge){
        TDMPUserAgePredict age = new TDMPUserAgePredict();
        age.setAge((double) pAge.getAge());
        age.setExpiryDate(ExpiryDate.setExpriDate((int) pAge.getUpdatedDate(), ExpiryDate._3MONTHS));
        age.setScore(1.0);
        age.setError(0);
        ByteBuffer serializedData = serialize(age);
        return notify(zaloId, TDMPUpdatedField.UserAgePredict, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyIdentifiedGender(int globalId, ZUDM_UserPredictedGender pGender){
        TDMPUserGenderPredict gender = new TDMPUserGenderPredict();
        gender.setGender(pGender.gender_id);
        gender.setExpiryDate(ExpiryDate.setExpriDate((int) pGender.getUpdatedDate(), ExpiryDate._3MONTHS));
        gender.setScore(pGender.getScore());
        gender.setError(0);
        ByteBuffer serializedData = serialize(gender);
        return notify(globalId, TDMPUpdatedField.UserGenderPredict, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyMaritalStatus(int zaloId, ZUDM_UserMaritalStatus pMaritalStatus){
        TDMPUserMaritalStatusPredict maritalStatus = new TDMPUserMaritalStatusPredict();
        maritalStatus.setStatus(pMaritalStatus.getStatus());
        maritalStatus.setExpiryDate(ExpiryDate.setExpriDate((int) pMaritalStatus.getUpdatedDate(), ExpiryDate._3MONTHS));
        maritalStatus.setScore(1.0);
        maritalStatus.setError(0);
        ByteBuffer serializedData = serialize(maritalStatus);
        return notify(zaloId, TDMPUpdatedField.UserMaritalStatusPredict, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyRealname(int zaloId, ZUDM_UserRealNameList realName){
        TDMPUserRealNamePredict namePredict = new TDMPUserRealNamePredict(-1);
        namePredict.setReal_names(realName.getNames());
        namePredict.setExpiryDate(ExpiryDate.setExpriDate((int) realName.getUpdatedDate(), ExpiryDate._3MONTHS));
        namePredict.setScore(1.0);
        namePredict.setError(0);
        ByteBuffer serializedData = serialize(namePredict);
        return notify(zaloId, TDMPUpdatedField.UserRealNamePredict, serializedData, TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyHometown(int zaloId, ZUDM_UserHometown hometown){
        TDMPUserHometownPredict hometownPredict = new TDMPUserHometownPredict(-1);
        TDMPUserAddress address = new TDMPUserAddress();
        address.setCountry(TCountry.VIET_NAM);
        address.setCity(TProvince.findByValue(hometown.getProvince().getCityId()).name());
        address.setLocation_id(TCountry.VIET_NAM.getValue() * 1000000 + hometown.getProvince().getCityId() * 10000);
        hometownPredict.setProvince(address);
        hometownPredict.score = hometown.getConfident();
        hometownPredict.expiryDate = ExpiryDate.setExpriDate((int) hometown.getUpdatedDate(), ExpiryDate.WEEK);
        hometownPredict.setError(0);
        ByteBuffer serializedData = serialize(hometownPredict);
        return notify(zaloId, TDMPUpdatedField.UserHometownPredict, serializedData, TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyParentalStatus(int zaloId, ZUDM_UserParentalStatus pParentalStatus){
        TDMPUserParentalStatusPredict parentStatus = new TDMPUserParentalStatusPredict();
        List<TDMPAgeRangeOfChild> listAgeRangeOfChild = pParentalStatus.getAgeRangeOfChilds().stream().map(age -> {
            TDMPAgeRangeOfChild ageRangeOfChild = new TDMPAgeRangeOfChild();
            if (age.getAgeRangeOfChild().equals("[0, 6)") )
            {
                ageRangeOfChild.setAgeRangeOfChild(TDMPAgeRangeOfChildEnum.FROM_0_TO_6);
            }
            else if (age.getAgeRangeOfChild().equals("[6, inf)") )
            {
                ageRangeOfChild.setAgeRangeOfChild(TDMPAgeRangeOfChildEnum.FROM_6_TO_INF);
            }
            ageRangeOfChild.setCount(age.getCount());
            return ageRangeOfChild;
        }).collect(Collectors.toList());
        parentStatus.setParentalStatus(TDMPUserParentalStatus.findByValue(pParentalStatus.getParentalStatus()));
        parentStatus.setListAgeRangeOfChild(listAgeRangeOfChild);
        parentStatus.setExpiryDate(ExpiryDate.setExpriDate((int) pParentalStatus.getUpdatedDate(), ExpiryDate._3MONTHS));
        parentStatus.setScore(1.0);
        parentStatus.setError(0);
        ByteBuffer serializedData = serialize(parentStatus);
        return notify(zaloId, TDMPUpdatedField.UserParentalStatusPredict, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyEducationLevel(int zaloId, ZUDM_UserEducationLevel pEducationLevel){
        TDMPUserEducationLevelPredict educationLevel = new TDMPUserEducationLevelPredict();
        educationLevel.setLevel(TDMPEducationLevel.findByValue((int)pEducationLevel.getLevel()));
        educationLevel.setExpiryDate(ExpiryDate.setExpriDate((int) pEducationLevel.getUpdatedDate(), ExpiryDate._3MONTHS));
        educationLevel.setScore(1.0);
        educationLevel.setError(0);
        ByteBuffer serializedData = serialize(educationLevel);
        return notify(zaloId, TDMPUpdatedField.UserEducationLevelPredict, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyUserCreditScore(int zaloId, ZUDM_UserCreditScore pCreditScore){
        TDMPUserCreditScore creditScore = new TDMPUserCreditScore();
        creditScore.setValue(pCreditScore.getValue());
        creditScore.setExpiryDate(ExpiryDate.setExpriDate((int) pCreditScore.getExpiryDate(), ExpiryDate._3MONTHS));
        creditScore.setError(0);
        ByteBuffer serializedData = serialize(creditScore);
        return notify(zaloId, TDMPUpdatedField.UserCreditScore, serializedData,TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyRemoveUserCreditScore(int zaloId){
        TDMPUserCreditScore creditScore = new TDMPUserCreditScore();
        ByteBuffer serializedData = serialize(creditScore);
        return notify(zaloId, TDMPUpdatedField.UserCreditScore, serializedData,TAction.Remove, "IDENTIFIED");
    }
    
    public static long notifyNewUserCreditScore(int zaloId, ZUDM_UserCreditScore pCreditScore){
        TDMPUserCreditScore creditScore = new TDMPUserCreditScore();
        creditScore.setValue(pCreditScore.getValue());
        creditScore.setExpiryDate(ExpiryDate.setExpriDate((int) pCreditScore.getExpiryDate(), ExpiryDate._3MONTHS));
        creditScore.setError(0);
        ByteBuffer serializedData = serialize(creditScore);
        return notify(zaloId, TDMPUpdatedField.UserCreditScore, serializedData,TAction.New, "IDENTIFIED");
    }
}
