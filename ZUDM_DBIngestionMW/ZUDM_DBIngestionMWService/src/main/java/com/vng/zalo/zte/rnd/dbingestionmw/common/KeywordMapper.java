/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.common;

import com.vng.zing.common.ZErrorDef;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserInterestTagDescription;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;
import org.apache.log4j.Logger;

/**
 *
 * @author lap13633
 */
public class KeywordMapper {

    private final Class _ThisClass = KeywordMapper.class;
    private final Logger LOGGER = ZLogger.getLogger(_ThisClass);
    private final static KeywordMapper INSTANCE = new KeywordMapper();

    private List<String> csvList = Arrays.asList(
            "1010", "1020", "1030",
            "2010", "2020",
            "4020", "4030", "4040","4050", "4060",
            "5000",
            "6010", "6020", "6030",
            "7010", "7020",
            "8010", "8020",
            "9010", "9020",
            "10010", "10020",
            "11010", "11020",
            "12010",
            "13000",
            "14010", "14020");
    private String csvNamePrefixDev = "data/";
    private String csvNamePrefixLive = "/zserver/java-projects/zudm/ZUDM_DBIngestionMWService/data/";
//    private String csvNamePrefixLive = "/home/lap13633/dbingestionmw/ZUDM_DBIngestionMWService/data/old_stuff/userinterest_";
    private String csvNamePostfix = ".csv";
    String csvNamePrefix;

    private HashMap<String, String> idToKeyword = new HashMap<>();
    private HashMap<String, String> idToCategory = new HashMap<>();
    private HashMap<String, String> keywordToId = new HashMap<>();

    public KeywordMapper() {
        if (DeployMode.CURRENT_DEPLOY_MODE.equals(DeployMode.DEV)) {
            this.csvNamePrefix = this.csvNamePrefixDev;
        } else {
            this.csvNamePrefix = this.csvNamePrefixLive;
        }

        for (String name : csvList) {
            String csvPath = this.csvNamePrefix + name + csvNamePostfix;
//            System.out.println(csvPath);
            readCsvToMap(csvPath);
        }
        
        if (DeployMode.CURRENT_DEPLOY_MODE.equals(DeployMode.DEV)){
            for (String key: keywordToId.keySet()){
//                System.out.println(key);
            }
        }
    }

    public static KeywordMapper getInstance() {
        return INSTANCE;
    }

    public final void readCsvToMap(String filePath) {
        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            stream
                    .map(line -> line.split("\\t"))
                    .forEach(listCol -> {
                        String cateId = listCol[0];
                        String id = listCol[1];
                        String keyword = listCol[2].toLowerCase();
                        String keywordOriginal = listCol[2];
                        if (listCol.length > 3) {
                            String category = listCol[3];
                            idToCategory.put(id, category);
                        }
                        idToKeyword.put(id, keywordOriginal);
                        
                        keywordToId.put(keyword, id);

                    });
            LOGGER.info("UserInterestMap: done readCsvToMap " + filePath);
            System.out.println("UserInterestMap: done readCsvToMap " + filePath);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("error read CSV to map");
            LOGGER.error("error read CSV to map", e);
        }
    }

    public String getCategoryFromId(String id) {
        String result = null;
        if (this.idToCategory.containsKey(id)) {
            result = this.idToCategory.get(id);
        }
        return result;
    }

    public TDMPUserInterestTagDescription getTagDescriptionFromId(String src, int id) {
        String idS = String.valueOf(id);
        TDMPUserInterestTagDescription result = new TDMPUserInterestTagDescription();
        String cateResult = getCategoryFromId(idS);
        String keyword = getKeywordFromId(idS);
        if (keyword != null && keyword.length() > 0) {
            result.setError(ZErrorDef.SUCCESS);
            result.setTagValue(keyword);
        } else {
            result.setError(ZErrorDef.FAIL);
            return result;
        }
        if (cateResult != null && cateResult.length() > 0) {
            result.setTagCategory(cateResult);
        }
        return result;
    }

    public String getKeywordFromId(String id) {
        String result = null;
        if (idToKeyword.containsKey(id)) {
//            System.out.println("get ID:"+idToKeyword.get(id));
            result = idToKeyword.get(id);
        }
        return result;
    }

    public String getIdFromKeyword(String keyword) {
        String result = "0";
        if (this.keywordToId.containsKey(keyword.toLowerCase())) {
            result = this.keywordToId.get(keyword.toLowerCase());
        }
        return result;
    }

}
