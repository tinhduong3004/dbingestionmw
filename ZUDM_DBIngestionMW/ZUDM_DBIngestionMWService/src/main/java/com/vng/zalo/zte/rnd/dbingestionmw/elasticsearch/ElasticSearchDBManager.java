package com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author phucpt2
 */
public class ElasticSearchDBManager {

    public static DBHandleLocationQuery location_elastic_search = new DBHandleLocationQuery("ES_Location");
    public static DBHandleUserInterestOperations userinterst_elastic_search = new DBHandleUserInterestOperations("ES_UserInterest");
    public static DBHandleUserInterestOperations anonymous_userinterst_elastic_search = new DBHandleUserInterestOperations("ES_AnonymousUserInterest");

    // test section
    public static void main(String[] args) {
        List<Long> user_list = new ArrayList<>();
        user_list.add(107696953l);
        user_list.add(153316533l);
        user_list.add(169204920l);
        user_list.add(193060654l);
        user_list.add(1930606541l);
//
        System.out.println(location_elastic_search.getListUser(user_list));

        
        System.exit(0);
    }

}
