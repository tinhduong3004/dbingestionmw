/*
 * Copyright (c) 2012-2020 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.admin.AdminModel;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import static com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients.CreditScoreDB;
import com.vng.zalo.zte.rnd.dbingestionmw.eventbus.DemographicNotifier;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zudm.common.centralizedkeys.DemographicPredictionKey;
import org.apache.log4j.Logger;

/**
 * @date Apr 13, 2020
 * @author quydm
 */
public class KeyRemovingModel extends BaseModel{
    private static final Class THIS_CLASS = DemographicModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final KeyRemovingModel INSTANCE = new KeyRemovingModel();


    
    public TActionResult removeKey(String src, int zaloId) {
        if (AdminModel.INSTANCE.validCaller(src)){
            long key = DemographicPredictionKey.forUserRealNameOf(zaloId);
            if ( DBClients.DemographicDB.remove(key) > 0 ){
                return TActionResult.SUCCESS;
            }else{
                return TActionResult.ERROR;
            }
        }else{
            return TActionResult.WRONGAUTH;
        }
    }
    
    
    //remove keys of DemographicDB (note: madeKey)
    public TActionResult removeDemographicKey(String src, long dataKey) {
        if (AdminModel.INSTANCE.validCaller(src)){
            if ( DBClients.DemographicDB.remove(dataKey) > 0 ){
                return TActionResult.SUCCESS;
            }else{
                return TActionResult.ERROR;
            }
        }else{
            return TActionResult.WRONGAUTH;
        }
    }
    
    //remove keys of CreditScoreDB 
    public TActionResult removeCreditScoreV2Key(String src, int zaloId, boolean notify){
        return removeAndNotify(CreditScoreDB, "removeCreditScoreKeyV2", 
                src,  zaloId,x->DemographicPredictionKey.forCreditScoreV2Of(x),
                () -> DemographicNotifier.notifyRemoveUserCreditScore(zaloId), notify, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult removeCreditScoreV3Key(String src, int zaloId, boolean notify){
        return removeAndNotify(CreditScoreDB, "removeCreditScoreKeyV3", 
                src,  zaloId,x->DemographicPredictionKey.forCreditScoreV3Of(x),
                () -> DemographicNotifier.notifyRemoveUserCreditScore(zaloId), notify,
                THIS_CLASS,LOGGER);
    }
    
}
