package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TArtistInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TKeywordInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserInterestArtists;
import com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift.TUserKeywords;
import com.vng.zalo.zte.rnd.dbingestionmw.eventbus.UserInterestNotifier;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb.thrift.TValueResult;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.ZUDM_Util;
import com.vng.zing.zudm.common.centralizedkeys.UserInterestKey;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;

/**
 *
 * @author tinhdt
 */
public class UserInterestModel extends BaseModel {

    private static final Class THIS_CLASS = UserInterestModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final UserInterestModel INSTANCE = new UserInterestModel();
    public static final ZiDB64Client USERKW = new ZiDB64Client("ZUDM_ZiDB64UserKWDB");
    public static final ZiDBClient ANONYMOUSUSERINTEREST = new ZiDBClient("ZUDMAnonymousUserInterest_ZiDB");

    public List<TKeywordInfo> limit50(List<TKeywordInfo> keywordList) {
        return keywordList.stream().limit(50).collect(Collectors.toList());
    }

    public List<TArtistInfo> limit200(List<TArtistInfo> ArtistList) {
        return ArtistList.stream().limit(200).collect(Collectors.toList());
    }

    public TActionResult uploadUserKeywords(String authenToken, int zaloId, int hour, TUserKeywords inputData) {
        inputData.setKeywords(limit50(inputData.getKeywords())); // add limit 50 'cause overheating db
        return ingestWithHour(USERKW, "uploadUserKeywords", authenToken, zaloId, hour,
                (x, y) -> UserInterestKey.forUserKeyworksOf(x, y), inputData,
                THIS_CLASS, LOGGER);
    }

    public TActionResult uploadUserInterest(String authenToken, int dataKey, int hour, TUserKeywords inputData, boolean notify) {
        inputData.setKeywords(limit50(inputData.getKeywords())); // add limit 50 'cause overheating db
        return ingestWithHourAndNotify(USERKW, "uploadUserInterest", authenToken, dataKey, hour,
                (x, y) -> UserInterestKey.forUserInterestOf(x, y), inputData,
                (zid) -> UserInterestNotifier.notifyUserInterest(zid, inputData), notify,
                THIS_CLASS, LOGGER);
    }

    public TActionResult uploadUserInterestByVectorSimilarity(String authenToken, int dataKey, int hour, TUserKeywords inputData) {
        inputData.setKeywords(limit50(inputData.getKeywords())); // add limit 50 'cause overheating db
        return ingestWithHour(USERKW, "uploadUserInterestByVectorSimilarity", authenToken, dataKey, hour,
                (x, y) -> UserInterestKey.forUserInterestByVectorSimilarityOf(x, y), inputData, THIS_CLASS, LOGGER);
    }

    public TActionResult uploadUserInterestByAls(String authenToken, int dataKey, int hour, TUserKeywords inputData) {
        inputData.setKeywords(limit50(inputData.getKeywords())); // add limit 50 'cause overheating db
        return ingestWithHour(USERKW, "uploadUserInterestByAls", authenToken, dataKey, hour,
                (x, y) -> UserInterestKey.forUserInterestByAlsOf(x, y), inputData, THIS_CLASS, LOGGER);
    }

    public TActionResult uploadAnonymousUserInterest(String authenToken, long gId, TUserKeywords inputData) {
        inputData.setKeywords(limit50(inputData.getKeywords())); // add limit 50 'cause overheating db
        return ingestClient(ANONYMOUSUSERINTEREST, "uploadAnonymousUserInterest", authenToken, gId, inputData,
                (gid) -> UserInterestNotifier.notifyAnonymousUserInterest(gid, inputData),
                THIS_CLASS, LOGGER);
    }

    public TActionResult uploadUserInterestArtists(String authenToken, int uid, TUserInterestArtists inputData, boolean bln) {
        inputData.setList_artists(limit200(inputData.getList_artists())); // add limit 200 'cause overheating db
        return ingestAndNotify(USERKW, "uploadUserInterestArtists",
                authenToken, uid, x -> UserInterestKey.forUserInterestArtistsOf(x), inputData,
                () -> UserInterestNotifier.notifyUserInterestArtists(uid, inputData),
                bln,
                THIS_CLASS, LOGGER);
    }

    public TActionResult UpdateUserInterest(String authenToken, int dataKey) {
        TUserKeywords userInterest = (TUserKeywords) WithCompression.getFrom(USERKW,
                UserInterestKey.forUserInterestOf(dataKey, 23), new TUserKeywords());
        if (userInterest != null && userInterest.keywords.get(0).value.length() == 4) {
            TUserKeywords userInterestNew = new TUserKeywords();
            userInterestNew.setUpdatedOn(userInterest.getUpdatedOn());
            for (TKeywordInfo info : userInterest.keywords) {
                TKeywordInfo newInfo = new TKeywordInfo();
                newInfo.setValue(info.getValue() + "0");
                newInfo.setScore(info.getScore());
                newInfo.setType(info.getType());
                userInterestNew.addToKeywords(newInfo);
            }

//                return Notify(USERKW, "UpdateUserInterest", authenToken, dataKey,
//                        userInterestNew, (zid) -> UserInterestNotifier.notifyUserInterest(zid, userInterestNew),
//                        THIS_CLASS, LOGGER);
            return ingest(USERKW, "UpdateUserInterest",
                    authenToken, UserInterestKey.forUserInterestOf(dataKey, 23), userInterestNew,
                    THIS_CLASS, LOGGER);
        } else {
            return TActionResult.NOT_EXIST;
        }
    }

    public TActionResult UpdateAnonymousUserInterest(String authenToken, long gid) {
        ByteBuffer key = ByteBufferUtil.fromString(String.valueOf(gid));
        TValueResult rel = ANONYMOUSUSERINTEREST.get(key);
        TUserKeywords userInterest = ZUDM_Util.getTbase(LOGGER, new TUserKeywords(), rel);
        if (userInterest != null && userInterest.keywords.get(0).value.length() == 4) {
            TUserKeywords userInterestNew = new TUserKeywords();
            userInterestNew.setUpdatedOn(userInterest.getUpdatedOn());
            for (TKeywordInfo info : userInterest.keywords) {
                TKeywordInfo newInfo = new TKeywordInfo();
                newInfo.setValue(info.getValue() + "0");
                newInfo.setScore(info.getScore());
                newInfo.setType(info.getType());
                userInterestNew.addToKeywords(newInfo);
            }

//            return NotifyAnonymousUserInterest(USERKW, "UpdateAnonymousUserInterest", authenToken, gid,
//                    userInterestNew, (gId) -> UserInterestNotifier.notifyAnonymousUserInterest(gId, userInterestNew),
//                    THIS_CLASS, LOGGER);
        
            return ingestZiDBClient(ANONYMOUSUSERINTEREST, "UpdateAnonymousUserInterest",
                    authenToken, gid, userInterestNew,
                    THIS_CLASS, LOGGER);

        } else {
            return TActionResult.NOT_EXIST;
        }
    }
}
