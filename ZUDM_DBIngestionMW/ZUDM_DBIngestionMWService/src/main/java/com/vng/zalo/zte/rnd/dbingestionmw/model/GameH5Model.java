/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBUtils;
import com.vng.zalo.zte.rnd.dbingestionmw.gameh5.thrift.ZUDM_Gameh5PlayingInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.gameh5.thrift.ZUDM_Gameh5TienlenPlayingInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.strlist32bm.thrift.ChangeType;
import com.vng.zing.strlist32bm.thrift.ChangeTypeOption;
import com.vng.zing.strlist32bm.thrift.SetOp;
import com.vng.zing.zcommon.thrift.PutPolicy;
import com.vng.zing.zudm.common.ZUDM_Util;
import com.vng.zing.zudm.common.centralizedkeys.Gameh5Key;
import org.apache.log4j.Logger;

import java.nio.ByteBuffer;
import java.util.List;

/**
 *
 * @author thient
 */
public class GameH5Model {

    private static final Class THIS_CLASS = GameH5Model.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    private static final GameH5Model INSTANCE = new GameH5Model();

    private GameH5Model() {

    }

    public static GameH5Model getInstance() {
        return INSTANCE;
    }

    /**
     * put db playing info contain win match, total match
     *
     * @deprecated use putPlayingInfo(int uid, int appid,
     * ZUDM_Gameh5TienlenPlayingInfo playingInfo) instead
     * @param uid
     * @param playingInfo
     * @return
     */
    public TActionResult putTienlenPlayingInfo(int uid, ZUDM_Gameh5TienlenPlayingInfo playingInfo) {
        // make key from uid
        String key = Gameh5Key.forTienLenPlayingInfo(uid);

        TActionResult putdbResult = TActionResult.ERROR;
        try {
            // convert key to ByteBuffer
            ByteBuffer keyBB = ByteBufferUtil.fromString(key);
            // put db
            long errorCode = ZUDM_Util.putWithProfilerWithLog(LOGGER, THIS_CLASS, "putTienlenPlayingInfo", DBClients.playingInfoDB, keyBB, playingInfo, PutPolicy.ADD_OR_UDP);
            if (ZErrorHelper.isSuccess(errorCode)) {
                putdbResult = TActionResult.SUCCESS;
            } else {
                LOGGER.error("putTienlenPlayingInfo with uid " + String.valueOf(uid) + " " + "error code " + ZErrorHelper.errorToString(errorCode));
            }
        } catch (Exception e) {
            LOGGER.error("putTienlenPlayingInfo with uid " + String.valueOf(uid) + " " + "exception");
        }
        return putdbResult;
    }

    /**
     * put db playing info contain win match, total match
     *
     * @param uid
     * @param appid
     * @param playingInfo
     * @return
     */
    public TActionResult putPlayingInfo(int uid, int appid, ZUDM_Gameh5PlayingInfo playingInfo) {
        ThreadProfiler profiler = Profiler.getThreadProfiler();
        // make key from uid
        String key = Gameh5Key.forPlayingInfo(uid, appid);

        TActionResult putdbResult = TActionResult.ERROR;

        if ((playingInfo.appid != appid) || (playingInfo.uid != uid)) {
            profiler.push(THIS_CLASS, "putPlayingInfo.wrong_data");
            // uid and appid must match in playingInfo object
            profiler.pop(THIS_CLASS, "putPlayingInfo.wrong_data");
            return putdbResult;
        }
        String profilerName = "putPlayingInfo_appid_" + String.valueOf(appid);
        try {
            // convert key to ByteBuffer
            ByteBuffer keyBB = ByteBufferUtil.fromString(key);
            // put db

            long errorCode = ZUDM_Util.putWithProfilerWithLog(LOGGER, THIS_CLASS, profilerName, DBClients.playingInfoDB, keyBB, playingInfo, PutPolicy.ADD_OR_UDP);
            if (ZErrorHelper.isSuccess(errorCode)) {
                // errorCode is SUCCESS
                putdbResult = TActionResult.SUCCESS;
            } else {
                // error in step putdb
                profiler.push(THIS_CLASS, profilerName + ".putdb_errorcode_fail");
                profiler.pop(THIS_CLASS, profilerName + ".putdb_errorcode_fail");
                LOGGER.error(profilerName + " with uid " + String.valueOf(uid) + " " + "error code " + ZErrorHelper.errorToString(errorCode));
            }
        } catch (Exception e) {
            profiler.push(THIS_CLASS, profilerName + ".exception");
            profiler.pop(THIS_CLASS, profilerName + ".exception");
            LOGGER.error(profilerName + " with uid " + String.valueOf(uid) + " " + "exception");
        }
        return putdbResult;
    }



    /**
     * remove uid list of UserHighWinrate must run before putUserHighWinrate
     *
     * @param appid
     * @return
     */
    public TActionResult removeUserHighWinrate(int appid) {
        ThreadProfiler profiler = Profiler.getThreadProfiler();
        TActionResult result = TActionResult.ERROR;
        String profilerName = "putUserHighWinrate_appid_" + String.valueOf(appid);

        try {
            String key = Gameh5Key.forWeeklyUserHighWinrate(appid);
            long errorCode = DBUtils.removeAllEntries(DBClients.strList32bmCommon6Client, key);
            if (ZErrorHelper.isSuccess(errorCode)) {
                result = TActionResult.SUCCESS;
            } else {
                // error in step putdb
                System.out.println(ZErrorHelper.errorToString(errorCode));
                profiler.push(THIS_CLASS, profilerName + ".putdb_errorcode_fail");
                profiler.pop(THIS_CLASS, profilerName + ".putdb_errorcode_fail");
                LOGGER.error(profilerName + " with appid " + String.valueOf(appid) + " " + "error code " + ZErrorHelper.errorToString(errorCode));
            }
        } catch (Exception e) {

        }
        return result;
    }

    /**
     * put user list into key of UserHighWinrate can run multiple time to join
     * the list
     *
     * @param uidList
     * @param appid
     * @return
     */
    public TActionResult putUserHighWinrate(List<Integer> uidList, int appid) {
        ThreadProfiler profiler = Profiler.getThreadProfiler();
        TActionResult result = TActionResult.ERROR;
        String profilerName = "putUserHighWinrate_appid_" + String.valueOf(appid);
        profiler.push(THIS_CLASS, profilerName);
        try {
            String key = Gameh5Key.forWeeklyUserHighWinrate(appid);

            ChangeTypeOption option = new ChangeTypeOption();
            option.changeType = ChangeType.ASSIGN_FIRST;
            option.policy = PutPolicy.ADD_OR_UDP;

            long errorCode = DBClients.strList32bmCommon6Client.combinWithL32V2(key, uidList, SetOp.C_OR, option);
            
            if (ZErrorHelper.isSuccess(errorCode)) {
                result = TActionResult.SUCCESS;
            } else {
                // error in step putdb
                System.out.println(ZErrorHelper.errorToString(errorCode));
                profiler.push(THIS_CLASS, profilerName + ".putdb_errorcode_fail");
                profiler.pop(THIS_CLASS, profilerName + ".putdb_errorcode_fail");
                LOGGER.error(profilerName + " with appid " + String.valueOf(appid) + " " + "error code " + ZErrorHelper.errorToString(errorCode));
            }
        } catch (Exception e) {
            e.printStackTrace();
            profiler.push(THIS_CLASS, profilerName + ".exception");
            profiler.pop(THIS_CLASS, profilerName + ".exception");
            LOGGER.error(profilerName + " with appid " + String.valueOf(appid) + " " + "exception");
        }
        profiler.pop(THIS_CLASS, profilerName);
        return result;
    }

}
