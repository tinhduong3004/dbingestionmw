package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.*;
import com.vng.zalo.zte.rnd.dbingestionmw.eventbus.DemographicNotifier;
import com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift.ZUDM_UserNumFriend;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.ZUDM_UserHometown;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zudm.common.centralizedkeys.DemographicPredictionKey;
import com.vng.zing.zudm.common.centralizedkeys.UserIncome;
import com.vng.zing.zudm.common.centralizedkeys.UserKwKey;
import com.vng.zing.zudm.util.encrypt.CryptoUtil;
import org.apache.log4j.Logger;

import java.security.MessageDigest;

import static com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients.*;

/**
 *
 * @author tinhdt
 */
public class DemographicModel extends DMPModel{
    private static final Class THIS_CLASS = DemographicModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final DemographicModel INSTANCE = new DemographicModel();
    
    public TActionResult uploadPhonebook(String src, int zaloId, ZUDM_UserPhonebookName inputData){
        return ingest(UserKWDB, "ingestPhonebook",
               src, UserKwKey.forUserPhonebookOf(zaloId),inputData,
               THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadUserNumFriend(String src, int zaloId, ZUDM_UserNumFriend inputData){
        return ingest(DemographicDB2, "ingestUserNumFriend", 
                src, DemographicPredictionKey.forNumFriendOf(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadFullName(String src, int zaloId, ZUDM_UserRealNameList inputData){
        return ingest(DemographicDB2, "ingestFullName", 
                src, DemographicPredictionKey.forUserFullNameOf(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestBidirectionalPhonebook(String src, int src_id, ZUDM_BidirectionalPhonebookList inputData){
        return ingest(UserKWDB, "ingestBidirectionalPhonebook", 
                src, UserKwKey.forBidirectionalPhonebookOf(src_id),inputData,
                THIS_CLASS,LOGGER);
    }
    // TODO: data key should be clarified the type
    public TActionResult uploadUserIncome(String src, long dataKey, ZUDM_UserIncome inputData){
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            String passphrase = "";
            if (String.valueOf(dataKey).length() < 6){
                passphrase = String.valueOf(dataKey * dataKey);
            } else {
                passphrase = String.valueOf(dataKey * dataKey).substring(0, 10);
            }
            messageDigest.update(passphrase.getBytes());
            String encrkey = new String(messageDigest.digest());
            inputData.setValue(CryptoUtil.encryptF(Float.valueOf(inputData.getValue()), encrkey));
            return ingest(UserIncomeDB, "ingestUserIncome", src, UserIncome.forOrginalKeyOf(dataKey),inputData,THIS_CLASS,LOGGER);
        } catch (Exception e) {
            return TActionResult.ERROR;
        }
    }
    // TODO: data key should be clarified the type
    public TActionResult uploadIncomeRank(String src, long dataKey, ZUDM_UserIncome inputData){
        try {
        long key = UserIncome.forIncomeRankOf(dataKey);
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        String passphrase = String.valueOf(key * key).substring(0, 10);
        messageDigest.update(passphrase.getBytes());
        String encrkey = new String(messageDigest.digest());
        inputData.setValue(CryptoUtil.encryptF(Float.valueOf(inputData.getValue()), encrkey));
        return ingest(UserIncomeDB, "ingestUserIncome", src, UserIncome.forOrginalKeyOf(dataKey),inputData,THIS_CLASS,LOGGER);
        } catch (Exception e) {
            return TActionResult.ERROR;
        }
    }
    
    public TActionResult uploadCreditScore(String src, int zaloId, ZUDM_UserCreditScore inputData){
        return ingest(CreditScoreDB, "ingestCreditScore", 
                src, DemographicPredictionKey.forCreditScoreOf(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }

    public TActionResult uploadAgeV2(String src, int zaloId, ZUDM_UserAge inputData){
        return ingest(DemographicDB3, "uploadAgeV2", 
                src, DemographicPredictionKey.forUserAgeV2Of(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadGenderV2(String src, int zaloId, ZUDM_UserPredictedGender inputData){
        return ingest(DemographicDB3, "uploadGenderV2", 
                src, DemographicPredictionKey.forUserGenderV2Of(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }
    //--------------------------------------notify update apis---------------------------------------------------------
    public TActionResult uploadMaritalStatus(String src, int zaloId, ZUDM_UserMaritalStatus inputData){
        return ingestAndNotify(DemographicDB2, "ingestMaritalStatus", 
                src, zaloId,x->DemographicPredictionKey.forMaritalStatusOf(x),inputData, 
                () -> DemographicNotifier.notifyMaritalStatus(zaloId, inputData),
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadAge(String src, int zaloId, com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_UserAge inputData, boolean notify){
        return ingestAndNotify(DemographicDB2, "ingestAge", 
                src, zaloId,x->DemographicPredictionKey.forUserAgeOf(x),inputData, 
                () -> DemographicNotifier.notifyIdentifiedAge(zaloId, inputData), notify,
                THIS_CLASS,LOGGER);
    }
      
    public TActionResult uploadGender(String src, int zaloId, ZUDM_UserPredictedGender inputData, boolean notify){
        return ingestAndNotify(DemographicDB2, "ingestGender", 
                src, zaloId, x->DemographicPredictionKey.forUserGenderOf(x),inputData,
                () -> DemographicNotifier.notifyIdentifiedGender(zaloId, inputData), notify,
                THIS_CLASS,LOGGER);
    }

    public TActionResult uploadRealName(String src, int zaloId, ZUDM_UserRealNameList inputData){
        return ingest(DemographicDB3, "ingestRealName", 
                src, DemographicPredictionKey.forUserRealNameOf(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadHometown(String src, int zaloId, ZUDM_UserHometown inputData, boolean notify){
        return ingestAndNotify(DemographicDB2, "uploadHometown",
                src, zaloId,x->DemographicPredictionKey.forUserHometownOf(x),inputData,
                () -> DemographicNotifier.notifyHometown(zaloId, inputData), notify,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadParentalStatus(String src, int zaloId, ZUDM_UserParentalStatus inputData, boolean bln){
        return ingestAndNotify(DemographicDB2, "ingestParentalStatus", 
                src, zaloId,x->DemographicPredictionKey.forParentalStatusOf(x),inputData, 
                () -> DemographicNotifier.notifyParentalStatus(zaloId, inputData), 
                bln,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadEducationLevel(String src, int zaloId, ZUDM_UserEducationLevel inputData, boolean bln){
        return ingestAndNotify(DemographicDB2, "ingestEducationLevel", 
                src, zaloId,x->DemographicPredictionKey.forEducationLevelOf(x),inputData, 
                () -> DemographicNotifier.notifyEducationLevel(zaloId, inputData), bln,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadCreditScoreV2(String src, int zaloId, ZUDM_UserCreditScore inputData, boolean bln){
        return customIngestAndNotify(CreditScoreDB, "ingestCreditScoreV2", 
                src,  zaloId,x->DemographicPredictionKey.forCreditScoreV2Of(x),inputData,
                () -> DemographicNotifier.notifyUserCreditScore(zaloId, inputData), bln,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadNewCreditScoreV2(String src, int zaloId, ZUDM_UserCreditScore inputData, boolean bln){
        return customIngestAndNotify(CreditScoreDB, "uploadNewCreditScoreV2", 
                src,  zaloId,x->DemographicPredictionKey.forCreditScoreV2Of(x),inputData,
                () -> DemographicNotifier.notifyNewUserCreditScore(zaloId, inputData), bln,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadCreditScoreV3(String src, int zaloId, ZUDM_UserCreditScore inputData, boolean bln){
        return customIngestAndNotify(CreditScoreDB, "ingestCreditScoreV3", 
                src,  zaloId,x->DemographicPredictionKey.forCreditScoreV3Of(x),inputData,
                () -> DemographicNotifier.notifyUserCreditScore(zaloId, inputData), bln,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadNewCreditScoreV3(String src, int zaloId, ZUDM_UserCreditScore inputData, boolean bln){
        return customIngestAndNotify(CreditScoreDB, "uploadNewCreditScoreV3", 
                src,  zaloId,x->DemographicPredictionKey.forCreditScoreV3Of(x),inputData,
                () -> DemographicNotifier.notifyNewUserCreditScore(zaloId, inputData), bln,
                THIS_CLASS,LOGGER);
    }

}
