/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.fraudmining.thrift.TListDuplicateAccount;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zudm.common.centralizedkeys.AuthenKey;
import org.apache.log4j.Logger;

import static com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients.Tokenizer;

/**
 *
 * @author tinhdt
 */

public class Authen extends DMPModel{
    private static final Class THIS_CLASS = Authen.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final Authen INSTANCE = new Authen();
    
    public TActionResult uploadListDuplicateAccount(String src, int zaloId, TListDuplicateAccount inputData){
        return ingest(Tokenizer, "uploadListDuplicateAccount",
               src, AuthenKey.forDuplicateAccountKeyOf(zaloId),inputData,
               THIS_CLASS,LOGGER);
    }
}
