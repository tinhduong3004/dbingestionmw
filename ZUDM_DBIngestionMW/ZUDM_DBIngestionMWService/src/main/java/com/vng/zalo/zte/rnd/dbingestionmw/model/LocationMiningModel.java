// Created By PTP
package com.vng.zalo.zte.rnd.dbingestionmw.model;
import com.vng.zalo.zte.rnd.dbingestionmw.admin.AdminModel;
import com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients;
import com.vng.zalo.zte.rnd.dbingestionmw.eventbus.LocationMiningNotifier;
import com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift.*;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zcommon.thrift.PutPolicy;
import com.vng.zing.zidb.thrift.TValue;
import com.vng.zing.zidb.thrift.TValueResult;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.ZUDM_Util;
import com.vng.zing.zudm.common.centralizedkeys.DemographicPredictionKey;
import com.vng.zing.zudm.common.centralizedkeys.LocationMining;
import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_Household;
import com.vng.zing.exception.InvalidParamException;
import com.vng.zing.zudm.common.centralizedkeys.Ip2LocationKey;
import com.vng.zing.zuserdatamining.thrift.ZUDM_LivingLoc;
import com.vng.zing.zuserdatamining.thrift.ZUDM_TravelPath_City;
import com.vng.zing.zuserdatamining.thrift.ZUDM_TravelPath_POI;
import com.vng.zing.zuserdatamining.thrift.ZUDM_WifiInfo;
import java.nio.ByteBuffer;
import java.util.function.Consumer;
import java.util.function.Supplier;
import org.apache.log4j.Logger;
import org.apache.thrift.TBase;

/**
 *
 * @author tinhdt
 */
public class LocationMiningModel extends DMPModel {

    private static final Class THIS_CLASS = LocationMiningModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final LocationMiningModel INSTANCE = new LocationMiningModel();
    public static final ZiDB64Client HOUSE_HOLD = new ZiDB64Client("HouseholdExtension");
    public static final ZiDBClient GEO_CELL_DENSITY = new ZiDBClient("ZUDMGeoCellDensity");
    
    
    //ingest to ZiDBClient, no eventbus
    private <T extends TBase> TActionResult ingestClient(
            ZiDBClient client, String apiName, String src, // config info
            String dataKey, T inputData, // key-value
            Class clazz, Logger logger) { // eventbus
        if (checkValidCaller(src)) {
            try {
                ByteBuffer key = ByteBufferUtil.fromString(dataKey);
                TValue tvalue = ZUDM_Util.createZiDBTValueFromTBase(inputData);
                long error = client.put(key, tvalue, PutPolicy.ADD_OR_UDP);

                if (ZErrorHelper.isSuccess(error)) {
                    countSuccessFor(clazz, apiName);
                    return TActionResult.SUCCESS;
                } else {
                    countErrorFor(clazz, apiName);
                    return TActionResult.ERROR;
                }
            } catch (Exception ex) {
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }
    //For ZiDB64Client with profiler with log (LocationModel)
    public <T extends TBase> TActionResult ingestWithProfilerWithLog(
            ZiDB64Client client, String apiName, String src, // config info
            long key, T inputData, // key-value
            Class clazz, Logger logger
    ) { 
        if (AdminModel.INSTANCE.validCaller(src)) {
            long error = ZUDM_Util.putWithProfilerWithLog(logger, clazz, apiName, client, 
                    key, inputData, PutPolicy.ADD_OR_UDP); // return error code
            if (ZErrorHelper.isSuccess(error)) { // ingestion is successful
                countSuccessFor(clazz, apiName);
                logSuccessData(logger, apiName, key, inputData);
                return TActionResult.SUCCESS;
            } else {
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }
    
    
    public TActionResult ingestDensityCellCheckinInfo(String callerName, String cellId, ZUDM_DensityCellCheckinInfo inputData) {
        return ingestClient(GEO_CELL_DENSITY, "ingestDensityCellCheckinInfo", callerName, cellId,inputData, THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestDensityCellHomeInfo(String callerName, String cellId, ZUDM_DensityCellHomeInfo inputData) {
        return ingestClient(GEO_CELL_DENSITY, "ingestDensityCellHomeInfo", callerName,cellId, inputData, THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestHousehold(String src, int zaloId, ZUDM_Household inputData) {
        return ingest(HOUSE_HOLD, "ingestHousehold", src, LocationMining.forHouseHoldOf(zaloId), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestLivingProvince(String callerName, int zaloId, ZUDM_LivingLoc inputData) {
        return ingestWithProfilerWithLog(DBClients._DBCmon3Cli, "ingestLivingProvince", callerName, 
                LocationMining.forLivingProvinceOf(zaloId), inputData, 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestTravelPath(String callerName, String zaloId, ZUDM_TravelPath_City inputData) {
        return ingestClient(DBClients._DBClientTravelPathCity, "ingestTravelPath", callerName, zaloId, inputData, THIS_CLASS, LOGGER);
    }

    
    public TActionResult ingestPOIpath(String callerName, String locationId, ZUDM_TravelPath_POI inputData) {
        return ingestClient(DBClients._ZiDBTravelPathPOI, "ingestPOIpath", callerName, locationId,inputData, THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestSSIDInfo(String callerName, String locationId, ZUDM_WifiInfo inputData) {
        return ingestClient(DBClients._ZiDBWifiInfo, "ingestSSIDInfo", callerName, locationId,inputData, THIS_CLASS, LOGGER);
    }
    
    //-----------------------------------------------Event bus---------------------------------------------------------------
    
    private <T extends TBase> TActionResult ingestAndNotify(
            ZiDB64Client client, String apiName, String src, // config info
            int zaloId, T inputData, // key-value
            Supplier<Long> notifier, // eventbus
            Class clazz, Logger logger) {
        if (checkValidCaller(src)) {
                                        
            long error = ZUDM_Util.putWithProfilerWithLog( // The name is too cancer
                    logger, clazz, apiName, client, Long.valueOf(zaloId), inputData, PutPolicy.ADD_OR_UDP);
            if (ZErrorHelper.isSuccess(error)) {
                countSuccessFor(clazz, apiName);
                notifier.get();
                countEventBusFor(clazz, apiName);
                return TActionResult.SUCCESS;
            } else {
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }
    
    
    public TActionResult ingestForeignTripCount(String callerName, int zaloId, ZUDM_ForeignTripCount inputData, boolean notify) {
        return ingestAndNotify(DBClients.DemographicDB2, "ingestForeignTripCount", callerName, zaloId,
                x -> DemographicPredictionKey.forForeignTripCountOf(x), inputData, 
                ()-> LocationMiningNotifier.notifyForeignTripCount(zaloId, inputData), notify,
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestMostVisitedCity(String callerName, int zaloId, ZUDM_MostVisitedProvinces inputData, boolean notify) {
        return ingestAndNotify(DBClients.DemographicDB2, "ingestMostVisitedCity", callerName, zaloId,
                x -> DemographicPredictionKey.forMostVisitedProvinceOf(x), inputData,
                ()-> LocationMiningNotifier.notifyMostVisitedCity(zaloId, inputData), notify,
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestFlightCount(String callerName, int zaloId, ZUDM_FlightList inputData, boolean notify) {
        inputData.setCount(inputData.getFlight_listSize());
        return ingestAndNotify(DBClients.DemographicDB2, "ingestFlightCount", callerName, zaloId,
                x -> DemographicPredictionKey.forFlightOf(x), inputData,
                ()-> LocationMiningNotifier.notifyFightCount(zaloId, inputData), notify,
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestMostVisitedAdminArea(String callerName, int zaloId, ZUDM_MostVisitedArea inputData) {
        return ingestAndNotify(DBClients.MostVisitedAADB, "ingestMostVisitedAdminArea", 
                callerName, zaloId, inputData, 
                () -> LocationMiningNotifier.notifyUserLivingProvince(zaloId, inputData), 
                THIS_CLASS, LOGGER);
    }
    
    
    // ZUDM_ListLocationHomeWork is too "cancer"
    private <T extends TBase> TActionResult ingestClientWithProfiler(
            ZiDBClient client, String apiName, String src, // config info
            int zaloId, T inputData,Consumer<Integer> notifierHome, Consumer<Integer> notifierWork, // key-value
            Class clazz, Logger logger) { // eventbus

        if (checkValidCaller(src)) {
            ByteBuffer key = ByteBufferUtil.fromString(Integer.toString(zaloId));
            long error = ZUDM_Util.putWithProfilerWithLog(
                    logger, clazz, apiName,
                    client, key, inputData, PutPolicy.ADD_OR_UDP
            );

            if (ZErrorHelper.isSuccess(error)) {
                notifierHome.accept(zaloId);
                notifierWork.accept(zaloId);
                countEventBusFor(clazz,apiName);
                countSuccessFor(clazz, apiName);
                return TActionResult.SUCCESS;
            } else {
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }
    
    private <T extends TBase> TActionResult ingestClientWithProfiler(
            ZiDBClient client, String apiName, String src, // config info
            int zaloId, T inputData, Consumer<Integer> notifier, // key-value
            Class clazz, Logger logger) { // eventbus

        if (checkValidCaller(src)) {
            ByteBuffer key = ByteBufferUtil.fromString(Integer.toString(zaloId));
            long error = ZUDM_Util.putWithProfilerWithLog(
                    logger, clazz, apiName,
                    client, key, inputData, PutPolicy.ADD_OR_UDP
            );

            if (ZErrorHelper.isSuccess(error)) {
                notifier.accept(zaloId);
                countEventBusFor(clazz,apiName);
                countSuccessFor(clazz, apiName);
                return TActionResult.SUCCESS;
            } else {
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }
    
    public TActionResult ingestFullHomeWorkLocation(String callerName, int zaloId, ZUDM_ListLocationHomeWork inputData) {
        return ingestClientWithProfiler(DBClients._ZiDBHomeWorkLocation, "ingestFullHomeWorkLocation", callerName, zaloId, 
                inputData,(zId) -> LocationMiningNotifier.notifyHomeLocation(zId, inputData),(zId) -> LocationMiningNotifier.notifyWorkLocation(zId, inputData),
                THIS_CLASS, LOGGER);
    }

    public TActionResult ingestHomeLocation(String callerName, int zaloId, ZUDM_ListLocationHomeWork inputData) {
        ByteBuffer key = ByteBufferUtil.fromString(Integer.toString(zaloId));
        TValueResult valres = ZUDM_Util.getWithProfiler(THIS_CLASS, "updateDB.ingestHomeLocation.get", 
                DBClients._ZiDBHomeWorkLocation, key);
        ZUDM_ListLocationHomeWork old_result = ZUDM_Util.getTbase(LOGGER, new ZUDM_ListLocationHomeWork(), valres);

        if (old_result != null){
            for (ZUDM_LocationHomeWork loc : old_result.listLocationHomeWork){
                if (loc.type != ZUDM_LocationType.Home ){
                    inputData.addToListLocationHomeWork(loc);
                    break;
                }
            }
        }
        // Notify
        return ingestClientWithProfiler(DBClients._ZiDBHomeWorkLocation, "ingestHomeLocation", 
                callerName, zaloId, inputData,(zId) -> LocationMiningNotifier.notifyHomeLocation(zId, inputData), THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestWorkLocation(String callerName, int zaloId, ZUDM_ListLocationHomeWork inputData) {
        ByteBuffer key = ByteBufferUtil.fromString(Integer.toString(zaloId));
        TValueResult valres = ZUDM_Util.getWithProfiler(THIS_CLASS, "updateDB.ingestWorkLocation.get", 
                DBClients._ZiDBHomeWorkLocation, key);
        ZUDM_ListLocationHomeWork old_result = ZUDM_Util.getTbase(LOGGER, new ZUDM_ListLocationHomeWork(), valres);

        if (old_result != null){
            for (ZUDM_LocationHomeWork loc : old_result.listLocationHomeWork){
                if (loc.type != ZUDM_LocationType.Work ){
                    inputData.addToListLocationHomeWork(loc);
                    break;
                }
            }
        }
        // Notify
        return ingestClientWithProfiler(DBClients._ZiDBHomeWorkLocation, "ingestWorkLocation", 
                callerName, zaloId, inputData,(zId) -> LocationMiningNotifier.notifyWorkLocation(zId, inputData), THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestPartialHomeWorkLocation(String callerName, int zaloId, ZUDM_ListLocationHomeWork inputData) {
        ZUDM_LocationHomeWork homework = inputData.getListLocationHomeWork().get(0);
        int countData = inputData.getListLocationHomeWorkSize();
        ByteBuffer key = ByteBufferUtil.fromString(Integer.toString(zaloId));
        TValueResult valres = ZUDM_Util.getWithProfiler(THIS_CLASS, "updateDB.ZiDBHomeWorkLocation.get", 
                DBClients._ZiDBHomeWorkLocation, key);
        ZUDM_ListLocationHomeWork old_result = ZUDM_Util.getTbase(LOGGER, new ZUDM_ListLocationHomeWork(), valres);
        if (old_result != null && countData == 1){
            for (ZUDM_LocationHomeWork loc : old_result.listLocationHomeWork){
                if (loc.type != homework.type ){
                    inputData.addToListLocationHomeWork(loc);
                    break;
                }
            }
        }
        // Notify
        if (countData == 1 && homework.type == ZUDM_LocationType.Home) {
            return ingestClientWithProfiler(DBClients._ZiDBHomeWorkLocation, "ingestHomeLocation", 
                callerName, zaloId, inputData,
                (zId) -> LocationMiningNotifier.notifyHomeLocation(zId, inputData), THIS_CLASS, LOGGER);
                
        } else if (countData == 1 && homework.type == ZUDM_LocationType.Work) {
            return ingestClientWithProfiler(DBClients._ZiDBHomeWorkLocation, "ingestWorkLocation", 
                callerName, zaloId, inputData,
                (zId) -> LocationMiningNotifier.notifyWorkLocation(zId, inputData), THIS_CLASS, LOGGER);
        } else {
            return ingestClientWithProfiler(DBClients._ZiDBHomeWorkLocation, "ingestFullHomeWorkLocation", callerName, zaloId, 
                inputData,(zId) -> LocationMiningNotifier.notifyHomeLocation(zId, inputData),(zId) -> LocationMiningNotifier.notifyWorkLocation(zId, inputData),
                THIS_CLASS, LOGGER);
        }
    }
    
    public TActionResult ingestUserSSIDInterest(String src, int zaloId, ZUDM_UserInterestLocation inputData, boolean bln){
        return ingestAndNotify(DBClients.LOCATIONINTEREST, "IngestUserSSIDInterest", 
                src, zaloId,x-> LocationMining.forUserSSISInterestOf(x),inputData, 
                () -> LocationMiningNotifier.notifyUserSSIDInterest(zaloId, inputData), bln,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestIP2Location(String callerName, String dataKey, ZUDM_IP2Location inputData) throws InvalidParamException {
        return ingestWithProfilerWithLog(DBClients._DBCmon5Cli, "ingestIP2Location", callerName, 
                Ip2LocationKey.forIp2LocationV2Of(dataKey), inputData, 
                THIS_CLASS, LOGGER);
    }
    
    public TActionResult ingestListIP2Location(String callerName, String ip, boolean put) throws InvalidParamException {
        return ingestStrList32bmClient(DBClients._DBipbmcommon6write, "ingestListIP2Location", callerName, 
                Ip2LocationKey.IPv4TextToI32(ip), put, 
                THIS_CLASS, LOGGER);
    }
}