/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.cache;

import com.vng.zing.common.CompressMed;
import com.vng.zing.common.IZStructor;
import com.vng.zing.common.ZCommonDef;
import com.vng.zing.jni.zicache.ZiCache;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zuserdatamining.thrift.LastUpdateHour;
import org.apache.log4j.Logger;
import org.apache.thrift.TBase;
/**
 *
 * @author tinhdt
 */

public class LastUpdateHourCache extends BaseCache {
    private static final Class THIS_CLASS = LastUpdateHourCache.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    private final IZStructor<String> KEY_STRUCTOR = new IZStructor.StringStructor();
    private final IZStructor<LastUpdateHour> VALUE_STRUCTOR = new LastUpdateHourCache.TUpdateHourStructor();
    
    public LastUpdateHourCache(String cacheName) {
        this.CACHE = initCache(this.CACHE, this.KEY_STRUCTOR, this.VALUE_STRUCTOR, cacheName);
    }
    
    private static <E, V extends TBase> ZiCache<E, V> initCache(ZiCache<E, V> ziCache, IZStructor key, IZStructor value, String name) {
        uninitCache(ziCache);
        long tblSize = 1048576L;
        long lruSize = 1000000L;
        long maxNfSize = 1000000L;
        int nloadsAtime = 50;
        int nthreadsLoad = 16;
        int expSeconds = 86400;
        String strCompressMed =ZCommonDef.CacheCompressMedDefault.toString();
        CompressMed compressMed = CompressMed.findByName(strCompressMed);
        if (compressMed == null) {
            compressMed = CompressMed.NOCOMP;
        }

        int thresholdComp = 256;
        boolean copyOnGet = false;
        boolean autoDump = true;
        int autoDumpRestSecs = 86400;
        int dumpSleepMicrosecs = 5000;
        boolean restoreFlag = true ;
        

        int autoRestorationSecs = 172800;
        int purgeCount = 5;
        ziCache = new ZiCache(key, value, tblSize, lruSize, name, restoreFlag, autoRestorationSecs, purgeCount);
        ziCache.setMaxNfSize(maxNfSize);
        ziCache.setNumLoadsAtime(nloadsAtime);
        ziCache.setNumThreadsLoad(nthreadsLoad);
        ziCache.setExpTime(expSeconds);
        ziCache.setCompressMed(compressMed);
        ziCache.setThresholdComp(thresholdComp);
        ziCache.setCopyOnGet(copyOnGet);
        ziCache.setAutoDumpRestSecs(autoDumpRestSecs);
        ziCache.setDumpSleepMicrosecs(dumpSleepMicrosecs);
        ziCache.setAutoDump(autoDump, true);
        tblSize = ziCache.getTblSize();
        lruSize = ziCache.getLruSize();
        maxNfSize = ziCache.getMaxNfSize();
        nloadsAtime = ziCache.getNumLoadsAtime();
        nthreadsLoad = ziCache.getNumThreadsLoad();
        expSeconds = ziCache.getExpTime();
        compressMed = ziCache.getCompressMed();
        thresholdComp = ziCache.getThresholdComp();
        copyOnGet = ziCache.getCopyOnGet();
        autoDump = ziCache.getAutoDump();
        autoDumpRestSecs = ziCache.getAutoDumpRestSecs();
        dumpSleepMicrosecs = ziCache.getDumpSleepMicrosecs();
        restoreFlag = ziCache.getRestoreFlag();
        autoRestorationSecs = ziCache.getAutoRestorationSecs();
        purgeCount = ziCache.getPurgeCount();
        LOGGER.info("init " + name + " " + "tblSize=" + tblSize + ", lruSize=" + lruSize + ", maxNfSize=" + maxNfSize + ", nloadsAtime=" + nloadsAtime + ", nthreadsLoad=" + nthreadsLoad + ", expSeconds=" + expSeconds + ", compressMed=" + compressMed + ", thresholdComp=" + thresholdComp + ", copyOnGet=" + copyOnGet + ", autoDump=" + autoDump + ", autoDumpRestSecs=" + autoDumpRestSecs + ", dumpSleepMicrosecs=" + dumpSleepMicrosecs + ", restoreFlag=" + restoreFlag + ", autoRestorationSecs=" + autoRestorationSecs + ", purgeCount=" + purgeCount + ")");
        return ziCache;
    }
    
    private class TUpdateHourStructor implements IZStructor<LastUpdateHour> {
        private TUpdateHourStructor() {
        }

        @Override
        public LastUpdateHour ctor() {
            return new LastUpdateHour();
        }
    }
}
