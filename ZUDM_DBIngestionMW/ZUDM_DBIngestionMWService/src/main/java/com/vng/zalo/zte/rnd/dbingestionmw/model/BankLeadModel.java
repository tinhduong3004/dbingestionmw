package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScore;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_DisburseLeadScoreV2;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandScore;
import com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift.ZUDM_LoanDemandTag;
import com.vng.zalo.zte.rnd.dbingestionmw.eventbus.BankLeadNotifier;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.centralizedkeys.BankLeadKey;
import org.apache.log4j.Logger;


/**
 *
 * @author tinhdt
 * 
 */
public class BankLeadModel extends DMPModel {
    private static final Class THIS_CLASS = BankLeadModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final BankLeadModel INSTANCE = new BankLeadModel();
    private static final ZiDB64Client BANKLEAD = new ZiDB64Client("ZUDMBankLead_ZiDB");
    
    
    // Why we need boolean parameter?
    public TActionResult uploadBankLead(String src, int zaloId, ZUDM_LoanDemandScore inputData, boolean notify){
        return ingestAndNotify(BANKLEAD, "uploadBankLead", 
                src, zaloId, x-> BankLeadKey.forBankleadKeyOf(x), inputData, 
                notify? () -> BankLeadNotifier.notifyLoanDemandScore(zaloId, inputData): () -> 0L,
                THIS_CLASS,LOGGER);
    } 
    
    public TActionResult uploadDisburseLead(String src, int zaloId, ZUDM_DisburseLeadScore inputData, boolean notify){
        return ingestAndNotify(BANKLEAD, "uploadDisburseLead", 
                src, zaloId, x-> BankLeadKey.forDisburseleadKeyOf(x) , inputData, 
                notify? () -> BankLeadNotifier.notifyDisburseLeadScore(zaloId, inputData): () -> 0L,
                THIS_CLASS,LOGGER);
    } 
    
    public TActionResult uploadDisburseLeadSh(String src, int zaloId, ZUDM_DisburseLeadScoreV2 inputData, boolean notify){
        return ingestAndNotify(BANKLEAD, "uploadDisburseLeadSh", 
                src, zaloId, x-> BankLeadKey.forDisburseleadShKeyOf(x) , inputData, 
                notify? () -> BankLeadNotifier.notifyDisburseLeadShScore(zaloId, inputData): () -> 0L,
                THIS_CLASS,LOGGER);
    } 
    
    public TActionResult uploadLoanDemandTag(String src, int zaloId, ZUDM_LoanDemandTag inputData, boolean bln){
        return ingestAndNotify(BANKLEAD, "ingestLoanDemandTag", 
                src,  zaloId,x->BankLeadKey.forLoanDemandTagKeyOf(x),inputData,
                () -> BankLeadNotifier.notifyLoanDemandTag(zaloId, inputData), bln,
                THIS_CLASS,LOGGER); 
    }
}
