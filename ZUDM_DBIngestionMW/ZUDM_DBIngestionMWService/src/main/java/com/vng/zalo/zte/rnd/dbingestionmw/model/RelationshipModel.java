/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.centralizedkeys.RelationshipKey;
import com.vng.zalo.zte.rnd.dbingestionmw.relationship.thrift.ZUDM_RelationshipList;
import org.apache.log4j.Logger;

/**
 *
 * @author tinhdt
 */
public class RelationshipModel extends DMPModel{
    
    private static final Class THIS_CLASS = RelationshipModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final RelationshipModel INSTANCE = new RelationshipModel();
    public static final ZiDB64Client relationShipDB = new ZiDB64Client("relationship");
    
    public TActionResult uploadRelationship(String src, int zaloId, ZUDM_RelationshipList inputData){
        return ingest(relationShipDB, "uploadRelationship", 
                src, RelationshipKey.forListRelationshipOf(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }
}
