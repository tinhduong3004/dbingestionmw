package com.vng.zalo.zte.rnd.dbingestionmw.common;

import com.vng.zing.centralozeid.common.ZCentralizeCommon;
import com.vng.zing.jni.ClassLoaderUtil;
import com.vng.zing.zcentralizeid.thrift.ZPlatform;

public class ZCentralize {
    static{
        ClassLoaderUtil.loadCommonLibs();
    }
    public static final Long  Uid2Gid(long Uid) {
        if ( Uid > 0 ){
            Long LongGid = ZCentralizeCommon.encodeIntoGlobalId((long) Uid, ZPlatform.ZP_Zalo);
            return LongGid;
        }else{
            return 0L;
        }
    }
}

