/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;


import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch.ElasticSearchDBManager;
import com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift.KeywordInfoList;
import com.vng.zalo.zte.rnd.dbingestionmw.eventbus.UserInterestNotifier;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TStringSearchResult;
import com.vng.zing.logger.ZLogger;
import org.apache.log4j.Logger;

/**
 *
 * @author ngocbk
 */
public class ElasticSearchUserInterestModel extends BaseModel{
    private static final Class THIS_CLASS = ElasticSearchUserInterestModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final ElasticSearchUserInterestModel INSTANCE = new ElasticSearchUserInterestModel();
    
    
    public TStringSearchResult ingestUserKw(String src, KeywordInfoList kw_list, boolean notify){
        TStringSearchResult res = new TStringSearchResult();
        String result = ElasticSearchDBManager.userinterst_elastic_search.ingestUserKw("ingestUserKw" , kw_list, ()-> UserInterestNotifier.INSTANCE.notifyUserInterestTags(kw_list), notify, THIS_CLASS);
        res.setResult(result);
        return res;
    }
    
    
    public TStringSearchResult ingestAnonymousUserKw(String src, KeywordInfoList kw_list, boolean notify){
        TStringSearchResult res = new TStringSearchResult();
        String result = ElasticSearchDBManager.anonymous_userinterst_elastic_search.ingestUserKw("ingestAnonymousUserKw", kw_list, ()-> UserInterestNotifier.INSTANCE.notifyAUInterestTags(kw_list), notify, THIS_CLASS);
        res.setResult(result);
        return res;
    }
}
