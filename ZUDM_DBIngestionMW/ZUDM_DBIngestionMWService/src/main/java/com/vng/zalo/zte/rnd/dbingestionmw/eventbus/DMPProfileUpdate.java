/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import com.vng.zalo.zte.rnd.dbingestionmw.common.ZCentralize;
import com.vng.zing.common.TSerializerFactory;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.common.ZUtil;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.zeventbus.Notifier;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TAction;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TDMPUpdatedField;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TUpdateEvent;
import java.nio.ByteBuffer;
import java.time.LocalDateTime;
import java.time.LocalTime;
import org.apache.log4j.Logger;
import org.apache.thrift.TBase;
import org.apache.thrift.TSerializer;

/**
 * 
 * @author tinhdt
 */
public abstract class DMPProfileUpdate {
    private static final Notifier NOTIFIER = new Notifier("DMPProfilesMining_EVB");
    private static final String EVENT_NAME = "dmp.mining.profile_qualified.updated";
    
    private static final Class THIS_CLASS = DMPProfileUpdate.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    
    protected static ByteBuffer serialize(TBase tbase){
        TSerializer SERIALIZER;
        try {
            SERIALIZER = TSerializerFactory.Borrow();
            byte[] serializedData = SERIALIZER.serialize(tbase);
            TSerializerFactory.Return(SERIALIZER);
            return ByteBuffer.wrap(serializedData);
        } catch (Exception ex) {
            LOGGER.info("[EV] TSerializerFactory err: "+ex.getMessage());
        }
        return null;
    }
    
    protected static long notify(String globalId,TDMPUpdatedField updateField,
            ByteBuffer data,TAction action, String userType){
        long gid = ZCentralize.Uid2Gid(Long.valueOf(globalId));
        return notify(gid, updateField,data, action, userType);
    }
    
    protected static long notify(int zaloId,TDMPUpdatedField updateField,
            ByteBuffer data,TAction action, String userType){
        long gid = ZCentralize.Uid2Gid(Long.valueOf(zaloId));
        return notify(gid, updateField, data, action, userType);
    }
    
    protected static long notify(long globalId,TDMPUpdatedField updateField,
            ByteBuffer data,TAction action, String userType){
        ThreadProfiler tp = Profiler.getThreadProfiler();
        if ( globalId > 0 ){
            tp.push(THIS_CLASS, "SentNewEventBus");
            TUpdateEvent event = new TUpdateEvent();
            event.setGid(globalId);
            event.setAction(action);
            event.setAttrId((short)updateField.getValue());
            event.setAttrData(data);
            
            long error = NOTIFIER.notify(EVENT_NAME, event);
            if ( ZErrorHelper.isSuccess(error)){
                scribe(String.valueOf(updateField.getValue()), globalId, userType);
                LOGGER.info("Notified "+globalId+" update field "+updateField);
            }
            tp.pop(THIS_CLASS, "SentNewEventBus");
            
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime at0 = now.with(LocalTime.of(0,0));
            LocalDateTime at7 = now.with(LocalTime.of(7, 0));
            LocalDateTime at11 = now.with(LocalTime.of(11, 0));
            if (at0.plusMinutes(1).isAfter(now)) {
                tp.push(THIS_CLASS, "SentNewEventBus.midnight");
                ZUtil.sleep(100);
                tp.pop(THIS_CLASS, "SentNewEventBus.midnight");
            }else if ( at7.isBefore(now) && at11.isAfter(now) ){
                tp.push(THIS_CLASS, "SentNewEventBus.morning");
                ZUtil.sleep(5);
                tp.pop(THIS_CLASS, "SentNewEventBus.morning");
            }else{
                ZUtil.sleep(1);
            }
            return error;
        }else{
            tp.push(THIS_CLASS, "SentNewEventBus.ZeroGlobalID");
            tp.pop(THIS_CLASS, "SentNewEventBus.ZeroGlobalID");
            return -1;
        }
    }
    
    private static void scribe(String updatedField, long globalId, String userType){
        ThreadProfiler tp = Profiler.getThreadProfiler();
        tp.push(THIS_CLASS, "scribe");
        ZLogger.scribe("DMPProfileUpdate", "SEND", updatedField, globalId, userType, 0);
        tp.pop(THIS_CLASS, "scribe");
    }
}
