package com.vng.zalo.zte.rnd.dbingestionmw.cache;
import com.vng.zing.common.PutPolicy;
import com.vng.zing.jni.zicache.ZiCache;
import com.vng.zing.zuserdatamining.thrift.LastUpdateHour;
/**
 *
 * @author cpu11232
 */
public class BaseCache {
    protected ZiCache<String, LastUpdateHour> CACHE;
    public BaseCache() {
    }

    protected static void uninitCache(ZiCache ziCache) {
        if (ziCache != null) {
            ziCache.stopThreadsLoad();
            ziCache = null;
        }

    }
    
    public void put(String key,  int hour) {
        this.CACHE.put(key, new LastUpdateHour(hour), PutPolicy.ADD_OR_UDP);
    }

    public int get(String key) {
        try {
            return ((LastUpdateHour)this.CACHE.get(key)).getHour();
        } catch (Exception e) {
            return 0;
        }
    }

    public boolean containsKey(String key) {
        return this.CACHE.exist(key);
    }
}
