/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TDeviceInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TListDevice;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictAgeGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedAge;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedAgeRange;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedProvince;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TAction;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TDMPUpdatedField;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPDeviceInfoRaw;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUAgeRange;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserAddress;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserAgePredict;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserAgeRangePredict;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserDeviceRaw;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserGenderPredict;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserLivingProvincePredict;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * @date Sep 26, 2019
 * @author quydm
 */
public class AnonymousNotifier extends DMPNotifier{
    
    public static long notifyAnonymousAge(long globalId,TPredictedAge pAge){
        TDMPUserAgePredict age = new TDMPUserAgePredict();
        age.setError(pAge.getError());
        age.setExpiryDate(pAge.getExpiryDate());
        age.setScore(0.0);
        age.setAge(pAge.getValue());
        ByteBuffer serializedData = serialize(age);
        return notify(globalId, TDMPUpdatedField.UserAgePredict, serializedData,TAction.Change, "ANONYMOUS");
    }
    
    public static long notifyAnonymousGender(long globalId,TPredictedGender pGender){
        TDMPUserGenderPredict gender = new TDMPUserGenderPredict();
        gender.setError(pGender.getError());
        gender.setExpiryDate(pGender.getExpiryDate());
        gender.setGender((byte) (pGender.value==TGender.MALE?1:2));
        ByteBuffer serializedData = serialize(gender);
        return notify(globalId, TDMPUpdatedField.UserGenderPredict, serializedData,TAction.Change, "ANONYMOUS" );
    }
    
    public static long notifyAnonymousProvince(long globalId,TPredictedProvince pProvince){
        TDMPUserLivingProvincePredict province = new TDMPUserLivingProvincePredict();
        province.setError(pProvince.getError());
        province.setExpiryDate(pProvince.getExpiryDate());
        province.setScore(0.0);

        TDMPUserAddress add = new TDMPUserAddress();
        add.setLocation_id(pProvince.getId());
        province.setLiving(add);
        
        ByteBuffer serializedData = serialize(province);
        return notify(globalId, TDMPUpdatedField.UserLivingProvincePredict, serializedData,TAction.Change, "ANONYMOUS");
    }
    
    public static long notifyAnonymousProvinceNew(long globalId,TPredictedProvince pProvince){
        TDMPUserLivingProvincePredict province = new TDMPUserLivingProvincePredict();
        province.setError(pProvince.getError());
        province.setExpiryDate(pProvince.getExpiryDate());
        province.setScore(0.0);

        TDMPUserAddress add = new TDMPUserAddress();
        add.setLocation_id(pProvince.getId());
        province.setLiving(add);
        
        ByteBuffer serializedData = serialize(province);
        return notify(globalId, TDMPUpdatedField.UserLivingProvincePredict, serializedData,TAction.New, "ANONYMOUS" );
    }
    
    public static long notifyAnonymousDevice(long globalId,TListDevice device){
        TDMPUserDeviceRaw deviceRaw = new TDMPUserDeviceRaw();
        TDMPDeviceInfoRaw devInfo = new TDMPDeviceInfoRaw();
        TDeviceInfo pDevInfo = device.devices.get(0);
        devInfo.setBrandName(pDevInfo.brand);
        devInfo.setDeviceName(pDevInfo.name);
        devInfo.setOsName(pDevInfo.os);
        devInfo.setOsVersion(pDevInfo.osVersion);

        deviceRaw.setDevices(Arrays.asList(devInfo));
        deviceRaw.setExpiryDate(device.getExpiryDate());
        
        ByteBuffer serializedData = serialize(deviceRaw);
        return notify(globalId, TDMPUpdatedField.UserDevice, serializedData,TAction.Change, "ANONYMOUS" );
    }
    
    public static long notifyAnonymousAgeRange(long globalId,TPredictedAgeRange pAge){
        TDMPUserAgeRangePredict age = new TDMPUserAgeRangePredict();
        age.setError(pAge.getError());
        age.setExpiryDate(pAge.getExpiryDate());
        age.setScore(0.0);
        age.setAgeRange(TDMPUAgeRange.findByValue(pAge.value.getValue()));
        ByteBuffer serializedData = serialize(age);
        return notify(globalId, TDMPUpdatedField.UserAgeRangePredict, serializedData,TAction.Change, "ANONYMOUS");
    }
}
