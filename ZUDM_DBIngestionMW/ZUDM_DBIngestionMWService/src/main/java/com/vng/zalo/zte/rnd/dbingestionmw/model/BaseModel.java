/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.admin.AdminModel;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift.Hour;
import com.vng.zing.alert_bot.thrift.ZUDM_AlertResult;
import com.vng.zing.alert_bot.thrift.wrapper.ZUDM_AlertBotThriftClient;
import com.vng.zing.common.ByteBufferUtil;
import com.vng.zing.common.ZErrorHelper;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.strlist32bm.thrift.wrapper.StrList32bmClient;
import com.vng.zing.zcommon.thrift.PutPolicy;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.WithCompression;
import com.vng.zing.zudm.common.ZUDM_Util;
import com.vng.zing.zudm.common.centralizedkeys.ip.IPBitSet;
import com.vng.zing.zudm.common.centralizedkeys.ip.IPDatabaseV2;
import java.nio.ByteBuffer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import org.apache.log4j.Logger;
import org.apache.thrift.TBase;

/**
 * @date Jul 15, 2019
 * @author quydm
 */
public class BaseModel {

    public static ZUDM_AlertBotThriftClient alertBotClient = new ZUDM_AlertBotThriftClient("alertbotclient");

    /**
     * A handler function to run all the step of ingestion process 1. Validate
     * the source 2. Put data to db 3. Push event bus (if step 2 is succeed)
     *
     * @param <T>
     * @param client
     * @param apiName
     * @param src
     * @param key
     * @param inputData
     * @param clazz
     * @param logger
     * @return Error enumerate
     */
    //Ingest ZiDB64Client, no eventbus (AnonymousUserModel, DemoGraphicModel,, ZaloDeviceModel )
    public <T extends TBase> TActionResult ingest(
            ZiDB64Client client, String apiName, String src, // config info
            long key, T inputData, // key-value
            Class clazz, Logger logger
    ) { // eventbus

        if (AdminModel.INSTANCE.validCaller(src)) {
            long error = WithCompression.putTo(client,
                    key, inputData, logger, clazz); // return error code
            if (ZErrorHelper.isSuccess(error)) { // ingestion is successful
                countSuccessFor(clazz, apiName);
                logSuccessData(logger, apiName, key, inputData);
                return TActionResult.SUCCESS;
            } else {
                sendAlertBotMsgForError(src, apiName, Long.toString(key));
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            sendAlertBotMsgForWrongAuth(src, apiName, Long.toString(key));
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }

    public <T extends TBase> TActionResult ingestStrList32bmClient(
            StrList32bmClient client, String apiName, String src, // config info
            int key, boolean put,// key-value
            Class clazz, Logger logger
    ) { // eventbus

        if (AdminModel.INSTANCE.validCaller(src)) {
            if (put == false) {
                ipBitSet.addIP(key);
            } else {
                long error = IPDatabaseV2.Instance.put(client, ipBitSet); // return error code
                ipBitSet.cleanIP();
                if (ZErrorHelper.isSuccess(error)) { // ingestion is successful
                    countSuccessFor(clazz, apiName);
                    return TActionResult.SUCCESS;
                } else {
                    sendAlertBotMsgForError(src, apiName, Long.toString(key));
                    countErrorFor(clazz, apiName);
                    return TActionResult.ERROR;
                }
            }
            return TActionResult.SUCCESS;
        } else {
            sendAlertBotMsgForWrongAuth(src, apiName, Long.toString(key));
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }

    //Ingest ZiDBClient (no eventbus)
    public <T extends TBase> TActionResult ingestZiDBClient(
            ZiDBClient client, String apiName, String src, // config info
            long dataKey, T inputData, // key-value
            Class clazz, Logger logger) { // eventbus
        if (checkValidCaller(src)) {
            try {
                ByteBuffer key = ByteBufferUtil.fromString(String.valueOf(dataKey));
                com.vng.zing.zidb.thrift.TValue tvalue = ZUDM_Util.createZiDBTValueFromTBase(inputData);
                long error = client.put(key, tvalue, PutPolicy.ADD_OR_UDP);

                if (ZErrorHelper.isSuccess(error)) {
                    countSuccessFor(clazz, apiName);
                    return TActionResult.SUCCESS;
                } else {
                    countErrorFor(clazz, apiName);
                    return TActionResult.ERROR;
                }
            } catch (Exception ex) {
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }

    //remove key for ZiDB64Client
    public <T extends TBase> TActionResult remove(
            ZiDB64Client client, String apiName, String src, // config info
            long key, // key-value
            Class clazz, Logger logger
    ) { // eventbus

        if (AdminModel.INSTANCE.validCaller(src)) {
            long error = client.remove(key); // return error code
            if (ZErrorHelper.isSuccess(error)) { // ingestion is successful
                countSuccessFor(clazz, apiName);
                return TActionResult.SUCCESS;
            } else {
                sendAlertBotMsgForError(src, apiName, Long.toString(key));
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            sendAlertBotMsgForWrongAuth(src, apiName, Long.toString(key));
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }

    //Ingest ZiDB64Client, custom eventbus
    public <T extends TBase> TActionResult ingestAndNotify(
            ZiDB64Client client, String apiName, String src,
            int zaloId, Function<Integer, Long> keyMaker, T inputData,
            Supplier<Long> notifier, boolean bln,
            Class clazz, Logger logger
    ) {

        TActionResult ingestionResult = ingest(client, apiName, src,
                keyMaker.apply(zaloId), inputData,
                clazz, logger);

        if (ingestionResult == TActionResult.SUCCESS && isNormalGlobalId(zaloId) && bln == true) { // Ingestion successfully and not-QoS globalId
            notifier.get();
            countEventBusFor(clazz, apiName);
        }
        return TActionResult.SUCCESS;
    }

    //custom eventbus, ingest for CreditScore
    public <T extends TBase> TActionResult customIngestAndNotify(
            ZiDB64Client client, String apiName, String src,
            int zaloId, Function<Integer, Long> keyMaker, T inputData,
            Supplier<Long> notifier, boolean bln,
            Class clazz, Logger logger
    ) {

        if (bln == false) {
            ingest(client, apiName, src, keyMaker.apply(zaloId), inputData, clazz, logger);
        }

        if (isNormalGlobalId(zaloId) && bln == true) { // Ingestion successfully and not-QoS globalId
            notifier.get();
            countEventBusFor(clazz, apiName);
        }
        return TActionResult.SUCCESS;
    }

    // Ingest to ZiDB64Client with hourlyKey,no eventbus (zaloId)
    public <T extends TBase> TActionResult ingestWithHour(
            ZiDB64Client client, String apiName, String src, // config info
            int zaloId, int hour, BiFunction<Integer, Integer, Long> keyMaker, T inputData,// key-value
            Class clazz, Logger logger) { // eventbus
        if (AdminModel.INSTANCE.validCaller(src) == true && hour == Hour.findByValue(hour).getValue()) {
            long error = WithCompression.putTo(client,
                    keyMaker.apply(zaloId, hour), inputData, logger, clazz); // return error code

            if (ZErrorHelper.isSuccess(error)) { // ingestion is successful
                countSuccessFor(clazz, apiName);
                logSuccessData(logger, apiName, keyMaker.apply(zaloId, hour), inputData);
                return TActionResult.SUCCESS;
            } else {
                sendAlertBotMsgForError(src, apiName, keyMaker.apply(zaloId, hour).toString());
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }

        } else {
            sendAlertBotMsgForWrongAuth(src, apiName, keyMaker.apply(zaloId, hour).toString());
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }

    }

    // Ingest to ZiDB64Client with hourlyKey, eventbus (zaloId)
    public <T extends TBase> TActionResult ingestWithHourAndNotify(
            ZiDB64Client client, String apiName, String src, // config info
            int zaloId, int hour, BiFunction<Integer, Integer, Long> keyMaker, T inputData,// key-value
            Consumer<Integer> notifier, boolean notify,
            Class clazz, Logger logger) { // eventbus

        TActionResult ingestionResult = ingestWithHour(client, apiName, src, zaloId,
                hour, keyMaker, inputData, clazz, logger);
        if (ingestionResult == TActionResult.SUCCESS && isNormalGlobalId(zaloId) && notify == true) { // Ingestion successfully and not-QoS globalId
            notifier.accept(zaloId);
            countEventBusFor(clazz, apiName);
        }
        return ingestionResult;
    }

    // notify with new Enum-UserInterest (zaloId)
    public <T extends TBase> TActionResult Notify(
            ZiDB64Client client, String apiName, String src, // config info
            int zaloId, T inputData,// key-value
            Consumer<Integer> notifier,
            Class clazz, Logger logger) { // eventbus

        if (isNormalGlobalId(zaloId)) { // Ingestion successfully and not-QoS globalId
            notifier.accept(zaloId);
            countEventBusFor(clazz, apiName);
        }
        return TActionResult.SUCCESS;
    }

    // notify with new Enum-AnonymousUserInterest (gid)
    public <T extends TBase> TActionResult NotifyAnonymousUserInterest(
            ZiDB64Client client, String apiName, String src, // config info
            long gid, T inputData,// key-value
            Consumer<Long> notifier,
            Class clazz, Logger logger) { // eventbus

        if (isNormalGlobalId(gid)) { // Ingestion successfully and not-QoS globalId
            notifier.accept(gid);
            countEventBusFor(clazz, apiName);
        }
        return TActionResult.SUCCESS;
    }

    // ingest to ZiDClient with ByteBuffer key, eventbus (UserInterestModel: AnonymousUserInterest)
    public <T extends TBase> TActionResult ingestClient(
            ZiDBClient client, String apiName, String src, // config info
            long dataKey, T inputData, Consumer<Long> notifier, // key-value
            Class clazz, Logger logger) { // eventbus
        if (checkValidCaller(src)) {
            try {
                ByteBuffer key = ByteBufferUtil.fromString(String.valueOf(dataKey));
                com.vng.zing.zidb.thrift.TValue tvalue = ZUDM_Util.createZiDBTValueFromTBase(inputData);
                long error = client.put(key, tvalue, PutPolicy.ADD_OR_UDP);

                if (ZErrorHelper.isSuccess(error)) {
                    notifier.accept(dataKey);
                    countEventBusFor(clazz, apiName);
                    countSuccessFor(clazz, apiName);
                    return TActionResult.SUCCESS;
                } else {
                    countErrorFor(clazz, apiName);
                    return TActionResult.ERROR;
                }
            } catch (Exception ex) {
                countErrorFor(clazz, apiName);
                return TActionResult.ERROR;
            }
        } else {
            countWrongAuthenFor(clazz, apiName);
            logWrongAuthenSrc(logger, apiName, src);
            return TActionResult.WRONGAUTH;
        }
    }

    //remove ZiDB64Client, custom eventbus
    public <T extends TBase> TActionResult removeAndNotify(
            ZiDB64Client client, String apiName, String src,
            int zaloId, Function<Integer, Long> keyMaker,
            Supplier<Long> notifier, boolean bln,
            Class clazz, Logger logger
    ) {

        TActionResult ingestionResult = remove(client, apiName, src,
                keyMaker.apply(zaloId),
                clazz, logger);

        if (ingestionResult == TActionResult.SUCCESS && isNormalGlobalId(zaloId) && bln == true) { // Ingestion successfully and not-QoS globalId
            notifier.get();
            countEventBusFor(clazz, apiName);
        }
        return TActionResult.SUCCESS;
    }

    public static boolean isNormalGlobalId(long globalId) {
        return globalId != 9000000000000000000L; // this is at the dump global used for QoS service
    }

    public static void countWrongAuthenFor(Class clazz, String apiName) {
        ThreadProfiler tp = com.vng.zing.stats.Profiler.getThreadProfiler();
        tp.push(clazz, apiName + ".WRONGAUTH");
        tp.pop(clazz, apiName + ".WRONGAUTH");
    }

    public static void countSuccessFor(Class clazz, String apiName) {
        ThreadProfiler tp = com.vng.zing.stats.Profiler.getThreadProfiler();
        tp.push(clazz, apiName + ".SUCCESS");
        tp.pop(clazz, apiName + ".SUCCESS");
    }

    public static void countErrorFor(Class clazz, String apiName) {
        ThreadProfiler tp = com.vng.zing.stats.Profiler.getThreadProfiler();
        tp.push(clazz, apiName + ".ERROR");
        tp.pop(clazz, apiName + ".ERROR");
    }

    public static void countEventBusFor(Class clazz, String apiName) {
        ThreadProfiler tp = com.vng.zing.stats.Profiler.getThreadProfiler();
        tp.push(clazz, apiName + ".EVENTBUS");
        tp.pop(clazz, apiName + ".EVENTBUS");
    }

    public static void logWrongAuthenSrc(Logger logger, String apiName, String src) {
        logger.error("Wrong Authen " + apiName + " with token " + src);
    }

    public static <T extends TBase> void logSuccessData(Logger logger, String apiName, long dataKey, T data) {
        logger.info(apiName + "\tkey: " + dataKey + "\tvalue: " + data.toString());
    }

    public static <T extends IPBitSet> void logSuccessData(Logger logger, String apiName, long dataKey, T data) {
        logger.info(apiName + "\tkey: " + dataKey + "\tvalue: " + data.toString());
    }

    @FunctionalInterface //Declare TriFunction
    interface TriFunction<One, Two, Three, Four> {

        public Four apply(One one, Two two, Three three);
    }

    boolean checkValidCaller(String caller) {
        return AdminModel.INSTANCE.validCaller(caller);
    }

    public ZUDM_AlertResult sendAlertBotMsgForError(String projectName, String apiName, String dataKey) {
        ZUDM_AlertResult result = alertBotClient.sendAllMemberInProject(projectName, "ZUDM_DBIngestionMW \napiName: " + apiName + " , dataKey: " + dataKey + " \nreason: ERROR");
        return result;
    }

    public ZUDM_AlertResult sendAlertBotMsgForWrongAuth(String projectName, String apiName, String dataKey) {
        ZUDM_AlertResult result = alertBotClient.sendAllMemberInProject(projectName, "ZUDM_DBIngestionMW \napiName: " + apiName + " , dataKey: " + dataKey + " \nreason: WRONGAUTH");
        return result;
    }

    private static IPBitSet ipBitSet = new IPBitSet();
}
