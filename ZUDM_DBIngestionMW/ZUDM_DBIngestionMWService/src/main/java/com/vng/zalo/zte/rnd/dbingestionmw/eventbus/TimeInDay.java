package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import org.joda.time.DateTime;
import org.joda.time.Days;

public class TimeInDay {
    
        public static final DateTime BeginingDay = new DateTime(2013, 10, 10, 0, 0);
	public static final TimeInDay Instance = new TimeInDay(BeginingDay);
	private final Object _lock;
	private final DateTime _from;
	private int _curDayOfMonth;
	private int _curDate;

	public TimeInDay(DateTime from) {
		assert (from != null);
		_lock = new Object();
		_from = from;
		_curDayOfMonth = 0;
		_curDate = 0;
	}

	public int getCurDate() {
		synchronized (_lock) {
			DateTime now = new DateTime();
			if (now.getDayOfMonth() != _curDayOfMonth) {
				_curDayOfMonth = now.getDayOfMonth();
				_curDate = Days.daysBetween(_from, now).getDays();
			}
			return _curDate;
		}
		//int cur_minute = now.getMinuteOfDay();
	}

	public int getDateOf(DateTime date) {
		return Days.daysBetween(_from, date).getDays();
	}

	public String toString(int date) {
		return _from.plusDays(date).toString("dd/MM/yyyy");
	}

	public static int getCurDayOfWeek() {
		DateTime now = new DateTime();
		return now.getDayOfWeek();
	}

	public static int getCurHourOfDay() {
		DateTime now = new DateTime();
		return now.getHourOfDay();
	}
        
        public static int getCurMinuteOfDay(){
            DateTime now = new DateTime();
            return now.getMinuteOfDay();
        }

	public static boolean IsOlderThanNowNDays(int date, int ndays) {
		return (Instance.getCurDate() - date) > ndays;
	}

	public static boolean IsOlderThanNowNDays(int curdate, int date, int ndays) {
		return (curdate - date) > ndays;
	}

	public static int nDaysOld(int date) {
		return Instance.getCurDate() - date;
	}

	public static int nDaysOld(int curdate, int date) {
		return curdate - date;
	}
}

