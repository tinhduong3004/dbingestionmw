package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyList;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyTop500;
import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_OccupationInfo;
import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_UserOccupationList;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TAction;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TDMPUpdatedField;
import com.vng.zing.zudm_dmpmiddleware.thrift.*;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tinhdt
 */
public class UserOccupationNotifier extends DMPNotifier {

    public static long notifyUserStudentOccupation(int globalId, ZUDM_UserOccupationList pStudentOccupation) {
        ZUDM_OccupationInfo occupationInfo = pStudentOccupation.getOccupations().get(0);
        TDMPUserStudentOccupationPredict studentOccupation = new TDMPUserStudentOccupationPredict();
        studentOccupation.setError(0);
        studentOccupation.setUniversityId(occupationInfo.getOccupationId());
        studentOccupation.setScore(occupationInfo.getScore());
        studentOccupation.setExpiryDate(ExpiryDate.setExpriDate((int) pStudentOccupation.getUpdatedDate(), ExpiryDate._3MONTHS));
        ByteBuffer serializedData = serialize(studentOccupation);
        return notify(globalId, TDMPUpdatedField.UserStudentOccupationPredict, serializedData, TAction.Change, "IDENTIFIED");
    }

    public static long notifyUserUnionOccupation(int globalId, ZUDM_UserOccupationList pUserUnionOccupation) {
        List<TDMPUserOccupationInfo> userOccupationInfoList = 
                pUserUnionOccupation.getOccupations()
                .stream().map(occ->{
                    TDMPUserOccupationInfo dmpOcc = new TDMPUserOccupationInfo();
                    dmpOcc.setId(occ.getOccupationId());
                    dmpOcc.setScore(occ.getScore());
                    return dmpOcc;
                }).collect(Collectors.toList());
        TDMPUserOccupationPredict occupation = new TDMPUserOccupationPredict();
        occupation.setError(0);
        occupation.setOccupations(userOccupationInfoList);
        occupation.setExpiryDate(ExpiryDate.setExpriDate((int) pUserUnionOccupation.getUpdatedDate(), ExpiryDate._3MONTHS));

        ByteBuffer serializedData = serialize(occupation);
        return notify(globalId, TDMPUpdatedField.UserOccupationPredict, serializedData, TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyUserBlueWhiteOccupation(int globalId, ZUDM_UserOccupationList pUserBlueWhiteOccupation) {
        ZUDM_OccupationInfo occupationInfo = pUserBlueWhiteOccupation.getOccupations().get(0);
        TDMPUserBlueWhiteOccupationPredict userBlueWhiteOccupation = new TDMPUserBlueWhiteOccupationPredict();
        userBlueWhiteOccupation.setError(0);
        userBlueWhiteOccupation.setOccupationId(occupationInfo.getOccupationId());
        userBlueWhiteOccupation.setScore(occupationInfo.getScore());
        userBlueWhiteOccupation.setExpiryDate(ExpiryDate.setExpriDate((int) pUserBlueWhiteOccupation.getUpdatedDate(), ExpiryDate._3MONTHS));
        ByteBuffer serializedData = serialize(userBlueWhiteOccupation);
        return notify(globalId, TDMPUpdatedField.UserBlueWhiteOccupationPredict, serializedData, TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyUserCompany(int globalId, ZUDM_UserCompanyList pUserCompanyList) {
        List<TDMPUserCompanyInfo> UserCompanyInfo = 
                pUserCompanyList.getCompanys()
                .stream().map(occ->{
                    TDMPUserCompanyInfo dmpOcc = new TDMPUserCompanyInfo();
                    dmpOcc.setName(occ.getName());
                    dmpOcc.setOccupation_id(occ.getOccupationId());
                    dmpOcc.setScore(occ.getConfident());
                    return dmpOcc;
                }).collect(Collectors.toList());
        TDMPUserCompanyPredict occupation = new TDMPUserCompanyPredict();
        occupation.setError(0);
        occupation.setCompanys(UserCompanyInfo);
        occupation.setExpiryDate(ExpiryDate.setExpriDate((int) pUserCompanyList.getUpdatedDate(), ExpiryDate._3MONTHS));

        ByteBuffer serializedData = serialize(occupation);
        return notify(globalId, TDMPUpdatedField.UserCompanyPredict, serializedData, TAction.Change, "IDENTIFIED");
    }
    
    public static long notifyUserCompanyTop500(int globalId, ZUDM_UserCompanyTop500 pCompanyTop500) {
        TDMPUserCompanyTop500 UserCompanyTop500 = new TDMPUserCompanyTop500();
        UserCompanyTop500.setError(pCompanyTop500.getError());
        UserCompanyTop500.setTop500(pCompanyTop500.getTop500());
        ByteBuffer serializedData = serialize(UserCompanyTop500);
        return notify(globalId, TDMPUpdatedField.UserCompanyTop500, serializedData, TAction.Change, "IDENTIFIED");
    }
}
