/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import java.text.SimpleDateFormat;
import org.joda.time.DateTime;

/**
 * * A mudule of UserMiningInfoModel provide Databases
 *
 * @date
 * @author thuannth
 */
public class ExpiryDate {

    public static final int DAY = 1;
    public static final int WEEK = 7;
    public static final int MONTH = 30;
    public static final int _3MONTHS = 90;
    
    public static String setExpriDate (int updateDate, int duration){
        
        String pattern = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        int today_vngformat = TimeInDay.Instance.getCurDate();
        DateTime today = DateTime.now();
        String lastdate = simpleDateFormat.format(today.plusDays(duration - (today_vngformat - updateDate)).toDate());
        return lastdate;
    }
    
}
