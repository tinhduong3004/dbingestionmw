/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zalo.zte.rnd.dbingestionmw.model;

import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TListDevice;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TListHistory;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TListLocation;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictAgeGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedAge;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedAgeRange;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedGender;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedGenderV2;
import com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift.TPredictedProvince;
import com.vng.zalo.zte.rnd.dbingestionmw.eventbus.AnonymousNotifier;
import static com.vng.zalo.zte.rnd.dbingestionmw.model.BaseModel.countEventBusFor;
import static com.vng.zalo.zte.rnd.dbingestionmw.model.BaseModel.countSuccessFor;
import static com.vng.zalo.zte.rnd.dbingestionmw.model.BaseModel.isNormalGlobalId;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;
import com.vng.zing.zudm.common.centralizedkeys.AnonymousTraceKey;
import com.vng.zing.zudm.common.centralizedkeys.AnonymousUnstableKey;
import com.vng.zing.zudm.common.centralizedkeys.AnonymousUserKey;
import java.util.function.Function;
import java.util.function.Supplier;
import org.apache.log4j.Logger;
import org.apache.thrift.TBase;



/**
 * @date Feb 13, 2019
 * @author quydm
 */
public class AnonymousUserModel extends BaseModel{
    
    private static final Class THIS_CLASS = AnonymousUserModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final AnonymousUserModel INSTANCE = new AnonymousUserModel();
    private static final ZiDB64Client ANONYMOUSUSER_DB = new ZiDB64Client("ZUDM_ZiDB64AnonymousUser");
    private static final ZiDB64Client ANONYMOUSUSER_DB2 = new ZiDB64Client("ZUDM_ZiDB64AnonymousUser2");
    private static final ZiDB64Client ANONYMOUSUSER_DB_TRACE = new ZiDB64Client("ZUDM_ZiDB64AnonymousUserTrace");
    private static final ZiDB64Client ANONYMOUSUSER_DB_UNSTABLE = new ZiDB64Client("UnstableAnonymousUser");
    
    
    //ingest ZiDB64Client with eventbus (globalId)
    public <T extends TBase> TActionResult ingestAndNotify(
            ZiDB64Client client,String apiName,String src, 
            long globalId,Function<Long,Long> keyMaker, T inputData, 
            Supplier<Long> notifier,
            Class clazz,Logger logger
            ){ 
        
        TActionResult ingestionResult = ingest(client, apiName, src, 
                                            keyMaker.apply(globalId), inputData, 
                                            clazz, logger);
        
        if ( ingestionResult == TActionResult.SUCCESS && isNormalGlobalId(globalId) ){ // Ingestion successfully and not-QoS globalId
            notifier.get();
            countEventBusFor(clazz,apiName);
        }
        countSuccessFor(clazz, apiName);
        return TActionResult.SUCCESS;
    }
    
    //ingest ZiDB64Client,can custom eventbus (globalId)
    public <T extends TBase> TActionResult ingestAndNotify(
            ZiDB64Client client,String apiName,String src, 
            long globalId,Function<Long,Long> keyMaker, T inputData, 
            Supplier<Long> notifier, boolean bln,
            Class clazz,Logger logger
            ){ 
        
        TActionResult ingestionResult = ingest(client, apiName, src, 
                                            keyMaker.apply(globalId), inputData, 
                                            clazz, logger);
        
        if ( ingestionResult == TActionResult.SUCCESS && isNormalGlobalId(globalId) && bln == true ){ // Ingestion successfully and not-QoS globalId
            notifier.get();
            countEventBusFor(clazz,apiName);
        }
        countSuccessFor(clazz, apiName);
        return TActionResult.SUCCESS;
    }
    
    public TActionResult ingestAUPredictedAge(String src, long globalId, TPredictedAge inputData){
        return ingestAndNotify(ANONYMOUSUSER_DB, "ingestAUPredictedAge", 
                src, globalId,x->AnonymousUserKey.forAgeOf(x),inputData, 
                () -> AnonymousNotifier.notifyAnonymousAge(globalId, inputData),
                THIS_CLASS,LOGGER);
    }
    public TActionResult ingestAUPredictedGender(String src, long globalId, TPredictedGender inputData){
        return ingestAndNotify(ANONYMOUSUSER_DB, "ingestAUPredictedGender", 
                src, globalId,x->AnonymousUserKey.forGenderOf(x),inputData, 
                () -> AnonymousNotifier.notifyAnonymousGender(globalId, inputData),
                THIS_CLASS,LOGGER);
    }
    public TActionResult ingestAUPredictedProvince(String src, long globalId, TPredictedProvince inputData){
        return ingestAndNotify(ANONYMOUSUSER_DB, "ingestAUPredictedProvince", 
                src, globalId,x-> AnonymousUserKey.forProvinceOf(x), inputData, 
                () -> AnonymousNotifier.notifyAnonymousProvince(globalId, inputData),
                THIS_CLASS,LOGGER);
    }
    public TActionResult ingestAUListDevice(String src, long globalId, TListDevice inputData){
        return ingestAndNotify(ANONYMOUSUSER_DB, "ingestAUListDevice", 
                src, globalId,x-> AnonymousUserKey.forDeviceOf(x), inputData, 
                () -> AnonymousNotifier.notifyAnonymousDevice(globalId, inputData),
                THIS_CLASS,LOGGER);
    }

    public TActionResult ingestUAUProvince(String src, long globalId, TPredictedProvince inputData){
        TActionResult result = ingestAndNotify(ANONYMOUSUSER_DB_UNSTABLE, "ingestUnstableAUProvince", 
                src, globalId, x-> AnonymousUnstableKey.forProvinceOf(x), inputData, 
                () -> AnonymousNotifier.notifyAnonymousProvinceNew(globalId, inputData),
                THIS_CLASS,LOGGER);
        return result;
    }
    
    public TActionResult ingestAUAdtimaHistory(String src, long globalId, TListHistory inputData){
        return ingest(ANONYMOUSUSER_DB_TRACE, "ingestAUAdtimaHistory", 
                src,AnonymousTraceKey.forAdtimaOf(globalId), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestAUZingNewsHistory(String src, long globalId, TListHistory inputData){
        return ingest(ANONYMOUSUSER_DB_TRACE, "ingestAUZingNewsHistory", 
                src, AnonymousTraceKey.forZingNewsOf(globalId), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestAUZingMp3History(String src, long globalId, TListHistory inputData){
        return ingest(ANONYMOUSUSER_DB_TRACE, "ingestAUZingMp3History", 
                src, AnonymousTraceKey.forZingMp3Of(globalId), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestAUListLocation(String src, long globalId, TListLocation inputData){   
        return ingest(ANONYMOUSUSER_DB_TRACE, "ingestAUListLocation", 
                src,AnonymousTraceKey.forLocationOf(globalId), inputData, 
                THIS_CLASS,LOGGER);
    }

    public TActionResult ingestAUPredictedGenderV2(String src, long globalId, TPredictedGender inputData){   
        return ingest(ANONYMOUSUSER_DB, "ingestAUPredictedGenderV2", 
                src,AnonymousUserKey.forGenderV2Of(globalId), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestPredictedGender(String src, long globalId, TPredictedGenderV2 inputData){   
        return ingest(ANONYMOUSUSER_DB, "ingestPredictedGender", 
                src,AnonymousUserKey.forPredictedGenderOf(globalId), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestShiftedPredictedGender(String src, long globalId, TPredictedGenderV2 inputData){   
        return ingest(ANONYMOUSUSER_DB, "ingestShiftedPredictedGender", 
                src,AnonymousUserKey.forShiftedPredictedGenderOf(globalId), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestAUPredictedAge2(String src, long globalId, TPredictedAgeRange inputData){   
        return ingest(ANONYMOUSUSER_DB, "ingestAUPredictedAge2", 
                src,AnonymousUserKey.forAgeV2Of(globalId), inputData, 
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult ingestAUPredictedAgeRange(String src, long globalId, TPredictedAgeRange inputData, boolean bln){
        return ingestAndNotify(ANONYMOUSUSER_DB2, "ingestAUPredictedAgeRange", 
                src, globalId,x->AnonymousUserKey.forAgeRangeOf(x),inputData, 
                () -> AnonymousNotifier.notifyAnonymousAgeRange(globalId, inputData), 
                bln,
                THIS_CLASS,LOGGER);
    }
}
