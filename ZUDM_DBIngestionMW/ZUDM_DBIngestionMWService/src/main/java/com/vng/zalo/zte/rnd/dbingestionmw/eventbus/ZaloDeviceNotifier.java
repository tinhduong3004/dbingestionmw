/*
 * Copyright (c) 2012-2016 by Zalo Group.
 * All Rights Reserved.
 */

package com.vng.zalo.zte.rnd.dbingestionmw.eventbus;

import static com.vng.zalo.zte.rnd.dbingestionmw.eventbus.DMPNotifier.serialize;
import com.vng.zing.stats.Profiler;
import com.vng.zing.stats.ThreadProfiler;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TAction;
import com.vng.zing.zudm_dmpmiddleware.eventbus.thrift.TDMPUpdatedField;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPDeviceInfoRaw;
import com.vng.zing.zudm_dmpmiddleware.thrift.TDMPUserDeviceRaw;
import com.vng.zalo.zte.rnd.dbingestionmw.device.thrift.ZUDM_UserDeviceRaw;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @date Oct 3, 2019
 * @author quydm
 */
public class ZaloDeviceNotifier extends DMPNotifier{
    public static long notifyZaloDevice(int zaloId,ZUDM_UserDeviceRaw zudmDevice){
        TDMPUserDeviceRaw userDevice = new TDMPUserDeviceRaw(-9);
        List<TDMPDeviceInfoRaw> deviceList = zudmDevice.devices.stream()
            .filter(device -> device.osName != null 
                    && device.deviceName != null && device.osVersion != null 
            ).map(device -> {
                TDMPDeviceInfoRaw deviceRaw = new TDMPDeviceInfoRaw();
                deviceRaw.setBrandName(device.brandName);
                deviceRaw.setDeviceName(device.deviceName);
                deviceRaw.setOsName(device.osName);
                deviceRaw.setOsVersion(device.osVersion);
                return deviceRaw;
            })
            .collect(Collectors.toList());
        if ( deviceList.size() > 0 ){    
            userDevice.setDevices(deviceList);
            userDevice.setExpiryDate(ExpiryDate.setExpriDate((int) zudmDevice.getUpdatedDate(), ExpiryDate._3MONTHS));
            userDevice.setError(0);
            ByteBuffer serializedData = serialize(userDevice);
            return notify(zaloId, TDMPUpdatedField.UserDevice, serializedData,TAction.Change, "IDENTIFIED" );
        }else{
            ThreadProfiler tp = Profiler.getThreadProfiler();
            tp.push(DMPNotifier.class, "ZaloDevice.nullObject");
            tp.pop(DMPNotifier.class, "ZaloDevice.nullObject");
            return -1;
        }
    }
}
