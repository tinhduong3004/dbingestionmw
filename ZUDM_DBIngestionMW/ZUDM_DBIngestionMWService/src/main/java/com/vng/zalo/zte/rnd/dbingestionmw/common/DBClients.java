// Created By PTP
package com.vng.zalo.zte.rnd.dbingestionmw.common;

import com.vng.zing.strlist32bm.thrift.wrapper.StrList32bmClient;
import com.vng.zing.zidb.thrift.wrapper.ZiDBClient;
import com.vng.zing.zidb64.thrift.wrapper.ZiDB64Client;

/**
 *
 * @author phucpt2
 */
//TODO remove it after the centralized was done
public class DBClients {
    ////////////////////////////////////////////
    /////////////// client /////////////////////
    ////////////////////////////////////////////
 
    public static final ZiDB64Client _DBCmon3Cli = new ZiDB64Client("zudmcommon3");
    public static final ZiDB64Client DemographicDB = new ZiDB64Client("ZUDM_ZiDB64DemographicDB");
    public static final ZiDB64Client UserIncomeDB = new ZiDB64Client("ZUDMUserIncome");
    public static final ZiDB64Client CreditScoreDB = new ZiDB64Client("ZUDMUserCreditScore");
    public static final ZiDBClient _DBClientTravelPathCity = new ZiDBClient("ZUDM_ZiDBTravelPathCity");
    public static final ZiDBClient _ZiDBHomeWorkLocation = new ZiDBClient("ZUDM_ZiDBHomeWorkLocation");
    public static final ZiDB64Client UserKWDB = new ZiDB64Client("ZUDM_ZiDB64UserKWDB");
    public static final ZiDBClient _ZiDBTravelPathPOI = new ZiDBClient("ZiDBTravelPathPOI");
    public static final ZiDB64Client _OccupationDB = new ZiDB64Client("ZUDM_ZiDB64Occupation");
    public static final ZiDBClient _ZiDBWifiInfo = new ZiDBClient("ZiDBWifiInfo");
    public static final ZiDB64Client OccupationDB = new ZiDB64Client("ZUDM_ZiDB64Occupation");
    public static final ZiDB64Client MostVisitedAADB = new ZiDB64Client("ZiDB64LivingLocation");
    public static final ZiDB64Client USERKEYWORDSDB = new ZiDB64Client("ZUDM_ZiDB64UserKeyword");
    public static final ZiDB64Client DeviceDB = new ZiDB64Client("ZUDMDevices_ZiDB64");
    public static final ZiDB64Client ANONYMOUSUSER_DB = new ZiDB64Client("ZUDM_ZiDB64AnonymousUser");
    public static final ZiDB64Client HOUSE_HOLD = new ZiDB64Client("HouseholdExtension");
    public static final StrList32bmClient CustomAudienceDB = new StrList32bmClient("zudm_customaudience_READ");
    public static final ZiDB64Client DEVICE_DB = new ZiDB64Client("ZUDMDevices_ZiDB64");
    public static final ZiDBClient ANONYMOUSUSERINTEREST = new ZiDBClient("ZUDMAnonymousUserInterest_ZiDB");
    public static final ZiDB64Client LOCATIONINTEREST = new ZiDB64Client("ZUDMLocationInterest_ZiDB64");
    public static final ZiDB64Client ANONYMOUSUSER_DB2 = new ZiDB64Client("ZUDM_ZiDB64AnonymousUser2");
    public static final ZiDB64Client DemographicDB3 = new ZiDB64Client("ZUDMDemographicDB3_ZiDB64");
    public static final ZiDB64Client DemographicDB2 = new ZiDB64Client("ZUDM_RollZiDB64DemographicDB2");
    public static final ZiDB64Client FriendRankingDB = new ZiDB64Client("ZUDM_FriendRanking_ZIDB64");
    public static final ZiDB64Client _DBCmon5Cli = new ZiDB64Client("zudmcommon5");
    public static final StrList32bmClient _DBipbmcommon6write = new StrList32bmClient("ipbmcommon6write");
    public static final StrList32bmClient _DBipbmcommon6read = new StrList32bmClient("ipbmcommon6read");
    public static final ZiDB64Client Tokenizer = new ZiDB64Client("ZUDM_ZiDB64UserTokenizedKWDB");
    
    /**
     * ZUDMPlayingInfo_ZiDB | 10.50.9.15:16136 | zudm:m3Yj6AyR, zudmr:4v4NfQlW
     */
    public static final ZiDBClient playingInfoDB = new ZiDBClient("ZUDM_PlayingInfo");

    /**
     * ZUDM_StrList32bmCommon6 10.30.58.52 18602,18603 zudm:H3dl5p, zudmr:5Pr8M7
     */
    public static final StrList32bmClient strList32bmCommon6Client = new StrList32bmClient("ZUDM_StrList32bmCommon6");
    
    ////////////////////////////////////////////
    /////////////// key ////////////////////////
    ////////////////////////////////////////////
    public static final long FOREIGN_TRIP_COUNT = 1170000000000000000L;
    public static final long FLIGHT_COUNT = 1200000000000000000L;
    public static final long MOST_VISITED_CITY = 1150000000000000000L;


}
