package com.vng.zalo.zte.rnd.dbingestionmw.model;

import static com.vng.zalo.zte.rnd.dbingestionmw.common.DBClients._OccupationDB;
import com.vng.zalo.zte.rnd.dbingestionmw.eventbus.UserOccupationNotifier;
import com.vng.zalo.zte.rnd.dbingestionmw.thrift.TActionResult;
import com.vng.zing.logger.ZLogger;
import com.vng.zing.zudm.common.centralizedkeys.UserOccupationKey;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyList;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyListV2;
import com.vng.zalo.zte.rnd.dbingestionmw.company.thrift.ZUDM_UserCompanyTop500;
import com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift.ZUDM_UserOccupationList;
import org.apache.log4j.Logger;

/**
 *
 * @author baopng
 */
public class UserOccupationModel extends DMPModel {

    private static final Class THIS_CLASS = UserOccupationModel.class;
    private static final Logger LOGGER = ZLogger.getLogger(THIS_CLASS);
    public static final UserOccupationModel INSTANCE = new UserOccupationModel();

    public TActionResult uploadModelOccupation(String src, int zaloId, ZUDM_UserOccupationList inputData){
        return ingest(_OccupationDB, "ingestModelOccupation", 
                src, UserOccupationKey.forUserModelOf(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadUserStudentOccupation(String src, int zaloId, ZUDM_UserOccupationList inputData){
        return ingestAndNotify(_OccupationDB, "ingestUserStudentOccupation", 
                src, zaloId,x->UserOccupationKey.forUserStudentOf(x),inputData,
                () -> UserOccupationNotifier.notifyUserStudentOccupation(zaloId, inputData),
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadUserUnionOccupation(String src, int zaloId, ZUDM_UserOccupationList inputData){
        return ingestAndNotify(_OccupationDB, "ingestUserUnionOccupation", 
                src, zaloId,x->UserOccupationKey.forUserUnionOf(x),inputData,
                () -> UserOccupationNotifier.notifyUserUnionOccupation(zaloId, inputData),
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadUserBlueWhiteOccupation(String src, int zaloId, ZUDM_UserOccupationList inputData){
        return ingestAndNotify(_OccupationDB, "ingestUserBlueWhiteOccupation", 
                src, zaloId,x->UserOccupationKey.forUserBluewhiteOf(x),inputData, 
                () -> UserOccupationNotifier.notifyUserBlueWhiteOccupation(zaloId, inputData),
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadUserCompany(String src, int zaloId, ZUDM_UserCompanyList inputData, boolean bln){
        return ingestAndNotify(_OccupationDB, "uploadUserCompany", 
                src, zaloId,x->UserOccupationKey.forCompanyOf(x),inputData, 
                () -> UserOccupationNotifier.notifyUserCompany(zaloId, inputData), bln,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadUserCompanyV2(String src, int zaloId, ZUDM_UserCompanyListV2 inputData, boolean bln){
        return ingest(_OccupationDB, "uploadUserCompanyV2", 
                src,UserOccupationKey.forUserCompanyOf(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadUserCompanyTop500(String src, int zaloId, ZUDM_UserCompanyTop500 inputData, boolean bln){
        return ingestAndNotify(_OccupationDB, "uploadUserCompanyTop500", 
                src, zaloId,x->UserOccupationKey.forCompanyTop500Of(x),inputData, 
                () -> UserOccupationNotifier.notifyUserCompanyTop500(zaloId, inputData), bln,
                THIS_CLASS,LOGGER);
    }
    
    public TActionResult uploadWorkType (String src, int zaloId, ZUDM_UserOccupationList inputData){
        return ingest(_OccupationDB, "ingestWorkType", 
                src, UserOccupationKey.forWorkTypeOf(zaloId),inputData,
                THIS_CLASS,LOGGER);
    }
}
