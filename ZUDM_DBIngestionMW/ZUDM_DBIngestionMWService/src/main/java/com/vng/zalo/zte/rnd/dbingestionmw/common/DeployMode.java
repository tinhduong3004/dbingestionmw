/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vng.zalo.zte.rnd.dbingestionmw.common;

import com.vng.zing.configer.ZConfig;
import com.vng.zing.thriftserver.ThriftServers;

/**
 *
 * @author lap13633
 */
public class DeployMode {

    public static String DEV = "dev";
    public static String LIVE = "live";

    /**
[ThriftServers@zudm_dbingestionmw]
host=0.0.0.0
port=18782
ncoreThreads=32
nmaxThreads=64
maxFrameSize=5000000
deploymode=dev
     */
    public static String CURRENT_DEPLOY_MODE = ZConfig.Instance.getString(ThriftServers.class, "zudm_dbingestionmw", "deploymode", DEV);
}