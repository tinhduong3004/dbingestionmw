#!/bin/sh

#setup JAVA environment
#. /zserver/java/set_env.cmd

_DEBUG=false
_COMPRESS=true

_CLEAN="gradle clean"
_CMD="./gradlew build"
$_CLEAN
$_CMD
echo Done by build command: $_CMD
