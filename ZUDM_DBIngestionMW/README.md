### Hướng dẫn
#### 1. Khai báo API và datamodel trong thrift 
* Hiện tại Các API chính của middleware được định nghĩa trong file duy nhất `dbingestionmw-thrift/thrift/dbingestionmw.thrift`. 
* *Datamodel* dùng chung thì khai báo `thrift/dbingestionmw_shared.thrift` 
* *Datamodel* dùng riêng thì có thể tạo file tương ứng với từng project để tiện quản lý
* Ngoài ra, có thể import *datamodel* có sẵn từ file thrift của các project cũ.
* Chạy `./gradlew build` để compile thrift thành file jar và tự động copy vào thư mục `/zserver/java/lib/zudm/` với tên `jzudmdbingestionmw-thrift9-{VERSION}.jar`

#### 2. Hiện thực các API trong ZUDM_DBIngestionMWService
* *Middleware* cần import file jar được tạo ra ở phần 1. 
* *Middleware* sẽ tiếp nhận các request trong class `com.vng.zalo.zte.rnd.dbingestionmw.handlers.DBMiddlewareHandler`. Tại đây *handle* sẽ chỉ định *model* xử lý tương ứng với từng request.
* Mỗi project nên định nghĩa một class *model* riêng để hiện thực hóa tác vụ put db của mình.
* Để dễ dàng cho việc tracking trên profiler và ghi log, mỗi project cần tự khai báo một *caller* riêng khi truy xuất đến *middleware*. Việc khai báo này có thể thực hiện trong class `com.vng.zalo.zte.rnd.dbingestionmw.admin.CallerManager`
* Sau khi test ở local và commit thành công, liên hệ ``quydm@vng.com.vn`` để merge git và deploy. 

#### 3. Khai báo client trong dbingestionmw-client
* *Middleware client* cần import file jar được tạo ra ở phần 1. Yêu cầu file này phải đồng bộ với *middleware service* ở phần 2.
* Mỗi project nên định nghĩa một client tương ứng. Vì client được đưa lên YARN nên các config cơ bản của `middleware` cần phải được hardcode trong các class *java* hoặc *scala*.
* Chạy file `./gradlew build` để compile project thành file jar và tự động copy vào thư mục `/zserver/java/lib/zudm/` với tên `zudmdbingestionmwclient-{VERSION}.jar`

#### 4. Sử dụng middleware client
* Các project upload database khi sử dụng `middleware` cần phải import 2 file jar được tạo ra trong phần 1 và phần 3.
* Ví dụ tạo client

```
import com.vng.zalo.zte.rnd.dbingestionmw.client.ArticleInterestDBMWClient;

public class App {
    public static void main(String[] args) throws Exception{
        ArticleInterestDBMWClient client = new ArticleInterestDBMWClient("127.0.0.1",18782);
        System.out.println(client.ping());
    }
}
```


