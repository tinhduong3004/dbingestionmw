namespace java com.vng.zalo.zte.rnd.dbingestionmw.bank_lead.thrift

struct ZUDM_LoanDemandScore {
	1: optional double score,
	2: optional i16 updatedDate
}

struct ZUDM_DisburseLeadScore {
	1: optional i32 score,
	2: optional i16 updatedDate
}

struct ZUDM_DisburseLeadScoreV2 {
	1: optional double score,
	2: optional i32 tag,
	3: optional i16 updatedDate
}

struct ZUDM_LoanDemandTag {
	1: required i32 tag,
	2: optional i16 updateDate
}
