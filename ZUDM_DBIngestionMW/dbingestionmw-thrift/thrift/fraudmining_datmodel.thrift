namespace java com.vng.zalo.zte.rnd.dbingestionmw.fraudmining.thrift

struct TListDuplicateAccount {
	1: optional i16 count,
	2: optional list<i32> ListId,
	3: optional i16 updatedDate
}
