namespace java com.vng.zalo.zte.rnd.dbingestionmw.relationship.thrift

//Relatonship
struct ZUDM_RelationshipInfo {
	1:required i32 des_id,
	2:optional i32 relationship_code,
}

struct ZUDM_RelationshipList {
	1: optional list<ZUDM_RelationshipInfo> relationships,
	2: optional i16 updatedDate
}