include "dbingestionmw_shared.thrift"
include "articleinterest_datamodel.thrift"
include "customaudience_datamodel.thrift"
include "locationmining_datamodel.thrift"
include "user_interest_datamodel.thrift"
include "zingmp3_suggestion_datamodel.thrift"
include "anonymoususer_datamodel.thrift"
include "gameh5_suggestion_datamodel.thrift"
include "elasticsearch_datamodel.thrift"
include "banklead_datamodel.thrift"
include "friendranking_datamodel.thrift"
include "demographic_datamodel.thrift"
include "relationship_datamodel.thrift"
include "device_datamodel.thrift"
include "company_datamodel.thrift"
include "fraudmining_datmodel.thrift"

namespace java com.vng.zalo.zte.rnd.dbingestionmw.thrift

service MiddlewareServiceBase {
	///test if connection to service is alive
	i32 ping();
}

service DBIngestionMW extends MiddlewareServiceBase {
	// Common
	dbingestionmw_shared.TActionResult addClientCaller(1:required string adminKey,2:required string callerName);
	dbingestionmw_shared.TActionResult removeClientCaller(1:required string adminKey,2:required string callerName);
	bool validCaller(1:required string adminKey,2:required string callerName);

	// ArticleInterest
	dbingestionmw_shared.TActionResult uploadUserKeywords(1:required string callerName,2:required i32 dataKey, 3:required i32 hour, 4:required articleinterest_datamodel.TUserKeywords inputData);
	dbingestionmw_shared.TActionResult uploadUserInterest(1:required string callerName,2:required i32 dataKey, 3:required i32 hour, 4:required articleinterest_datamodel.TUserKeywords inputData, 5:required bool notify);
	dbingestionmw_shared.TActionResult updateUserInterest(1:required string callerName,2:required i32 dataKey);
	dbingestionmw_shared.TActionResult updateAnonymousUserInterest(1:required string callerName,2:required i64 dataKey);
	dbingestionmw_shared.TActionResult uploadUserInterestByVectorSimilarity(1:required string callerName, 2:required i32 dataKey, 3:required i32 hour, 4:required articleinterest_datamodel.TUserKeywords inputData);
	dbingestionmw_shared.TActionResult uploadUserInterestByAls(1:required string callerName,2:required i32 dataKey, 3:required i32 hour, 4:required articleinterest_datamodel.TUserKeywords inputData);
	dbingestionmw_shared.TActionResult uploadAnonymousUserInterest(1:required string callerName,2:required i64 gid, 3:required articleinterest_datamodel.TUserKeywords inputData);
	dbingestionmw_shared.TActionResult uploadUserInterestArtists(1:required string callerName,2:required i32 uid, 3:required articleinterest_datamodel.TUserInterestArtists inputData, 4:required bool notify);
	
	// AnonymousUser
	dbingestionmw_shared.TActionResult ingestAUPredictedGender(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TPredictedGender inputData);
	dbingestionmw_shared.TActionResult ingestAUPredictedGenderV2(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TPredictedGender inputData);
	dbingestionmw_shared.TActionResult ingestAUPredictedAge(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TPredictedAge inputData);
	dbingestionmw_shared.TActionResult ingestAUPredictedAge2(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TPredictedAgeRange inputData);
	dbingestionmw_shared.TActionResult ingestAUPredictedAgeRange(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TPredictedAgeRange inputData,4:required bool notify);
	dbingestionmw_shared.TActionResult ingestAUPredictedProvince(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TPredictedProvince inputData);
	dbingestionmw_shared.TActionResult ingestAUListDevice(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TListDevice inputData);
	dbingestionmw_shared.TActionResult ingestAUAdtimaHistory(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TListHistory inputData);
	dbingestionmw_shared.TActionResult ingestAUZingNewsHistory(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TListHistory inputData);
	dbingestionmw_shared.TActionResult ingestAUZingMp3History(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TListHistory inputData);
	dbingestionmw_shared.TActionResult ingestAUListLocation(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TListLocation inputData);
	dbingestionmw_shared.TActionResult ingestUAUProvince(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TPredictedProvince inputData);
	dbingestionmw_shared.TActionResult ingestPredictedGender(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TPredictedGenderV2 inputData);
	dbingestionmw_shared.TActionResult ingestShiftedPredictedGender(1:required string callerName, 2:required i64 dataKey,3:required anonymoususer_datamodel.TPredictedGenderV2 inputData);

	//Location Mining
	dbingestionmw_shared.TActionResult ingestMostVisitedCity(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_MostVisitedProvinces inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestMostVisitedAdminArea(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_MostVisitedArea inputData);
	dbingestionmw_shared.TActionResult ingestFlightCount(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_FlightList inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestForeignTripCount(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_ForeignTripCount inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestFullHomeWorkLocation(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_ListLocationHomeWork inputData);
	dbingestionmw_shared.TActionResult ingestPartialHomeWorkLocation(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_ListLocationHomeWork inputData);
	dbingestionmw_shared.TActionResult ingestHomeLocation(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_ListLocationHomeWork inputData);
	dbingestionmw_shared.TActionResult ingestWorkLocation(1:required string callerName,2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_ListLocationHomeWork inputData);
	dbingestionmw_shared.TActionResult ingestUserSSIDInterest(1:required string callerName,2:required i32 uid, 3:required locationmining_datamodel.ZUDM_UserInterestLocation inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestIP2Location(1:required string callerName, 2:required string dataKey, 3:required locationmining_datamodel.ZUDM_IP2Location inputData);
	dbingestionmw_shared.TActionResult ingestListIP2Location(1:required string callerName, 2:required string ip, 3:required bool finish);
	dbingestionmw_shared.TActionResult ingestHometown(1:required string callerName, 2:required i32 zaloId, 3:required locationmining_datamodel.ZUDM_UserHometown inputData, 4:required bool notify);
	
	//ElasticSearch
	dbingestionmw_shared.TStringSearchResult ESLocationGetListUser(1:required string callerName, 2:required list<i64> list_user);
	dbingestionmw_shared.TStringSearchResult ESLocationQueryNearBy(1:required string callerName, 2:required double lat, 3:required double lon, 4:required double distance, 5:required double score_threshold);
	dbingestionmw_shared.TStringSearchResult ESLocationInsertListUser(1:required string callerName, 2:required list<elasticsearch_datamodel.UserLocationCheckinCount> list_user);
	
	dbingestionmw_shared.TStringSearchResult ESIngestUserKeyword(1:required string callerName, 2:required elasticsearch_datamodel.KeywordInfoList inputData, 3:required bool notify);
	dbingestionmw_shared.TStringSearchResult ESIngestAnonymousUserKeyword(1:required string callerName, 2:required elasticsearch_datamodel.KeywordInfoList inputData, 3:required bool notify);

	// Demographic
	dbingestionmw_shared.TActionResult uploadHousehold(1:required string callerName,2:required i32 userId, 3:required demographic_datamodel.ZUDM_Household inputData);
	dbingestionmw_shared.TActionResult uploadDeviceRaw(1:required string callerName,2:required i32 userId, 3:required device_datamodel.ZUDM_UserDeviceRaw inputData);
	dbingestionmw_shared.TActionResult ingestPhonebook(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserPhonebookName inputData);
	dbingestionmw_shared.TActionResult ingestGender(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserPredictedGender inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestGender2(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserPredictedGender inputData);
	dbingestionmw_shared.TActionResult ingestUserNumFriend(1:required string callerName,2:required i32 zaloId, 3:required friendranking_datamodel.ZUDM_UserNumFriend inputData);
	dbingestionmw_shared.TActionResult ingestAge(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserAge inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestAgeV2(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserAge inputData);
	dbingestionmw_shared.TActionResult ingestRealname(1:required string callerName, 2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserRealNameList inputData);
	dbingestionmw_shared.TActionResult ingestFullName(1:required string callerName, 2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserRealNameList inputData);
	dbingestionmw_shared.TActionResult ingestUserIncome(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserIncome inputData);
	dbingestionmw_shared.TActionResult ingestIncomeRank(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserIncome inputData);
	dbingestionmw_shared.TActionResult ingestCreditscore(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserCreditScore inputData);
	dbingestionmw_shared.TActionResult ingestCreditscoreV2(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserCreditScore inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestCreditscoreV3(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserCreditScore inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestNewCreditscoreV2(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserCreditScore inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestNewCreditscoreV3(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserCreditScore inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestMaritalStatus(1:required string callerName, 2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserMaritalStatus inputData);
	
	dbingestionmw_shared.TActionResult ingestParentalStatus(1:required string callerName, 2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserParentalStatus inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestEducationLevel(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserEducationLevel inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestBidirectionalPhonebook(1:required string callerName,2:required i32 src_id, 3:required demographic_datamodel.ZUDM_BidirectionalPhonebookList inputData);

	// User Occupation
	dbingestionmw_shared.TActionResult uploadUserStudentOccupation(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserOccupationList inputData);
	dbingestionmw_shared.TActionResult uploadUserUnionOccupation(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserOccupationList inputData);
	dbingestionmw_shared.TActionResult uploadUserBlueWhiteOccupation(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserOccupationList inputData);
	dbingestionmw_shared.TActionResult uploadModelOccupation(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserOccupationList inputData);
	dbingestionmw_shared.TActionResult ingestUserCompany(1:required string callerName,2:required i32 src_id, 3:required company_datamodel.ZUDM_UserCompanyList inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestUserCompanyTop500 (1:required string callerName,2:required i32 src_id, 3:required company_datamodel.ZUDM_UserCompanyTop500 inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestUserCompanyV2(1:required string callerName,2:required i32 src_id, 3:required company_datamodel.ZUDM_UserCompanyListV2 inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult uploadWorkType(1:required string callerName,2:required i32 zaloId, 3:required demographic_datamodel.ZUDM_UserOccupationList inputData);

	// User Coverage
	dbingestionmw_shared.TUserCoverageResult checkCoverage(1:required string callerName,2:required list<i64> list_user, 3:required i16 dbtype);
	dbingestionmw_shared.TMultigetIntResult multigetIntFromDB(1:required string callerName,2:required list<i64> list_user, 3:required string dbtype);
	dbingestionmw_shared.TMultigetLongResult multigetLongFromDB(1:required string callerName,2:required list<i64> list_user, 3:required string dbtype);
	dbingestionmw_shared.TMultigetDoubleResult multigetDoubleFromDB(1:required string callerName,2:required list<i64> list_user, 3:required string dbtype);
	

	//Cache
	i32 getHourCache(1:required string api);

	// GameH5 
	// deprecated, use uploadGameh5PlayingInfo with appid instead
	dbingestionmw_shared.TActionResult uploadGameh5TienlenPlayingInfo(1:required string callerName,2:required i32 userId, 3:required gameh5_suggestion_datamodel.ZUDM_Gameh5TienlenPlayingInfo inputData)
	dbingestionmw_shared.TActionResult uploadGameh5PlayingInfo(1:required string callerName,2:required i32 userId, 3:required i32 appid, 4:required gameh5_suggestion_datamodel.ZUDM_Gameh5PlayingInfo inputData)
	// for UserHighWinrate
	dbingestionmw_shared.TActionResult putUserHighWinrate(1:required list<i32> userIdList, 2:required i32 appid);
	dbingestionmw_shared.TActionResult removeUserHighWinrate(1:required i32 appid);

	//bank_lead
	dbingestionmw_shared.TActionResult uploadLoanDemandScore(1:required string callerName,2:required i32 zaloId, 3:required banklead_datamodel.ZUDM_LoanDemandScore inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult uploadDisburseLeadScore(1:required string callerName,2:required i32 zaloId, 3:required banklead_datamodel.ZUDM_DisburseLeadScore inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult uploadDisburseLeadShScore(1:required string callerName,2:required i32 zaloId, 3:required banklead_datamodel.ZUDM_DisburseLeadScoreV2 inputData, 4:required bool notify);
	dbingestionmw_shared.TActionResult ingestLoanDemandTag(1:required string callerName,2:required i32 zaloId, 3:required banklead_datamodel.ZUDM_LoanDemandTag inputData, 4:required bool notify);

	//remove key
	//dbingestionmw_shared.TActionResult removeKey(1:required string callerName,2:required i32 zaloId);
	dbingestionmw_shared.TActionResult removeDemographicKey(1:required string callerName,2:required i64 zaloId);
	dbingestionmw_shared.TActionResult removeCreditScoreV2Key(1:required string callerName,2:required i32 zaloId, 3:required bool notify);
	dbingestionmw_shared.TActionResult removeCreditScoreV3Key(1:required string callerName,2:required i32 zaloId, 3:required bool notify);

	//relationship
	dbingestionmw_shared.TActionResult ingestRelationship (1:required string callerName,2:required i32 src_id, 3:required relationship_datamodel.ZUDM_RelationshipList inputData);

	//friend ranking
	dbingestionmw_shared.TActionResult ingestInterestFriend (1:required string callerName,2:required i32 uid, 3:required friendranking_datamodel.ZUDM_ListFriendRanking inputData);
	dbingestionmw_shared.TActionResult ingestFollower (1:required string callerName,2:required i32 uid, 3:required friendranking_datamodel.ZUDM_ListFriendRanking inputData);
	dbingestionmw_shared.TActionResult ingestFollowerV2 (1:required string callerName,2:required i32 uid, 3:required friendranking_datamodel.ZUDM_ListFriendRanking inputData);
	dbingestionmw_shared.TActionResult ingestFollowerV3 (1:required string callerName,2:required i32 uid, 3:required friendranking_datamodel.ZUDM_ListFriendRanking inputData);
	dbingestionmw_shared.TActionResult ingestFollowerV4 (1:required string callerName,2:required i32 uid, 3:required friendranking_datamodel.ZUDM_ListFriendRanking inputData);

	//authen
	dbingestionmw_shared.TActionResult ingestListDuplicateAccount(1:required string callerName,2:required i32 uid, 3:required fraudmining_datmodel.TListDuplicateAccount inputData);
}
