namespace java com.vng.zalo.zte.rnd.dbingestionmw.notify.thrift

struct TGender{
	1: optional i32 value
}

struct TAge{
	1: optional double value
}

struct TAgeGender{
	1: optional i32 age
	2: optional i32 gender
}

