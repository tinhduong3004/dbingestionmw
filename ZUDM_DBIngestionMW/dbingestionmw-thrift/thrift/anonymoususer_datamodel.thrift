namespace java com.vng.zalo.zte.rnd.dbingestionmw.anonymoususer.thrift

//-----------------------------------------------------
//Data types definition

enum TGender {
	MALE = 1,
	FEMALE = 2,
	OTHER = 3
}

enum TAge {
	FROM_0_TO_17 = 0,
	FROM_18_TO_24 = 1,
	FROM_25_TO_34 = 2,
	FROM_35_TO_44 = 3,
	FROM_45_TO_54 = 4,
	FROM_55_TO_64 = 5,
	FROM_65_TO_INF = 6
}

struct TPredictedGender{
	1: required i32 error, 
	2: optional TGender value,
	3: optional string expiryDate
}

struct TPredictedGenderV2{
	1: required i32 error, 
	2: optional TGender pred_gender_v1,
	3: optional TGender pred_gender_v2,
	4: optional string expiryDate
}

struct TPredictedAge{
	1: required i32 error, 
	2: optional double value,
	3: optional string expiryDate
}

struct TPredictedAgeRange{
	1: required i32 error, 
	2: optional TAge value,
	3: optional string expiryDate
}

struct TPredictedProvince{
	1: required i32 error, 
	2: optional i32 id,
	3: optional string expiryDate
}

struct TListHistory{
	1: required i32 error, 
	2: optional list<string> value,
	3: optional string expiryDate
}

struct TLocationIP{
	1: optional i64 startDate,
	2: optional i64 endDate,
	3: optional i32 count,
	4: optional string ip,
	5: optional i32 administrationId
}

struct TListLocation{
	1: required i32 error,
	2: optional list<TLocationIP> value,
}

struct TDeviceInfo {
	1: optional string brand,
	2: optional string name,
	3: optional string os,
	4: optional string osVersion,
	5: optional i64 latestLogin
}

struct TListDevice {
	1: optional list<TDeviceInfo> devices,
	2: optional string expiryDate
}

struct TPredictAgeGender {
	1: required i32 Age,
	2: required i32 Gender
}
