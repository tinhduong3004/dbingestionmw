namespace java com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift 

enum TCountry {
	NOT_IN_VN = 0,
	AFGHANISTAN = 1,
	ALAND_ISLANDS = 2,
	ALBANIA = 3,
	ALGERIA = 4,
	AMERICAN_SAMOA = 5,
	ANDORRA = 6,
	ANGOLA = 7,
	ANGUILLA = 8,
	ANTARCTICA = 9,
	ANTIGUA_AND_BARBUDA = 10,
	ARGENTINA = 11,
	ARMENIA = 12,
	ARUBA = 13,
	AUSTRALIA = 14,
	AUSTRIA = 15,
	AZERBAIJAN = 16,
	BAHAMAS = 17,
	BAHRAIN = 18,
	BANGLADESH = 19,
	BARBADOS = 20,
	BELARUS = 21,
	BELGIUM = 22,
	BELIZE = 23,
	BENIN = 24,
	BERMUDA = 25,
	BHUTAN = 26,
	BOLIVIA = 27,
	BOSNIA_AND_HERZEGOVINA = 28,
	BOTSWANA = 29,
	BOUVET_ISLAND = 30,
	BRAZIL = 31,
	BRITISH_INDIAN_OCEAN_TERRITORY = 32,
	BRITISH_VIRGIN_ISLANDS = 33,
	BRUNEI_DARUSSALAM = 34,
	BULGARIA = 35,
	BURKINA_FASO = 36,
	BURUNDI = 37,
	CAMBODIA = 38,
	CAMEROON = 39,
	CANADA = 40,
	CAPE_VERDE = 41,
	CAYMAN_ISLANDS = 42,
	CENTRAL_AFRICAN_REPUBLIC = 43,
	CHAD = 44,
	CHILE = 45,
	CHINA = 46,
	CHRISTMAS_ISLAND = 47,
	COCOS_ISLANDS = 48,
	COLOMBIA = 49,
	COMOROS = 50,
	CONGO = 51,
	DEMOCRATIC_REPUBLIC_OF_THE_CONGO = 52,
	COOK_ISLANDS = 53,
	COSTA_RICA = 54,
	CROATIA = 55,
	CUBA = 56,
	CYPRUS = 57,
	CZECH_REPUBLIC = 58,
	COTE_DIVOIRE = 59,
	DENMARK = 60,
	DJIBOUTI = 61,
	DOMINICA = 62,
	DOMINICAN_REPUBLIC = 63,
	ECUADOR = 64,
	EGYPT = 65,
	EL_SALVADOR = 66,
	EQUATORIAL_GUINEA = 67,
	ERITREA = 68,
	ESTONIA = 69,
	ETHIOPIA = 70,
	FALKLAND_ISLANDS = 71,
	FAROE_ISLANDS = 72,
	FIJI = 73,
	FINLAND = 74,
	FRANCE = 75,
	FRENCH_GUIANA = 76,
	FRENCH_POLYNESIA = 77,
	FRENCH_SOUTHERN_TERRITORIES = 78,
	GABON = 79,
	GAMBIA = 80,
	GEORGIA = 81,
	GERMANY = 82,
	GHANA = 83,
	GIBRALTAR = 84,
	GREECE = 85,
	GREENLAND = 86,
	GRENADA = 87,
	GUADELOUPE = 88,
	GUAM = 89,
	GUATEMALA = 90,
	GUERNSEY = 91,
	GUINEA = 92,
	GUINEA_BISSAU = 93,
	GUYANA = 94,
	HAITI = 95,
	HEARD_ISLAND_AND_MCDONALD_ISLANDS = 96,
	HOLY_SEE = 97,
	HONDURAS = 98,
	HONG_KONG = 99,
	HUNGARY = 100,
	ICELAND = 101,
	INDIA = 102,
	INDONESIA = 103,
	IRAN = 104,
	IRAQ = 105,
	IRELAND = 106,
	ISLE_OF_MAN = 107,
	ISRAEL = 108,
	ITALY = 109,
	JAMAICA = 110,
	JAPAN = 111,
	JERSEY = 112,
	JORDAN = 113,
	KAZAKHSTAN = 114,
	KENYA = 115,
	KIRIBATI = 116,
	DEMOCRATIC_PEOPLES_REPUBLIC_OF_KOREA = 117,
	REPUBLIC_OF_KOREA = 118,
	KUWAIT = 119,
	KYRGYZSTAN = 120,
	LAO_PDR = 121,
	LATVIA = 122,
	LEBANON = 123,
	LESOTHO = 124,
	LIBERIA = 125,
	LIBYA = 126,
	LIECHTENSTEIN = 127,
	LITHUANIA = 128,
	LUXEMBOURG = 129,
	MACAO = 130,
	MACEDONIA = 131,
	MADAGASCAR = 132,
	MALAWI = 133,
	MALAYSIA = 134,
	MALDIVES = 135,
	MALI = 136,
	MALTA = 137,
	MARSHALL_ISLANDS = 138,
	MARTINIQUE = 139,
	MAURITANIA = 140,
	MAURITIUS = 141,
	MAYOTTE = 142,
	MEXICO = 143,
	FEDERATED_STATES_OF_MICRONESIA = 144,
	MOLDOVA = 145,
	MONACO = 146,
	MONGOLIA = 147,
	MONTENEGRO = 148,
	MONTSERRAT = 149,
	MOROCCO = 150,
	MOZAMBIQUE = 151,
	MYANMAR = 152,
	NAMIBIA = 153,
	NAURU = 154,
	NEPAL = 155,
	NETHERLANDS = 156,
	NETHERLANDS_ANTILLES = 157,
	NEW_CALEDONIA = 158,
	NEW_ZEALAND = 159,
	NICARAGUA = 160,
	NIGER = 161,
	NIGERIA = 162,
	NIUE = 163,
	NORFOLK_ISLAND = 164,
	NORTHERN_MARIANA_ISLANDS = 165,
	NORWAY = 166,
	OMAN = 167,
	PAKISTAN = 168,
	PALAU = 169,
	PALESTINIAN_TERRITORY = 170,
	PANAMA = 171,
	PAPUA_NEW_GUINEA = 172,
	PARAGUAY = 173,
	PERU = 174,
	PHILIPPINES = 175,
	PITCAIRN = 176,
	POLAND = 177,
	PORTUGAL = 178,
	PUERTO_RICO = 179,
	QATAR = 180,
	ROMANIA = 181,
	RUSSIAN_FEDERATION = 182,
	RWANDA = 183,
	REUNION = 184,
	SAINT_HELENA = 185,
	SAINT_KITTS_AND_NEVIS = 186,
	SAINT_LUCIA = 187,
	SAINT_PIERRE_AND_MIQUELON = 188,
	SAINT_VINCENT_AND_GRENADINES = 189,
	SAINT_BARTHELEMY = 190,
	SAINT_MARTIN = 191,
	SAMOA = 192,
	SAN_MARINO = 193,
	SAO_TOME_AND_PRINCIPE = 194,
	SAUDI_ARABIA = 195,
	SENEGAL = 196,
	SERBIA = 197,
	SEYCHELLES = 198,
	SIERRA_LEONE = 199,
	SINGAPORE = 200,
	SLOVAKIA = 201,
	SLOVENIA = 202,
	SOLOMON_ISLANDS = 203,
	SOMALIA = 204,
	SOUTH_AFRICA = 205,
	SOUTH_GEORGIA_AND_THE_SOUTH_SANDWICH_ISLANDS = 206,
	SOUTH_SUDAN = 207,
	SPAIN = 208,
	SRI_LANKA = 209,
	SUDAN = 210,
	SURINAME = 211,
	SVALBARD_AND_JAN_MAYEN_ISLANDS = 212,
	SWAZILAND = 213,
	SWEDEN = 214,
	SWITZERLAND = 215,
	SYRIAN_ARAB_REPUBLIC = 216,
	TAIWAN = 217,
	TAJIKISTAN = 218,
	TANZANIA = 219,
	THAILAND = 220,
	TIMOR_LESTE = 221,
	TOGO = 222,
	TOKELAU = 223,
	TONGA = 224,
	TRINIDAD_AND_TOBAGO = 225,
	TUNISIA = 226,
	TURKEY = 227,
	TURKMENISTAN = 228,
	TURKS_AND_CAICOS_ISLANDS = 229,
	TUVALU = 230,
	UGANDA = 231,
	UKRAINE = 232,
	UNITED_ARAB_EMIRATES = 233,
	UNITED_KINGDOM = 234,
	UNITED_STATES_MINOR_OUTLYING_ISLANDS = 235,
	UNITED_STATES_OF_AMERICA = 236,
	URUGUAY = 237,
	UZBEKISTAN = 238,
	VANUATU = 239,
	VENEZUELA = 240,
	VIET_NAM = 241,
	VIRGIN_ISLANDS = 242,
	WALLIS_AND_FUTUNA_ISLANDS = 243,
	WESTERN_SAHARA = 244,
	YEMEN = 245,
	ZAMBIA = 246,
	ZIMBABWE = 247
}

struct TCountryCity {
	1:required TCountry country,
	2:required i16 cityId,
}

struct ZUDM_UserHometown {
	1: required TCountryCity province,
	2: optional double confident,
	3: optional i16 updatedDate
}

struct ZUDM_UserVisitedProvince {
	1: optional TCountryCity province,
	2: optional i16 count
}

struct ZUDM_MostVisitedProvinces {
	1: optional list<ZUDM_UserVisitedProvince> provinces,
	2: optional i16 updatedDate
}

struct ZUDM_MostVisitedArea {
	1: required i32 administrativeLocID,
	2: optional i32 updatedDate,
}

struct ZUDM_MostVisitedArea {
	1: required i32 administrativeLocID,
	2: optional i32 updatedDate,
}

struct ZUDM_Flight{
	1: required i32 error,
	2: optional TCountryCity from_location,
	3: optional TCountryCity to_location,
	4: optional i32 start_time,
	5: optional i32 end_time
}

struct ZUDM_FlightList {
	1: required i32 error,
	2: optional list<ZUDM_Flight> flight_list,
	3: optional i32 count,
	4: optional i16 updatedDate
}

struct ZUDM_ForeignTripCount {
	1: optional i16 foreignTripCount,
	2: optional i16 updatedDate
}

enum ZUDM_LocationType {
	Other = 0,
	Home = 1,
	Work = 2,
}

struct ZUDM_LocationHomeWork {
	1:required double lat,
	2:required double lon,
	3:required ZUDM_LocationType type,
	4:optional double score,
	5:optional i32 administrativeAreaId,
}

struct ZUDM_ListLocationHomeWork {
	1:required list<ZUDM_LocationHomeWork> listLocationHomeWork,
	2:optional i16 updatedDate,
}

struct ZUDM_UserInterestLocationCategory {
	1: required i32 value,
	2: optional double confidence
}

struct ZUDM_UserInterestLocation {
	1: optional i32 updateDate,
	2: required list<ZUDM_UserInterestLocationCategory> interest_category
}




// Density
struct ZUDM_DensityCellCheckinInfo {
	1: required map<byte, i16> checkin_hist
}

struct ZUDM_DensityCellHomeInfo {
	1: required i16 count
}

struct ZUDM_IP2Location {
	1:optional i64 locationIndex,
	2:optional i32 cityId,
	3:optional i32 lastCityID,
	4:optional list<i32> cityList,
	5:optional i32 consecutiveDays,
	6:optional i32 absentDays,
	7:optional i64 lastLocationIndex,
	8:optional string lastDate,	
	9:optional i16 putDate
}

struct ZUDM_UserHometown {
	1: required TCountryCity province,
	2: optional double confident,
	3: optional i16 updatedDate
}