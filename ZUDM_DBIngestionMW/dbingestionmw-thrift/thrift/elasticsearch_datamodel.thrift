namespace java com.vng.zalo.zte.rnd.dbingestionmw.elasticsearch_datamodel.thrift

struct LocationScore{
	1: optional double lat,
	2: optional double lon,
	3: optional double score,
	4: optional i64 last_date
}


struct UserLocationCheckinCount{
	1: optional i64 uid,
	2: optional list<LocationScore> list_location
}

struct KeywordInfo{
	1: required string value
	2: optional i32 kw_count
	3: optional i32 article_count
	4: optional double score
}

struct KeywordInfoList{
	1: required i64 uid
	2: required list<KeywordInfo> list_kw
	3: optional string updateOn
}
