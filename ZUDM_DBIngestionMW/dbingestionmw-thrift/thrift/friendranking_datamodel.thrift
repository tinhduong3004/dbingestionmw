namespace java com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift

struct ZUDM_FriendRanking {
	1: required i32 uid,
	2: required double score,
	3: required i16 rank,
	4: required i16 count,
	5: required i16 age
}

struct ZUDM_ListFriendRanking {
	1:required list<ZUDM_FriendRanking> listFriendRanking,
	2:optional i16 updatedDate
}

//User number of friend
struct ZUDM_UserNumFriend {
	1: required i32 error,
	2: required i32 numFriend,
	3: optional string expiryDate
}