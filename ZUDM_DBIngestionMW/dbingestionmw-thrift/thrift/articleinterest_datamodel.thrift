namespace java com.vng.zalo.zte.rnd.dbingestionmw.articleinterest.thrift

struct TArticleInfo{
	1: required string url,
	2: required string title,
	4: required string avatarUrl,
	5: required double score,
	6: required string date
}

struct TUserArticles{
	1: required i32 userId,
	2: required list<TArticleInfo> articles,
	3: optional string updatedOn
}

struct TKeywordInfo{
	1: required string value,
	2: required i32 score,
	3: optional i32 type
}

struct TUserKeywords {
	1: required i32 userId,
	2: required list<TKeywordInfo> keywords
	3: optional string updatedOn
}

struct TArtistInfo{
	1: required i32 artist_id,
	2: required double score
}

struct TUserInterestArtists{
	1: required list<TArtistInfo> list_artists,
	2: optional i16 updatedDate
}

