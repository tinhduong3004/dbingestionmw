namespace java com.vng.zalo.zte.rnd.dbingestionmw.demographic.thrift

struct ZUDM_AgeRangeOfChild {
	1: optional string ageRangeOfChild,
	2: optional i16 count
}

struct ZUDM_UserParentalStatus {
	1: optional i16 parentalStatus,
	2: optional i16 updatedDate,
	3: optional list<ZUDM_AgeRangeOfChild> ageRangeOfChilds
}

struct ZUDM_UserEducationLevel {
        1: optional i16 level,
	2: optional i16 updatedDate
}

struct ZUDM_BidirectionalPhonebook {
	1: optional i32 des_id,
	2: optional string src_pb_name,
	3: optional string des_pb_name
}

struct ZUDM_BidirectionalPhonebookList {
	1: required i32 error,
	2: optional list<ZUDM_BidirectionalPhonebook> BidirectionalPhonebook,
	3: optional i16 updatedDate
}



struct ZUDM_UserAge {
	1: optional double age,
	2: optional i16 updatedDate
}

struct ZUDM_Household {
	1: optional set<i32> members,
	2: optional i16 updatedDate
}

struct ZUDM_UserPredictedGender {
	1: optional byte gender_id,
	2: optional double score,
	3: optional i16 updatedDate
}

struct ZUDM_UserIncome {
	1: required string value,
	2: optional double confidence,
	3: optional i16 expiryDate
}

struct ZUDM_UserCreditScore {
	1: required double value,
	2: optional double confidence,
	3: optional i16 expiryDate
}

struct ZUDM_UserRealNameList {
	1: optional list<string> names,
	2: optional i16 updatedDate
}

struct ZUDM_UserMaritalStatus {
	1: optional i16 status,
	2: optional i16 updatedDate
}

// -- ## -- ZUDM_ZiDB64UserKWDB
struct ZUDM_UserPhonebookName {
	1: optional list<string> name,
	2: optional i16 updatedDate
}

//-- ## -- ZUDM_ZiDB64Occupation
struct ZUDM_OccupationInfo {
	1: required i32 error,
	2: required i32 OccupationId,
	3: optional double score
}

struct ZUDM_UserOccupationList {
	1: required i32 error,
	2: optional list<ZUDM_OccupationInfo> occupations,
	3: optional i16 updatedDate
}



