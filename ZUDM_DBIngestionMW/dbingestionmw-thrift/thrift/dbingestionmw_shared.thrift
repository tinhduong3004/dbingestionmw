namespace java com.vng.zalo.zte.rnd.dbingestionmw.thrift

//-----------------------------------------------------
//Data types definition

// Represent the result status (succeeded/failed) for method api
enum TActionResult {
    SUCCESS = 0,
    WRONGAUTH = 1,
    ERROR = 2,
    NOT_EXIST = 3
}

//---------------------------------------------------------------------------
// ZUDM_UserCoverage
struct TUserCoverageResult {
	1: required i32 error,
	2: optional list<i64> result,
}

struct TMultigetIntResult {
	1: required i32 error,
	2: optional map<i64, i32> map_result,
}

struct TMultigetDoubleResult {
	1: required i32 error,
	2: optional map<i64, double> map_result,
}

struct TMultigetLongResult {
	1: required i32 error,
	2: optional map<i64, i64> map_result,
}

//---------------------------------------------------------------------------
// Elastic Search
struct TStringSearchResult {
	1: required i32 error,
	2: optional string result
}
