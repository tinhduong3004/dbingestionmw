namespace java com.vng.zalo.zte.rnd.dbingestionmw.gameh5.thrift

# deprecated, use ZUDM_Gameh5PlayingInfo instead
struct ZUDM_Gameh5TienlenPlayingInfo {
    1:optional i32 uid;
    2:optional i32 n_match;
    3:optional i32 n_win;
}

# use this thrift object to save playing info of all game
struct ZUDM_Gameh5PlayingInfo {
    1:optional i32 uid;
    2:optional i32 appid;
    3:optional i32 n_match;
    4:optional i32 n_win;
}