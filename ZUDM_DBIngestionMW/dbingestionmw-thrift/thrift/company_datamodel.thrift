namespace java com.vng.zalo.zte.rnd.dbingestionmw.company.thrift

struct ZUDM_UserCompanyTop500 {
	1:required i32 error,
	2:required i16 top500,
	3:optional i16 updatedDate
}

struct ZUDM_CompanyInfoV2 {
	1:required string organization,
	2:optional double score
}

struct ZUDM_UserCompanyListV2 {
	1: optional list<ZUDM_CompanyInfoV2> companys,
	2: optional i16 updatedDate
}

struct ZUDM_CompanyInfo {
	1:required string name,
	2:required i32 OccupationId,
	3:optional double confident
}

struct ZUDM_UserCompanyList {
    1: optional list<ZUDM_CompanyInfo> companys,
	2: optional i16 updatedDate
}