namespace java com.vng.zalo.zte.rnd.dbingestionmw.device.thrift


struct ZUDM_DeviceRaw {
	1: optional string brandName,
	2: optional string deviceName,
	3: optional string osName,
	4: optional string osVersion,
}
struct ZUDM_UserDeviceRaw {
	1: optional list<ZUDM_DeviceRaw> devices,
	2: optional i16 updatedDate
}