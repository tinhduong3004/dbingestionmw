/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.vng.zalo.zte.rnd.dbingestionmw.locationmining.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZUDM_LocationHomeWork implements org.apache.thrift.TBase<ZUDM_LocationHomeWork, ZUDM_LocationHomeWork._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("ZUDM_LocationHomeWork");

  private static final org.apache.thrift.protocol.TField LAT_FIELD_DESC = new org.apache.thrift.protocol.TField("lat", org.apache.thrift.protocol.TType.DOUBLE, (short)1);
  private static final org.apache.thrift.protocol.TField LON_FIELD_DESC = new org.apache.thrift.protocol.TField("lon", org.apache.thrift.protocol.TType.DOUBLE, (short)2);
  private static final org.apache.thrift.protocol.TField TYPE_FIELD_DESC = new org.apache.thrift.protocol.TField("type", org.apache.thrift.protocol.TType.I32, (short)3);
  private static final org.apache.thrift.protocol.TField SCORE_FIELD_DESC = new org.apache.thrift.protocol.TField("score", org.apache.thrift.protocol.TType.DOUBLE, (short)4);
  private static final org.apache.thrift.protocol.TField ADMINISTRATIVE_AREA_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("administrativeAreaId", org.apache.thrift.protocol.TType.I32, (short)5);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new ZUDM_LocationHomeWorkStandardSchemeFactory());
    schemes.put(TupleScheme.class, new ZUDM_LocationHomeWorkTupleSchemeFactory());
  }

  public double lat; // required
  public double lon; // required
  /**
   * 
   * @see ZUDM_LocationType
   */
  public ZUDM_LocationType type; // required
  public double score; // optional
  public int administrativeAreaId; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    LAT((short)1, "lat"),
    LON((short)2, "lon"),
    /**
     * 
     * @see ZUDM_LocationType
     */
    TYPE((short)3, "type"),
    SCORE((short)4, "score"),
    ADMINISTRATIVE_AREA_ID((short)5, "administrativeAreaId");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // LAT
          return LAT;
        case 2: // LON
          return LON;
        case 3: // TYPE
          return TYPE;
        case 4: // SCORE
          return SCORE;
        case 5: // ADMINISTRATIVE_AREA_ID
          return ADMINISTRATIVE_AREA_ID;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __LAT_ISSET_ID = 0;
  private static final int __LON_ISSET_ID = 1;
  private static final int __SCORE_ISSET_ID = 2;
  private static final int __ADMINISTRATIVEAREAID_ISSET_ID = 3;
  private byte __isset_bitfield = 0;
  private _Fields optionals[] = {_Fields.SCORE,_Fields.ADMINISTRATIVE_AREA_ID};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.LAT, new org.apache.thrift.meta_data.FieldMetaData("lat", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.LON, new org.apache.thrift.meta_data.FieldMetaData("lon", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.TYPE, new org.apache.thrift.meta_data.FieldMetaData("type", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.EnumMetaData(org.apache.thrift.protocol.TType.ENUM, ZUDM_LocationType.class)));
    tmpMap.put(_Fields.SCORE, new org.apache.thrift.meta_data.FieldMetaData("score", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    tmpMap.put(_Fields.ADMINISTRATIVE_AREA_ID, new org.apache.thrift.meta_data.FieldMetaData("administrativeAreaId", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(ZUDM_LocationHomeWork.class, metaDataMap);
  }

  public ZUDM_LocationHomeWork() {
  }

  public ZUDM_LocationHomeWork(
    double lat,
    double lon,
    ZUDM_LocationType type)
  {
    this();
    this.lat = lat;
    setLatIsSet(true);
    this.lon = lon;
    setLonIsSet(true);
    this.type = type;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public ZUDM_LocationHomeWork(ZUDM_LocationHomeWork other) {
    __isset_bitfield = other.__isset_bitfield;
    this.lat = other.lat;
    this.lon = other.lon;
    if (other.isSetType()) {
      this.type = other.type;
    }
    this.score = other.score;
    this.administrativeAreaId = other.administrativeAreaId;
  }

  public ZUDM_LocationHomeWork deepCopy() {
    return new ZUDM_LocationHomeWork(this);
  }

  @Override
  public void clear() {
    setLatIsSet(false);
    this.lat = 0.0;
    setLonIsSet(false);
    this.lon = 0.0;
    this.type = null;
    setScoreIsSet(false);
    this.score = 0.0;
    setAdministrativeAreaIdIsSet(false);
    this.administrativeAreaId = 0;
  }

  public double getLat() {
    return this.lat;
  }

  public ZUDM_LocationHomeWork setLat(double lat) {
    this.lat = lat;
    setLatIsSet(true);
    return this;
  }

  public void unsetLat() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __LAT_ISSET_ID);
  }

  /** Returns true if field lat is set (has been assigned a value) and false otherwise */
  public boolean isSetLat() {
    return EncodingUtils.testBit(__isset_bitfield, __LAT_ISSET_ID);
  }

  public void setLatIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __LAT_ISSET_ID, value);
  }

  public double getLon() {
    return this.lon;
  }

  public ZUDM_LocationHomeWork setLon(double lon) {
    this.lon = lon;
    setLonIsSet(true);
    return this;
  }

  public void unsetLon() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __LON_ISSET_ID);
  }

  /** Returns true if field lon is set (has been assigned a value) and false otherwise */
  public boolean isSetLon() {
    return EncodingUtils.testBit(__isset_bitfield, __LON_ISSET_ID);
  }

  public void setLonIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __LON_ISSET_ID, value);
  }

  /**
   * 
   * @see ZUDM_LocationType
   */
  public ZUDM_LocationType getType() {
    return this.type;
  }

  /**
   * 
   * @see ZUDM_LocationType
   */
  public ZUDM_LocationHomeWork setType(ZUDM_LocationType type) {
    this.type = type;
    return this;
  }

  public void unsetType() {
    this.type = null;
  }

  /** Returns true if field type is set (has been assigned a value) and false otherwise */
  public boolean isSetType() {
    return this.type != null;
  }

  public void setTypeIsSet(boolean value) {
    if (!value) {
      this.type = null;
    }
  }

  public double getScore() {
    return this.score;
  }

  public ZUDM_LocationHomeWork setScore(double score) {
    this.score = score;
    setScoreIsSet(true);
    return this;
  }

  public void unsetScore() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __SCORE_ISSET_ID);
  }

  /** Returns true if field score is set (has been assigned a value) and false otherwise */
  public boolean isSetScore() {
    return EncodingUtils.testBit(__isset_bitfield, __SCORE_ISSET_ID);
  }

  public void setScoreIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __SCORE_ISSET_ID, value);
  }

  public int getAdministrativeAreaId() {
    return this.administrativeAreaId;
  }

  public ZUDM_LocationHomeWork setAdministrativeAreaId(int administrativeAreaId) {
    this.administrativeAreaId = administrativeAreaId;
    setAdministrativeAreaIdIsSet(true);
    return this;
  }

  public void unsetAdministrativeAreaId() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __ADMINISTRATIVEAREAID_ISSET_ID);
  }

  /** Returns true if field administrativeAreaId is set (has been assigned a value) and false otherwise */
  public boolean isSetAdministrativeAreaId() {
    return EncodingUtils.testBit(__isset_bitfield, __ADMINISTRATIVEAREAID_ISSET_ID);
  }

  public void setAdministrativeAreaIdIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __ADMINISTRATIVEAREAID_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case LAT:
      if (value == null) {
        unsetLat();
      } else {
        setLat((Double)value);
      }
      break;

    case LON:
      if (value == null) {
        unsetLon();
      } else {
        setLon((Double)value);
      }
      break;

    case TYPE:
      if (value == null) {
        unsetType();
      } else {
        setType((ZUDM_LocationType)value);
      }
      break;

    case SCORE:
      if (value == null) {
        unsetScore();
      } else {
        setScore((Double)value);
      }
      break;

    case ADMINISTRATIVE_AREA_ID:
      if (value == null) {
        unsetAdministrativeAreaId();
      } else {
        setAdministrativeAreaId((Integer)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case LAT:
      return Double.valueOf(getLat());

    case LON:
      return Double.valueOf(getLon());

    case TYPE:
      return getType();

    case SCORE:
      return Double.valueOf(getScore());

    case ADMINISTRATIVE_AREA_ID:
      return Integer.valueOf(getAdministrativeAreaId());

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case LAT:
      return isSetLat();
    case LON:
      return isSetLon();
    case TYPE:
      return isSetType();
    case SCORE:
      return isSetScore();
    case ADMINISTRATIVE_AREA_ID:
      return isSetAdministrativeAreaId();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof ZUDM_LocationHomeWork)
      return this.equals((ZUDM_LocationHomeWork)that);
    return false;
  }

  public boolean equals(ZUDM_LocationHomeWork that) {
    if (that == null)
      return false;

    boolean this_present_lat = true;
    boolean that_present_lat = true;
    if (this_present_lat || that_present_lat) {
      if (!(this_present_lat && that_present_lat))
        return false;
      if (this.lat != that.lat)
        return false;
    }

    boolean this_present_lon = true;
    boolean that_present_lon = true;
    if (this_present_lon || that_present_lon) {
      if (!(this_present_lon && that_present_lon))
        return false;
      if (this.lon != that.lon)
        return false;
    }

    boolean this_present_type = true && this.isSetType();
    boolean that_present_type = true && that.isSetType();
    if (this_present_type || that_present_type) {
      if (!(this_present_type && that_present_type))
        return false;
      if (!this.type.equals(that.type))
        return false;
    }

    boolean this_present_score = true && this.isSetScore();
    boolean that_present_score = true && that.isSetScore();
    if (this_present_score || that_present_score) {
      if (!(this_present_score && that_present_score))
        return false;
      if (this.score != that.score)
        return false;
    }

    boolean this_present_administrativeAreaId = true && this.isSetAdministrativeAreaId();
    boolean that_present_administrativeAreaId = true && that.isSetAdministrativeAreaId();
    if (this_present_administrativeAreaId || that_present_administrativeAreaId) {
      if (!(this_present_administrativeAreaId && that_present_administrativeAreaId))
        return false;
      if (this.administrativeAreaId != that.administrativeAreaId)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(ZUDM_LocationHomeWork other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    ZUDM_LocationHomeWork typedOther = (ZUDM_LocationHomeWork)other;

    lastComparison = Boolean.valueOf(isSetLat()).compareTo(typedOther.isSetLat());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetLat()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.lat, typedOther.lat);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetLon()).compareTo(typedOther.isSetLon());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetLon()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.lon, typedOther.lon);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetType()).compareTo(typedOther.isSetType());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetType()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.type, typedOther.type);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetScore()).compareTo(typedOther.isSetScore());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetScore()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.score, typedOther.score);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetAdministrativeAreaId()).compareTo(typedOther.isSetAdministrativeAreaId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetAdministrativeAreaId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.administrativeAreaId, typedOther.administrativeAreaId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("ZUDM_LocationHomeWork(");
    boolean first = true;

    sb.append("lat:");
    sb.append(this.lat);
    first = false;
    if (!first) sb.append(", ");
    sb.append("lon:");
    sb.append(this.lon);
    first = false;
    if (!first) sb.append(", ");
    sb.append("type:");
    if (this.type == null) {
      sb.append("null");
    } else {
      sb.append(this.type);
    }
    first = false;
    if (isSetScore()) {
      if (!first) sb.append(", ");
      sb.append("score:");
      sb.append(this.score);
      first = false;
    }
    if (isSetAdministrativeAreaId()) {
      if (!first) sb.append(", ");
      sb.append("administrativeAreaId:");
      sb.append(this.administrativeAreaId);
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // alas, we cannot check 'lat' because it's a primitive and you chose the non-beans generator.
    // alas, we cannot check 'lon' because it's a primitive and you chose the non-beans generator.
    if (type == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'type' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class ZUDM_LocationHomeWorkStandardSchemeFactory implements SchemeFactory {
    public ZUDM_LocationHomeWorkStandardScheme getScheme() {
      return new ZUDM_LocationHomeWorkStandardScheme();
    }
  }

  private static class ZUDM_LocationHomeWorkStandardScheme extends StandardScheme<ZUDM_LocationHomeWork> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, ZUDM_LocationHomeWork struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // LAT
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.lat = iprot.readDouble();
              struct.setLatIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // LON
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.lon = iprot.readDouble();
              struct.setLonIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // TYPE
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.type = ZUDM_LocationType.findByValue(iprot.readI32());
              struct.setTypeIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 4: // SCORE
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.score = iprot.readDouble();
              struct.setScoreIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 5: // ADMINISTRATIVE_AREA_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.administrativeAreaId = iprot.readI32();
              struct.setAdministrativeAreaIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetLat()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'lat' was not found in serialized data! Struct: " + toString());
      }
      if (!struct.isSetLon()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'lon' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, ZUDM_LocationHomeWork struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(LAT_FIELD_DESC);
      oprot.writeDouble(struct.lat);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(LON_FIELD_DESC);
      oprot.writeDouble(struct.lon);
      oprot.writeFieldEnd();
      if (struct.type != null) {
        oprot.writeFieldBegin(TYPE_FIELD_DESC);
        oprot.writeI32(struct.type.getValue());
        oprot.writeFieldEnd();
      }
      if (struct.isSetScore()) {
        oprot.writeFieldBegin(SCORE_FIELD_DESC);
        oprot.writeDouble(struct.score);
        oprot.writeFieldEnd();
      }
      if (struct.isSetAdministrativeAreaId()) {
        oprot.writeFieldBegin(ADMINISTRATIVE_AREA_ID_FIELD_DESC);
        oprot.writeI32(struct.administrativeAreaId);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class ZUDM_LocationHomeWorkTupleSchemeFactory implements SchemeFactory {
    public ZUDM_LocationHomeWorkTupleScheme getScheme() {
      return new ZUDM_LocationHomeWorkTupleScheme();
    }
  }

  private static class ZUDM_LocationHomeWorkTupleScheme extends TupleScheme<ZUDM_LocationHomeWork> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, ZUDM_LocationHomeWork struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      oprot.writeDouble(struct.lat);
      oprot.writeDouble(struct.lon);
      oprot.writeI32(struct.type.getValue());
      BitSet optionals = new BitSet();
      if (struct.isSetScore()) {
        optionals.set(0);
      }
      if (struct.isSetAdministrativeAreaId()) {
        optionals.set(1);
      }
      oprot.writeBitSet(optionals, 2);
      if (struct.isSetScore()) {
        oprot.writeDouble(struct.score);
      }
      if (struct.isSetAdministrativeAreaId()) {
        oprot.writeI32(struct.administrativeAreaId);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, ZUDM_LocationHomeWork struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      struct.lat = iprot.readDouble();
      struct.setLatIsSet(true);
      struct.lon = iprot.readDouble();
      struct.setLonIsSet(true);
      struct.type = ZUDM_LocationType.findByValue(iprot.readI32());
      struct.setTypeIsSet(true);
      BitSet incoming = iprot.readBitSet(2);
      if (incoming.get(0)) {
        struct.score = iprot.readDouble();
        struct.setScoreIsSet(true);
      }
      if (incoming.get(1)) {
        struct.administrativeAreaId = iprot.readI32();
        struct.setAdministrativeAreaIdIsSet(true);
      }
    }
  }

}

