/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.vng.zalo.zte.rnd.dbingestionmw.user_interest.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TSuggestionList2 implements org.apache.thrift.TBase<TSuggestionList2, TSuggestionList2._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("TSuggestionList2");

  private static final org.apache.thrift.protocol.TField SUGGEST_LIST_FIELD_DESC = new org.apache.thrift.protocol.TField("suggestList", org.apache.thrift.protocol.TType.LIST, (short)1);
  private static final org.apache.thrift.protocol.TField HOUR_FIELD_DESC = new org.apache.thrift.protocol.TField("hour", org.apache.thrift.protocol.TType.I32, (short)2);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new TSuggestionList2StandardSchemeFactory());
    schemes.put(TupleScheme.class, new TSuggestionList2TupleSchemeFactory());
  }

  public List<TSuggestion> suggestList; // required
  public int hour; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    SUGGEST_LIST((short)1, "suggestList"),
    HOUR((short)2, "hour");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // SUGGEST_LIST
          return SUGGEST_LIST;
        case 2: // HOUR
          return HOUR;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __HOUR_ISSET_ID = 0;
  private byte __isset_bitfield = 0;
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.SUGGEST_LIST, new org.apache.thrift.meta_data.FieldMetaData("suggestList", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.ListMetaData(org.apache.thrift.protocol.TType.LIST, 
            new org.apache.thrift.meta_data.StructMetaData(org.apache.thrift.protocol.TType.STRUCT, TSuggestion.class))));
    tmpMap.put(_Fields.HOUR, new org.apache.thrift.meta_data.FieldMetaData("hour", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(TSuggestionList2.class, metaDataMap);
  }

  public TSuggestionList2() {
  }

  public TSuggestionList2(
    List<TSuggestion> suggestList,
    int hour)
  {
    this();
    this.suggestList = suggestList;
    this.hour = hour;
    setHourIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public TSuggestionList2(TSuggestionList2 other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetSuggestList()) {
      List<TSuggestion> __this__suggestList = new ArrayList<TSuggestion>();
      for (TSuggestion other_element : other.suggestList) {
        __this__suggestList.add(new TSuggestion(other_element));
      }
      this.suggestList = __this__suggestList;
    }
    this.hour = other.hour;
  }

  public TSuggestionList2 deepCopy() {
    return new TSuggestionList2(this);
  }

  @Override
  public void clear() {
    this.suggestList = null;
    setHourIsSet(false);
    this.hour = 0;
  }

  public int getSuggestListSize() {
    return (this.suggestList == null) ? 0 : this.suggestList.size();
  }

  public java.util.Iterator<TSuggestion> getSuggestListIterator() {
    return (this.suggestList == null) ? null : this.suggestList.iterator();
  }

  public void addToSuggestList(TSuggestion elem) {
    if (this.suggestList == null) {
      this.suggestList = new ArrayList<TSuggestion>();
    }
    this.suggestList.add(elem);
  }

  public List<TSuggestion> getSuggestList() {
    return this.suggestList;
  }

  public TSuggestionList2 setSuggestList(List<TSuggestion> suggestList) {
    this.suggestList = suggestList;
    return this;
  }

  public void unsetSuggestList() {
    this.suggestList = null;
  }

  /** Returns true if field suggestList is set (has been assigned a value) and false otherwise */
  public boolean isSetSuggestList() {
    return this.suggestList != null;
  }

  public void setSuggestListIsSet(boolean value) {
    if (!value) {
      this.suggestList = null;
    }
  }

  public int getHour() {
    return this.hour;
  }

  public TSuggestionList2 setHour(int hour) {
    this.hour = hour;
    setHourIsSet(true);
    return this;
  }

  public void unsetHour() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __HOUR_ISSET_ID);
  }

  /** Returns true if field hour is set (has been assigned a value) and false otherwise */
  public boolean isSetHour() {
    return EncodingUtils.testBit(__isset_bitfield, __HOUR_ISSET_ID);
  }

  public void setHourIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __HOUR_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case SUGGEST_LIST:
      if (value == null) {
        unsetSuggestList();
      } else {
        setSuggestList((List<TSuggestion>)value);
      }
      break;

    case HOUR:
      if (value == null) {
        unsetHour();
      } else {
        setHour((Integer)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case SUGGEST_LIST:
      return getSuggestList();

    case HOUR:
      return Integer.valueOf(getHour());

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case SUGGEST_LIST:
      return isSetSuggestList();
    case HOUR:
      return isSetHour();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof TSuggestionList2)
      return this.equals((TSuggestionList2)that);
    return false;
  }

  public boolean equals(TSuggestionList2 that) {
    if (that == null)
      return false;

    boolean this_present_suggestList = true && this.isSetSuggestList();
    boolean that_present_suggestList = true && that.isSetSuggestList();
    if (this_present_suggestList || that_present_suggestList) {
      if (!(this_present_suggestList && that_present_suggestList))
        return false;
      if (!this.suggestList.equals(that.suggestList))
        return false;
    }

    boolean this_present_hour = true;
    boolean that_present_hour = true;
    if (this_present_hour || that_present_hour) {
      if (!(this_present_hour && that_present_hour))
        return false;
      if (this.hour != that.hour)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(TSuggestionList2 other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    TSuggestionList2 typedOther = (TSuggestionList2)other;

    lastComparison = Boolean.valueOf(isSetSuggestList()).compareTo(typedOther.isSetSuggestList());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetSuggestList()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.suggestList, typedOther.suggestList);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetHour()).compareTo(typedOther.isSetHour());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetHour()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.hour, typedOther.hour);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("TSuggestionList2(");
    boolean first = true;

    sb.append("suggestList:");
    if (this.suggestList == null) {
      sb.append("null");
    } else {
      sb.append(this.suggestList);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("hour:");
    sb.append(this.hour);
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (suggestList == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'suggestList' was not present! Struct: " + toString());
    }
    // alas, we cannot check 'hour' because it's a primitive and you chose the non-beans generator.
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class TSuggestionList2StandardSchemeFactory implements SchemeFactory {
    public TSuggestionList2StandardScheme getScheme() {
      return new TSuggestionList2StandardScheme();
    }
  }

  private static class TSuggestionList2StandardScheme extends StandardScheme<TSuggestionList2> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, TSuggestionList2 struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // SUGGEST_LIST
            if (schemeField.type == org.apache.thrift.protocol.TType.LIST) {
              {
                org.apache.thrift.protocol.TList _list8 = iprot.readListBegin();
                struct.suggestList = new ArrayList<TSuggestion>(_list8.size);
                for (int _i9 = 0; _i9 < _list8.size; ++_i9)
                {
                  TSuggestion _elem10; // required
                  _elem10 = new TSuggestion();
                  _elem10.read(iprot);
                  struct.suggestList.add(_elem10);
                }
                iprot.readListEnd();
              }
              struct.setSuggestListIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // HOUR
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.hour = iprot.readI32();
              struct.setHourIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetHour()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'hour' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, TSuggestionList2 struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.suggestList != null) {
        oprot.writeFieldBegin(SUGGEST_LIST_FIELD_DESC);
        {
          oprot.writeListBegin(new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.STRUCT, struct.suggestList.size()));
          for (TSuggestion _iter11 : struct.suggestList)
          {
            _iter11.write(oprot);
          }
          oprot.writeListEnd();
        }
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(HOUR_FIELD_DESC);
      oprot.writeI32(struct.hour);
      oprot.writeFieldEnd();
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class TSuggestionList2TupleSchemeFactory implements SchemeFactory {
    public TSuggestionList2TupleScheme getScheme() {
      return new TSuggestionList2TupleScheme();
    }
  }

  private static class TSuggestionList2TupleScheme extends TupleScheme<TSuggestionList2> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, TSuggestionList2 struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      {
        oprot.writeI32(struct.suggestList.size());
        for (TSuggestion _iter12 : struct.suggestList)
        {
          _iter12.write(oprot);
        }
      }
      oprot.writeI32(struct.hour);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, TSuggestionList2 struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      {
        org.apache.thrift.protocol.TList _list13 = new org.apache.thrift.protocol.TList(org.apache.thrift.protocol.TType.STRUCT, iprot.readI32());
        struct.suggestList = new ArrayList<TSuggestion>(_list13.size);
        for (int _i14 = 0; _i14 < _list13.size; ++_i14)
        {
          TSuggestion _elem15; // required
          _elem15 = new TSuggestion();
          _elem15.read(iprot);
          struct.suggestList.add(_elem15);
        }
      }
      struct.setSuggestListIsSet(true);
      struct.hour = iprot.readI32();
      struct.setHourIsSet(true);
    }
  }

}

