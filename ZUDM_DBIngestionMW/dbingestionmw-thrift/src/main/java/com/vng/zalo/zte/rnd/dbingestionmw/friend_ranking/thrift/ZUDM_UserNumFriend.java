/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.vng.zalo.zte.rnd.dbingestionmw.friend_ranking.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZUDM_UserNumFriend implements org.apache.thrift.TBase<ZUDM_UserNumFriend, ZUDM_UserNumFriend._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("ZUDM_UserNumFriend");

  private static final org.apache.thrift.protocol.TField ERROR_FIELD_DESC = new org.apache.thrift.protocol.TField("error", org.apache.thrift.protocol.TType.I32, (short)1);
  private static final org.apache.thrift.protocol.TField NUM_FRIEND_FIELD_DESC = new org.apache.thrift.protocol.TField("numFriend", org.apache.thrift.protocol.TType.I32, (short)2);
  private static final org.apache.thrift.protocol.TField EXPIRY_DATE_FIELD_DESC = new org.apache.thrift.protocol.TField("expiryDate", org.apache.thrift.protocol.TType.STRING, (short)3);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new ZUDM_UserNumFriendStandardSchemeFactory());
    schemes.put(TupleScheme.class, new ZUDM_UserNumFriendTupleSchemeFactory());
  }

  public int error; // required
  public int numFriend; // required
  public String expiryDate; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    ERROR((short)1, "error"),
    NUM_FRIEND((short)2, "numFriend"),
    EXPIRY_DATE((short)3, "expiryDate");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // ERROR
          return ERROR;
        case 2: // NUM_FRIEND
          return NUM_FRIEND;
        case 3: // EXPIRY_DATE
          return EXPIRY_DATE;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __ERROR_ISSET_ID = 0;
  private static final int __NUMFRIEND_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  private _Fields optionals[] = {_Fields.EXPIRY_DATE};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.ERROR, new org.apache.thrift.meta_data.FieldMetaData("error", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.NUM_FRIEND, new org.apache.thrift.meta_data.FieldMetaData("numFriend", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.EXPIRY_DATE, new org.apache.thrift.meta_data.FieldMetaData("expiryDate", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(ZUDM_UserNumFriend.class, metaDataMap);
  }

  public ZUDM_UserNumFriend() {
  }

  public ZUDM_UserNumFriend(
    int error,
    int numFriend)
  {
    this();
    this.error = error;
    setErrorIsSet(true);
    this.numFriend = numFriend;
    setNumFriendIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public ZUDM_UserNumFriend(ZUDM_UserNumFriend other) {
    __isset_bitfield = other.__isset_bitfield;
    this.error = other.error;
    this.numFriend = other.numFriend;
    if (other.isSetExpiryDate()) {
      this.expiryDate = other.expiryDate;
    }
  }

  public ZUDM_UserNumFriend deepCopy() {
    return new ZUDM_UserNumFriend(this);
  }

  @Override
  public void clear() {
    setErrorIsSet(false);
    this.error = 0;
    setNumFriendIsSet(false);
    this.numFriend = 0;
    this.expiryDate = null;
  }

  public int getError() {
    return this.error;
  }

  public ZUDM_UserNumFriend setError(int error) {
    this.error = error;
    setErrorIsSet(true);
    return this;
  }

  public void unsetError() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __ERROR_ISSET_ID);
  }

  /** Returns true if field error is set (has been assigned a value) and false otherwise */
  public boolean isSetError() {
    return EncodingUtils.testBit(__isset_bitfield, __ERROR_ISSET_ID);
  }

  public void setErrorIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __ERROR_ISSET_ID, value);
  }

  public int getNumFriend() {
    return this.numFriend;
  }

  public ZUDM_UserNumFriend setNumFriend(int numFriend) {
    this.numFriend = numFriend;
    setNumFriendIsSet(true);
    return this;
  }

  public void unsetNumFriend() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __NUMFRIEND_ISSET_ID);
  }

  /** Returns true if field numFriend is set (has been assigned a value) and false otherwise */
  public boolean isSetNumFriend() {
    return EncodingUtils.testBit(__isset_bitfield, __NUMFRIEND_ISSET_ID);
  }

  public void setNumFriendIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __NUMFRIEND_ISSET_ID, value);
  }

  public String getExpiryDate() {
    return this.expiryDate;
  }

  public ZUDM_UserNumFriend setExpiryDate(String expiryDate) {
    this.expiryDate = expiryDate;
    return this;
  }

  public void unsetExpiryDate() {
    this.expiryDate = null;
  }

  /** Returns true if field expiryDate is set (has been assigned a value) and false otherwise */
  public boolean isSetExpiryDate() {
    return this.expiryDate != null;
  }

  public void setExpiryDateIsSet(boolean value) {
    if (!value) {
      this.expiryDate = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case ERROR:
      if (value == null) {
        unsetError();
      } else {
        setError((Integer)value);
      }
      break;

    case NUM_FRIEND:
      if (value == null) {
        unsetNumFriend();
      } else {
        setNumFriend((Integer)value);
      }
      break;

    case EXPIRY_DATE:
      if (value == null) {
        unsetExpiryDate();
      } else {
        setExpiryDate((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case ERROR:
      return Integer.valueOf(getError());

    case NUM_FRIEND:
      return Integer.valueOf(getNumFriend());

    case EXPIRY_DATE:
      return getExpiryDate();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case ERROR:
      return isSetError();
    case NUM_FRIEND:
      return isSetNumFriend();
    case EXPIRY_DATE:
      return isSetExpiryDate();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof ZUDM_UserNumFriend)
      return this.equals((ZUDM_UserNumFriend)that);
    return false;
  }

  public boolean equals(ZUDM_UserNumFriend that) {
    if (that == null)
      return false;

    boolean this_present_error = true;
    boolean that_present_error = true;
    if (this_present_error || that_present_error) {
      if (!(this_present_error && that_present_error))
        return false;
      if (this.error != that.error)
        return false;
    }

    boolean this_present_numFriend = true;
    boolean that_present_numFriend = true;
    if (this_present_numFriend || that_present_numFriend) {
      if (!(this_present_numFriend && that_present_numFriend))
        return false;
      if (this.numFriend != that.numFriend)
        return false;
    }

    boolean this_present_expiryDate = true && this.isSetExpiryDate();
    boolean that_present_expiryDate = true && that.isSetExpiryDate();
    if (this_present_expiryDate || that_present_expiryDate) {
      if (!(this_present_expiryDate && that_present_expiryDate))
        return false;
      if (!this.expiryDate.equals(that.expiryDate))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(ZUDM_UserNumFriend other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    ZUDM_UserNumFriend typedOther = (ZUDM_UserNumFriend)other;

    lastComparison = Boolean.valueOf(isSetError()).compareTo(typedOther.isSetError());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetError()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.error, typedOther.error);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetNumFriend()).compareTo(typedOther.isSetNumFriend());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetNumFriend()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.numFriend, typedOther.numFriend);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetExpiryDate()).compareTo(typedOther.isSetExpiryDate());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetExpiryDate()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.expiryDate, typedOther.expiryDate);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("ZUDM_UserNumFriend(");
    boolean first = true;

    sb.append("error:");
    sb.append(this.error);
    first = false;
    if (!first) sb.append(", ");
    sb.append("numFriend:");
    sb.append(this.numFriend);
    first = false;
    if (isSetExpiryDate()) {
      if (!first) sb.append(", ");
      sb.append("expiryDate:");
      if (this.expiryDate == null) {
        sb.append("null");
      } else {
        sb.append(this.expiryDate);
      }
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // alas, we cannot check 'error' because it's a primitive and you chose the non-beans generator.
    // alas, we cannot check 'numFriend' because it's a primitive and you chose the non-beans generator.
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class ZUDM_UserNumFriendStandardSchemeFactory implements SchemeFactory {
    public ZUDM_UserNumFriendStandardScheme getScheme() {
      return new ZUDM_UserNumFriendStandardScheme();
    }
  }

  private static class ZUDM_UserNumFriendStandardScheme extends StandardScheme<ZUDM_UserNumFriend> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, ZUDM_UserNumFriend struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // ERROR
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.error = iprot.readI32();
              struct.setErrorIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // NUM_FRIEND
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.numFriend = iprot.readI32();
              struct.setNumFriendIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // EXPIRY_DATE
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.expiryDate = iprot.readString();
              struct.setExpiryDateIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetError()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'error' was not found in serialized data! Struct: " + toString());
      }
      if (!struct.isSetNumFriend()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'numFriend' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, ZUDM_UserNumFriend struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      oprot.writeFieldBegin(ERROR_FIELD_DESC);
      oprot.writeI32(struct.error);
      oprot.writeFieldEnd();
      oprot.writeFieldBegin(NUM_FRIEND_FIELD_DESC);
      oprot.writeI32(struct.numFriend);
      oprot.writeFieldEnd();
      if (struct.expiryDate != null) {
        if (struct.isSetExpiryDate()) {
          oprot.writeFieldBegin(EXPIRY_DATE_FIELD_DESC);
          oprot.writeString(struct.expiryDate);
          oprot.writeFieldEnd();
        }
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class ZUDM_UserNumFriendTupleSchemeFactory implements SchemeFactory {
    public ZUDM_UserNumFriendTupleScheme getScheme() {
      return new ZUDM_UserNumFriendTupleScheme();
    }
  }

  private static class ZUDM_UserNumFriendTupleScheme extends TupleScheme<ZUDM_UserNumFriend> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, ZUDM_UserNumFriend struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      oprot.writeI32(struct.error);
      oprot.writeI32(struct.numFriend);
      BitSet optionals = new BitSet();
      if (struct.isSetExpiryDate()) {
        optionals.set(0);
      }
      oprot.writeBitSet(optionals, 1);
      if (struct.isSetExpiryDate()) {
        oprot.writeString(struct.expiryDate);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, ZUDM_UserNumFriend struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      struct.error = iprot.readI32();
      struct.setErrorIsSet(true);
      struct.numFriend = iprot.readI32();
      struct.setNumFriendIsSet(true);
      BitSet incoming = iprot.readBitSet(1);
      if (incoming.get(0)) {
        struct.expiryDate = iprot.readString();
        struct.setExpiryDateIsSet(true);
      }
    }
  }

}

