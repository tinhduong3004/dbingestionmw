/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.vng.zalo.zte.rnd.dbingestionmw.customaudience.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TTrackedAppInfo implements org.apache.thrift.TBase<TTrackedAppInfo, TTrackedAppInfo._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("TTrackedAppInfo");

  private static final org.apache.thrift.protocol.TField APP_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("appID", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField PLATFORM_FIELD_DESC = new org.apache.thrift.protocol.TField("platform", org.apache.thrift.protocol.TType.STRING, (short)2);
  private static final org.apache.thrift.protocol.TField LAST_ACCESS_FIELD_DESC = new org.apache.thrift.protocol.TField("lastAccess", org.apache.thrift.protocol.TType.STRING, (short)3);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new TTrackedAppInfoStandardSchemeFactory());
    schemes.put(TupleScheme.class, new TTrackedAppInfoTupleSchemeFactory());
  }

  public String appID; // required
  public String platform; // required
  public String lastAccess; // required

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    APP_ID((short)1, "appID"),
    PLATFORM((short)2, "platform"),
    LAST_ACCESS((short)3, "lastAccess");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // APP_ID
          return APP_ID;
        case 2: // PLATFORM
          return PLATFORM;
        case 3: // LAST_ACCESS
          return LAST_ACCESS;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.APP_ID, new org.apache.thrift.meta_data.FieldMetaData("appID", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.PLATFORM, new org.apache.thrift.meta_data.FieldMetaData("platform", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.LAST_ACCESS, new org.apache.thrift.meta_data.FieldMetaData("lastAccess", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(TTrackedAppInfo.class, metaDataMap);
  }

  public TTrackedAppInfo() {
  }

  public TTrackedAppInfo(
    String appID,
    String platform,
    String lastAccess)
  {
    this();
    this.appID = appID;
    this.platform = platform;
    this.lastAccess = lastAccess;
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public TTrackedAppInfo(TTrackedAppInfo other) {
    if (other.isSetAppID()) {
      this.appID = other.appID;
    }
    if (other.isSetPlatform()) {
      this.platform = other.platform;
    }
    if (other.isSetLastAccess()) {
      this.lastAccess = other.lastAccess;
    }
  }

  public TTrackedAppInfo deepCopy() {
    return new TTrackedAppInfo(this);
  }

  @Override
  public void clear() {
    this.appID = null;
    this.platform = null;
    this.lastAccess = null;
  }

  public String getAppID() {
    return this.appID;
  }

  public TTrackedAppInfo setAppID(String appID) {
    this.appID = appID;
    return this;
  }

  public void unsetAppID() {
    this.appID = null;
  }

  /** Returns true if field appID is set (has been assigned a value) and false otherwise */
  public boolean isSetAppID() {
    return this.appID != null;
  }

  public void setAppIDIsSet(boolean value) {
    if (!value) {
      this.appID = null;
    }
  }

  public String getPlatform() {
    return this.platform;
  }

  public TTrackedAppInfo setPlatform(String platform) {
    this.platform = platform;
    return this;
  }

  public void unsetPlatform() {
    this.platform = null;
  }

  /** Returns true if field platform is set (has been assigned a value) and false otherwise */
  public boolean isSetPlatform() {
    return this.platform != null;
  }

  public void setPlatformIsSet(boolean value) {
    if (!value) {
      this.platform = null;
    }
  }

  public String getLastAccess() {
    return this.lastAccess;
  }

  public TTrackedAppInfo setLastAccess(String lastAccess) {
    this.lastAccess = lastAccess;
    return this;
  }

  public void unsetLastAccess() {
    this.lastAccess = null;
  }

  /** Returns true if field lastAccess is set (has been assigned a value) and false otherwise */
  public boolean isSetLastAccess() {
    return this.lastAccess != null;
  }

  public void setLastAccessIsSet(boolean value) {
    if (!value) {
      this.lastAccess = null;
    }
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case APP_ID:
      if (value == null) {
        unsetAppID();
      } else {
        setAppID((String)value);
      }
      break;

    case PLATFORM:
      if (value == null) {
        unsetPlatform();
      } else {
        setPlatform((String)value);
      }
      break;

    case LAST_ACCESS:
      if (value == null) {
        unsetLastAccess();
      } else {
        setLastAccess((String)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case APP_ID:
      return getAppID();

    case PLATFORM:
      return getPlatform();

    case LAST_ACCESS:
      return getLastAccess();

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case APP_ID:
      return isSetAppID();
    case PLATFORM:
      return isSetPlatform();
    case LAST_ACCESS:
      return isSetLastAccess();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof TTrackedAppInfo)
      return this.equals((TTrackedAppInfo)that);
    return false;
  }

  public boolean equals(TTrackedAppInfo that) {
    if (that == null)
      return false;

    boolean this_present_appID = true && this.isSetAppID();
    boolean that_present_appID = true && that.isSetAppID();
    if (this_present_appID || that_present_appID) {
      if (!(this_present_appID && that_present_appID))
        return false;
      if (!this.appID.equals(that.appID))
        return false;
    }

    boolean this_present_platform = true && this.isSetPlatform();
    boolean that_present_platform = true && that.isSetPlatform();
    if (this_present_platform || that_present_platform) {
      if (!(this_present_platform && that_present_platform))
        return false;
      if (!this.platform.equals(that.platform))
        return false;
    }

    boolean this_present_lastAccess = true && this.isSetLastAccess();
    boolean that_present_lastAccess = true && that.isSetLastAccess();
    if (this_present_lastAccess || that_present_lastAccess) {
      if (!(this_present_lastAccess && that_present_lastAccess))
        return false;
      if (!this.lastAccess.equals(that.lastAccess))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(TTrackedAppInfo other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    TTrackedAppInfo typedOther = (TTrackedAppInfo)other;

    lastComparison = Boolean.valueOf(isSetAppID()).compareTo(typedOther.isSetAppID());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetAppID()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.appID, typedOther.appID);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetPlatform()).compareTo(typedOther.isSetPlatform());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetPlatform()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.platform, typedOther.platform);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetLastAccess()).compareTo(typedOther.isSetLastAccess());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetLastAccess()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.lastAccess, typedOther.lastAccess);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("TTrackedAppInfo(");
    boolean first = true;

    sb.append("appID:");
    if (this.appID == null) {
      sb.append("null");
    } else {
      sb.append(this.appID);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("platform:");
    if (this.platform == null) {
      sb.append("null");
    } else {
      sb.append(this.platform);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("lastAccess:");
    if (this.lastAccess == null) {
      sb.append("null");
    } else {
      sb.append(this.lastAccess);
    }
    first = false;
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (appID == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'appID' was not present! Struct: " + toString());
    }
    if (platform == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'platform' was not present! Struct: " + toString());
    }
    if (lastAccess == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'lastAccess' was not present! Struct: " + toString());
    }
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class TTrackedAppInfoStandardSchemeFactory implements SchemeFactory {
    public TTrackedAppInfoStandardScheme getScheme() {
      return new TTrackedAppInfoStandardScheme();
    }
  }

  private static class TTrackedAppInfoStandardScheme extends StandardScheme<TTrackedAppInfo> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, TTrackedAppInfo struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // APP_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.appID = iprot.readString();
              struct.setAppIDIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // PLATFORM
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.platform = iprot.readString();
              struct.setPlatformIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // LAST_ACCESS
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.lastAccess = iprot.readString();
              struct.setLastAccessIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, TTrackedAppInfo struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.appID != null) {
        oprot.writeFieldBegin(APP_ID_FIELD_DESC);
        oprot.writeString(struct.appID);
        oprot.writeFieldEnd();
      }
      if (struct.platform != null) {
        oprot.writeFieldBegin(PLATFORM_FIELD_DESC);
        oprot.writeString(struct.platform);
        oprot.writeFieldEnd();
      }
      if (struct.lastAccess != null) {
        oprot.writeFieldBegin(LAST_ACCESS_FIELD_DESC);
        oprot.writeString(struct.lastAccess);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class TTrackedAppInfoTupleSchemeFactory implements SchemeFactory {
    public TTrackedAppInfoTupleScheme getScheme() {
      return new TTrackedAppInfoTupleScheme();
    }
  }

  private static class TTrackedAppInfoTupleScheme extends TupleScheme<TTrackedAppInfo> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, TTrackedAppInfo struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      oprot.writeString(struct.appID);
      oprot.writeString(struct.platform);
      oprot.writeString(struct.lastAccess);
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, TTrackedAppInfo struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      struct.appID = iprot.readString();
      struct.setAppIDIsSet(true);
      struct.platform = iprot.readString();
      struct.setPlatformIsSet(true);
      struct.lastAccess = iprot.readString();
      struct.setLastAccessIsSet(true);
    }
  }

}

