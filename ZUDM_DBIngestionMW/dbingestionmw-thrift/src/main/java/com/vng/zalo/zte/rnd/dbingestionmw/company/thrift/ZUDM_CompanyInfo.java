/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.vng.zalo.zte.rnd.dbingestionmw.company.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZUDM_CompanyInfo implements org.apache.thrift.TBase<ZUDM_CompanyInfo, ZUDM_CompanyInfo._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("ZUDM_CompanyInfo");

  private static final org.apache.thrift.protocol.TField NAME_FIELD_DESC = new org.apache.thrift.protocol.TField("name", org.apache.thrift.protocol.TType.STRING, (short)1);
  private static final org.apache.thrift.protocol.TField OCCUPATION_ID_FIELD_DESC = new org.apache.thrift.protocol.TField("OccupationId", org.apache.thrift.protocol.TType.I32, (short)2);
  private static final org.apache.thrift.protocol.TField CONFIDENT_FIELD_DESC = new org.apache.thrift.protocol.TField("confident", org.apache.thrift.protocol.TType.DOUBLE, (short)3);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new ZUDM_CompanyInfoStandardSchemeFactory());
    schemes.put(TupleScheme.class, new ZUDM_CompanyInfoTupleSchemeFactory());
  }

  public String name; // required
  public int OccupationId; // required
  public double confident; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    NAME((short)1, "name"),
    OCCUPATION_ID((short)2, "OccupationId"),
    CONFIDENT((short)3, "confident");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // NAME
          return NAME;
        case 2: // OCCUPATION_ID
          return OCCUPATION_ID;
        case 3: // CONFIDENT
          return CONFIDENT;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __OCCUPATIONID_ISSET_ID = 0;
  private static final int __CONFIDENT_ISSET_ID = 1;
  private byte __isset_bitfield = 0;
  private _Fields optionals[] = {_Fields.CONFIDENT};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.NAME, new org.apache.thrift.meta_data.FieldMetaData("name", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.STRING)));
    tmpMap.put(_Fields.OCCUPATION_ID, new org.apache.thrift.meta_data.FieldMetaData("OccupationId", org.apache.thrift.TFieldRequirementType.REQUIRED, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.CONFIDENT, new org.apache.thrift.meta_data.FieldMetaData("confident", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.DOUBLE)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(ZUDM_CompanyInfo.class, metaDataMap);
  }

  public ZUDM_CompanyInfo() {
  }

  public ZUDM_CompanyInfo(
    String name,
    int OccupationId)
  {
    this();
    this.name = name;
    this.OccupationId = OccupationId;
    setOccupationIdIsSet(true);
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public ZUDM_CompanyInfo(ZUDM_CompanyInfo other) {
    __isset_bitfield = other.__isset_bitfield;
    if (other.isSetName()) {
      this.name = other.name;
    }
    this.OccupationId = other.OccupationId;
    this.confident = other.confident;
  }

  public ZUDM_CompanyInfo deepCopy() {
    return new ZUDM_CompanyInfo(this);
  }

  @Override
  public void clear() {
    this.name = null;
    setOccupationIdIsSet(false);
    this.OccupationId = 0;
    setConfidentIsSet(false);
    this.confident = 0.0;
  }

  public String getName() {
    return this.name;
  }

  public ZUDM_CompanyInfo setName(String name) {
    this.name = name;
    return this;
  }

  public void unsetName() {
    this.name = null;
  }

  /** Returns true if field name is set (has been assigned a value) and false otherwise */
  public boolean isSetName() {
    return this.name != null;
  }

  public void setNameIsSet(boolean value) {
    if (!value) {
      this.name = null;
    }
  }

  public int getOccupationId() {
    return this.OccupationId;
  }

  public ZUDM_CompanyInfo setOccupationId(int OccupationId) {
    this.OccupationId = OccupationId;
    setOccupationIdIsSet(true);
    return this;
  }

  public void unsetOccupationId() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __OCCUPATIONID_ISSET_ID);
  }

  /** Returns true if field OccupationId is set (has been assigned a value) and false otherwise */
  public boolean isSetOccupationId() {
    return EncodingUtils.testBit(__isset_bitfield, __OCCUPATIONID_ISSET_ID);
  }

  public void setOccupationIdIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __OCCUPATIONID_ISSET_ID, value);
  }

  public double getConfident() {
    return this.confident;
  }

  public ZUDM_CompanyInfo setConfident(double confident) {
    this.confident = confident;
    setConfidentIsSet(true);
    return this;
  }

  public void unsetConfident() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __CONFIDENT_ISSET_ID);
  }

  /** Returns true if field confident is set (has been assigned a value) and false otherwise */
  public boolean isSetConfident() {
    return EncodingUtils.testBit(__isset_bitfield, __CONFIDENT_ISSET_ID);
  }

  public void setConfidentIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __CONFIDENT_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case NAME:
      if (value == null) {
        unsetName();
      } else {
        setName((String)value);
      }
      break;

    case OCCUPATION_ID:
      if (value == null) {
        unsetOccupationId();
      } else {
        setOccupationId((Integer)value);
      }
      break;

    case CONFIDENT:
      if (value == null) {
        unsetConfident();
      } else {
        setConfident((Double)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case NAME:
      return getName();

    case OCCUPATION_ID:
      return Integer.valueOf(getOccupationId());

    case CONFIDENT:
      return Double.valueOf(getConfident());

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case NAME:
      return isSetName();
    case OCCUPATION_ID:
      return isSetOccupationId();
    case CONFIDENT:
      return isSetConfident();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof ZUDM_CompanyInfo)
      return this.equals((ZUDM_CompanyInfo)that);
    return false;
  }

  public boolean equals(ZUDM_CompanyInfo that) {
    if (that == null)
      return false;

    boolean this_present_name = true && this.isSetName();
    boolean that_present_name = true && that.isSetName();
    if (this_present_name || that_present_name) {
      if (!(this_present_name && that_present_name))
        return false;
      if (!this.name.equals(that.name))
        return false;
    }

    boolean this_present_OccupationId = true;
    boolean that_present_OccupationId = true;
    if (this_present_OccupationId || that_present_OccupationId) {
      if (!(this_present_OccupationId && that_present_OccupationId))
        return false;
      if (this.OccupationId != that.OccupationId)
        return false;
    }

    boolean this_present_confident = true && this.isSetConfident();
    boolean that_present_confident = true && that.isSetConfident();
    if (this_present_confident || that_present_confident) {
      if (!(this_present_confident && that_present_confident))
        return false;
      if (this.confident != that.confident)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(ZUDM_CompanyInfo other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    ZUDM_CompanyInfo typedOther = (ZUDM_CompanyInfo)other;

    lastComparison = Boolean.valueOf(isSetName()).compareTo(typedOther.isSetName());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetName()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.name, typedOther.name);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetOccupationId()).compareTo(typedOther.isSetOccupationId());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetOccupationId()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.OccupationId, typedOther.OccupationId);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetConfident()).compareTo(typedOther.isSetConfident());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetConfident()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.confident, typedOther.confident);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("ZUDM_CompanyInfo(");
    boolean first = true;

    sb.append("name:");
    if (this.name == null) {
      sb.append("null");
    } else {
      sb.append(this.name);
    }
    first = false;
    if (!first) sb.append(", ");
    sb.append("OccupationId:");
    sb.append(this.OccupationId);
    first = false;
    if (isSetConfident()) {
      if (!first) sb.append(", ");
      sb.append("confident:");
      sb.append(this.confident);
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    if (name == null) {
      throw new org.apache.thrift.protocol.TProtocolException("Required field 'name' was not present! Struct: " + toString());
    }
    // alas, we cannot check 'OccupationId' because it's a primitive and you chose the non-beans generator.
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class ZUDM_CompanyInfoStandardSchemeFactory implements SchemeFactory {
    public ZUDM_CompanyInfoStandardScheme getScheme() {
      return new ZUDM_CompanyInfoStandardScheme();
    }
  }

  private static class ZUDM_CompanyInfoStandardScheme extends StandardScheme<ZUDM_CompanyInfo> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, ZUDM_CompanyInfo struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // NAME
            if (schemeField.type == org.apache.thrift.protocol.TType.STRING) {
              struct.name = iprot.readString();
              struct.setNameIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // OCCUPATION_ID
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.OccupationId = iprot.readI32();
              struct.setOccupationIdIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // CONFIDENT
            if (schemeField.type == org.apache.thrift.protocol.TType.DOUBLE) {
              struct.confident = iprot.readDouble();
              struct.setConfidentIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      if (!struct.isSetOccupationId()) {
        throw new org.apache.thrift.protocol.TProtocolException("Required field 'OccupationId' was not found in serialized data! Struct: " + toString());
      }
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, ZUDM_CompanyInfo struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.name != null) {
        oprot.writeFieldBegin(NAME_FIELD_DESC);
        oprot.writeString(struct.name);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldBegin(OCCUPATION_ID_FIELD_DESC);
      oprot.writeI32(struct.OccupationId);
      oprot.writeFieldEnd();
      if (struct.isSetConfident()) {
        oprot.writeFieldBegin(CONFIDENT_FIELD_DESC);
        oprot.writeDouble(struct.confident);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class ZUDM_CompanyInfoTupleSchemeFactory implements SchemeFactory {
    public ZUDM_CompanyInfoTupleScheme getScheme() {
      return new ZUDM_CompanyInfoTupleScheme();
    }
  }

  private static class ZUDM_CompanyInfoTupleScheme extends TupleScheme<ZUDM_CompanyInfo> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, ZUDM_CompanyInfo struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      oprot.writeString(struct.name);
      oprot.writeI32(struct.OccupationId);
      BitSet optionals = new BitSet();
      if (struct.isSetConfident()) {
        optionals.set(0);
      }
      oprot.writeBitSet(optionals, 1);
      if (struct.isSetConfident()) {
        oprot.writeDouble(struct.confident);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, ZUDM_CompanyInfo struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      struct.name = iprot.readString();
      struct.setNameIsSet(true);
      struct.OccupationId = iprot.readI32();
      struct.setOccupationIdIsSet(true);
      BitSet incoming = iprot.readBitSet(1);
      if (incoming.get(0)) {
        struct.confident = iprot.readDouble();
        struct.setConfidentIsSet(true);
      }
    }
  }

}

