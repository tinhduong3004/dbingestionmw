/**
 * Autogenerated by Thrift Compiler (0.9.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
package com.vng.zalo.zte.rnd.dbingestionmw.gameh5.thrift;

import org.apache.thrift.scheme.IScheme;
import org.apache.thrift.scheme.SchemeFactory;
import org.apache.thrift.scheme.StandardScheme;

import org.apache.thrift.scheme.TupleScheme;
import org.apache.thrift.protocol.TTupleProtocol;
import org.apache.thrift.protocol.TProtocolException;
import org.apache.thrift.EncodingUtils;
import org.apache.thrift.TException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.EnumMap;
import java.util.Set;
import java.util.HashSet;
import java.util.EnumSet;
import java.util.Collections;
import java.util.BitSet;
import java.nio.ByteBuffer;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZUDM_Gameh5TienlenPlayingInfo implements org.apache.thrift.TBase<ZUDM_Gameh5TienlenPlayingInfo, ZUDM_Gameh5TienlenPlayingInfo._Fields>, java.io.Serializable, Cloneable {
  private static final org.apache.thrift.protocol.TStruct STRUCT_DESC = new org.apache.thrift.protocol.TStruct("ZUDM_Gameh5TienlenPlayingInfo");

  private static final org.apache.thrift.protocol.TField UID_FIELD_DESC = new org.apache.thrift.protocol.TField("uid", org.apache.thrift.protocol.TType.I32, (short)1);
  private static final org.apache.thrift.protocol.TField N_MATCH_FIELD_DESC = new org.apache.thrift.protocol.TField("n_match", org.apache.thrift.protocol.TType.I32, (short)2);
  private static final org.apache.thrift.protocol.TField N_WIN_FIELD_DESC = new org.apache.thrift.protocol.TField("n_win", org.apache.thrift.protocol.TType.I32, (short)3);

  private static final Map<Class<? extends IScheme>, SchemeFactory> schemes = new HashMap<Class<? extends IScheme>, SchemeFactory>();
  static {
    schemes.put(StandardScheme.class, new ZUDM_Gameh5TienlenPlayingInfoStandardSchemeFactory());
    schemes.put(TupleScheme.class, new ZUDM_Gameh5TienlenPlayingInfoTupleSchemeFactory());
  }

  public int uid; // optional
  public int n_match; // optional
  public int n_win; // optional

  /** The set of fields this struct contains, along with convenience methods for finding and manipulating them. */
  public enum _Fields implements org.apache.thrift.TFieldIdEnum {
    UID((short)1, "uid"),
    N_MATCH((short)2, "n_match"),
    N_WIN((short)3, "n_win");

    private static final Map<String, _Fields> byName = new HashMap<String, _Fields>();

    static {
      for (_Fields field : EnumSet.allOf(_Fields.class)) {
        byName.put(field.getFieldName(), field);
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, or null if its not found.
     */
    public static _Fields findByThriftId(int fieldId) {
      switch(fieldId) {
        case 1: // UID
          return UID;
        case 2: // N_MATCH
          return N_MATCH;
        case 3: // N_WIN
          return N_WIN;
        default:
          return null;
      }
    }

    /**
     * Find the _Fields constant that matches fieldId, throwing an exception
     * if it is not found.
     */
    public static _Fields findByThriftIdOrThrow(int fieldId) {
      _Fields fields = findByThriftId(fieldId);
      if (fields == null) throw new IllegalArgumentException("Field " + fieldId + " doesn't exist!");
      return fields;
    }

    /**
     * Find the _Fields constant that matches name, or null if its not found.
     */
    public static _Fields findByName(String name) {
      return byName.get(name);
    }

    private final short _thriftId;
    private final String _fieldName;

    _Fields(short thriftId, String fieldName) {
      _thriftId = thriftId;
      _fieldName = fieldName;
    }

    public short getThriftFieldId() {
      return _thriftId;
    }

    public String getFieldName() {
      return _fieldName;
    }
  }

  // isset id assignments
  private static final int __UID_ISSET_ID = 0;
  private static final int __N_MATCH_ISSET_ID = 1;
  private static final int __N_WIN_ISSET_ID = 2;
  private byte __isset_bitfield = 0;
  private _Fields optionals[] = {_Fields.UID,_Fields.N_MATCH,_Fields.N_WIN};
  public static final Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> metaDataMap;
  static {
    Map<_Fields, org.apache.thrift.meta_data.FieldMetaData> tmpMap = new EnumMap<_Fields, org.apache.thrift.meta_data.FieldMetaData>(_Fields.class);
    tmpMap.put(_Fields.UID, new org.apache.thrift.meta_data.FieldMetaData("uid", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.N_MATCH, new org.apache.thrift.meta_data.FieldMetaData("n_match", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    tmpMap.put(_Fields.N_WIN, new org.apache.thrift.meta_data.FieldMetaData("n_win", org.apache.thrift.TFieldRequirementType.OPTIONAL, 
        new org.apache.thrift.meta_data.FieldValueMetaData(org.apache.thrift.protocol.TType.I32)));
    metaDataMap = Collections.unmodifiableMap(tmpMap);
    org.apache.thrift.meta_data.FieldMetaData.addStructMetaDataMap(ZUDM_Gameh5TienlenPlayingInfo.class, metaDataMap);
  }

  public ZUDM_Gameh5TienlenPlayingInfo() {
  }

  /**
   * Performs a deep copy on <i>other</i>.
   */
  public ZUDM_Gameh5TienlenPlayingInfo(ZUDM_Gameh5TienlenPlayingInfo other) {
    __isset_bitfield = other.__isset_bitfield;
    this.uid = other.uid;
    this.n_match = other.n_match;
    this.n_win = other.n_win;
  }

  public ZUDM_Gameh5TienlenPlayingInfo deepCopy() {
    return new ZUDM_Gameh5TienlenPlayingInfo(this);
  }

  @Override
  public void clear() {
    setUidIsSet(false);
    this.uid = 0;
    setN_matchIsSet(false);
    this.n_match = 0;
    setN_winIsSet(false);
    this.n_win = 0;
  }

  public int getUid() {
    return this.uid;
  }

  public ZUDM_Gameh5TienlenPlayingInfo setUid(int uid) {
    this.uid = uid;
    setUidIsSet(true);
    return this;
  }

  public void unsetUid() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __UID_ISSET_ID);
  }

  /** Returns true if field uid is set (has been assigned a value) and false otherwise */
  public boolean isSetUid() {
    return EncodingUtils.testBit(__isset_bitfield, __UID_ISSET_ID);
  }

  public void setUidIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __UID_ISSET_ID, value);
  }

  public int getN_match() {
    return this.n_match;
  }

  public ZUDM_Gameh5TienlenPlayingInfo setN_match(int n_match) {
    this.n_match = n_match;
    setN_matchIsSet(true);
    return this;
  }

  public void unsetN_match() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __N_MATCH_ISSET_ID);
  }

  /** Returns true if field n_match is set (has been assigned a value) and false otherwise */
  public boolean isSetN_match() {
    return EncodingUtils.testBit(__isset_bitfield, __N_MATCH_ISSET_ID);
  }

  public void setN_matchIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __N_MATCH_ISSET_ID, value);
  }

  public int getN_win() {
    return this.n_win;
  }

  public ZUDM_Gameh5TienlenPlayingInfo setN_win(int n_win) {
    this.n_win = n_win;
    setN_winIsSet(true);
    return this;
  }

  public void unsetN_win() {
    __isset_bitfield = EncodingUtils.clearBit(__isset_bitfield, __N_WIN_ISSET_ID);
  }

  /** Returns true if field n_win is set (has been assigned a value) and false otherwise */
  public boolean isSetN_win() {
    return EncodingUtils.testBit(__isset_bitfield, __N_WIN_ISSET_ID);
  }

  public void setN_winIsSet(boolean value) {
    __isset_bitfield = EncodingUtils.setBit(__isset_bitfield, __N_WIN_ISSET_ID, value);
  }

  public void setFieldValue(_Fields field, Object value) {
    switch (field) {
    case UID:
      if (value == null) {
        unsetUid();
      } else {
        setUid((Integer)value);
      }
      break;

    case N_MATCH:
      if (value == null) {
        unsetN_match();
      } else {
        setN_match((Integer)value);
      }
      break;

    case N_WIN:
      if (value == null) {
        unsetN_win();
      } else {
        setN_win((Integer)value);
      }
      break;

    }
  }

  public Object getFieldValue(_Fields field) {
    switch (field) {
    case UID:
      return Integer.valueOf(getUid());

    case N_MATCH:
      return Integer.valueOf(getN_match());

    case N_WIN:
      return Integer.valueOf(getN_win());

    }
    throw new IllegalStateException();
  }

  /** Returns true if field corresponding to fieldID is set (has been assigned a value) and false otherwise */
  public boolean isSet(_Fields field) {
    if (field == null) {
      throw new IllegalArgumentException();
    }

    switch (field) {
    case UID:
      return isSetUid();
    case N_MATCH:
      return isSetN_match();
    case N_WIN:
      return isSetN_win();
    }
    throw new IllegalStateException();
  }

  @Override
  public boolean equals(Object that) {
    if (that == null)
      return false;
    if (that instanceof ZUDM_Gameh5TienlenPlayingInfo)
      return this.equals((ZUDM_Gameh5TienlenPlayingInfo)that);
    return false;
  }

  public boolean equals(ZUDM_Gameh5TienlenPlayingInfo that) {
    if (that == null)
      return false;

    boolean this_present_uid = true && this.isSetUid();
    boolean that_present_uid = true && that.isSetUid();
    if (this_present_uid || that_present_uid) {
      if (!(this_present_uid && that_present_uid))
        return false;
      if (this.uid != that.uid)
        return false;
    }

    boolean this_present_n_match = true && this.isSetN_match();
    boolean that_present_n_match = true && that.isSetN_match();
    if (this_present_n_match || that_present_n_match) {
      if (!(this_present_n_match && that_present_n_match))
        return false;
      if (this.n_match != that.n_match)
        return false;
    }

    boolean this_present_n_win = true && this.isSetN_win();
    boolean that_present_n_win = true && that.isSetN_win();
    if (this_present_n_win || that_present_n_win) {
      if (!(this_present_n_win && that_present_n_win))
        return false;
      if (this.n_win != that.n_win)
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return 0;
  }

  public int compareTo(ZUDM_Gameh5TienlenPlayingInfo other) {
    if (!getClass().equals(other.getClass())) {
      return getClass().getName().compareTo(other.getClass().getName());
    }

    int lastComparison = 0;
    ZUDM_Gameh5TienlenPlayingInfo typedOther = (ZUDM_Gameh5TienlenPlayingInfo)other;

    lastComparison = Boolean.valueOf(isSetUid()).compareTo(typedOther.isSetUid());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetUid()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.uid, typedOther.uid);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetN_match()).compareTo(typedOther.isSetN_match());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetN_match()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.n_match, typedOther.n_match);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    lastComparison = Boolean.valueOf(isSetN_win()).compareTo(typedOther.isSetN_win());
    if (lastComparison != 0) {
      return lastComparison;
    }
    if (isSetN_win()) {
      lastComparison = org.apache.thrift.TBaseHelper.compareTo(this.n_win, typedOther.n_win);
      if (lastComparison != 0) {
        return lastComparison;
      }
    }
    return 0;
  }

  public _Fields fieldForId(int fieldId) {
    return _Fields.findByThriftId(fieldId);
  }

  public void read(org.apache.thrift.protocol.TProtocol iprot) throws org.apache.thrift.TException {
    schemes.get(iprot.getScheme()).getScheme().read(iprot, this);
  }

  public void write(org.apache.thrift.protocol.TProtocol oprot) throws org.apache.thrift.TException {
    schemes.get(oprot.getScheme()).getScheme().write(oprot, this);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("ZUDM_Gameh5TienlenPlayingInfo(");
    boolean first = true;

    if (isSetUid()) {
      sb.append("uid:");
      sb.append(this.uid);
      first = false;
    }
    if (isSetN_match()) {
      if (!first) sb.append(", ");
      sb.append("n_match:");
      sb.append(this.n_match);
      first = false;
    }
    if (isSetN_win()) {
      if (!first) sb.append(", ");
      sb.append("n_win:");
      sb.append(this.n_win);
      first = false;
    }
    sb.append(")");
    return sb.toString();
  }

  public void validate() throws org.apache.thrift.TException {
    // check for required fields
    // check for sub-struct validity
  }

  private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    try {
      write(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(out)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
    try {
      // it doesn't seem like you should have to do this, but java serialization is wacky, and doesn't call the default constructor.
      __isset_bitfield = 0;
      read(new org.apache.thrift.protocol.TCompactProtocol(new org.apache.thrift.transport.TIOStreamTransport(in)));
    } catch (org.apache.thrift.TException te) {
      throw new java.io.IOException(te);
    }
  }

  private static class ZUDM_Gameh5TienlenPlayingInfoStandardSchemeFactory implements SchemeFactory {
    public ZUDM_Gameh5TienlenPlayingInfoStandardScheme getScheme() {
      return new ZUDM_Gameh5TienlenPlayingInfoStandardScheme();
    }
  }

  private static class ZUDM_Gameh5TienlenPlayingInfoStandardScheme extends StandardScheme<ZUDM_Gameh5TienlenPlayingInfo> {

    public void read(org.apache.thrift.protocol.TProtocol iprot, ZUDM_Gameh5TienlenPlayingInfo struct) throws org.apache.thrift.TException {
      org.apache.thrift.protocol.TField schemeField;
      iprot.readStructBegin();
      while (true)
      {
        schemeField = iprot.readFieldBegin();
        if (schemeField.type == org.apache.thrift.protocol.TType.STOP) { 
          break;
        }
        switch (schemeField.id) {
          case 1: // UID
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.uid = iprot.readI32();
              struct.setUidIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 2: // N_MATCH
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.n_match = iprot.readI32();
              struct.setN_matchIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          case 3: // N_WIN
            if (schemeField.type == org.apache.thrift.protocol.TType.I32) {
              struct.n_win = iprot.readI32();
              struct.setN_winIsSet(true);
            } else { 
              org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
            }
            break;
          default:
            org.apache.thrift.protocol.TProtocolUtil.skip(iprot, schemeField.type);
        }
        iprot.readFieldEnd();
      }
      iprot.readStructEnd();

      // check for required fields of primitive type, which can't be checked in the validate method
      struct.validate();
    }

    public void write(org.apache.thrift.protocol.TProtocol oprot, ZUDM_Gameh5TienlenPlayingInfo struct) throws org.apache.thrift.TException {
      struct.validate();

      oprot.writeStructBegin(STRUCT_DESC);
      if (struct.isSetUid()) {
        oprot.writeFieldBegin(UID_FIELD_DESC);
        oprot.writeI32(struct.uid);
        oprot.writeFieldEnd();
      }
      if (struct.isSetN_match()) {
        oprot.writeFieldBegin(N_MATCH_FIELD_DESC);
        oprot.writeI32(struct.n_match);
        oprot.writeFieldEnd();
      }
      if (struct.isSetN_win()) {
        oprot.writeFieldBegin(N_WIN_FIELD_DESC);
        oprot.writeI32(struct.n_win);
        oprot.writeFieldEnd();
      }
      oprot.writeFieldStop();
      oprot.writeStructEnd();
    }

  }

  private static class ZUDM_Gameh5TienlenPlayingInfoTupleSchemeFactory implements SchemeFactory {
    public ZUDM_Gameh5TienlenPlayingInfoTupleScheme getScheme() {
      return new ZUDM_Gameh5TienlenPlayingInfoTupleScheme();
    }
  }

  private static class ZUDM_Gameh5TienlenPlayingInfoTupleScheme extends TupleScheme<ZUDM_Gameh5TienlenPlayingInfo> {

    @Override
    public void write(org.apache.thrift.protocol.TProtocol prot, ZUDM_Gameh5TienlenPlayingInfo struct) throws org.apache.thrift.TException {
      TTupleProtocol oprot = (TTupleProtocol) prot;
      BitSet optionals = new BitSet();
      if (struct.isSetUid()) {
        optionals.set(0);
      }
      if (struct.isSetN_match()) {
        optionals.set(1);
      }
      if (struct.isSetN_win()) {
        optionals.set(2);
      }
      oprot.writeBitSet(optionals, 3);
      if (struct.isSetUid()) {
        oprot.writeI32(struct.uid);
      }
      if (struct.isSetN_match()) {
        oprot.writeI32(struct.n_match);
      }
      if (struct.isSetN_win()) {
        oprot.writeI32(struct.n_win);
      }
    }

    @Override
    public void read(org.apache.thrift.protocol.TProtocol prot, ZUDM_Gameh5TienlenPlayingInfo struct) throws org.apache.thrift.TException {
      TTupleProtocol iprot = (TTupleProtocol) prot;
      BitSet incoming = iprot.readBitSet(3);
      if (incoming.get(0)) {
        struct.uid = iprot.readI32();
        struct.setUidIsSet(true);
      }
      if (incoming.get(1)) {
        struct.n_match = iprot.readI32();
        struct.setN_matchIsSet(true);
      }
      if (incoming.get(2)) {
        struct.n_win = iprot.readI32();
        struct.setN_winIsSet(true);
      }
    }
  }

}

