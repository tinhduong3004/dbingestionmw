#!/bin/bash
set -xe
if [[ -d dbingestionmw-thrift ]]; then
    (   
        cd dbingestionmw-thrift
        ./gradlew build
    )
else
    notfound dbingestionmw-thrift 
fi

if [[ -d dbingestionmw-client ]]; then
    (
        cd dbingestionmw-client
        ./gradlew build
    )
else
    notfound dbingestionmw-client
fi

if [[ -d ZUDM_DBIngestionMWService ]]; then
    (
        cd ZUDM_DBIngestionMWService
        ./deploy.sh
    )
else
    notfound ZUDM_DBIngestionMWService
fi

